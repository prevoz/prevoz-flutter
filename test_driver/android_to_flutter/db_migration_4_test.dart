import 'dart:async';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  //* removes debug banner for screenshots

  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Testing DB migration', () async {
      await Future.delayed(Duration(seconds: 1));
      //* 1) check if all 6 locations are inserted (3 from old db, 3 from new)
      await driver.waitFor(find.text("Velenje"));
      await driver.scroll(find.byValueKey("_homePageListView"), 0, -200,
          Duration(milliseconds: 100));
      await driver.waitFor(find.text("Kobarid"));
      await driver.scroll(find.byValueKey("_homePageListView"), 0, -200,
          Duration(milliseconds: 100));
      await driver.waitFor(find.text("Arja Vas"));
      await driver.scroll(find.byValueKey("_homePageListView"), 0, -200,
          Duration(milliseconds: 100));
      await driver.waitFor(find.text("Velenje"));
      await driver.scroll(find.byValueKey("_homePageListView"), 0, -200,
          Duration(milliseconds: 100));
      await driver.waitFor(find.text("Novo mesto"));
      //* 2) check if notifications are inserted (1)
      await driver.tap(find.byValueKey('_bottomNavNotificationIcon'));
      await driver.waitFor(find.text("Kočevje > Polzela"));
      await driver.waitFor(find.text("Velenje > Šmartno ob Paki"));
    });
  });
}
