import 'package:prevoz_org/main_dev.dart' as app;

void main() {
  // let the main() function know we're running an integration test
  // so that it can use mocked dependencies
  // the `main()` function can only accept List<String> as an argument

  List<String> integrationTest = ["integration_test", "android_to_flutter_1"];
  app.main(integrationTest);
}
