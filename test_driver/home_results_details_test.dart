import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
//import 'package:screenshots/screenshots.dart';
import 'package:test_api/src/backend/invoker.dart';

void main() {
  //* removes debug banner for screenshots

//  final config = Config();
  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test(
        'Complete Flow: SearchPage -> ResultsPage -> DetailsPage -> Save to Bookmarks -> Check bookmarks',
        () async {
      await Future.delayed(Duration(seconds: 1));
      await driver.waitFor(find.byValueKey('_fromLocationInput'));
      await driver.waitFor(find.byValueKey('_toLocationInput'));
//      await screenshot(driver, config, 'homepage1');

      //* Navigate to SearchLocationsDelegate - choose from location
      await driver.tap(find.byValueKey('_fromLocationInput'));
      await Future.delayed(Duration(milliseconds: 300));
//      await screenshot(driver, config, 'locations1');
      await driver.tap(find.text('Ljubljana'));
      //* Navigated back to HomePage
      await Future.delayed(Duration(milliseconds: 300));
      await driver.tap(find.byValueKey('_toLocationInput'));
      await Future.delayed(Duration(milliseconds: 300));
      await driver.tap(find.text('Maribor'));
      //* Search for Ljubljana - Maribor
      //* Should return "Za izbrano relacijo ni prevozov"
      await driver.tap(find.byValueKey('_searchButton'));
      await driver.waitFor(find.text('Za izbrano relacijo ni prevozov'));
      //* go back to home page
      await driver.tap(find.byValueKey('_prevozAppBarNavIcon'));
      //* swap locations, search for Maribor - Ljubljana, there should be 46 rides
      await Future.delayed(Duration(milliseconds: 500));
      await driver.tap(find.byValueKey('_locationSwapButton'));
      await driver.tap(find.byValueKey('_searchButton'));

      await driver.tap(find.text('46'));
//      await screenshot(driver, config, 'results1');
      //* go back to home page, scroll down, click a tomorrow card
      Invoker.current.heartbeat();
      await driver.tap(find.byValueKey('_prevozAppBarNavIcon'));
      await driver.scroll(find.byValueKey("_homePageListView"), 0, -800,
          Duration(milliseconds: 200));
      await driver.tap(find.byValueKey("clickableDayNeum"));

      await driver.tap(find.text('Amadej'));
//      await screenshot(driver, config, 'details1');
      await driver.waitFor(find.text('19:00'));
      await driver.waitFor(find.text('Nova Gorica'));
      await driver.waitFor(find.text('Ljubljana'));
      await driver.waitFor(find.text('nedelja, 24.3.'));
      await driver.waitFor(find.text('5€'));
      await driver.waitFor(find.text('Amadej'));
      await driver.waitFor(find.text('041123456'));
    });
  });
}
