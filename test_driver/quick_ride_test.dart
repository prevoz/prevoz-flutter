import 'dart:async';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:test/test.dart';

void main() {
  //* removes debug banner for screenshots

  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Quick rides flow', () async {
      SerializableFinder addRideBtnFinder = find.byValueKey("addRideSubmitBtn");
      Logger logger = Logger(printer: CustomLogPrinter("Quick Ride Test"));
      //* 1) Login inside AddRidePage
      logger.d("Login inside AddRidePage");
      // set logged in state
      driver.requestData("mockSuccessfulGoogleSignIn");

      await driver.tap(find.byValueKey('_bottomNavAddRideIcon'));
      await driver.tap(find.text('Prijavi se'));
      await Future.delayed(Duration(seconds: 2));
      await driver.tap(find.text('Prijava z Google računom'));
      await driver.tap(find.byValueKey('_bottomNavAddRideIcon'));

      //Hitri vnosi
      await driver.tap(find.text('Moji hitri vnosi'));
      await driver.tap(find.text('Izola > Ptuj'));

      // //* 2) Open quick rides page and click today for first item
      // await driver.tap(find.byValueKey("_ActionableAppBarNavIcon"));
      // await driver.tap(find.byValueKey("clickableDay_today0"));
      // //* 3) Check if quick ride data is properly transfered to AddRideFrom
      await driver.waitFor(find.text("Izola"));
      await driver.waitFor(find.text("Ptuj"));
      await driver.waitFor(find.text("12:59"));
      await driver.waitFor(find.text("4"));
      await driver.waitFor(find.text("4.0"));
      await driver.waitFor(find.text("Clio Adio"));
      await driver.waitFor(find.text("#sms #drobiz"));
      await driver.waitFor(find.text("051 611 781"));
      // //* 4) Submit ride
      await driver.scrollIntoView(addRideBtnFinder);
      await driver.tap(addRideBtnFinder);
      await Future.delayed(Duration(seconds: 1));
      // //* 5) Check if submited ride is shown in rides results
      await driver.waitFor(find.text("Izola > Ptuj"));
    });
  });
}
