import 'dart:async';
import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:test/test.dart';
import 'package:test_api/src/backend/invoker.dart';

void main() {
  //* removes debug banner for screenshots
//  final config = Config();
  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Complete AddRide&MyRides Flow', () async {
      await Future.delayed(Duration(seconds: 1));

      if (Platform.isIOS) {
        await driver.tap(find.text('Allow'));
      }

      // set logged in state
      driver.requestData("mockSuccessfulGoogleSignIn");

      Logger logger = Logger(printer: CustomLogPrinter("AddRideTest"));
      //! 1) Login inside AddRidePage
      logger.i("Login inside AddRidePage");
      await driver.tap(find.byValueKey('_bottomNavAddRideIcon'));
      await driver.tap(find.text('Prijavi se'));
      await Future.delayed(Duration(seconds: 2));
      await driver.tap(find.text('Prijava z Google računom'));
      await driver.tap(find.byValueKey('_bottomNavAddRideIcon'));
      //! 2) pick a departure location
      logger.i("pick a departure location");
      await driver.tap(find.byValueKey('_addRideFromLocation'));
      await driver.tap(find.text('Maribor'));
//      await screenshot(driver, config, 'addRidePage');
      //! 3) try submiting without filling any other data
      logger.i("try submiting without filling any other data");
      await driver.scroll(find.byValueKey("addRideScrollView"), 0, -800,
          Duration(milliseconds: 200));
      await driver.tap(find.byValueKey('addRideSubmitBtn'));
      find.text('Kraj prihoda - to polje je obvezno.');
      find.text('Cas odhoda - to polje je obvezno.');
      find.text('Stevilo prostih mest - to polje je obvezno.');
      //! 4) try submiting without time and free seats
      logger.i("try submiting without time and free seats");
      await Future.delayed(Duration(seconds: 1)); //* workaround
      await driver.tap(find.byValueKey('_addRideToLocation'));
      await driver.tap(find.text('Ljubljana'));
      await driver.scroll(find.byValueKey("addRideScrollView"), 0, -800,
          Duration(milliseconds: 200));
      await driver.tap(find.byValueKey('addRideSubmitBtn'));
      find.text('Cas odhoda - to polje je obvezno.');
      find.text('Stevilo prostih mest - to polje je obvezno.');
      //! 5) Choose time
      logger.i("Choose time");
      await driver.tap(find.byValueKey('addRideTimePicker'));
      await driver.tap(find.text("V REDU"));

      //! 6) Submit without num of free seats
      logger.i("Submit without num of free seats");
      await driver.scroll(find.byValueKey("addRideScrollView"), 0, -400,
          Duration(milliseconds: 200));
      await driver.tap(find.byValueKey('addRideSubmitBtn'));
      find.text('Stevilo prostih mest - to polje je obvezno.');
      Invoker.current.heartbeat(); //* prolongs the test
      //! 7) add num of free seats and price
      await driver.tap(find.byValueKey('addRideNumOfPlaces'));
      await driver.enterText("2");
      await driver.tap(find.byValueKey('addRidePrice'));
      await driver.enterText("11");
      await Future.delayed(Duration(seconds: 1));
      await driver.scroll(find.byValueKey("addRideScrollView"), 0, -400,
          Duration(milliseconds: 200));
      await driver.tap(find.byValueKey('addRideSubmitBtn'));
      Invoker.current.heartbeat(); //* prolongs the test
      //! 8) Successfull ride post, navigate back to MyRides
      logger.i("Navigated back to MyRides");
      await driver.tap(find.text('Moji prevozi'));
//      await screenshot(driver, config, 'myRidesPage');
      //! 9) Check if new ride added to state
      find.text('Maribor > Ljubljana');
    });
  });
}
