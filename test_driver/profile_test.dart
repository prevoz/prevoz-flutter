import 'dart:async';
import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:test/test.dart';

void main() {
  //* removes debug banner for screenshots
  final prevoziApiMock = "MockPrevozApi";
  final profileEntryKeyPrefix = "_profileCardEntry";
  final googleSignInMock = "MockGoogleSignIn";

  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    Future<bool> _isErrorShown(FlutterDriver driver) async {
      try {
        await driver.waitFor(
            find.byValueKey("_profileCard_partialError"),
            timeout: Duration(seconds: 1)
        );
        return true;
      } catch(exception) {
        return false;
      }
    }

    test('Full profile view', () async {
      await Future.delayed(Duration(seconds: 1));

      if (Platform.isIOS) {
        await driver.tap(find.text('Allow'));
      }

      // set logged in state
      driver.requestData("${prevoziApiMock}_mockLoginSuccessful");
      // set profile data
      driver.requestData("${prevoziApiMock}_mockFullProfile");
      // set google sign in state
      driver.requestData("${googleSignInMock}_mockSuccessfulGoogleSignIn");

      Logger logger = Logger(printer: CustomLogPrinter("ProfileTest"));

      logger.i("Login inside ProfilePage");
      await driver.tap(find.byValueKey('_bottomNavProfileIcon'));
      await Future.delayed(Duration(seconds: 2));
      await driver.tap(find.text('Prijava z Google računom'));

      final errorShown = await _isErrorShown(driver);
      logger.i("Error widget should not be rendered");
      expect(
          errorShown,
          false
      );

      logger.i("Check the first and last name");
      expect(
        await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_firstAndLastNameTitle")),
        "Ime in Priimek"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_firstAndLastNameValue")),
          "Janez Novak"
      );

      logger.i("Check address");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_addressTitle")),
          "Naslov"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_addressValue")),
          "Za cesto 1, 1000 Ljubljana"
      );

      logger.i("Check email");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_emailTitle")),
          "E-mail"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_emailValue")),
          "info@test.si"
      );
    });

    test('Handle error fetching profile with stored fallback', () async {
      // set profile data
      driver.requestData("${prevoziApiMock}_mockErrorFetchingProfile");

      Logger logger = Logger(printer: CustomLogPrinter("ProfileTest"));

      logger.i("Force reload of profile page");
      await driver.tap(find.byValueKey('_bottomNavMyRidesIcon'));
      await driver.tap(find.byValueKey('_bottomNavProfileIcon'));

      final errorShown = await _isErrorShown(driver);
      logger.i("Expect the error widget to be shown");
      expect(
        errorShown,
        true
      );

      logger.i("Check the stored first and last name");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_firstAndLastNameTitle")),
          "Ime in Priimek"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_firstAndLastNameValue")),
          "Janez Novak"
      );

      logger.i("Check the stored address");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_addressTitle")),
          "Naslov"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_addressValue")),
          "Za cesto 1, 1000 Ljubljana"
      );

      logger.i("Check the stored email");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_emailTitle")),
          "E-mail"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_emailValue")),
          "info@test.si"
      );
    });

    test('Empty profile view', () async {
      // set empty profile data
      driver.requestData("${prevoziApiMock}_mockEmptyProfile");

      Logger logger = Logger(printer: CustomLogPrinter("ProfileTest"));

      logger.i("Force reload of profile page");
      await driver.tap(find.byValueKey('_bottomNavMyRidesIcon'));
      await driver.tap(find.byValueKey('_bottomNavProfileIcon'));

      final errorShown = await _isErrorShown(driver);
      logger.i("Error widget should not be rendered");
      expect(
          errorShown,
          false
      );

      logger.i("Check the first and last name");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_firstAndLastNameTitle")),
          "Ime in Priimek"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_firstAndLastNameValue")),
          "Dodaj ime in priimek"
      );

      logger.i("Check address");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_addressTitle")),
          "Naslov"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_addressValue")),
          "Dodaj naslov"
      );

      logger.i("Check email");
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_emailTitle")),
          "E-mail"
      );
      expect(
          await driver.getText(find.byValueKey("${profileEntryKeyPrefix}_emailValue")),
          "info@test.si"
      );
    });
  });
}
