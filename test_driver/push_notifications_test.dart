import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  //* removes debug banner for screenshots

  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Testing push notifications', () async {
      //1) Pojdi v notifications tab, preveri ce je tam msg "Niste subscribeani ..."
      //2) Podji na SearchPage, klikni spodaj eno izmed moznosti za search
      //3) SearchResultsPage: preveri ce je viden button za subscription
      //4) Subscribe-aj se ...
      //5) nadaljuj s flow-om

      await driver.tap(find.byValueKey('_bottomNavNotificationIcon'));
      await driver
          .waitFor(find.text("Nisi naročen na obveščanja o novih prevozih."));
      await driver.tap(find.byValueKey('_bottomNavSearchIcon'));
      //* Navigate to SearchLocationsDelegate - choose from location
      await driver.tap(find.byValueKey('_fromLocationInput'));
      await Future.delayed(Duration(milliseconds: 300));
      await driver.tap(find.text('Maribor'));
      //* Navigated back to HomePage

      await Future.delayed(Duration(milliseconds: 300));
      await driver.tap(find.byValueKey('_toLocationInput'));
      await Future.delayed(Duration(milliseconds: 300));
      await driver.tap(find.text('Ljubljana'));

      //* Search for Ljubljana - Maribor
      await driver.tap(find.byValueKey('_searchButton'));

      //* subscribe to notification
      await Future.delayed(Duration(seconds: 1));
      await driver.tap(find.text("Obveščaj me o novih prevozih"));
      //Prenehaj z obveščanjem"
      await Future.delayed(Duration(milliseconds: 500));
      await driver.waitFor(find.text("Prenehaj z obveščanjem"));

      //* go back to home page
      await driver.tap(find.byValueKey('_prevozAppBarNavIcon'));

      //* go back to search results, check if button text really changed
      await driver.tap(find.byValueKey('_searchButton'));
      await driver.waitFor(find.text("Prenehaj z obveščanjem"));
      //* go back to home page
      await driver.tap(find.byValueKey('_prevozAppBarNavIcon'));

      await driver.tap(find.byValueKey('_bottomNavNotificationIcon'));
      //await driver.waitFor(find.text("Ljubljana"));

      //* delete subscription dialog
      await driver.tap(find.byValueKey('deleteNotificationBtn'));
      await driver.waitFor(find.text("Odstrani obvestilo?"));

      await driver.tap(find.text('ODSTRANI'));

      await driver
          .waitFor(find.text("Nisi naročen na obveščanja o novih prevozih."));
    });
  });
}
