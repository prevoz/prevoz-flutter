import 'dart:async';
import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:test/test.dart';

void main() {
  //* removes debug banner for screenshots
  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Edit Ride Flow', () async {
      await Future.delayed(Duration(seconds: 1));

      if (Platform.isIOS) {
        await driver.tap(find.text('Allow'));
      }

      Logger logger = Logger(printer: CustomLogPrinter("AddRideTest"));
      //! 1) Login
      logger.i("Login inside AddRidePage");
      // set logged in state
      driver.requestData("mockSuccessfulGoogleSignIn");

      await driver.tap(find.byValueKey('_bottomNavMyRidesIcon'));
      await driver.tap(find.text('Prijavi se'));
      await Future.delayed(Duration(seconds: 2));
      await driver.tap(find.text('Prijava z Google računom'));
      await driver.tap(find.byValueKey('_bottomNavMyRidesIcon'));
      //! 2) testing comments visibility with keyboard open
      await driver.tap(find.text('Polzela > Ljubljana'));
      await driver.tap(find.byValueKey("commentField"));
      await driver
          .enterText("first line \n second line \n third line \n fourth line");
      await driver.tap(find.text("Uredi prevoz")); //* focus away from textfield
      //? turn on the "show keyboard" mode
      await driver.setTextEntryEmulation(
          enabled: false); // we want the keyboard to come up
      await driver.tap(find.byValueKey("commentField"));
      await Future.delayed(Duration(seconds: 1));
      find.text("first line \n second line \n third line \n fourth line");
      //Invoker.current.heartbeat(); //* prolongs the test
      //! 3) close keyboard
      await driver.tap(find.text('Uredi prevoz'));
      await driver.scroll(find.byValueKey("addRideScrollView"), 0, -400,
          Duration(milliseconds: 200));
      await driver.tap(find.byValueKey("emptyCheckbox"));
      await driver.tap(find.text('Shrani spremembe'));
      await driver.tap(find.text('Moji prevozi'));
      await driver.tap(find.text("Polzela > Ljubljana"));
      await driver.waitFor(find.byValueKey("fullCheckbox"));
    });
  });
}
