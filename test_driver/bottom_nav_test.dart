import 'dart:async';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
//import 'package:screenshots/screenshots.dart';

void main() {
  //* removes debug banner for screenshots

//  final config = Config();
  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Bottom Nav Test', () async {
      await Future.delayed(Duration(seconds: 1));
      await driver.waitFor(find.byValueKey('_bottomNavMyRidesIcon'));
      await driver.tap(find.byValueKey('_bottomNavMyRidesIcon'));
      await driver.tap(find.byValueKey('_bottomNavAddRideIcon'));
      await driver.tap(find.byValueKey('_bottomNavProfileIcon'));
//      await screenshot(driver, config, 'profileLogin');
      await driver.tap(find.byValueKey('_bottomNavAddRideIcon'));
//      await screenshot(driver, config, 'addRide');
      await driver.tap(find.byValueKey('_bottomNavMyRidesIcon'));
//      await screenshot(driver, config, 'myRides');
    });
  });
}
