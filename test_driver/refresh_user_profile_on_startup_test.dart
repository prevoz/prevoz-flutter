import 'dart:async';
import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  //* removes debug banner for screenshots

  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Refresh user profile on startup flow', () async {
      await Future.delayed(Duration(seconds: 1));

      if (Platform.isIOS) {
        await driver.tap(find.text('Allow'));
      }

      await Future.delayed(Duration(seconds: 3));
      await driver.tap(find.byValueKey('_bottomNavAddRideIcon'));

      // await driver.scroll(find.byValueKey("addRideScrollView"), 0, -400,
      //     Duration(milliseconds: 200));
      await Future.delayed(Duration(seconds: 2));
      await driver.scrollIntoView(find.byValueKey("phoneNumberField"));
      find.text("040040040");
    });
  });
}
