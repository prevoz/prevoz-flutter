import 'dart:async';
import 'package:test/test.dart';
import 'package:flutter_driver/flutter_driver.dart';

void main() {
  //* removes debug banner for screenshots

  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Slow DB init flow', () async {
      //* 1) Go to search location delegate that's waiting for the DB to be initialised
      await driver.tap(find.byValueKey('_fromLocationInput'));
      await Future.delayed(Duration(seconds: 8));
      //* 2) After 8 seconds DB should be initialised and Maribor is visible in results. Navigate back to HomePage.
      await driver.tap(find.text("Maribor"));
      //* 3) Find Murska Sobota at the end of the list on HomePage
      final listFinder = find.byValueKey('_homePageListView');
      final itemFinder = find.text('Murska Sobota');
      await driver.scrollUntilVisible(
        // Scroll through the list
        listFinder,
        // Until finding this item
        itemFinder,
        dyScroll: -120.0,
      );
    });
  });
}
