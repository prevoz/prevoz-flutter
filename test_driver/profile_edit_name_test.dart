import 'dart:async';
import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:test/test.dart';

void main() {
  //* removes debug banner for screenshots
  final prevoziApiMock = "MockPrevozApi";
  final profileEntryKeyPrefix = "_profileCardEntry";
  final googleSignInMock = "MockGoogleSignIn";

  group('Prevoz App', () {
    FlutterDriver driver;
    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });
    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    Future<bool> _isWidgetShown(FlutterDriver driver, String widgetType) async {
      try {
        await driver.waitFor(
            find.byType(widgetType),
            timeout: Duration(seconds: 1)
        );
        return true;
      } catch(exception) {
        return false;
      }
    }

    test('Edit profile name - show submit after post succesful', () async {
      await Future.delayed(Duration(seconds: 1));

      if (Platform.isIOS) {
        await driver.tap(find.text('Allow'));
      }

      // set logged in state
      driver.requestData("${prevoziApiMock}_mockLoginSuccessful");
      // set profile data
      driver.requestData("${prevoziApiMock}_mockFullProfile");
      // set google sign in state
      driver.requestData("${googleSignInMock}_mockSuccessfulGoogleSignIn");

      Logger logger = Logger(printer: CustomLogPrinter("ProfileEditNameTest"));

      logger.i("Login inside ProfilePage");
      await driver.tap(find.byValueKey('_bottomNavProfileIcon'));
      await Future.delayed(Duration(seconds: 2));
      await driver.tap(find.text('Prijava z Google računom'));

      logger.i("Go to EditProfilePageFirstAndLastName");
      await driver.tap(find.byValueKey("${profileEntryKeyPrefix}_firstAndLastNameTitle"));

      logger.i("Enter first name value");
      await driver.tap(find.byValueKey("_textFieldWithTitle_firstName"));
      await driver.enterText("Janez 2");

      logger.i("Enter last name value");
      await driver.tap(find.byValueKey("_textFieldWithTitle_lastName"));
      await driver.enterText("Novak 2");

      logger.i("Set request to be successful");
      driver.requestData("${prevoziApiMock}_mockProfilePostSuccessful");
      driver.tap(find.text("Shrani podatke"));

      logger.i("Expect the success widget to be shown");
      bool successShown = await _isWidgetShown(driver, "SuccessfulProfileUpdateWidget");
      expect(
          successShown,
          true
      );
      logger.i("and the submit button to be gone");
      bool submitShown = await _isWidgetShown(driver, "RaisedButton");
      expect(
          submitShown,
          false
      );

      logger.i("After typing into the fist name field");
      await driver.tap(find.byValueKey("_textFieldWithTitle_firstName"));
      await driver.enterText("aaa");
      logger.i("and the submit button to be shown again");
      submitShown = await _isWidgetShown(driver, "RaisedButton");
      expect(
          submitShown,
          true
      );
      logger.i("expect the success widget to be gone");
      successShown = await _isWidgetShown(driver, "SuccessfulProfileUpdateWidget");
      expect(
          successShown,
          false
      );

      logger.i("Submit the data again");
      driver.tap(find.text("Shrani podatke"));

      logger.i("Expect the success widget to be shown");
      successShown = await _isWidgetShown(driver, "SuccessfulProfileUpdateWidget");
      expect(
          successShown,
          true
      );
      logger.i("and the submit button to be gone");
      submitShown = await _isWidgetShown(driver, "RaisedButton");
      expect(
          submitShown,
          false
      );

      logger.i("After typing into the last name field");
      await driver.tap(find.byValueKey("_textFieldWithTitle_lastName"));
      await driver.enterText("bbb");
      logger.i("and the submit button to be shown again");
      submitShown = await _isWidgetShown(driver, "RaisedButton");
      expect(
          submitShown,
          true
      );
      logger.i("expect the success widget to be gone");
      successShown = await _isWidgetShown(driver, "SuccessfulProfileUpdateWidget");
      expect(
          successShown,
          false
      );
    });

  });
}
