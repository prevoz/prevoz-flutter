
import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class TextFieldWithTitle extends StatelessWidget {

  static const TEXT_FIELD_WITH_TITLE_KEY_PREFIX = "_textFieldWithTitle";

  final TextInputType inputType;
  final String fieldKey;
  final String title;
  final String hint;
  final String error;
  final maxLines;
  final bool obscureText;
  final TextEditingController controller;
  final Function onTextChanged;

  TextFieldWithTitle({
    this.inputType = TextInputType.text,
    @required this.fieldKey,
    @required this.title,
    this.hint = "",
    this.error = "",
    this.maxLines = 1,
    this.obscureText = false,
    @required this.controller,
    this.onTextChanged
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTitle(),
        SizedBox(height: 5,),
        _buildTextField()
      ],
    );
  }

  Widget _buildTitle() {
    return Text(
      title,
      key: Key("some_key"),
      style: TextStyle(
        fontSize: 14,
        color: HexColor("#808080"),
        fontWeight: FontWeight.bold
      ),
    );
  }

  Widget _buildTextField() {
    return TextField(
      key: Key("${TEXT_FIELD_WITH_TITLE_KEY_PREFIX}_$fieldKey"),
      controller: controller,
      maxLines: maxLines,
      keyboardType: maxLines > 1 ? TextInputType.multiline : inputType,
      obscureText: obscureText,
      decoration: InputDecoration(
        errorText: error,
        hintText: hint,
        contentPadding: EdgeInsets.all(12),
        border: OutlineInputBorder(

        ),
      ),
      onChanged: (value) {
        if (onTextChanged != null) {
          onTextChanged(value);
        }
      },
    );
  }
}