
import 'package:flutter/material.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

class SuccessfulProfileUpdateWidget extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.check,
              color:Colors.green,
              size: 25,
            ),
            SizedBox(width: 5,),
            Text(
              AppLocalizations.of(context).editProfilePageDefaultSuccessMessage,
              style: TextStyle(
                  color: Colors.green
              ),
            ),
          ],
        )
    );
  }
}