
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_event.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_state.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/profile_edit/widgets/successful_profile_update_widget.dart';
import 'package:prevoz_org/utils/form_validators.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';

import 'widgets/text_field_with_title.dart';

class EditProfilePageAddress extends StatefulWidget {

  final UserProfile userProfile;

  EditProfilePageAddress({@required this.userProfile});

  @override
  _EditProfilePageAddressState createState() => _EditProfilePageAddressState();
}

class _EditProfilePageAddressState extends State<EditProfilePageAddress> {

  EditProfileBloc _editProfileBloc;
  TextEditingController _addressController;
  Map<String, dynamic> _errorsFromApi;
  bool _showLoading = false;
  bool _postSuccessful = false;
  bool _showGeneralError = false;

  @override
  void initState() {
    super.initState();
    _editProfileBloc = BlocProvider.of<EditProfileBloc>(context);
    _addressController = TextEditingController();
    _addressController.text = widget.userProfile.address;
  }


  @override
  void dispose() {
    _allowSubmit();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        BlocBuilder(
          cubit: _editProfileBloc,
          builder: (BuildContext buildContext, EditProfileState state) {
            _showGeneralError = false;
            if (state is ErrorSubmittingProfile) {
              _showLoading = false;
              _postSuccessful = false;
              _handleSubmitError(state);
            } else if (state is SubmittingProfile) {
              _showLoading = true;
              _errorsFromApi = null;
              _postSuccessful = false;
            } else {
              _postSuccessful = state is SuccessfullySubmittedProfile;
              _showLoading = false;
              _errorsFromApi = null;
            }

            if (state is SuccessfullySubmittedProfile) {
              // Update the address value on the UserProfile object from the ProfilePage
              widget.userProfile.address = state.userProfile.address;
            }
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: _buildAppBar(),
              body: _buildBody(),
            );
          },
        )
      ],
    );
  }

  Widget _buildAppBar() {
    return PrevozAppBar(
      isBackIcon: true,
      isNavIconShown: true,
      context: context,
      pageTitle: AppLocalizations.of(context).profile,
      onBackPress: () {
        // empty
      },
    );
  }

  Widget _buildBody() {
    return ListView(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 20, top: 25, right: 20),
          child: Card(
            color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(height: 28,),
                  _buildTitle(context),
                  SizedBox(height: 28,),
                  TextFieldWithTitle(
                    fieldKey: "address",
                    title: AppLocalizations.of(context).editProfilePageAddressEntryTitle,
                    hint: AppLocalizations.of(context).editProfilePageAddressEntryHint,
                    error: FormValidators.addressValidator(_addressController.text, _errorsFromApi, context),
                    maxLines: 4,
                    controller: _addressController,
                    onTextChanged: (value) => _allowSubmit(),
                  ),
                  SizedBox(height: 28,),
                  _buildSaveButton(context),
                  _buildGeneralError(context),
                  SizedBox(height: 58,),
                  _buildInfoLabourLaw(),
                  SizedBox(height: 10,)
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Text(
      AppLocalizations.of(context).editProfilePageFirstAndLastNameTitle,
      style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
          color: HexColor("#4D4D4D")
      ),
    );
  }

  Widget _buildSaveButton(BuildContext context) {
    if (_showLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_postSuccessful) {
      return SuccessfulProfileUpdateWidget();
    }

    return Container(
        width: double.infinity,
        child: RaisedButton(
            padding: EdgeInsets.only(
                top: 10, bottom: 10, left: 22, right: 22),
            child: Text(
              AppLocalizations.of(context).editProfilePageDefaultSaveButtonText,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            colorBrightness: Brightness.light,
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4)),
            splashColor: Theme.of(context).accentColor,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              _submitAddress();
            })
    );
  }

  Widget _buildGeneralError(BuildContext context) {
    if (!_showGeneralError) {
      return Container();
    }

    return Column(
      key: Key("_editProfilePageAddress_generalError"),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 20,),
        Icon(
          Icons.error,
          color: Colors.red,
          size: 40,
        ),
        SizedBox(height: 4,),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Text(
            AppLocalizations.of(context).errorPostingDataToServer,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
    );
  }

  Widget _buildInfoLabourLaw() {
    return Text(
      AppLocalizations.of(context).labourLaw,
      key: Key("_editProfilePageAddress_labourLaw"),
      style: TextStyle(color: Colors.black),
    );
  }

  void _allowSubmit() {
    _editProfileBloc?.add(Reset());
  }

  void _handleSubmitError(ErrorSubmittingProfile errorState) {
    if (errorState.response != null &&
        errorState.response.data != null &&
        errorState.response.data["error"] != null) {
      _errorsFromApi = errorState.response.data["error"];
    }
    else {
      _showGeneralError = true;
    }
  }

  void _submitAddress() {
    String address = _addressController.text.toString();
    FocusScope.of(context).unfocus();
    _editProfileBloc?.add(PostAddress(
        address: address
    ));
  }
}