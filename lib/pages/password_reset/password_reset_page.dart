

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_event.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_state.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/profile_edit/widgets/successful_profile_update_widget.dart';
import 'package:prevoz_org/pages/profile_edit/widgets/text_field_with_title.dart';
import 'package:prevoz_org/utils/form_validators.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';


class PasswordResetPage extends StatefulWidget {

  PasswordResetPage();

  @override
  _PasswordResetPageState createState() => _PasswordResetPageState();
}

class _PasswordResetPageState extends State<PasswordResetPage> {

  AuthenticationBloc _authenticationBloc;
  EditProfileBloc _editProfileBloc;
  TextEditingController _passwordController;
  TextEditingController _passwordCheckController;
  Map<String, dynamic> _errorsFromApi;
  bool _showLoading = false;
  bool _postSuccessful = false;
  bool _showGeneralError = false;

  @override
  void initState() {
    super.initState();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _editProfileBloc = BlocProvider.of<EditProfileBloc>(context);
    _passwordController = TextEditingController();
    _passwordCheckController = TextEditingController();
  }


  @override
  void dispose() {
    // We need to get the new account status
    _authenticationBloc.add(AppStarted()); // TODO create a refresh event instead
    _allowSubmit();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        BlocBuilder(
          cubit: _editProfileBloc,
          builder: (BuildContext buildContext, EditProfileState state) {
//            _showGeneralError = false;
//            if (state is ErrorSubmittingProfile) {
//              _showLoading = false;
//              _postSuccessful = false;
//              _handleSubmitError(state);
//            } else if (state is SubmittingProfile) {
//              _showLoading = true;
//              _errorsFromApi = null;
//              _postSuccessful = false;
//            } else {
//              _postSuccessful = state is SuccessfullySubmittedProfile;
//              _showLoading = false;
//              _errorsFromApi = null;
//            }
//
//            if (state is SuccessfullySubmittedProfile) {
//              // Update the address value on the UserProfile object from the ProfilePage
//              widget.userProfile.address = state.userProfile.address;
//            }
            return Scaffold(
              backgroundColor: Colors.transparent,
              appBar: _buildAppBar(),
              body: _buildBody(),
            );
          },
        )
      ],
    );
  }

  Widget _buildAppBar() {
    return PrevozAppBar(
      isBackIcon: true,
      isNavIconShown: true,
      context: context,
      pageTitle: AppLocalizations.of(context).passwordResetPageTitle,
      onBackPress: () {
        // empty
      },
    );
  }

  Widget _buildBody() {
    return ListView(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 20, top: 25, right: 20),
          child: Card(
            color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(height: 28,),
                  _buildInfo(context),
                  SizedBox(height: 28,),
                  TextFieldWithTitle(
                    fieldKey: "password",
                    title: AppLocalizations.of(context).passwordResetPagePasswordTitle,
                    controller: _passwordController,
                    error: FormValidators.firstNameValidator(_passwordController.text, _errorsFromApi, context),
                    onTextChanged: (value) => _allowSubmit(),
                  ),
                  SizedBox(height: 18,),
                  TextFieldWithTitle(
                    fieldKey: "passwordCheck",
                    title: AppLocalizations.of(context).passwordResetPagePasswordCheckTitle,
                    obscureText: true,
                    controller: _passwordCheckController,
                    error: FormValidators.lastNameValidator(_passwordCheckController.text, _errorsFromApi, context),
                    onTextChanged: (value) => _allowSubmit(),
                  ),
                  SizedBox(height: 28,),
                  _buildSubmitButton(context),
                  _buildGeneralError(context),
                  SizedBox(height: 38,),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildInfo(BuildContext context) {
    return Text(
      AppLocalizations.of(context).passwordResetPageInfo,
      style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: HexColor("#4D4D4D")
      ),
    );
  }

  Widget _buildSubmitButton(BuildContext context) {
    if (_showLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_postSuccessful) {
      return SuccessfulProfileUpdateWidget();
    }

    return Container(
        width: double.infinity,
        child: RaisedButton(
            padding: EdgeInsets.only(
                top: 10, bottom: 10, left: 22, right: 22),
            child: Text(
              AppLocalizations.of(context).passwordResetPageSubmitButton,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            colorBrightness: Brightness.light,
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4)),
            splashColor: Theme.of(context).accentColor,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              _submitAddress();
            })
    );
  }

  Widget _buildGeneralError(BuildContext context) {
    if (!_showGeneralError) {
      return Container();
    }

    return Column(
      key: Key("_editProfilePageAddress_generalError"),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 20,),
        Icon(
          Icons.error,
          color: Colors.red,
          size: 40,
        ),
        SizedBox(height: 4,),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Text(
            AppLocalizations.of(context).errorPostingDataToServer,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
    );
  }


  void _allowSubmit() {
    _editProfileBloc?.add(Reset());
  }

//  void _handleSubmitError(ErrorSubmittingProfile errorState) {
//    if (errorState.response != null &&
//        errorState.response.data != null &&
//        errorState.response.data["error"] != null) {
//      _errorsFromApi = errorState.response.data["error"];
//    }
//    else {
//      _showGeneralError = true;
//    }
//  }

  void _submitAddress() {
//    String address = _addressController.text.toString();
//    FocusScope.of(context).unfocus();
//    _editProfileBloc?.add(PostAddress(
//        address: address
//    ));
  }
}