
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/login/login_bloc.dart';
import 'package:prevoz_org/data/blocs/login/login_event.dart';
import 'package:prevoz_org/data/blocs/login/login_state.dart';
import 'package:prevoz_org/data/blocs/registration/registration_bloc.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/register/register_username_password_page.dart';
import 'package:prevoz_org/pages/profile_edit/widgets/text_field_with_title.dart';
import 'package:prevoz_org/utils/form_validators.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/google_sign_in_button.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';


class LoginUsernamePasswordPage extends StatefulWidget {

  LoginUsernamePasswordPage();

  @override
  _LoginUsernamePasswordPageState createState() => _LoginUsernamePasswordPageState();
}

class _LoginUsernamePasswordPageState extends State<LoginUsernamePasswordPage> {

  LoginBloc _loginBloc;
  TextEditingController _usernameController;
  TextEditingController _passwordController;
  Map<String, dynamic> _errorsFromApi;
  bool _showLoading = false;
  bool _showGoogleSignIn = false;
  bool _loginSuccessful = false;
  String _generalError;

  @override
  void initState() {
    super.initState();
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        BlocListener<AuthenticationBloc, AuthenticationState> (
          listener: (context, authState) {
            if (authState is AuthStateAuthenticated) {
              _showLoading = false;
              _errorsFromApi = null;
              _generalError = null;
              _showGoogleSignIn = false;
              _loginSuccessful = true;
              Navigator.pop(context);
            }
          },
          child: BlocBuilder(
            cubit: _loginBloc,
            builder: (BuildContext buildContext, LoginState loginState) {
              if (loginState is LoginErrorGmail) {
                _showLoading = false;
                _loginSuccessful = false;
                _errorsFromApi = null;
                _generalError = AppLocalizations.of(context).loginPageGmailError;
                _showGoogleSignIn = true;
              }
              else if (loginState is ErrorSubmittingLogin) {
                _showLoading = false;
                _loginSuccessful = false;
                _showGoogleSignIn = loginState.wasFromGoogle;
                _handleSubmitError(loginState);
              } else if (loginState is SubmittingLogin) {
                _showLoading = true;
                _errorsFromApi = null;
                _generalError = null;
                _showGoogleSignIn = false;
                _loginSuccessful = false;
              }

              return Scaffold(
                backgroundColor: Colors.transparent,
                appBar: _buildAppBar(),
                body: _buildBody(),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildAppBar() {
    return PrevozAppBar(
      isBackIcon: true,
      isNavIconShown: true,
      context: context,
      pageTitle: AppLocalizations.of(context).loginPageLoginButton,
      onBackPress: () {
        // empty
      },
    );
  }

  Widget _buildBody() {
    return ListView(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 20, top: 25, right: 20),
          child: Card(
            color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(height: 28,),
                  TextFieldWithTitle(
                    fieldKey: "username",
                    title: AppLocalizations.of(context).loginPageUsernameTitle,
                    controller: _usernameController,
                    error: FormValidators.usernameValidator(_usernameController.text, _errorsFromApi, context),
                  ),
                  SizedBox(height: 18,),
                  TextFieldWithTitle(
                    inputType: TextInputType.visiblePassword,
                    fieldKey: "password",
                    title: AppLocalizations.of(context).loginPagePasswordTitle,
                    obscureText: true,
                    controller: _passwordController,
                    error: FormValidators.passwordValidator(_passwordController.text, _errorsFromApi, context),
                  ),
                  SizedBox(height: 28,),
                  _buildSubmitButton(context),
                  _buildGeneralError(context),
                  _buildGoogleSignInButtonIfNeeded(context),
                  SizedBox(height: 38,),
//                  _buildNavigationToPasswordReset(context),
//                  SizedBox(height: 10,),
                  _buildNavigationToRegister(context),
                  SizedBox(height: 10,)
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildSubmitButton(BuildContext context) {
    if (_showLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_loginSuccessful) {
      return Container();
    }

    return Container(
        width: double.infinity,
        child: RaisedButton(
            padding: EdgeInsets.only(
                top: 10, bottom: 10, left: 22, right: 22),
            child: Text(
              AppLocalizations.of(context).loginPageLoginButton,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            colorBrightness: Brightness.light,
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4)),
            splashColor: Theme.of(context).accentColor,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              _submitLogin();
            })
    );
  }

//  Widget _buildNavigationToPasswordReset(BuildContext context) {
//    return Container(
//      width: double.infinity,
//      height: 70,
//      child: Center(
//        child: FlatButton(
//          child: Text(
//              AppLocalizations.of(context).loginPageGoToPasswordResetPage
//          ),
//          onPressed: () async {
//            await _navigateToPasswordReset(context);
//          },
//        ),
//      ),
//    );
//  }

  Widget _buildNavigationToRegister(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      child: Center(
        child: FlatButton(
          child: Text(
              AppLocalizations.of(context).loginPageGoToRegistrationPage
          ),
          onPressed: () async {
            await _navigateToRegistration(context);
          },
        ),
      ),
    );
  }

  Widget _buildGeneralError(BuildContext context) {
    if (_generalError == null) {
      return Container();
    }

    return Column(
      key: Key("_loginUsernamePassword_generalError"),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 20,),
        Icon(
          Icons.error,
          color: Colors.red,
          size: 40,
        ),
        SizedBox(height: 4,),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Text(
           _generalError,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
    );
  }


  Widget _buildGoogleSignInButtonIfNeeded(BuildContext context) {
    if (_showGoogleSignIn) {
      return Column(
        children: <Widget>[
          SizedBox(height: 10,),
          GoogleSignInButton(onClick: _submitGoogleSignIn)
        ],
      );
    }

    return Container();
  }

  void _handleSubmitError(ErrorSubmittingLogin errorState) {
    Response response = errorState.response;
    if (_isValidApiError(response)) {
      _generalError = null;
      _errorsFromApi = errorState.response.data["error"];
    } else if (_isValidSpecificError(response)) {
      _errorsFromApi = null;
      _generalError = errorState.response.data["detail"];
    } else {
      _errorsFromApi = null;
      _generalError = AppLocalizations.of(context).authErrorLoggingIn;
    }
  }

  bool _isValidApiError(Response response) {
    if (response == null) return false;
    if (response.data == null) return false;
    if (response.data["error"] == null) return false;
    if (!(response.data["error"] is Map)) return false;

    for (var value in (response.data["error"] as Map).values) {
      if (!(value is List)) return false;
    }

    return true;
  }

  bool _isValidSpecificError(Response response) {
    if (response == null) return false;
    if (response.data == null) return false;
    if (response.data["detail"] == null) return false;
    if (!(response.data["detail"] is String)) return false;

    return true;
  }

  void _submitLogin() {
    String username = _usernameController.text.toString();
    String password = _passwordController.text.toString();
    FocusScope.of(context).unfocus();
    _loginBloc.add(SubmitLogin(
      username: username,
      password: password
    ));
  }

  void _submitGoogleSignIn() {
    _loginBloc.add(SubmitGoogleLogin());
  }

//  Future _navigateToPasswordReset(BuildContext context) async {
//    await Navigator.of(context, rootNavigator: true)
//        .push(new CupertinoPageRoute<bool>(
//      builder: (BuildContext context) {
//        return ForgotPasswordPage();
//      },
//    ));
//  }

  Future _navigateToRegistration(BuildContext context) async {
    await Navigator.of(context, rootNavigator: true)
        .push(new CupertinoPageRoute<bool>(
      builder: (BuildContext context) {
        return BlocProvider<RegistrationBloc>(
          create: (context) => RegistrationBloc(
              RepositoryProvider.of<AuthRepository>(context),
              BlocProvider.of<AuthenticationBloc>(context)
          ),
          child: RegisterUsernamePasswordPage(),
        );
      },
    ));
  }
}