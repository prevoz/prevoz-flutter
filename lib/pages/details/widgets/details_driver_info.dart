import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:prevoz_org/pages/details/widgets/drajv_rating_row_item.dart';
import 'package:prevoz_org/pages/details/widgets/prevoz_rating_row_item.dart';
import 'package:prevoz_org/utils/other_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

class DetailsDriverInfo extends StatelessWidget {
  final RideModel ride;
  final GlobalKey<ScaffoldState> scaffoldStateKey;

  const DetailsDriverInfo({this.ride, this.scaffoldStateKey});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Divider(
              height: 1,
              color: Colors.grey,
            ),
            SizedBox(
              height: 12,
            ),
            _buildDriverNameAndCarDescription(context),
            SizedBox(
              height: 12,
            ),
            _buildPhoneNumberAndRatingRow(context),

            //* if DRAJV rating present, display Prevoz&DRAJV rating in same row
            (ride.drajvScore != null && ride.drajvScore > 0)
                ? _buildUserRatingRow(context)
                : Container()
          ],
        ),
      ),
    );
  }

  Widget _buildUserRatingRow(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 12,
        ),
        Row(
          children: <Widget>[
            _buildPrevozRating(context),
            _buildDrajvRating(context)
          ],
        )
      ],
    );
  }

  Widget _buildDrajvRating(BuildContext context) {
    return DrajvRatingRowItem(
      ride: ride,
    );
  }

  Widget _buildPrevozRating(BuildContext context) {
    return PrevozRatingRowItem(
      ride: ride,
    );
  }

  Widget _phoneNumber(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).phoneNumber,
          style: PrevozStyles.LABEL_TEXT,
        ),
        SizedBox(
          height: 8,
        ),
        GestureDetector(
          onTap: () {
            if (ride.contact != null && ride.contact.length > 0) {
              Clipboard.setData(new ClipboardData(text: ride.contact));
              scaffoldStateKey.currentState
                  .showSnackBar(SnackBar(content: Text("Številka kopirana")));
            }
          },
          child: Text(
            (ride.contact != null && ride.contact.toString() != "")
                ? ride.contact.toString().replaceAll("+386", "0")
                : "-",
            style: PrevozStyles.BOLD_INFO_TEXT,
          ),
        )
      ],
    );
  }

  Widget _buildPhoneNumber(BuildContext context) {
    return Expanded(flex: 1, child: _phoneNumber(context));
  }

  Widget _buildDriverNameAndCarDescription(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        //* DRIVER NAME
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).driver,
                style: PrevozStyles.LABEL_TEXT,
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                (ride.author != null && ride.author != "")
                    ? OtherUtils.capitaliseFirstLetter(ride.author)
                    : "-",
                style: PrevozStyles.BOLD_INFO_TEXT,
              ),
            ],
          ),
        ),
        //* CAR DESCRIPTION
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              (ride.businessName == null || ride.businessActive == null)
                  ? Text(
                      AppLocalizations.of(context).car,
                      style: PrevozStyles.LABEL_TEXT,
                    )
                  : Row(
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).car,
                          style: PrevozStyles.LABEL_TEXT,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Icon(Icons.airport_shuttle),
                      ],
                    ),
              SizedBox(
                height: 8,
              ),
              Text(
                (ride.carInfo != null && ride.carInfo != "")
                    ? OtherUtils.capitaliseFirstLetter(ride.carInfo)
                    : "-",
                style: PrevozStyles.BOLD_INFO_TEXT,
              )
            ],
          ),
        ),
      ],
    );
  }

  Row _buildPhoneNumberAndRatingRow(BuildContext context) {
    return Row(
      children: <Widget>[
        _buildPhoneNumber(context),
        //* if drajv rating not present and prevoz rating has data
        //* display prevoz rating in same row as contact to save space
        ((ride.drajvScore == null || ride.drajvScore == 0) &&
                ((ride.scoreNegative != null && ride.scoreNegative > 0) ||
                    (ride.scorePositive != null && ride.scorePositive > 0)))
            ? _buildPrevozRating(context)
            : Container()
      ],
    );
  }
}
