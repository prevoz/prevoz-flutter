import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/ride_details_redesign/ride_details_redesign_bloc.dart';
import 'package:prevoz_org/data/blocs/ride_details_redesign/ride_details_redesign_event.dart';
import 'package:prevoz_org/data/blocs/ride_details_redesign/ride_details_redesign_state.dart';
import 'package:prevoz_org/data/helpers/auth/auth_helper.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class DetailsLocationsInfo extends StatefulWidget {
  final RideModel ride;
  DetailsLocationsInfo({this.ride});

  @override
  _DetailsLocationsInfoState createState() => _DetailsLocationsInfoState();
}

class _DetailsLocationsInfoState extends State<DetailsLocationsInfo> {
  @override
  void initState() {
    super.initState();

    BlocProvider.of<RideDetailsRedesignBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Column(
        children: <Widget>[
          Divider(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).from,
                      style: PrevozStyles.LABEL_TEXT,
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      locationFrom(),
                      style: PrevozStyles.BOLD_INFO_TEXT,
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).to,
                      style: PrevozStyles.LABEL_TEXT,
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      locationTo(),
                      style: PrevozStyles.BOLD_INFO_TEXT,
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  String locationFrom() {
    String locationFrom = widget.ride.from;

    if (widget.ride.fromSublocation != null &&
        widget.ride.fromSublocation.length > 0) {
      String fromSubLocation = widget.ride.fromSublocation;

      locationFrom = locationFrom + " ($fromSubLocation)";
    }

    return locationFrom;
  }

  String locationTo() {
    String locationTo = widget.ride.to;

    if (widget.ride.toSublocation != null &&
        widget.ride.toSublocation.length > 0) {
      String toSubLocation = widget.ride.toSublocation;

      locationTo = locationTo + " ($toSubLocation)";
    }

    return locationTo;
  }

  void setBookmark(BuildContext context) {
    BlocProvider.of<RideDetailsRedesignBloc>(context).add(
        RideDetailsRedesignSetBookmark(
            _getNewBookmarkState(), widget.ride.id.toString()));
  }

  //todo - refactor and remove this... bookmarks are now handled inside a FAB on RideDetailsRedesing.
  bookmarkBlocBuilder() {
    BlocBuilder(
      cubit: BlocProvider.of<AuthenticationBloc>(context),
      builder: (context, AuthenticationState state) {
        if (state is AuthStateAuthenticated) {
          return BlocBuilder(
            cubit: BlocProvider.of<RideDetailsRedesignBloc>(context),
            builder: (context, RideDetailsRedesignState bookmarkState) {
              //LOADING SPINNER
              if (bookmarkState is RideDetailsRedesignSavingBookmarkState) {
                return setupBookmarkIcon(
                    SizedBox(height: 24, child: CircularProgressIndicator()),
                    context);
              }

              if (bookmarkState is RideDetailsRedesignBookmarkSavedState) {
                if (widget.ride.bookmark != null &&
                    (widget.ride.bookmark == MyConstants.BOOKMARK ||
                        widget.ride.bookmark == MyConstants.GOING_WITH)) {
                  // UNFAVOURITED
                  widget.ride.bookmark = null;
                } else {
                  // FAVOURITED
                  widget.ride.bookmark = MyConstants.BOOKMARK;
                }

                BlocProvider.of<RideDetailsRedesignBloc>(context)
                    .resetToInitialState();
              }

              Widget iconPlaceholder;

              if (widget.ride.bookmark == null) {
                iconPlaceholder = Icon(Icons.bookmark_border);
              } else {
                iconPlaceholder = Icon(Icons.bookmark);
              }

              return setupBookmarkIcon(iconPlaceholder, context);
            },
          );
        }

        return Expanded(
          flex: 15,
          child: InkWell(
            onTap: () {
              AuthHelper.showSigninDialog(context);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  AppLocalizations.of(context).bookmark,
                  style: PrevozStyles.LABEL_TEXT,
                ),
                SizedBox(
                  height: 6,
                ),
                Icon(
                  Icons.favorite_border,
                  size: 25,
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget setupBookmarkIcon(Widget iconPlaceholder, BuildContext context) {
    return Expanded(
      flex: 15,
      child: InkWell(
        onTap: () {
          setBookmark(context);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppLocalizations.of(context).bookmark,
              style: PrevozStyles.LABEL_TEXT,
            ),
            SizedBox(
              height: 6,
            ),
            iconPlaceholder
          ],
        ),
      ),
    );
  }

  String _getNewBookmarkState() {
    return widget.ride.bookmark == MyConstants.BOOKMARK
        ? null
        : MyConstants.BOOKMARK;
  }
}
