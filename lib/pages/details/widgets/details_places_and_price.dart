import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/other_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class DetailsPlacesPriceTime extends StatelessWidget {
  final RideModel ride;

  const DetailsPlacesPriceTime({Key key, this.ride}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8),
        child: Stack(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(flex: 1, child: _buildRideTime(context)),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
                _buildFreePlaces(context),
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
                _buildPriceField(context),
              ],
            )
          ],
        ));
  }

  Widget _buildPriceField(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).price,
          style: PrevozStyles.LABEL_TEXT,
        ),
        SizedBox(
          height: 6,
        ),
        _buildPriceText(context),
      ],
    );
  }

  Widget _buildPriceText(BuildContext context) {
    String priceString = "-";

    if (ride.price != null) {
      priceString = OtherUtils.removeZerosFromPrice(
            ride.price,
          ) +
          "€";
    }
    return Text(priceString, style: PrevozStyles.BOLD_INFO_TEXT);
  }

  Widget _buildFreePlaces(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).freePlaces,
          style: PrevozStyles.LABEL_TEXT,
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          ride.numPeople != null ? ride.numPeople.toString() : "-",
          style: PrevozStyles.BOLD_INFO_TEXT,
        )
      ],
    );
  }

  Widget _buildRideTime(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).hour,
          style: PrevozStyles.LABEL_TEXT,
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          ride.time != null ? DateTimeUtils.pretifyApiTime(ride.time) : "",
          style: PrevozStyles.BOLD_INFO_TEXT,
        )
      ],
    );
  }
}
