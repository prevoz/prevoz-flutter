import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class DetailsForBusiness extends StatelessWidget {
  final RideModel ride;
  const DetailsForBusiness({Key key, this.ride}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (ride.businessName == null || ride.businessActive == null) {
      return Container();
    }

    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Divider(
              height: 1,
              color: Colors.grey,
            ),
            SizedBox(
              height: 12,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        AppLocalizations.of(context).business,
                        style: PrevozStyles.LABEL_TEXT,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        (ride.businessName != null && ride.businessName != "")
                            ? ride.businessName
                            : "-",
                        style: PrevozStyles.BOLD_INFO_TEXT,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
