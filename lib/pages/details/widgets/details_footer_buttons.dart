import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailsFooterButtons extends StatelessWidget {
  final RideModel ride;
  final GlobalKey<ScaffoldState> scaffoldStateKey;
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  DetailsFooterButtons(this.ride, this.scaffoldStateKey, {Key key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: FlatButton(
              splashColor: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(4.0),
                  side: BorderSide(
                    width: 2,
                    color: HexColor("39D053"),
                  )),
              child: Container(
                height: 40,
                child: Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(AppLocalizations.of(context).call,
                          style: TextStyle(
                              color: HexColor("43BF4F"),
                              fontSize: 17,
                              fontWeight: FontWeight.bold)),
                      SizedBox(
                        width: 4,
                      ),
                      Icon(
                        Icons.call,
                        color: HexColor("43BF4F"),
                      )
                    ],
                  ),
                ),
              ),
              onPressed: () {
                analytics.logEvent(name: "details_page_call");
                _launchPhoneCall(ride.contact);
              },
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Expanded(
            flex: 3,
            child: RaisedButton(
              splashColor: Theme.of(context).accentColor,
              color: HexColor(PrevozStyles.PREVOZ_ORANGE_HEX),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(4.0)),
              child: Container(
                height: 40,
                child: Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(AppLocalizations.of(context).sendSms,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.bold)),
                      SizedBox(
                        width: 4,
                      ),
                      Icon(
                        Icons.message,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
              ),
              onPressed: () {
                analytics.logEvent(name: "details_page_sms");
                _launchSmsApp(ride.contact, context);
              },
            ),
          )
        ],
      ),
    );
  }

  _launchPhoneCall(String number) async {
    try {
      if (number == null) {
        return;
      }
      String url = 'tel:' + number;
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    } catch (e) {
      Clipboard.setData(new ClipboardData(text: number));
      scaffoldStateKey.currentState.showSnackBar(SnackBar(
          content: Text(
              "Napaka pri odpiranju aplikacije za telefonske klice. Številka je kopirana in jo lahko prilepiš.")));
    }
  }

  void _launchSmsApp(String number, BuildContext context) async {
    if (number == null) {
      return;
    }
    String url = 'sms:' + number;
    String relation = ride.from + "-" + ride.to;
    String msg = "";
    String time =
        DateTimeUtils.dateToStringForDisplay(ride.dateIso8601, context)
                .toLowerCase() +
            " " +
            ride.time;
    if (Platform.isAndroid) {
      msg =
          '?body=Živjo!%20Zanima me, ali imate za prevoz $relation, $time še kakšno prosto mesto?';
      url = url + msg;
    }

    if (Platform.isIOS) {
      final separator = Platform.isIOS ? '&' : '?';
      msg =
          "Živjo! Zanima me, ali imate za prevoz $relation, $time še kakšno prosto mesto?";
      url =
          'sms:$number${separator}body=${Uri.encodeFull(msg)}&subject=${Uri.encodeFull('0404032023')}';
    }

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
          "Could open SMS template with url $url", StackTrace.fromString(url));
      throw 'Could not launch $url';
    }
  }
}
