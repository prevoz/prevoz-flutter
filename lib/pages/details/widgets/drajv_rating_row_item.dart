import 'dart:math';

import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class DrajvRatingRowItem extends StatelessWidget {
  final RideModel ride;

  const DrajvRatingRowItem({Key key, this.ride}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 1,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppLocalizations.of(context).drajvScore,
              style: PrevozStyles.LABEL_TEXT,
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              drajvString(),
              style: TextStyle(fontSize: 21, color: HexColor("4D4D4D")),
            )
          ],
        ));
  }

  String drajvString() {
    String roundedScore = roundDouble(ride.drajvScore, 0).toString();

    if (roundedScore.contains(".0")) {
      roundedScore = roundedScore.replaceAll(".0", "");
    }

    return roundedScore + " / 100";
  }

  double roundDouble(double val, int places) {
    double mod = pow(10.0, places);
    return ((val * mod).round().toDouble() / mod);
  }
}
