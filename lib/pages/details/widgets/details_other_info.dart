import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/details/widgets/custom_expansion_tile.dart';
import 'package:prevoz_org/pages/details/widgets/prevoz_rating_row_item.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

/// verified phone, insurance, picks midway
class DetailsOtherInfo extends StatelessWidget {
  final RideModel ride;

  const DetailsOtherInfo({this.ride});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          dividerColor: Colors.transparent,
          highlightColor: Colors.black,
          accentColor: Colors.black,
          fontFamily: "Inter",
          focusColor: Colors.black),
      child: DetailsCustomExpansionTile(
        title: Text("Ostale informacije"),
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: EdgeInsets.only(top: 8, bottom: 8),
              child: Column(
                children: <Widget>[
                  Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                  _buildBusinessInfo(context),
                  _buildRating(context),
                  SizedBox(
                    height: 12,
                  ),
                  _buildInsuranceAndConfirmedNumberRow(context),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: <Widget>[
                      _buildPicksMidway(context),
                    ],
                  )
                  //todo - decide what to do here
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  /// builds prevoz rating if its not present in ride data
  Widget _buildRating(BuildContext context) {
    if (isPrevozScoreEmpty() && isDrajvScoreEmpty()) {
      return Column(
        children: <Widget>[
          SizedBox(
            height: 12,
          ),
          Row(
            children: <Widget>[_buildPrevozScore(context)],
          )
        ],
      );
    }
    //* if Prevoz or DRAJV rating is present, this is an empty row
    return Container();
  }

  bool isDrajvScoreEmpty() {
    if (ride.drajvScore == null || ride.drajvScore == 0) {
      return true;
    }

    return false;
  }

  bool isPrevozScoreEmpty() {
    if ((ride.scorePositive == null || ride.scorePositive == 0) &&
        (ride.scoreNegative == null || ride.scoreNegative == 0)) {
      return true;
    }

    return false;
  }

  Widget _buildPrevozScore(BuildContext context) {
    return PrevozRatingRowItem(
      ride: ride,
    );
  }

  Row _buildInsuranceAndConfirmedNumberRow(BuildContext context) {
    return Row(
      children: <Widget>[
        //* INSURANCE
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).insurance,
                style: PrevozStyles.LABEL_TEXT,
              ),
              SizedBox(
                height: 8,
              ),
              (ride.insured != null && ride.insured == true)
                  ? Image(
                      width: 22,
                      height: 22,
                      image: AssetImage('assets/icon/check_green.png'))
                  : Icon(
                      Icons.cancel,
                      color: Colors.red,
                    ),
            ],
          ),
        ),
        //* CONFIRMED CONTACT
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).confirmedContact,
                style: PrevozStyles.LABEL_TEXT,
              ),
              SizedBox(
                height: 8,
              ),
              (ride.confirmedContact != null && ride.confirmedContact == true)
                  ? Image(
                      width: 22,
                      height: 22,
                      image: AssetImage('assets/icon/check_green.png'))
                  : Icon(
                      Icons.cancel,
                      color: Colors.red,
                    )
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildPicksMidway(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).picksMidway,
            style: PrevozStyles.LABEL_TEXT,
          ),
          SizedBox(
            height: 8,
          ),
          (ride.picksMidway != null && ride.picksMidway == true)
              ? Image(
                  width: 22,
                  height: 22,
                  image: AssetImage('assets/icon/check_green.png'))
              : Icon(
                  Icons.cancel,
                  color: Colors.red,
                ),
        ],
      ),
    );
  }

  Widget _buildBusinessInfo(BuildContext context) {
    if (ride.businessName == null || ride.businessActive == null) {
      return Container();
    }

    return Column(
      children: <Widget>[
        SizedBox(
          height: 12,
        ),
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).business,
                          style: PrevozStyles.LABEL_TEXT,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          (ride.businessName != null && ride.businessName != "")
                              ? ride.businessName
                              : "-",
                          style: PrevozStyles.BOLD_INFO_TEXT,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
