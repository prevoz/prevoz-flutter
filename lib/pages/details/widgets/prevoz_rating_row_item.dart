import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class PrevozRatingRowItem extends StatelessWidget {
  final RideModel ride;
  const PrevozRatingRowItem({Key key, this.ride}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 1,
        child: Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  AppLocalizations.of(context).userScore,
                  style: PrevozStyles.LABEL_TEXT,
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      ride.scorePositive != null
                          ? ride.scorePositive.toString()
                          : "0",
                      style: TextStyle(fontSize: 21, color: HexColor("4D4D4D")),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Image(
                        width: 22,
                        height: 22,
                        image: AssetImage('assets/icon/score_positive.png')),
                    SizedBox(
                      width: 6,
                    ),
                    Text(
                      ride.scoreNegative != null
                          ? ride.scoreNegative.toString()
                          : "0",
                      style: TextStyle(fontSize: 21, color: HexColor("4D4D4D")),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Image(
                        width: 22,
                        height: 22,
                        image: AssetImage('assets/icon/score_negative.png'))
                  ],
                ),
              ],
            ),
          ],
        ));
  }
}
