import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class DetailsComment extends StatelessWidget {
  final RideModel ride;
  DetailsComment({this.ride});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(
            height: 1,
            color: Colors.grey,
          ),
          SizedBox(
            height: 12,
          ),
          Text(
            AppLocalizations.of(context).comments,
            style: PrevozStyles.LABEL_TEXT,
          ),
          SizedBox(
            height: 6,
          ),
          Text(
            ride.comment,
            style: PrevozStyles.BOLD_INFO_TEXT,
          ),
          SizedBox(
            height: 12,
          ),
          Divider(
            height: 1,
            color: Colors.grey,
          ),
        ],
      ),
    );
  }
}
