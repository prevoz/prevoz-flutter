import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class DetailsDateAndTime extends StatelessWidget {
  final RideModel ride;
  const DetailsDateAndTime({this.ride});

  @override
  Widget build(BuildContext context) {
    //DateTimeUtils.apiDateStringToDateTime

    DateTime dateTime =
        ride.dateIso8601; // DateTimeUtils.apiDateStringToDateTime(ride.date);
    String dateForDisplay =
        DateTimeUtils.dateTimeToDayAndMonthName(dateTime, context);

    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 30,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).datum,
                      style: PrevozStyles.LABEL_TEXT,
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      ride.date != null ? dateForDisplay : "",
                      style: PrevozStyles.BOLD_INFO_TEXT,
                    )
                  ],
                ),
              ),
              Expanded(
                  flex: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context).hour,
                            style: PrevozStyles.LABEL_TEXT,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            ride.time != null
                                ? DateTimeUtils.pretifyApiTime(ride.time)
                                : "",
                            style: PrevozStyles.BOLD_INFO_TEXT,
                          )
                        ],
                      ),
                    ],
                  )),
            ],
          )
        ],
      ),
    );
  }
}
