import 'package:flutter/material.dart';

class PrevozDetailsAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  @required
  final BuildContext context;

  PrevozDetailsAppBar({
    this.context,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.transparent,
        height: 80,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
          child: Stack(
            children: <Widget>[
              Positioned(
                left: 0,
                child: _navIcon(context),
              ),
              Row(
                //* for now the title of DetailsPAge appbar is empty but im leaving this here just in case
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 40,
                    alignment: AlignmentDirectional.center,
                    child: Text(
                      "",
                      style: TextStyle(fontSize: 29, fontFamily: "InterBlack"),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(120);

  Widget _navIcon(BuildContext context) {
    return InkResponse(
      onTap: () {
        Navigator.of(context).pop();
      },
      child: new Container(
        width: 40,
        height: 40,
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: new Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
      ),
    );
  }
}
