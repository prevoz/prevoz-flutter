import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/ride_details/ridedetails_bloc.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/pages/details/widgets/details_comment.dart';
import 'package:prevoz_org/pages/details/widgets/details_driver_info.dart';
import 'package:prevoz_org/pages/details/widgets/details_footer_buttons.dart';
import 'package:prevoz_org/pages/details/widgets/details_location_info.dart';
import 'package:prevoz_org/pages/details/widgets/details_other_info.dart';
import 'package:prevoz_org/pages/details/widgets/details_places_and_price.dart';
import 'package:prevoz_org/pages/search_results/widgets/ride_details_app_bar.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class RideDetailsPage extends StatefulWidget {
  final RideModel ride;
  final String accessToken;

  const RideDetailsPage({this.ride, this.accessToken});

  @override
  _RideDetailsPageState createState() => _RideDetailsPageState();
}

class _RideDetailsPageState extends State<RideDetailsPage> {
  final scaffoldStateKey = new GlobalKey<ScaffoldState>();
  RideDetailsBloc _rideDetailsBloc;

  FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {
    super.initState();
    _rideDetailsBloc = BlocProvider.of<RideDetailsBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        //* #1 - white background for the whole page
        _buildWhiteBackground(),
        //* #2 - PrevozDetailsAppBar needs to be transparent
        //*      but we put this container before it as background
        //*      to achieve seemless transition of gradient from appbar to page body
        _buildGradientBackground(),
        //* #3 - then business as usuall with a Scaffold widget that has a transparent background.
        _buildTransparentScaffold(),
      ],
    );
  }

  Widget _buildWhiteBackground() {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
    );
  }

  Widget _buildGradientBackground() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [
          HexColor("#ffffff"), //Background Gradient od: #ffffff do #C3E9F4
          HexColor("#C3E9F4")
        ], //UIColor(red:0.76, green:0.91, blue:0.96, alpha:1).cgColor
        begin: Alignment.bottomLeft,
        end: Alignment.topLeft,
      )),
      height: 140,
      width: MediaQuery.of(context).size.width,
    );
  }

  Widget _buildTransparentScaffold() {
    return Scaffold(
      backgroundColor: Colors.transparent,
      key: scaffoldStateKey,
      appBar: RideDetailsAppBar(
        ride: widget.ride,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 18, right: 18, bottom: 20),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              DetailsLocationsInfo(ride: widget.ride),
              DetailsPlacesPriceTime(ride: widget.ride),
              DetailsDriverInfo(
                  ride: widget.ride, scaffoldStateKey: scaffoldStateKey),
              widget.ride.isCommentPresent()
                  ? DetailsComment(ride: widget.ride)
                  : _buildSpacingForEmptyCommentField(),
              DetailsOtherInfo(
                ride: widget.ride,
              ),
              DetailsFooterButtons(widget.ride, scaffoldStateKey)
              ////////
            ],
          ),
        ),
      ),
      //persistentFooterButtons: _buildPersistantFooterButtons(context),
    );
  }

  @override
  void dispose() {
    _rideDetailsBloc.resetToInitialState();
    super.dispose();
  }

  Widget _buildSpacingForEmptyCommentField() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 12,
        ),
        Divider(
          height: 1,
          color: Colors.grey,
        ),
      ],
    );
  }
}
