import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/blocs/add_ride_profile/add_ride_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/add_ride_profile/add_ride_profile_event.dart';
import 'package:prevoz_org/data/blocs/add_ride_profile/add_ride_profile_state.dart';
import 'package:prevoz_org/data/helpers/event_bus/quick_ride_selected_event.dart';
import 'package:prevoz_org/data/models/add_ride_input_types.dart';
import 'package:prevoz_org/data/models/app_tab.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/quick_entries_widget.dart';
import 'package:prevoz_org/pages/my_rides/blocs/my_rides_list_bloc.dart/my_rides_list_bloc.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/enum_from_to.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/utils/form_validators.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_bloc.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_event.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_state.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bloc.dart';
import 'package:prevoz_org/pages/add_ride/widgets/date_and_time.dart';
import 'package:prevoz_org/widgets/other/location_search_delegate.dart';
import 'package:prevoz_org/widgets/form_fields/location_form_field.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_bloc.dart';
import 'package:prevoz_org/widgets/other/prevoz_gradient_blue_to_white.dart';

class AddRideForm extends StatefulWidget {
  final RideModel ride;
  final bool isLargeDevice;
  final bool needsVerification;
  AddRideForm({this.ride, this.isLargeDevice, this.needsVerification});

  @override
  _AddRideFormState createState() => _AddRideFormState();
}

class _AddRideFormState extends State<AddRideForm> {
  Logger logger = Logger(printer: CustomLogPrinter("_AddRideFormState"));
  bool _editMode = false;
  bool _hasBeenSubmitedToApi = false;
  bool _keyboardVisible = false;
  String _phoneNumberFromAuth;
  double dynamicBottomPadding =
      0; //* workaround for making the comments field visible when keyboard is shown

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  double _paddingBottom = 0.0;

  RideModel _ride;
  DateTime _selectedDate = DateTime.now();
  Map<String, dynamic> _errorsFromApi;
  List<int> _indexesOfInputsWithErrors =
      []; // for scrolling to fields with invalid inputs

  FocusNode _priceFocusNode = FocusNode();
  FocusNode _numPplFocusNode = FocusNode();
  FocusNode _commentFocusNode = FocusNode();
  FocusNode _phoneFocusNode = FocusNode();
  FocusNode _carDescriptionNode = FocusNode();
  FocusNode _datePickerFocusNode = FocusNode();
  FocusNode _firstNameNode = FocusNode();
  FocusNode _lastNameNode = FocusNode();
  FocusNode _addressNode = FocusNode();

  TextEditingController _toController = TextEditingController();
  TextEditingController _fromController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _carDescriptionController = TextEditingController();

  TextEditingController _phoneController = TextEditingController();
  TextEditingController _commentController = TextEditingController();
  TextEditingController _numPeopleController = TextEditingController();

  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _addressController = TextEditingController();

  ScrollController _formScrollController = ScrollController();

  FirebaseAnalytics analytics = FirebaseAnalytics();

  AuthenticationBloc _authenticationBloc;
  AddRideProfileBloc _profileBloc;
  StreamSubscription<bool> _keyboardVisibilitySubscription;
  final GlobalKey _dateAndTimeKey = GlobalKey();
  final GlobalKey _infoLabourKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _profileBloc = BlocProvider.of<AddRideProfileBloc>(context);
    addListeners();
//    _firstNameController.addListener(_disableProfilePreFill);
    _ride = widget.ride;
    if (_ride != null) {
      _setupEditRideMode();
    } else {
      _setupNewRideMode();
      _handlePhoneNumbers();
    }
    _setupKeyboardListener();
  }



  @override
  Widget build(BuildContext context) {
    _paddingBottom = MediaQuery.of(context).size.height / 10 * 3.2;
    return Stack(
      children: <Widget>[
        PrevozGradientBlueToWhite(),
        //* GestureDetector placed on top so that the user can close the keyboard when she taps away
        GestureDetector(
          onTap: () {
            _hideKeyboard(context);
          },
          child: BlocBuilder(
              cubit: BlocProvider.of<AddRideBloc>(context),
              builder: (BuildContext context, AddRideState state) {
                if (state is ErrorSubmitingRide) {
                  handleApiError(state);
                }

                if (widget.needsVerification && state is SuccessfullySubmitedNewRide) {
                  _refreshAuthState();
                }

                if (state is SuccessfullyDeletedRide ||
                    state is SuccessfullySubmitedNewRide ||
                    state is SuccessfullyEditedRide) {
                  handleRideFormChange(state);
                }

                if (state is SubmitingRide) {
                  return _buildLoadingScreen();
                }
                return Scaffold(
                    backgroundColor: Colors.transparent,
                    key: _scaffoldKey,
                    appBar: _buildAppBar(),
                    body: _buildBody());
              }),
        ),
      ],
    );
  }

  Column _buildFormFields(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        _buildQuickEntries(),
        _buildFromLocation(),
        _buildToLocation(),
        _buildDateAndTimeFields(),
        _buildNumOfPeopleAndPrice(),
        _buildCarDescriptionField(),
        _buildPhoneNumber(),
        _builcCommentsTextField(),
        _buildSetFull(),
        _buildInsurance(),
        _buildPicksMidway(),
        _buildVerificationRequirements(),
        _buildFooterButtons(),
        _buildDynamicSizingWidget()
      ],
    );
  }

  Widget _buildAppBar() {
    return _buildRegularAppBar();
  }

  Widget _buildRegularAppBar() {
    return PrevozAppBar(
        context: context,
        pageTitle: _setupAppBarTitle(),
        isBackIcon: false,
        isNavIconShown: false);
  }

  _hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  _handleQuickEntryCallback(RideModel quickEntry) {
    MyApp.eventBus.fire(QuickRideSelectedEvent(quickEntry));
    setState(() {
      _toController.text = quickEntry.to;
      _fromController.text = quickEntry.from;
      _phoneController.text = quickEntry?.contact?.toString() ?? "";
      _priceController.text = quickEntry?.price?.toString() ?? "";
      _numPeopleController.text = quickEntry?.numPeople?.toString() ?? "";
      _commentController.text = quickEntry?.comment ?? "";
      _carDescriptionController.text = quickEntry?.carInfo ?? "";
      _ride.from = quickEntry.from;
      _ride.fromCountry = quickEntry.fromCountry;
      _ride.to = quickEntry.to;
      _ride.toCountry = quickEntry.toCountry;
      _ride.dateIso8601 = quickEntry.dateIso8601;
      _ride.time = quickEntry.time;
      // _ride.date =
      //     DateTimeUtils.dateToStringForDisplay(quickRide.dateIso8601, context);
      //_selectedDate = quickRide.dateIso8601;
    });
  }

  Padding _buildBody() {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 20),
      child: SingleChildScrollView(
        key: Key("addRideScrollView"),
        controller: _formScrollController,
        child: _addRideForm(),
      ),
    );
  }

  Widget _addRideForm() {
    //* this check here makes the form revalidate with errors from API
    if (_hasBeenSubmitedToApi) {
      Future.delayed(Duration(milliseconds: 1), () {
        _formKey.currentState?.validate();
      });
    }

    return Form(
      key: _formKey,
      autovalidateMode: AutovalidateMode.disabled,
      child: _buildFormFields(context),
    );
  }

  Widget _buildNumOfPeopleAndPrice() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildNumPpl(),
          SizedBox(
            width: 14,
          ),
          _buildPrice(),
        ],
      ),
    );
  }

  Widget _buildPrice() {
    return Expanded(
      flex: 4,
      child: TextFormField(
          key: Key("addRidePrice"),
          style: PrevozStyles.formInuptTextStyle,
          decoration: PrevozStyles.setupResponsiveDecoration(
              context: context,
              focusNode: _priceFocusNode,
              inputType: AddRideInputType.price,
              isEditMode: _editMode,
              textController: _priceController,
              label: "Strošek",
              iconData: Icons.euro_symbol),
          enabled: !_editMode,
          controller: _priceController,
          focusNode: _priceFocusNode,
          keyboardType: TextInputType.numberWithOptions(decimal: true),
          validator: (val) {
            String priceErr =
                FormValidators.priceValidator(val, _errorsFromApi, context);

            if (priceErr != null) {
              print("______price err $priceErr");
              if (!_indexesOfInputsWithErrors.contains(3)) {
                _indexesOfInputsWithErrors.add(3);
              }
            }
            return priceErr;
          },
          onEditingComplete: () {
            if (!_editMode) {
              _requestFocus(_carDescriptionNode);
            }
          },
          onSaved: (val) {
            setState(() {
              if (val is String && val.contains(",")) {
                val = val.replaceAll(",", ".");
              }
              if (double.tryParse(val) is double) {
                _ride.price = double.parse(val);
              }
            });
          }),
    );
  }

  Widget _buildNumPpl() {
    return Expanded(
      flex: 5,
      child: TextFormField(
          key: Key("addRideNumOfPlaces"),
          decoration: PrevozStyles.setupResponsiveDecoration(
              focusNode: _numPplFocusNode,
              textController: _numPeopleController,
              label: AppLocalizations.of(context).numOfPeople,
              iconData: Icons.people,
              isEditMode: _editMode,
              context: context,
              inputType: AddRideInputType.numOfPpl),
          style: PrevozStyles.formInuptTextStyle,
          focusNode: _numPplFocusNode,
          controller: _numPeopleController,
          keyboardType: TextInputType.number,
          validator: (val) {
            String numPplError =
                FormValidators.numPplValidator(val, _errorsFromApi, context);

            if (numPplError != null) {
              print("_____numpplerr $numPplError");
              if (!_indexesOfInputsWithErrors.contains(3)) {
                _indexesOfInputsWithErrors.add(3);
              }
            }
            return numPplError;
          },
          onEditingComplete: () {
            if (!_editMode) {
              _requestFocus(_priceFocusNode);
            }
          },
          onSaved: (val) {
            setState(() {
              if (val != null && val.length > 0) {
                logger.i("num ppl value $val");
                _ride.numPeople = int.parse(val);
              }
            });
          }),
    );
  }

  Widget _buildQuickEntries() {
    if (_editMode) {
      return Container();
    }
    return QuickEntriesWidget(
      key: PageStorageKey(DateTime.now().millisecondsSinceEpoch),
      quickEntrySelectedCallback: (RideModel selectedQuickEntry) {
        _handleQuickEntryCallback(selectedQuickEntry);
      },
    );
  }

  Widget _buildFromLocation() {
    return Padding(
      padding: EdgeInsets.only(top: 15),
      child: LocationFormField(
          key: Key("_addRideFromLocation"),
          decoration: PrevozStyles.setupResponsiveDecoration(
              context: context,
              focusNode: null,
              isEditMode: _editMode,
              textController: _fromController,
              inputType: AddRideInputType.location,
              label: AppLocalizations.of(context).departureLocation,
              iconData: Icons.home),
          style: PrevozStyles.formInuptTextStyle,
          enabled: !_editMode,
          controller: _fromController,
          onInputTap: () {
            //* a bug occurs if you navigate to LocationSearchDelegate with an open keyboard,
            //* therefor we have to hide it
            if (_keyboardVisible) {
              _hideKeyboard(context);
            }

            LocationsSearchDelegate locationsSearchDelegate =
                LocationsSearchDelegate(
                    SearchType.SearchRideFrom,
                    _ride.from == null
                        ? null
                        : Location(
                            country: _ride.fromCountry, name: _ride.from));
            showSearch(context: context, delegate: locationsSearchDelegate)
                .then((onValue) {
              if (onValue is Location) {
                setState(() {
                  _fromController.text = onValue.name;
                  _ride.from = onValue.name;
                  _ride.fromCountry = onValue.country;
                });
              }
            });
          },
          validator: (val) {
            String fromError = FormValidators.fromLocationValidator(
                val, _errorsFromApi, context);

            if (fromError != null) {
              if (!_indexesOfInputsWithErrors.contains(0)) {
                _indexesOfInputsWithErrors.add(0);
              }
            }

            return fromError;
          }),
    );
  }

  Widget _buildToLocation() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: LocationFormField(
          key: Key("_addRideToLocation"),
          decoration: PrevozStyles.setupResponsiveDecoration(
              context: context,
              focusNode: null,
              textController: _toController,
              isEditMode: _editMode,
              inputType: AddRideInputType.location,
              label: AppLocalizations.of(context).arrivalLocation,
              iconData: Icons.directions_car),
          style: PrevozStyles.formInuptTextStyle,
          enabled: !_editMode,
          controller: _toController,
          onInputTap: () {
            //* a bug occurs if you navigate to LocationSearchDelegate with an open keyboard,
            //* therefor we have to hide it
            if (_keyboardVisible) {
              _hideKeyboard(context);
            }
            LocationsSearchDelegate locationsSearchDelegate =
                LocationsSearchDelegate(
                    SearchType.SearchRideTo,
                    _ride.to == null
                        ? null
                        : Location(country: _ride.toCountry, name: _ride.to));
            showSearch(context: context, delegate: locationsSearchDelegate)
                .then((onValue) {
              if (onValue is Location) {
                setState(() {
                  _toController.text = onValue.name;
                  _ride.to = onValue.name;
                  _ride.toCountry = onValue.country;
                  _requestFocus(_datePickerFocusNode);
                });
              }
            });
          },
          validator: (val) {
            String toError = FormValidators.toLocationValidator(
                val, _errorsFromApi, context);

            if (toError != null) {
              if (!_indexesOfInputsWithErrors.contains(1)) {
                _indexesOfInputsWithErrors.add(1);
              }
            }

            return toError;
          }),
    );
  }

  DateAndTimeFields _buildDateAndTimeFields() {
    return DateAndTimeFields(
      key: _dateAndTimeKey,
      ride: _ride,
      editMode: _editMode,
      formScrollController: _formScrollController,
      errorsFromApi: _errorsFromApi,
      onDateChanged: (DateTime changedDate) {
        logger.i("onDateChanged $changedDate");
        setState(() {
          _selectedDate = changedDate;
          _ride.date = DateTimeUtils.dateToStringForApiCalls(changedDate);
        });
      },
      onTimeChanged: (TimeOfDay changedTime) {
        if (changedTime != null) {
          setState(() {
            String prettyTime =
                DateTimeUtils.timeOfDayToPrettyString(changedTime);

            _ride.time = prettyTime;
            if (!_editMode) {
              //todo request focus to next field
            }
          });
        }
      },
      onValidationFailedCallback: () {
        print("_____ validation failed callback");
        if (!_indexesOfInputsWithErrors.contains(2)) {
          _indexesOfInputsWithErrors.add(2);
        }
      },
    );
  }

  Widget _buildCarDescriptionField() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: TextFormField(
        key: Key("addRideCarDescription"),
        style: PrevozStyles.formInuptTextStyle,
        focusNode: _carDescriptionNode,
        decoration: PrevozStyles.setupResponsiveDecoration(
            context: context,
            focusNode: _carDescriptionNode,
            isEditMode: _editMode,
            textController: _carDescriptionController,
            inputType: AddRideInputType.carDescription,
            label: AppLocalizations.of(context).car,
            iconData: Icons.directions_car),
        validator: (String value) {
          return null;
        },
        onEditingComplete: () {
          if (!_editMode && _phoneNumberFromAuth == null) {
            _requestFocus(_phoneFocusNode);
          }
        },
        onFieldSubmitted: (ha) {
          _hideKeyboard(context);
        },
        keyboardType: TextInputType.text,
        maxLines: 1,
        controller: _carDescriptionController,
        onSaved: (value) {
          setState(() {
            _ride.carInfo = value;
          });
        },
      ),
    );
  }

  Widget _buildPhoneNumber() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: TextFormField(
          key: Key("phoneNumberField"),
          style: PrevozStyles.formInuptTextStyle,
          enabled: !_editMode,
          focusNode: _phoneFocusNode,
          controller: _phoneController,
          keyboardType: TextInputType.phone,
          decoration: PrevozStyles.setupResponsiveDecoration(
              context: context,
              focusNode: _phoneFocusNode,
              textController: _phoneController,
              isEditMode: _editMode,
              inputType: AddRideInputType.phoneNumber,
              label: AppLocalizations.of(context).phoneNumber,
              iconData: Icons.phone),
          validator: (val) {
            String contactErr =
                FormValidators.contactValidator(val, _errorsFromApi, context);

            if (contactErr != null) {
              if (!_indexesOfInputsWithErrors.contains(5)) {
                _indexesOfInputsWithErrors.add(5);
              }
            }
            return contactErr;
          },
          onSaved: (val) {
            setState(() {
              _ride.contact = val;
            });
          }),
    );
  }

  Widget _buildSetFull() {
    if (_editMode) {
      Key checkBoxKey;
      if (_ride.full == null || _ride.full == false) {
        checkBoxKey = Key("emptyCheckbox");
      } else {
        checkBoxKey = Key("fullCheckbox");
      }

      return ListTileTheme(
        contentPadding: EdgeInsets.only(right: 2),
        child: CheckboxListTile(
            key: checkBoxKey,
            title: Text(
              AppLocalizations.of(context).rideIsFull,
              style: TextStyle(fontSize: 13, fontFamily: "InterRegular"),
            ),
            value: _ride.full != null ? _ride.full : false,
            onChanged: (bool val) {
              setState(() {
                _ride.full = val;
              });
            }),
      );
    }

    return Container();
  }

  Widget _buildInsurance() {
    return ListTileTheme(
        key: Key("addRideInsurance"),
        contentPadding: EdgeInsets.only(right: 2),
        child: CheckboxListTile(
            title: Text(
              AppLocalizations.of(context).passangerInsurance,
              style: TextStyle(fontSize: 13, fontFamily: "InterRegular"),
            ),
            //secondary: Icon(Icons.child_care),
            value: _ride.insured,
            onChanged: _editMode
                ? null
                : (bool val) {
                    setState(() {
                      _ride.insured = val;
                    });
                  }));
  }

  Widget _buildPicksMidway() {
    return ListTileTheme(
      key: Key("addRidePicksMidway"),
      contentPadding: EdgeInsets.only(right: 2),
      child: CheckboxListTile(
        title: Text(
          AppLocalizations.of(context).pickMidway,
          style: TextStyle(fontSize: 13, fontFamily: "InterRegular"),
        ),
        value: _ride.picksMidway,
        onChanged: _editMode
            ? null
            : (bool val) {
                setState(() {
                  _ride.picksMidway = val;
                });
              },
      ),
    );
  }

  Widget _buildVerificationRequirements() {
    if (!_editMode && widget.needsVerification) {
      _ride.extended = true;

      return BlocBuilder(
        cubit: _profileBloc,
        builder: (BuildContext buildContext, AddRideProfileState state) {
          if (state is InitialAddRideProfileState || state is FetchingAddRideProfile) {
            if (state is InitialAddRideProfileState) {
              _profileBloc.add(GetUserAddRideProfile());
            }
            return Container(
              key: Key("addRide_progressIndicatorContainer"),
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 30),
              child: Center(
                child: CircularProgressIndicator()
              )
            );
          }

          if (state is ErrorFetchingAddRideProfile) {
            return _buildProfileFailedBody();
          }

          if (state is SuccessfullyFetchedAddRideProfile || state is StoppedFetchingAddRideProfile) {
            if (state is SuccessfullyFetchedAddRideProfile) {
              // Pre-fill the data only once
              _firstNameController.text = state.userProfile.firstName;
              _lastNameController.text = state.userProfile.lastName;
              _addressController.text = state.userProfile.address;
              _profileBloc.add(StopFetchingAddRideProfile()); // Change the sate so it doesn't override the user input every time
            }
            return Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildInfoLabourLaw(),
                  _buildFirstName(),
                  _buildLastName(),
                  _buildAddress(),
                  SizedBox(height: 10,)
                ],
              ),
            );
          }

          return _buildProfileFailedBody();
        }
      );
    }

    return Container();
  }

  Widget _buildProfileFailedBody() {
    return Padding(
      key: Key("addRide_generalProfileError"),
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.error,
            color: Colors.red,
            size: 40,
          ),
          SizedBox(height: 4,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: Text(
              AppLocalizations.of(context).errorFetchingProfileDataFromServer,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.red),
            ),
          ),
          SizedBox(height: 40)
        ],
      ),
    );
  }

  Widget _buildInfoLabourLaw() {
    return Text(
      AppLocalizations.of(context).labourLaw,
      key: Key("addRide_labourLawContainer"),
      style: TextStyle(color: Colors.black),
    );
  }

  Widget _buildFirstName() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: TextFormField(
        key: Key("addRideFirstName"),
        style: PrevozStyles.formInuptTextStyle,
        focusNode: _firstNameNode,
        decoration: PrevozStyles.setupResponsiveDecoration(
            context: context,
            focusNode: _firstNameNode,
            isEditMode: _editMode,
            textController: _firstNameController,
            inputType: AddRideInputType.carDescription,
            label: AppLocalizations.of(context).firstName,
            iconData: Icons.person),
        validator: (String val) {
          return FormValidators.firstNameValidator(val, _errorsFromApi, context);
        },
        onEditingComplete: () {
          _requestFocus(_lastNameNode);
        },
        onFieldSubmitted: (ha) {
          _requestFocus(_lastNameNode);
        },
        keyboardType: TextInputType.text,
        maxLines: 1,
        controller: _firstNameController,
        onSaved: (value) {
          setState(() {
            _ride.firstName = value;
          });
        },
      ),
    );
  }

  Widget _buildLastName() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: TextFormField(
        key: Key("addRideLastName"),
        style: PrevozStyles.formInuptTextStyle,
        focusNode: _lastNameNode,
        decoration: PrevozStyles.setupResponsiveDecoration(
            context: context,
            focusNode: _lastNameNode,
            isEditMode: _editMode,
            textController: _lastNameController,
            inputType: AddRideInputType.carDescription,
            label: AppLocalizations.of(context).lastName,
            iconData: Icons.person),
        validator: (String val) {
          return FormValidators.lastNameValidator(val, _errorsFromApi, context);
        },
        onEditingComplete: () {
          _requestFocus(_addressNode);
        },
        onFieldSubmitted: (ha) {
          _requestFocus(_lastNameNode);
        },
        keyboardType: TextInputType.text,
        maxLines: 1,
        controller: _lastNameController,
        onSaved: (value) {
          setState(() {
            _ride.lastName = value;
          });
        },
      ),
    );
  }

  Widget _buildAddress() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: TextFormField(
        key: Key("addRideAddress"),
        style: PrevozStyles.formInuptTextStyle,
        focusNode: _addressNode,
        decoration: PrevozStyles.setupResponsiveDecoration(
            context: context,
            focusNode: _addressNode,
            isEditMode: _editMode,
            textController: _addressController,
            inputType: AddRideInputType.carDescription,
            label: AppLocalizations.of(context).address,
            isMultiline: true,
            iconData: Icons.home),
        validator: (String val) {
          return FormValidators.addressValidator(val, _errorsFromApi, context);
        },
        onEditingComplete: () {
          _hideKeyboard(context);
        },
        onFieldSubmitted: (ha) {
          _hideKeyboard(context);
        },
        keyboardType: TextInputType.multiline,
        maxLines: 4,
        controller: _addressController,
        onSaved: (value) {
          setState(() {
            _ride.address = value;
          });
        },
      ),
    );
  }

  Padding _builcCommentsTextField() {
    return Padding(
      padding: EdgeInsets.only(top: 15, bottom: 12),
      child: TextFormField(
        key: Key("commentField"),
        focusNode: _commentFocusNode,
        style: PrevozStyles.formInuptTextStyle,
        decoration: PrevozStyles.setupResponsiveDecoration(
            context: context,
            focusNode: _commentFocusNode,
            textController: _commentController,
            isEditMode: _editMode,
            inputType: AddRideInputType.comment,
            label: AppLocalizations.of(context).comments,
            isMultiline: true,
            iconData: Icons.comment),
        validator: (String value) {
          //has no validatior
          return null;
        },
        keyboardType: TextInputType.multiline,
        maxLines: 4,
        controller: _commentController,
        onSaved: (value) {
          setState(() {
            _ride.comment = value;
          });
        },
      ),
    );
  }

  _showDeleteDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            AppLocalizations.of(context).deleteRideDialogTitle,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                AppLocalizations.of(context).cancel.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text(
                AppLocalizations.of(context).deleteRide.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () async {
                await _deleteRide();
                analytics.logEvent(name: "add_ride_page_deleted");
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildFooterButtons() {
    if (!_editMode) {
      return Padding(
        padding: EdgeInsets.only(top: 12, bottom: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: _cancelAndClosePageBtn(),
              flex: 4,
            ),
            Expanded(
              flex: 10,
              child: _submitRideBtn(),
            )
          ],
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: _cancelAndClosePageBtn(),
                flex: 4,
              ),
              Expanded(
                flex: 7,
                child: _editRideBtn(),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Divider(
              height: 2,
              color: Colors.grey,
            ),
          ),
          Center(
            child: _deleteRideBtn(),
          )
        ],
      ),
    );
  }

  //* this is used for comments visibility when keyboard is on
  SizedBox _buildDynamicSizingWidget() {
    return SizedBox(
      height: dynamicBottomPadding,
    );
  }

  void _submit() {
    _errorsFromApi = null;
    _ride.added = DateTime.now();
    _ride.type = 0; //ride share
    _formKey.currentState.save();
    _hasBeenSubmitedToApi = true;

    if (!_editMode) {
      _ride.dateIso8601 =
          RideModel.buildDateTimeIsoFromStrings(_ride.date, _ride.time);
    }
    BlocProvider.of<AddRideBloc>(context)
        .add(PostRide(_ride, _ride.id != null ? true : false));
  }

  //? why we need listeners???
  //! problem desc: the default label size for TextInputField is too small when animated to the edge of the input field on field focus
  //* solution: custom logic for resizing labels...
  //* calling setState forces the widget to be rebuilt
  //* and it is rebuilt by new Decoration properties
  //* returned from PrevozStyles.setupResponsiveDecoration()
  //* For now this is the only way to dynamically
  //* change the label size when field gains or loses focus...
  //todo - refactor each field to it's own stateful widget
  //todo - to avoid potential performance issues because the whole
  //todo - page gets rebuilt
  void addListeners() {
//    _numPplFocusNode.addListener(() {
//      if (_numPplFocusNode.hasFocus && !widget.isLargeDevice) {
//        _formScrollController.animateTo(100,
//            curve: Curves.easeIn, duration: Duration(milliseconds: 600));
//      }
//      setState(() {});
//    });
//
//    _priceFocusNode.addListener(() {
//      if (_priceFocusNode.hasFocus && !widget.isLargeDevice) {
//        _formScrollController.animateTo(100,
//            curve: Curves.easeIn, duration: Duration(milliseconds: 600));
//      }
//
//      setState(() {});
//    });
//
//    _carDescriptionNode.addListener(() {
//      if (_carDescriptionNode.hasFocus && !widget.isLargeDevice) {
//        _formScrollController.animateTo(160,
//            curve: Curves.easeIn, duration: Duration(milliseconds: 600));
//      }
//      setState(() {});
//    });
//
//    _phoneFocusNode.addListener(() {
//      if (_phoneFocusNode.hasFocus) {
//        _formScrollController.en
//      }
//      setState(() {});
//    });
//
//    _commentFocusNode.addListener(() {
//      if (_commentFocusNode.hasFocus && !widget.isLargeDevice) {
//        _scrollToBottom();
//      }
//      setState(() {});
//    });
    _numPplFocusNode.addListener(() async {
      if (_numPplFocusNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_dateAndTimeKey.currentContext);
      }
    });
    _priceFocusNode.addListener(() async {
      if (_priceFocusNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_dateAndTimeKey.currentContext);
      }
    });
    _carDescriptionNode.addListener(() async {
      if (_carDescriptionNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_priceFocusNode.context);
      }
    });
    _phoneFocusNode.addListener(() async {
      if (_phoneFocusNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_carDescriptionNode.context);
      }
    });
    _commentFocusNode.addListener(() async {
      if (_commentFocusNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_phoneFocusNode.context);
      }
    });
    _firstNameNode.addListener(() async {
      if (_firstNameNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_infoLabourKey.currentContext);
      }
    });
    _lastNameNode.addListener(() async {
      if (_lastNameNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_firstNameNode.context);
      }
    });
    _addressNode.addListener(() async {
      if (_addressNode.hasFocus){
        await new Future.delayed(const Duration(milliseconds: 300));
        Scrollable.ensureVisible(_lastNameNode.context);
      }
    });
  }

//  void _scrollToBottom() {
//    _formScrollController.animateTo(
//        _formScrollController.position.maxScrollExtent,
//        duration: Duration(milliseconds: 600),
//        curve: Curves.easeIn);
//  }

  _setupNewRideMode() {
    _editMode = false;

    _ride = RideModel.empty();
    _ride.insured =
        false; //todo: this looks like it should be pulled from users settings
    _ride.picksMidway = false;
    _ride.date = DateTimeUtils.dateToStringForApiCalls(_selectedDate);
    _ride.time = DateTimeUtils.getNextFullHour();
  }

  _setupEditRideMode() {
    _editMode = true;
    _fromController.text = RideModel.getFullFromName(_ride);
    _toController.text = RideModel.getFullToName(_ride);

    //todo - use correct slovenian decimal formatting: , instead of .
    String ridePrice = _ride.price != null ? _ride.price.toString() : "";
    _priceController.text = ridePrice;

    if (_ride.time.contains("ob ")) {
      _ride.time = _ride.time.replaceAll("ob ", "");
    }

    _phoneController.text =
        _ride.contact != null ? _ride.contact.toString() : "";
    _commentController.text = _ride.comment != null ? _ride.comment : "";
    _numPeopleController.text =
        _ride.numPeople != null ? _ride.numPeople.toString() : "";
    _carDescriptionController.text = _ride.carInfo != null ? _ride.carInfo : "";
  }

  _deleteRide() {
    BlocProvider.of<AddRideBloc>(context).add(DeleteRide(widget.ride));
  }

  _requestFocus(FocusNode focusNode) {
    FocusScope.of(context).requestFocus(focusNode);
  }

  Widget _deleteRideBtn() {
    return InkWell(
      onTap: () {
        _showDeleteDialog();
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        child: Text(AppLocalizations.of(context).deleteRide,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
      ),
    );
  }

  Widget _cancelAndClosePageBtn() {
    return InkWell(
      onTap: () {
        _cancelAndClosePage();
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        child: Center(
          child: Text(AppLocalizations.of(context).cancel,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
        ),
      ),
    );
  }

  RaisedButton _submitRideBtn() {
    return RaisedButton(
      key: Key("addRideSubmitBtn"),
      splashColor: Theme.of(context).accentColor,
      color: HexColor(PrevozStyles.PREVOZ_ORANGE_HEX),
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(4.0)),
      child: Container(
        height: 40,
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(AppLocalizations.of(context).publishRide,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17)),
              SizedBox(
                width: 4,
              ),
              Icon(
                Icons.check,
                color: Colors.white,
                size: 25,
              )
            ],
          ),
        ),
      ),
      onPressed: () {
        analytics.logEvent(name: "add_ride_page_submit");
        _submit();
      },
    );
  }

  RaisedButton _editRideBtn() {
    return RaisedButton(
      splashColor: Theme.of(context).accentColor,
      color: HexColor(PrevozStyles.PREVOZ_ORANGE_HEX),
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(4.0)),
      child: Container(
        height: 40,
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(AppLocalizations.of(context).saveRideBtn,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17)),
              SizedBox(
                width: 4,
              ),
              Icon(Icons.arrow_forward_ios, color: Colors.white, size: 15),
            ],
          ),
        ),
      ),
      onPressed: () {
        analytics.logEvent(name: "add_ride_page_edit");
        _submit();
      },
    );
  }

  String _setupAppBarTitle() {
    return _editMode == true
        ? AppLocalizations.of(context).editRideTitle
        : AppLocalizations.of(context).addNewRideTitle;
  }

  @override
  void dispose() {
    _keyboardVisibilitySubscription?.cancel();
    _profileBloc.add(ResetAddRideProfileToInitialState());
    super.dispose();

    // _priceFocusNode.dispose();
    // _numPplFocusNode.dispose();
    // _commentFocusNode.dispose();
    // _phoneFocusNode.dispose();
    // _carDescriptionNode.dispose();
    // _datePickerFocusNode.dispose();
  }

  Widget _buildLoadingScreen() {
    return Scaffold(
        backgroundColor: Colors.transparent,
        key: _scaffoldKey,
        appBar: PrevozAppBar(
            context: context,
            pageTitle: _setupAppBarTitle(),
            isBackIcon: false,
            isNavIconShown: false),
        //persistentFooterButtons: _buildPersistentFooterButtons(),
        body: Center(
          child: CircularProgressIndicator(),
        ));
  }

  _handlePhoneNumbers() {
    AuthStateAuthenticated auth = _authenticationBloc.state;
    List<dynamic> phoneNumbers = auth.phoneNumbers;
    if (phoneNumbers != null && phoneNumbers.length > 0) {
      logger.d("Authenticated phonenumbers: $phoneNumbers");
      _phoneNumberFromAuth = phoneNumbers[(phoneNumbers.length - 1)].toString();
      _phoneController.text = _phoneNumberFromAuth;
    }
  }

  void _refreshAuthState() {
    _authenticationBloc.add(AppStarted()); // TODO create a refresh event instead
  }

  handleRideFormChange(AddRideState state) {
    RideFormChange rideFormChange;
    if (state is SuccessfullyDeletedRide) {
      rideFormChange = RideFormChange.rideDeleted;
      BlocProvider.of<MyRidesListBloc>(context).removeRide(state.ride);
    }

    if (state is SuccessfullySubmitedNewRide) {
      rideFormChange = RideFormChange.rideAdded;
      BlocProvider.of<MyRidesListBloc>(context).addRide(state.ride);
    }

    if (state is SuccessfullyEditedRide) {
      rideFormChange = RideFormChange.rideUpdated;
    }

    _rideFormChange(rideFormChange);
  }

  /// ensures that the comments field is visible when keyboard is shown
  /// by adding extra padding to the bottom of the form
  _setupKeyboardListener() {
    //todo: handle the case when keyboard visible before comments field was clicked...
    _keyboardVisibilitySubscription = KeyboardVisibility.onChange.listen((bool visible) {
      double newBottomPadding;
      if (visible) {
        if (_editMode) {
          newBottomPadding = 45;
        } else {
          if (widget.needsVerification) {
            newBottomPadding = _paddingBottom; //widget.isLargeDevice ? 375 : 175;
          } else {
            newBottomPadding = 75;
          }
        }
      } else {
        newBottomPadding = 0;
      }

      setState(() {
        dynamicBottomPadding = newBottomPadding;
        _keyboardVisible = visible;
//          if ((dynamicBottomPadding == 75 || dynamicBottomPadding == 45) &&
//              _commentFocusNode.hasFocus) {
//            Future.delayed(Duration(milliseconds: 1), () {
//              _formScrollController.animateTo(350,
//                  curve: Curves.easeIn, duration: Duration(milliseconds: 01));
//            });
//          }
      });
    });
  }

  void _rideFormChange(RideFormChange rideFormChange) {
    BlocProvider.of<BottomNavigationBloc>(context)
        .add(AddRideBottomNavEvent(rideFormChange));
    BlocProvider.of<AddRideBloc>(context).resetStateToInitial();
  }

  void _cancelAndClosePage() {
    BlocProvider.of<BottomNavigationBloc>(context)
        .add(UpdateTabBottomNavEvent(AppTab.myrides));
    BlocProvider.of<AddRideBloc>(context).resetStateToInitial();
  }

  //* api errors get saved to a _errorsFromApi field that is then used in forms
  handleApiError(ErrorSubmitingRide errorState) {
    if (errorState.response != null &&
        errorState.response.data != null &&
        errorState.response.data["error"] != null) {
      _errorsFromApi = errorState.response.data["error"];
      logger
          .i("ErrorState, handleApiError -> errors form api: $_errorsFromApi");
    }
  }
}
