import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/helpers/event_bus/ride_submited_event.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/bloc/bloc.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/bloc/quick_entries_event.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/bloc/quick_entries_state.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';

class QuickEntriesBloc extends Bloc<QuickEntriesEvent, QuickEntriesState> {
  final RidesRepository ridesRepository;
  QuickEntriesBloc(this.ridesRepository) : super(InitialQuickEntriesState()) {
    MyApp.eventBus.on<RideSubmitedEvent>().listen((RideSubmitedEvent event) {
      _addARideToQuickEntries(event.ride);
    });
  }
  Logger logger = Logger(printer: CustomLogPrinter("QuickRideBloc"));

  //todo add a simple cache for quick entries
  List<RideModel> _quickEntries = [];

  void fetchQuickEntries() {
    if (!(state is LoadingQuickEntries)) {
      //* dont fetch if already fetching
      if (_quickEntries.length == 0) {
        add(FetchQuickEntriesFromApi());
      } else {
        add(GetQuickEntriesFromBloc());
      }
    }
  }

  void _addARideToQuickEntries(RideModel ride) {
    bool isInList = false;
    for (var i = 0; i < _quickEntries.length; i++) {
      if (_quickEntries[i].to == ride.to &&
          _quickEntries[i].from == ride.from) {
        isInList = true;
      }
    }

    if (!isInList) {
      _quickEntries.add(ride);
    }
  }

  @override
  Stream<QuickEntriesState> mapEventToState(
    QuickEntriesEvent event,
  ) async* {
    if (event is FetchQuickEntriesFromApi) {
      yield* _mapFetchQuickRidesToState();
    }

    if (event is GetQuickEntriesFromBloc) {
      yield SuccessfullyLoadedQuickEntries(_quickEntries);
    }
  }

  Stream<QuickEntriesState> _mapFetchQuickRidesToState() async* {
    try {
      yield LoadingQuickEntries();
      List<RideModel> rides = await ridesRepository.getQuickRides();
      _quickEntries = rides;
      yield SuccessfullyLoadedQuickEntries(rides);
    } catch (e) {
      yield ErrorLoadingQuickEntries();
    }
  }
}
