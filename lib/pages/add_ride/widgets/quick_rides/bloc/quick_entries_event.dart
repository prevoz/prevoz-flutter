import 'package:equatable/equatable.dart';

abstract class QuickEntriesEvent extends Equatable {
  const QuickEntriesEvent();
}

class FetchQuickEntriesFromApi extends QuickEntriesEvent {
  @override
  List<Object> get props => [];
}

class GetQuickEntriesFromBloc extends QuickEntriesEvent {
  @override
  List<Object> get props => [];
}
