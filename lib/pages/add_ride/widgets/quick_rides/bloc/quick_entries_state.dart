import 'package:equatable/equatable.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

abstract class QuickEntriesState extends Equatable {
  const QuickEntriesState();
}

class InitialQuickEntriesState extends QuickEntriesState {
  @override
  List<Object> get props => [];
}

class LoadingQuickEntries extends QuickEntriesState {
  @override
  List<Object> get props => [];
}

class SuccessfullyLoadedQuickEntries extends QuickEntriesState {
  final List<RideModel> quickEntries;
  SuccessfullyLoadedQuickEntries(this.quickEntries);
  @override
  List<Object> get props => [quickEntries];
}

class ErrorLoadingQuickEntries extends QuickEntriesState {
  @override
  List<Object> get props => [];
}
