import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/bloc/bloc.dart';
import 'package:prevoz_org/utils/hex_color.dart';

import '../custom_expansion_tile.dart';

typedef QuickEntrySelectedCallback = void Function(RideModel quickEntry);

class QuickEntriesWidget extends StatefulWidget {
  final QuickEntrySelectedCallback quickEntrySelectedCallback;

  const QuickEntriesWidget({Key key, this.quickEntrySelectedCallback})
      : super(key: key);

  @override
  _QuickEntriesWidgetState createState() => _QuickEntriesWidgetState();
}

class _QuickEntriesWidgetState extends State<QuickEntriesWidget> {
  List<RideModel> quickEntries = [];
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  @override
  void initState() {
    super.initState();
    BlocProvider.of<QuickEntriesBloc>(context).fetchQuickEntries();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<QuickEntriesBloc>(context),
      builder: (BuildContext context, QuickEntriesState state) {
        return _quickRidesTile(state);
      },
    );
  }

  Widget _quickRidesTile(QuickEntriesState state) {
    if (state is LoadingQuickEntries) {
      return QuickRidesCustomExpansionTile(
          title: Text(
            AppLocalizations.of(context).myQuickEntries,
            style: TextStyle(fontFamily: "InterBlack", fontSize: 16),
          ),
          leading: Row(
            children: <Widget>[
              SizedBox(
                height: 22,
                width: 22,
                child: CircularProgressIndicator(),
              ),
              SizedBox(
                width: 12,
              )
            ],
          ),
          children: []);
    }

    if (state is ErrorLoadingQuickEntries) {
      //* ERROR LOADING
      return GestureDetector(
          onTap: () {
            BlocProvider.of<QuickEntriesBloc>(context).fetchQuickEntries();
          },
          child: Column(
            children: <Widget>[
              QuickRidesCustomExpansionTile(
                  title: Text(
                    AppLocalizations.of(context).myQuickEntries,
                    style: TextStyle(fontFamily: "InterBlack", fontSize: 16),
                  ),
                  leading: Row(
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        size: 22,
                      ),
                      SizedBox(
                        width: 12,
                      )
                    ],
                  ),
                  children: []),
              SizedBox(
                height: 12,
              ),
              Text("Napaka pri nalaganju hitrih vnosov"),
              SizedBox(
                height: 6,
              ),
            ],
          ));
    }

    if (state is SuccessfullyLoadedQuickEntries) {
      return QuickRidesCustomExpansionTile(
          title: Text(
            AppLocalizations.of(context).myQuickEntries,
            style: TextStyle(fontFamily: "InterBlack", fontSize: 16),
          ),
          trailing: Image(
              width: 22,
              height: 22,
              image: AssetImage('assets/icon/dropdown_expand.png')),
          children: _buildQuickEntriesList(state.quickEntries));
    }

    return Container();
  }

  List<Widget> _buildQuickEntriesList(List<RideModel> quickEntries) {
    // RideModel first = quickEntries[0];
    //RideModel last = quickEntries[quickEntries.length - 1];

    if (quickEntries.length == 0) {
      return [
        SizedBox(
          height: 12,
        ),
        Text("Nisi še dodal nobenega prevoza"),
        SizedBox(
          height: 6,
        ),
      ];
    }
    return quickEntries.map((RideModel quickEntry) {
      return InkWell(
        onTap: () {
          widget.quickEntrySelectedCallback(quickEntry);
        },
        child: Container(
            decoration: BoxDecoration(
              color: HexColor("fff"),
              border: Border(
                right: BorderSide(color: HexColor("EFECE7")),
                left: BorderSide(color: HexColor("EFECE7")),
                bottom: BorderSide(
                  color: HexColor("EFECE7"),
                ),
              ),
            ),
            child: Padding(
                padding: const EdgeInsets.only(
                    left: 8.0, right: 8, top: 10, bottom: 10),
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      "" + quickEntry.from + " > " + quickEntry.to,
                      style: TextStyle(fontSize: 17),
                    ),
                  ],
                ))),
      );
    }).toList();
  }
}
