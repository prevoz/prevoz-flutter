import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class BackgroundWithGradient extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [
          HexColor("#FFFFFF"),
          HexColor(
              "99e7ff"), //C3E9F4 <-- original HEX from design, but doesnt result in wanted contrast
        ],
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
      )),
    );
  }
}
