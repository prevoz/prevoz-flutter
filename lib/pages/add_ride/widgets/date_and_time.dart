import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/helpers/event_bus/quick_ride_selected_event.dart';
import 'package:prevoz_org/data/models/add_ride_input_types.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/form_validators.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

typedef DateChangedCallback = void Function(DateTime changedDate);
typedef TimeChangedCallback = void Function(TimeOfDay changedTime);
typedef ValidationFailedCallback = void Function();

class DateAndTimeFields extends StatefulWidget {
  final RideModel ride;
  final bool editMode;
  final Map<String, dynamic> errorsFromApi;
  final DateChangedCallback onDateChanged;
  final TimeChangedCallback onTimeChanged;
  final ValidationFailedCallback onValidationFailedCallback;
  final ScrollController formScrollController;

  const DateAndTimeFields(
      {Key key,
      this.errorsFromApi,
      this.editMode,
      this.ride,
      this.onDateChanged,
      this.onValidationFailedCallback,
      this.onTimeChanged,
      this.formScrollController})
      : super(key: key);

  @override
  _DateAndTimeFieldsState createState() => _DateAndTimeFieldsState();
}

class _DateAndTimeFieldsState extends State<DateAndTimeFields> {
  Logger logger = Logger(printer: CustomLogPrinter("DateAndTimeWidget"));
  DateTime _selectedDate = DateTime.now();
  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();
  FocusNode _datePickerFocusNode = FocusNode();
  FocusNode _timePickerFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    logger.i("widget.ride.dateIso8601 " +
        (widget.ride.dateIso8601 != null
            ? widget.ride.dateIso8601.toString()
            : "widget date null"));
    _selectedDate = widget.ride.dateIso8601 == null
        ? DateTime.now()
        : widget.ride.dateIso8601;

    if (widget.editMode != null) {
      _dateController.text = widget.ride.date != null
          ? DateTimeUtils.removeTimeFromRidesDate(widget.ride.date)
          : "";
      _timeController.text =
          widget.ride.time != null ? widget.ride.time.toString() : "";
    }

    MyApp.eventBus
        .on<QuickRideSelectedEvent>()
        .listen((QuickRideSelectedEvent event) {
      _timeController.text = event.quickRide.time;
    });
  }

  void _scrollToFieldsPosition() {
    if (widget.formScrollController != null) {
      widget.formScrollController.animateTo(70,
          curve: Curves.easeIn, duration: Duration(milliseconds: 600));
    }
  }

  @override
  Widget build(BuildContext context) {
    _dateController.text =
        DateTimeUtils.dateTimeToDisplayableDateWithShortenedDayname(
            _selectedDate, context);

    return Padding(
      padding: EdgeInsets.only(top: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildDate(),
          SizedBox(
            width: 14,
          ),
          _buildTime(),
        ],
      ),
    );
  }

  Expanded _buildTime() {
    return Expanded(
      flex: 4,
      child: GestureDetector(
        key: Key("addRideTimePicker"),
        behavior: HitTestBehavior.opaque,
        onTap: () {
          _selectTime();
          _scrollToFieldsPosition();
        },
        child: IgnorePointer(
            ignoring: true,
            child: TextFormField(
              style: PrevozStyles.formInuptTextStyle,
              decoration: PrevozStyles.setupResponsiveDecoration(
                  focusNode: _timePickerFocusNode,
                  isEditMode: widget.editMode,
                  inputType: AddRideInputType.time,
                  textController: _timeController,
                  label: AppLocalizations.of(context).hour,
                  iconData: Icons.timer),
              controller: _timeController,
              validator: (val) {
                //widget.onValidationFailedCallback();

                String timeError = FormValidators.timeValidator(
                    val,
                    widget.errorsFromApi,
                    context,
                    widget.editMode == false
                        ? _selectedDate
                        : widget.ride.dateIso8601);

                if (timeError != null) {
                  print("____timer error $timeError");
                  widget.onValidationFailedCallback();
                }

                return timeError;
              },
            )),
      ),
    );
  }

  Expanded _buildDate() {
    return Expanded(
      flex: 5,
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (!widget.editMode) {
            _scrollToFieldsPosition();
            if (Platform.isAndroid) {
              _showDatePickerAndSelectDate(context);
            }

            if (Platform.isIOS) {
              showModalBottomSheet(
                  context: context,
                  builder: (BuildContext builder) {
                    return Container(
                        height:
                            MediaQuery.of(context).copyWith().size.height / 3,
                        child: iosDatePicker());
                  });
            }
          }
        },
        child: IgnorePointer(
            ignoring: true,
            child: TextFormField(
              style: PrevozStyles.formInuptTextStyle,
              decoration: PrevozStyles.setupResponsiveDecoration(
                  focusNode: _datePickerFocusNode,
                  textController: _dateController,
                  isEditMode: widget.editMode,
                  inputType: AddRideInputType.date,
                  label: AppLocalizations.of(context).datum,
                  iconData: Icons.calendar_today),
              enabled: !widget.editMode,
              controller: _dateController,
              validator: (val) {
                String dateError = FormValidators.dateValidator(
                    _selectedDate, widget.errorsFromApi, context);

                if (dateError != null) {
                  print("____timer error $dateError");
                  widget.onValidationFailedCallback();
                }

                return dateError;
              },
              enableInteractiveSelection: true,
            )),
      ),
    );
  }

  Future<Null> _showDatePickerAndSelectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      selectableDayPredicate: (DateTime val) {
        return DateTimeUtils.selectableDatesForDatePicker(val);
      },
      initialDate: _selectedDate,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
    );

    if (picked != null) {
      setState(() {
        _selectedDate = picked;
        _dateController.text =
            DateTimeUtils.dateTimeToDisplayableDateWithShortenedDayname(
                _selectedDate, context);
        widget.ride.date = DateTimeUtils.dateToStringForApiCalls(picked);
        widget.onDateChanged(_selectedDate);
      });
    }
  }

  Widget iosDatePicker() {
    return CupertinoDatePicker(
      initialDateTime: _selectedDate,
      onDateTimeChanged: (DateTime newdate) {
        if (newdate.isAfter(DateTimeUtils.fortyFiveDaysFromNow())) {
          return;
        }

        if (DateTimeUtils.isSelectedDateBeforeCurrentDate(newdate)) {
          return;
        }
        setState(() {
          _selectedDate = newdate;
          _dateController.text =
              DateTimeUtils.dateTimeToDisplayableDateWithShortenedDayname(
                  _selectedDate, context);
          widget.onDateChanged(_selectedDate);
        });
      },
      maximumDate: DateTime.now().add(Duration(days: 45)),
      minimumYear: DateTime.now().year,
      maximumYear: DateTime.now().year + 1,
      mode: CupertinoDatePickerMode.date,
    );
  }

  Future<Null> _selectTime() async {
    if (Platform.isAndroid) {
      _showAndroidTimePicker();
    }

    if (Platform.isIOS) {
      _showIosTimePicker();
    }
  }

  void _showAndroidTimePicker() async {
    TimeOfDay selectedTime = await showTimePicker(
      context: context,
      initialTime: widget.ride.time != null
          ? DateTimeUtils.timeStringToTimeOfDay(widget.ride.time)
          : DateTimeUtils.nextFullHour(),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );

    if (selectedTime != null) {
      setState(() {
        String prettyTime = DateTimeUtils.timeOfDayToPrettyString(selectedTime);
        _timeController.text = prettyTime;
        widget.ride.time = prettyTime;
        widget.onTimeChanged(selectedTime);
      });
    }
  }

  /// third party library timepicker for ios
  /// https://github.com/ShivamGoyal1899/DateTimePicker/blob/master/lib/main.dart
  void _showIosTimePicker() {
    // new DateTime(time.year, time.month, time.day, newHour, time.minute, time.second, time.millisecond, time.microsecond);

    DatePicker.showDatePicker(
      context,
      dateFormat: 'HH:mm',
      pickerMode: DateTimePickerMode.time, // show TimePicker
      initialDateTime: widget.ride.time != null
          ? DateTimeUtils.stringTimeToDateTime(widget.ride.time)
          : DateTimeUtils.nextFullHour(),
      pickerTheme: DateTimePickerTheme(
          showTitle: false), //hides cancel & confirm buttons
      onChange: (dateTime, List<int> index) {
        setState(() {
          TimeOfDay timeOfDay =
              TimeOfDay(hour: dateTime.hour, minute: dateTime.minute);
          String prettyTime = DateTimeUtils.timeOfDayToPrettyString(timeOfDay);
          _timeController.text = prettyTime;
          widget.onTimeChanged(timeOfDay);
        });
      },
    );

    /*showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
              height: MediaQuery.of(context).copyWith().size.height / 3,
              child: _cupertinoTimePicker());
        });*/

    /*DatePicker.showDatePicker(
      context,
      dateFormat: 'HH:mm',
      pickerMode: DateTimePickerMode.time, // show TimePicker
      initialDateTime: _initialTime,
      pickerTheme: DateTimePickerTheme(
          showTitle: false), //hides cancel & confirm buttons
      onChange: (dateTime, List<int> index) {
        setState(() {
          TimeOfDay timeOfDay =
              TimeOfDay(hour: dateTime.hour, minute: dateTime.minute);
          String prettyTime = DateTimeUtils.timeOfDayToPrettyString(timeOfDay);
          _timeController.text = prettyTime;
          _initialTime = dateTime;
          widget.onTimeChanged(timeOfDay);
        });
      },
    );*/
  }

  @override
  void dispose() {
    super.dispose();
    _datePickerFocusNode.dispose();
    _timePickerFocusNode.dispose();
  }

  //! flutter issue with cupertino time picker not displaying minutes
  // https://github.com/flutter/flutter/issues/36112
  //todo: when the issue gets fixed, consider using this time picker instead of 3d party library in _showIosTimePicker()
  /*Widget _cupertinoTimePicker() {
    return CupertinoTimerPicker(
      //mode: CupertinoTimerPickerMode.hm,
      //minuteInterval: 1,
      mode: CupertinoTimerPickerMode.hm,
      initialTimerDuration: _initialtimer,
      onTimerDurationChanged: (Duration changedtimer) {
        setState(() {
          //double operator %(num other);
          int minutes = changedtimer.inMinutes % 60;
          int hours = changedtimer.inHours;
          print("_____$minutes");

          TimeOfDay timeOfDay = TimeOfDay(hour: hours, minute: minutes);

          String prettyTime = DateTimeUtils.timeOfDayToPrettyString(timeOfDay);
          _timeController.text = prettyTime;
          _initialtimer = changedtimer;
          widget.onTimeChanged(timeOfDay);
        });
      },
    );
  }*/
}
