import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_state.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/add_ride/add_ride_form.dart';
import 'package:prevoz_org/pages/profile/widgets/notify_card.dart';
import 'package:prevoz_org/widgets/cards/auth_error_card.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';
import 'package:prevoz_org/widgets/other/prevoz_gradient_blue_to_white.dart';

class AddRidePage extends StatefulWidget {
  final String accessToken;
  final RideModel ride;
  AddRidePage(this.accessToken, this.ride);
  @override
  _AddRidePageState createState() => _AddRidePageState();
}

class _AddRidePageState extends State<AddRidePage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<AuthenticationBloc>(context),
      builder: (BuildContext context, AuthenticationState state) {
        if (state is AuthStateAuthenticated) {
          return AddRideForm(
              ride: widget.ride,
              isLargeDevice: MediaQuery.of(context).size.height > 840,
              needsVerification: state.needsVerification);
        }

        if (state is AuthStateLoading) {
          return _authLoadingScreen();
        }

        if (state is AuthStateError) {
          return _authErrorScreen();
        }

        return _loginScreen();
      },
    );
  }

  Widget _authLoadingScreen() {
    return Stack(
      children: <Widget>[
        PrevozGradientBlueToWhite(),
        Scaffold(
            backgroundColor: Colors.transparent,
            appBar: PrevozAppBar(
                context: context,
                pageTitle: AppLocalizations.of(context).addRide,
                isBackIcon: false,
                isNavIconShown: false),
            body: Center(child: CircularProgressIndicator()))
      ],
    );
  }

  Widget _loginScreen() {
    return Stack(
      children: <Widget>[
        //PrevozGradientBlueToWhite(),
        PrevozBackground(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: PrevozAppBar(
              context: context,
              pageTitle: AppLocalizations.of(context).addRide,
              isBackIcon: false,
              isNavIconShown: false),
          body: Padding(
            padding: EdgeInsets.only(top: 25),
            child: NotifyCard(
              cardText: "Za objavo novega prevoza je potrebna prijava",
              imgSrc: "assets/images/add_ride_not_logged_in.png",
              pageName: "AddRidePage",
            ),
          ),
        )
      ],
    );
  }

  Widget _authErrorScreen() {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: PrevozAppBar(
            context: context,
            pageTitle: AppLocalizations.of(context).addRide,
            isBackIcon: false,
            isNavIconShown: false),
          body: Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: AuthErrorCard()
            )
          ),
        )
      ],
    );
  }
}
