class InfoItem {
  String title;
  String url;

  InfoItem({this.title, this.url});

  static List<InfoItem> allItems = [
    InfoItem(title: "O nas", url: "https://prevoz.org/about?ui=mobile"),
    InfoItem(
        title: "Pogosta vprašanja", url: "https://prevoz.org/faq?ui=mobile"),
    InfoItem(
        title: "Izjava o zasebnosti",
        url: "https://prevoz.org/privacy?ui=mobile"),
    InfoItem(
        title: "Prevoz.org in DRAJV",
        url: "https://prevoz.org/pages/drajv?ui=mobile"),
    //InfoItem(title: "Oglaševanje", url: "https://prevoz.org/advertising/")
  ];
}
