import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bloc.dart';
import 'package:prevoz_org/data/models/app_tab.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

class NotifyCard extends StatelessWidget {
  final String cardText;
  final bool showButton;
  final String imgSrc;
  final String pageName;
  final FirebaseAnalytics analytics = FirebaseAnalytics();

  NotifyCard({
    Key key,
    @required this.cardText,
    @required this.imgSrc,
    @required this.pageName,
    this.showButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Card(
              color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 45,
                      foregroundColor: Colors.transparent,
                      backgroundColor: Colors.transparent,
                      backgroundImage: AssetImage(imgSrc),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Text(
                      cardText,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: HexColor(PrevozStyles.GREY_TEXT_HEX),
                          fontSize: 18,
                          fontFamily: "Inter"),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    showButton
                        ? Padding(
                            padding: const EdgeInsets.only(left: 30, right: 30),
                            child: ButtonTheme(
                              minWidth: MediaQuery.of(context).size.width,
                              child: RaisedButton(
                                  padding: EdgeInsets.only(
                                      top: 10, bottom: 10, left: 22, right: 22),
                                  child: Text(
                                    AppLocalizations.of(context).login,
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  colorBrightness: Brightness.light,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(4)),
                                  splashColor: Theme.of(context).accentColor,
                                  color: Theme.of(context).primaryColor,
                                  onPressed: () async {
                                    BlocProvider.of<BottomNavigationBloc>(context)
                                        .add(UpdateTabBottomNavEvent(AppTab.profile));
                                  }),
                            ),
                          )
                        : Container(),
                  ],
                ),
              )),
        ),
      ],
    );
  }
}
