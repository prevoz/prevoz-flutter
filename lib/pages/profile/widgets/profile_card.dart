import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/profile/profile_bloc.dart';
import 'package:prevoz_org/data/blocs/profile/profile_event.dart';
import 'package:prevoz_org/data/blocs/profile/profile_state.dart';
import 'package:prevoz_org/data/helpers/auth/auth_helper.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/pages/profile/widgets/profile_card_entry.dart';
import 'package:prevoz_org/pages/profile_edit/edit_profile_page_address.dart';
import 'package:prevoz_org/pages/profile_edit/edit_profile_page_first_and_last_name.dart';

import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/widgets/cards/error_card.dart';

class ProfileCard extends StatefulWidget {

  ProfileCard();

  @override
  _ProfileCardState createState() => _ProfileCardState();
}

class _ProfileCardState extends State<ProfileCard> {
  final _verticalPaddingEntry = 2.0;
  final _horizontalPaddingEntry = 18.0;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProfileBloc>(context).add(GetUserProfile());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<ProfileBloc>(context),
      builder: (BuildContext buildContext, ProfileState state) {
        if (state is InitialProfileState || (state is FetchingProfile && state.storedProfile == null)) {
          return Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Card(
                color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 40),
                    child: CircularProgressIndicator(),
                  )
                ),
              )
            )
          );
        }

        if (state is ErrorFetchingProfile) {
          UserProfile userProfile = state.storedProfile;
          if (userProfile != null) {
            return _buildBody(userProfile, true);
          }
          else {
            return _buildFailedBody();
          }
        }

        if (state is SuccessfullyFetchedProfile || (state is FetchingProfile && state.storedProfile != null)) {
          UserProfile userProfile = state is SuccessfullyFetchedProfile ? state.userProfile : (state as FetchingProfile).storedProfile;
          return _buildBody(userProfile, false);
        }

        return _buildFailedBody();
      },
    );
  }

  Widget _buildFailedBody() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: ErrorCard(
          errorText: AppLocalizations.of(context).errorFetchingProfileDataFromServer,
          onErrorButtonPress: () {
            BlocProvider.of<ProfileBloc>(context).add(GetUserProfile());
          },
        ),
      )
    );
  }

  Widget _buildBody(UserProfile userProfile, bool hasError) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Card(
            color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: 28,),
                _buildAvatar(),
                SizedBox(height: 28,),
                hasError ? _buildErrorEntry() : Container(),
                Divider(),
                _buildFirstAndLastName(context, userProfile),
                Divider(),
                _buildAddress(context, userProfile),
                Divider(),
                _buildEmail(context, userProfile),
                Divider(),
                _buildLogoutButton(context)
              ],
            )
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircleAvatar(
          radius: 45,
          foregroundColor: Colors.transparent,
          backgroundColor: Colors.transparent,
          backgroundImage:
          AssetImage("assets/images/profile_logged_in.png"),
        ),
      ],
    );
  }

  Widget _buildErrorEntry() {
    return Column(
      key: Key("_profileCard_partialError"),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.error,
          color: Colors.red,
          size: 40,
        ),
        SizedBox(height: 4,),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Text(
            AppLocalizations.of(context).errorFetchingProfileDataFromServer,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.red),
          ),
        ),
        SizedBox(height: 20)
      ],
    );
  }

  Widget _buildEntry({
    @required String title,
    @required String titleKey,
    @required String value,
    @required String valueKey,
    Function onTap,
    bool notSet = false
  }) {
    return onTap != null ?
      _buildClickableEntry(
        title: title,
        titleKey: titleKey,
        value: value,
        valueKey: valueKey,
        onTap: onTap,
        notSet: notSet
      )
        :
    _buildNonClickableEntry(
        title: title,
        titleKey: titleKey,
        value: value,
        valueKey: valueKey,
      notSet: notSet
    );
  }

  Widget _buildClickableEntry({
    @required String title,
    @required String titleKey,
    @required String value,
    @required String valueKey,
    @required Function onTap,
    bool notSet = false
  }) {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.symmetric
          (horizontal: _horizontalPaddingEntry,
            vertical: _verticalPaddingEntry
        ),
        child: ProfileCardEntry(
          entryTitle: title,
          entryTitleKey: titleKey,
          entryValue: value,
          entryValueKey: valueKey,
          notSet: notSet,
        ),
      ),
      onTap: onTap,
    );
  }

  Widget _buildNonClickableEntry({
    @required String title,
    @required String titleKey,
    @required String value,
    @required String valueKey,
    bool notSet = false
  }) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: _horizontalPaddingEntry, vertical: _verticalPaddingEntry),
        child: ProfileCardEntry(
          entryTitle: title,
          entryTitleKey: titleKey,
          entryValue: value,
          entryValueKey: valueKey,
          notSet: notSet,
        ),
      ),
    );
  }

  Widget _buildFirstAndLastName(BuildContext context, UserProfile userProfile) {
    bool notSet = userProfile == null ||
        ((userProfile.firstName == null || userProfile.firstName.isEmpty) &&
          (userProfile.lastName == null || userProfile.lastName.isEmpty));

    String value = notSet ?
      AppLocalizations.of(context).profileEntryFirstAndLastNameEmpty :
      "${userProfile?.firstName} ${userProfile?.lastName}";

    return _buildEntry(
      title: AppLocalizations.of(context).profileEntryFirstAndLastName,
      titleKey: "firstAndLastNameTitle",
      value:  value,
      valueKey: "firstAndLastNameValue",
      notSet: notSet,
      onTap: () {
        Navigator.of(context, rootNavigator: true)
            .push(new CupertinoPageRoute<bool>(
            builder: (BuildContext context) {
              return EditProfilePageFirstAndLastName(
                userProfile: userProfile,
              );
            },
          )
        ).then((_) {
          BlocProvider.of<AuthenticationBloc>(context).add(AuthenticationRefreshAccountStatus());
          BlocProvider.of<ProfileBloc>(context).add(GetUserProfile());
        });
      }
    );
  }

  Widget _buildAddress(BuildContext context, UserProfile userProfile) {
    bool notSet = userProfile == null || userProfile.address == null || userProfile.address.isEmpty;
    String value = notSet ?
        AppLocalizations.of(context).profileEntryAddressEmpty :
        userProfile.address;

    return _buildEntry(
      title: AppLocalizations.of(context).profileEntryAddress,
      titleKey: "addressTitle",
      value: value,
      valueKey: "addressValue",
      notSet: notSet,
      onTap: () {
        Navigator.of(context, rootNavigator: true)
            .push(new CupertinoPageRoute<bool>(
             builder: (BuildContext context) {
              return EditProfilePageAddress(
                userProfile: userProfile,
              );
            },
          )
        ).then((_) {
          BlocProvider.of<AuthenticationBloc>(context).add(AuthenticationRefreshAccountStatus());
          BlocProvider.of<ProfileBloc>(context).add(GetUserProfile());
        });
      }
    );
  }

  Widget _buildEmail(BuildContext context, UserProfile userProfile) {
    bool notSet = userProfile == null || userProfile.email == null || userProfile.email.isEmpty;
    String value = notSet ?
        AppLocalizations.of(context).profileEntryEmailEmpty :
        userProfile.email;

    return _buildEntry(
      title: AppLocalizations.of(context).profileEntryEmail,
      titleKey: "emailTitle",
      value: value,
      valueKey: "emailValue"
    );
  }

//  Widget _buildPhone(BuildContext context) {
//    String value = AppLocalizations.of(context).profileEntryPhoneEmpty;
//    return _buildEntry(
//      title: AppLocalizations.of(context).profileEntryPhone,
//      titleKey: "phoneTitle",
//      value: value,
//      valueKey: "phoneValue",
//      notSet: true
//    );
//  }

  Widget _buildLogoutButton(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      child: Center(
        child: FlatButton(
          child: Text(
            AppLocalizations.of(context).logMeOut
          ),
          onPressed: () async {
            AuthHelper.showLogoutDialog(context);
          },
        ),
      ),
    );
  }
}
