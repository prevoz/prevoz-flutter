import 'package:flutter/material.dart';
import 'package:prevoz_org/pages/profile/models/info_item.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoItemCard extends StatelessWidget {
  final int index;

  const InfoItemCard({Key key, this.index})
      : super(key: key); //theres for info item cards

  @override
  Widget build(BuildContext context) {
    if (index == 4) {
      return SizedBox(
        height: 90,
      );
    }
    return InkWell(
      onTap: () {
        launch(InfoItem.allItems[index].url);
      },
      child: Material(
        child: Container(
          height: 40,
          decoration: getBoxDecoration(context),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 18),
            child: Text(InfoItem.allItems[index].title),
          ),
        ),
      ),
    );
  }

  BoxDecoration getBoxDecoration(BuildContext context) {
    if (index == 0) {
      return BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(4), topRight: Radius.circular(4)));
    }

    if (index == 1) {
      //* this is weird because borderRadius + border setting cant go together
      //* so we set the upper border on second item, cuz we cant set radius+border on first one
      return BoxDecoration(
          color: Colors.white,
          border: Border(
              top: BorderSide(color: Theme.of(context).dividerColor),
              bottom: BorderSide(color: Theme.of(context).dividerColor)));
    }

    if (index == 3) {
      return BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(4), bottomRight: Radius.circular(4)));
    }

    return BoxDecoration(
        color: Colors.white,
        border:
            Border(bottom: BorderSide(color: Theme.of(context).dividerColor)));
  }
}
