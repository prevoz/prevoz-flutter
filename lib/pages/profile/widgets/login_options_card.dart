
import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/login/login_bloc.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/login/login_username_password_page.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:prevoz_org/widgets/other/google_sign_in_button.dart';
import 'package:sentry/sentry.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class LoginOptionsCard extends StatefulWidget {
  
  LoginOptionsCard();

  @override
  _LoginOptionsCardState createState() => _LoginOptionsCardState();
}

class _LoginOptionsCardState extends State<LoginOptionsCard> {

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _isAppleLoginAvailable(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          return _buildDataBody(context, snapshot.data);
        }
        else if (snapshot.hasError) {
          Injector.getInjector().get<SentryErrorReporter>().submitMessageToSentry(
              message: "ProfilePageItems, Failed to get device info",
              level: SeverityLevel.error,
              extraData: {
                "reason": snapshot.error != null ? snapshot.error : "unknown"
              }
          );
          return _buildDataBody(context, false);
        }
        else {
          return _buildLoadingBody();
        }
      },
    );
  }

  Future<bool> _isAppleLoginAvailable() async {
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      var majorVersion = int.parse(iosInfo.systemVersion.split(".")[0]);
      return majorVersion >= 13;
    }

    return Future.value(false);
  }

  Widget _buildDataBody(BuildContext context, bool showLoginWithApple) {
    return Column(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Card(
                    color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
                      child: Column(
                        children: <Widget>[
                          _buildNotLoggedInAvatar(context),
                          SizedBox(height: 18),
                          _buildNotLoggedInInfo(context),
                          SizedBox(height: 25),
                          showLoginWithApple ? _buildLoginWithAppleButton(context) : Container(),
                          SizedBox(height: 20),
                          _buildLoginWithGoogleButton(context),
                          SizedBox(height: 20),
                          _buildLoginPasswordButton(context),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )
        )
      ],
    );
  }

  Widget _buildLoadingBody() {
    return Container(
        child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Card(
              color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
              child: Center(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 40),
                    child: CircularProgressIndicator(),
                  )
              ),
            )
        )
    );
  }

  Widget _buildNotLoggedInAvatar(BuildContext context) {
    return CircleAvatar(
      radius: 45,
      foregroundColor: Colors.transparent,
      backgroundColor: Colors.transparent,
      backgroundImage: AssetImage("assets/images/profile_not_logged_in.png"),
    );
  }

  Widget _buildNotLoggedInInfo(BuildContext context) {
    return Text(
      AppLocalizations.of(context).youAreCurrentlyNotLoggedIn,
      textAlign: TextAlign.center,
      style: TextStyle(
          color: HexColor(PrevozStyles.GREY_TEXT_HEX),
          fontSize: 18,
          fontFamily: "Inter"),
    );
  }

  Widget _buildLoginWithAppleButton(BuildContext context) {
    return SignInWithAppleButton(
      style: SignInWithAppleButtonStyle.black,
      borderRadius: BorderRadius.all(Radius.circular(6)),
      height: 48,
      onPressed: _signInWithApple,
    );
  }

  Widget _buildLoginWithGoogleButton(BuildContext context) {
    return GoogleSignInButton(onClick: () => _signInWithGoogle());
  }

  Widget _buildLoginPasswordButton(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: 48,
      child: FlatButton(
        splashColor: Colors.grey,
        child: Text(
          AppLocalizations.of(context).loginWithUsernameAndPasswordButton,
          style: TextStyle(
              inherit: false,
              fontFamily: '.SF Pro Text',
              letterSpacing: -0.41,
              fontWeight: FontWeight.w500,
              fontSize: 18,
              color: Colors.black),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
            side: BorderSide(color: Colors.black)
        ),
        onPressed: () async {
          await _navigateToUsernamePasswordLogin(context);
        },
      ),
    );
  }

  void _signInWithGoogle() {
    BlocProvider.of<AuthenticationBloc>(context).add(AuthenticationGoogleSignIn());
  }

  void _signInWithApple() {
    BlocProvider.of<AuthenticationBloc>(context).add(AuthenticationAppleSignIn());
  }

  Future _navigateToUsernamePasswordLogin(BuildContext context) async {
    await Navigator.of(context, rootNavigator: true)
        .push(new CupertinoPageRoute<bool>(
      builder: (BuildContext context) {
        return BlocProvider<LoginBloc>(
          create: (context) => LoginBloc(
              RepositoryProvider.of<AuthRepository>(context),
              BlocProvider.of<AuthenticationBloc>(context)
          ),
          child: LoginUsernamePasswordPage(),
        );
      },
    ));
  }
}