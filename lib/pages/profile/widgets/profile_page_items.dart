
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prevoz_org/pages/profile/widgets/login_options_card.dart';
import 'package:prevoz_org/pages/profile/widgets/profile_card.dart';
import 'package:prevoz_org/pages/profile/widgets/info_item_card.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_state.dart';

class ProfilePageItems extends StatelessWidget {
  final AuthenticationState authenticationState;
  ProfilePageItems({this.authenticationState});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 25),
      child: ListView.builder(
          itemCount: 7,
          itemBuilder: (BuildContext context, int index) {
            //* PROFILE CARD
            if (index == 0) {
              if (authenticationState is AuthStateAuthenticated) {
                return ProfileCard();
              }

              return LoginOptionsCard();
            }

            //* SPACING
            if (index == 1) {
              return SizedBox(
                height: 15,
              );
            }

            //* INFO CARDS
            return Padding(
              padding: const EdgeInsets.only(left: 24, right: 24),
              child: InfoItemCard(index: index - 2),
            );
          }),
    );
  }
}
