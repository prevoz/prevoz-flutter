
import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class ProfileCardEntry extends StatelessWidget {

  static const PROFILE_CARD_ENTRY_KEY_PREFIX = "_profileCardEntry";

  final String entryTitle;
  final String entryTitleKey;
  final String entryValue;
  final String entryValueKey;
  final bool notSet;

  ProfileCardEntry({
    @required this.entryTitle,
    @required this.entryTitleKey,
    @required this.entryValue,
    @required this.entryValueKey,
    this.notSet = false
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          entryTitle,
          key: Key("${PROFILE_CARD_ENTRY_KEY_PREFIX}_$entryTitleKey"),
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.bold,
            color: HexColor("#4D4D4D") // TODO define app level colors for text ect.
          ),
        ),
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: (MediaQuery.of(context).size.width) * 3 / 7),
          child: Text(
            entryValue,
            key: Key("${PROFILE_CARD_ENTRY_KEY_PREFIX}_$entryValueKey"),
            textAlign: TextAlign.end,
            style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.normal,
                fontStyle: notSet ? FontStyle.italic : FontStyle.normal,
                color: HexColor("#8a8a8a")
            ),
          ),
        )
      ],
    );
  }

}