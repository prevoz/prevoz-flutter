
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/widgets/cards/auth_error_card.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/profile/widgets/profile_page_items.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';


class ProfilePage extends StatefulWidget {
  ProfilePage();
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<AuthenticationBloc>(context),
      builder: (BuildContext context, AuthenticationState state) {

        if (state is AuthStateAuthenticated) {
          return _buildAuthenticatedBody(state);
        }

        if (state is AuthStateLoading) {
          return _buildAuthLoadingBody();
        }

        if (state is AuthStateError) {
          return _buildAuthErrorScreen(state);
        }

        return _buildUnauthenticatedBody(state);
      },
    );
  }

  Widget _buildUnauthenticatedBody(AuthenticationState state) {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Scaffold(
            backgroundColor: Colors.transparent,
            appBar: PrevozAppBar(
              context: context,
              pageTitle: AppLocalizations.of(context).profile,
              isNavIconShown: false,
              isBackIcon: false,
            ),
            body: ProfilePageItems(
              authenticationState: state,
            )),
      ],
    );
  }

  Widget _buildAuthLoadingBody() {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Center(
          child: CircularProgressIndicator(),
        )
      ],
    );
  }

  Widget _buildAuthenticatedBody(AuthenticationState state) {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Scaffold(
            backgroundColor: Colors.transparent,
            appBar: PrevozAppBar(
              context: context,
              pageTitle: AppLocalizations.of(context).profile,
              isNavIconShown: false,
              isBackIcon: false,
            ),
            body: ProfilePageItems(
              authenticationState: state,
            )),
      ],
    );
  }

  Widget _buildAuthErrorScreen(AuthStateError stateError) {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: PrevozAppBar(
            context: context,
            pageTitle: AppLocalizations.of(context).profile,
            isBackIcon: false,
            isNavIconShown: false),
          body: Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: AuthErrorCard()
            )
          ),
        )
      ],
    );
  }
}
