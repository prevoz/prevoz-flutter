import 'dart:async';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/helpers/event_bus/date_picker_selection.dart';
import 'package:prevoz_org/data/helpers/event_bus/location_selected_event.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/pages/search_results/search_results_page.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/enum_from_to.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/widgets/form_fields/location_input.dart';
import 'package:prevoz_org/data/blocs/home_page/home_page_bloc.dart';
import 'package:prevoz_org/widgets/other/location_search_delegate.dart';

class SearchFormCard extends StatefulWidget {
  final Location fromLocation = Location();
  final Location toLocation = Location();
  final scaffoldState;
  SearchFormCard({this.scaffoldState});

  @override
  _SearchFormCardState createState() => _SearchFormCardState();
}

class _SearchFormCardState extends State<SearchFormCard> {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  Location _toLocation;
  Location _fromLocation;
  //List<PrevozRoute> _routesSearchHistory = [];
  DateTime _selectedDate = DateTime.now();
  final _dateTextController = TextEditingController();
  final _toLocationTextController = TextEditingController();
  final _fromLocationTextController = TextEditingController();

  /// SearchFormCard is placed inside a ListView. This means that when its scrolled out of position
  /// it loses its state and data on its inputs... we prevent this by keeping these value in HomePageBloc
  /// and retaining them on init state.
  void _getStateFromBloc() {
    if (BlocProvider.of<HomePageBloc>(context).toLocation != null) {
      _toLocation = BlocProvider.of<HomePageBloc>(context).toLocation;
      _toLocationTextController.text = _toLocation.name;
    }
    if (BlocProvider.of<HomePageBloc>(context).fromLocation != null) {
      _fromLocation = BlocProvider.of<HomePageBloc>(context).fromLocation;
      _fromLocationTextController.text = _fromLocation.name;
    }

    if (BlocProvider.of<HomePageBloc>(context).selectedDate != null) {
      _selectedDate = BlocProvider.of<HomePageBloc>(context).selectedDate;
      _dateTextController.text =
          DateTimeUtils.dateTimeToDisplayableDate(_selectedDate, context);
    }
  }

  @override
  void initState() {
    super.initState();
    _listenForSearchDelegateResults();
    _listenForDatePickerEvents();
  }

  /// [EventBus] is used here because the [SearchFormCard] widget on [HomePage]
  /// can get rebuilt while the user is selecting the location in [LocationsSearchDelegate].
  /// This produces an error because when the [SearchDelegate] [Future] completes,
  /// the [SearchFormCard] widget that started it and awaits for [Future] result, no longer exists.
  /// [EventBus solves this problem]
  _listenForSearchDelegateResults() {
    MyApp.eventBus
        .on<LocationSelectedEvent>()
        .listen((LocationSelectedEvent onData) {
      if (onData.locationType == LocationType.FROM) {
        if (this.mounted) {
          setState(() {
            _fromLocationTextController.text = onData.location.name;
            _fromLocation = onData.location;
            BlocProvider.of<HomePageBloc>(context).fromLocation =
                onData.location;
          });
        }
      }

      if (onData.locationType == LocationType.TO) {
        if (this.mounted) {
          setState(() {
            _toLocationTextController.text = onData.location.name;
            _toLocation = onData.location;
            BlocProvider.of<HomePageBloc>(context).toLocation = onData.location;
          });
        }
      }
    });
  }

  ///this had to be used because of issue #105 on BitBucket repo
  _listenForDatePickerEvents() {
    MyApp.eventBus
        .on<DatePickerSelection>()
        .listen((DatePickerSelection selection) {
      if (this.mounted) {
        setState(() {
          _selectedDate = selection.date;
          BlocProvider.of<HomePageBloc>(context).selectedDate = _selectedDate;
          _dateTextController.text =
              DateTimeUtils.dateTimeToDisplayableDate(_selectedDate, context);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _getStateFromBloc();
    if (_dateTextController.text.length == 0) {
      _dateTextController.text =
          DateTimeUtils.dateTimeToDisplayableDate(_selectedDate, context);
    }

    return Container(
      child: Card(
        color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
        child: Padding(
          padding: EdgeInsets.all(18),
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      _fromLocationInput(),
                      SizedBox(
                        height: 12,
                      ),
                      _toLocationInput(),
                    ],
                  ),
                  _locationSwapButton(),
                ],
              ),
              SizedBox(
                height: 12,
              ),
              _datePicker(),
              SizedBox(
                height: 18,
              ),
              _searchButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _searchButton() {
    return ButtonTheme(
      minWidth: MediaQuery.of(context).size.width,
      child: RaisedButton(
          key: Key("_searchButton"),
          padding: EdgeInsets.only(top: 16, bottom: 16),
          child: Text(
            "Najdi prevoz",
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          colorBrightness: Brightness.light,
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(4)),
          splashColor: Theme.of(context).accentColor,
          color: Theme.of(context).primaryColor,
          onPressed: () async {
            if (_fromLocation == null ||
                _toLocation == null ||
                _fromLocation.name == null ||
                _toLocation.name == null) {
              _showSelectLocationsWarning();

              analytics.logEvent(
                  name: "home_page_search_locations_not_selected");
              return;
            }

            PrevozRoute searchRoute = PrevozRoute(
              locationFrom: _fromLocation.name,
              locationTo: _toLocation.name,
              countryFrom: _fromLocation.country,
              countryTo: _toLocation.country,
            );
            bool isSearchInternational = searchRoute.countryTo != "SI" ||
                searchRoute.countryFrom != "SI";
            RideSearchRequest request = RideSearchRequest(
                route: searchRoute,
                date: _selectedDate,
                intl: isSearchInternational);

            analytics.logEvent(
                name: "home_page_search",
                parameters: {"route": searchRoute.toString()});

            searchRides(searchRequest: request);
          }),
    );
  }

  Widget _locationSwapButton() {
    //* InkWell issue in positioned elements: https://tinyurl.com/yzbcjcar
    return Positioned(
        right: 0,
        top: 30,
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.grey
                      .shade300, // HexColor(PrevozStyles.PREVOZ_ORANGE_HEX),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10))),
              child: Padding(
                padding: EdgeInsets.all(2),
                child: Container(
                  width: 35,
                  height: 40,
                  child: Icon(
                    Icons.swap_vert,
                    size: 35,
                  ),
                ),
              ),
            ),
            Positioned.fill(
                child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                bottomLeft: Radius.circular(10),
              ),
              child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    key: Key("_locationSwapButton"),
                    onTap: () => _handleLocationSwap(),
                  )),
            ))
          ],
        ));
  }

  Widget _fromLocationInput() {
    return LocationInput(
      key: Key("_fromLocationInput"),
      controller: _fromLocationTextController,
      location: _fromLocation,
      isFromLocation: true,
      onInputTap: () {
        LocationsSearchDelegate locationsSearchDelegate =
            LocationsSearchDelegate(SearchType.SearchRideFrom, _fromLocation);
        showSearch(context: context, delegate: locationsSearchDelegate);
      },
    );
  }

  void _handleLocationSwap() {
    setState(() {
      Location frm = _fromLocation;
      Location to = _toLocation;

      if (to != null) {
        _fromLocation = _toLocation;
        _fromLocationTextController.text = _fromLocation.name;
        BlocProvider.of<HomePageBloc>(context).fromLocation = _fromLocation;

        if (frm == null) {
          _toLocation = null;
          _toLocationTextController.text = "";
          BlocProvider.of<HomePageBloc>(context).toLocation = null;
        }
      }

      if (frm != null) {
        _toLocation = frm;
        _toLocationTextController.text = _toLocation.name;
        BlocProvider.of<HomePageBloc>(context).toLocation = _toLocation;

        if (to == null) {
          _fromLocation = null;
          BlocProvider.of<HomePageBloc>(context).fromLocation = null;
          _fromLocationTextController.text = "";
        }
      }

      analytics.logEvent(name: "home_page_search_form_location_swap");
    });
  }

  Widget _toLocationInput() {
    return LocationInput(
        key: Key("_toLocationInput"),
        controller: _toLocationTextController,
        location: _toLocation,
        isFromLocation: false,
        onInputTap: () {
          LocationsSearchDelegate locationsSearchDelegate =
              LocationsSearchDelegate(SearchType.SearchRideTo, _toLocation);
          showSearch(context: context, delegate: locationsSearchDelegate);
        });
  }

  GestureDetector _datePicker() {
    //*Flutter note:
    //* TextField needs GestureDetector with opaque behaviour + Ignore pointer
    //* to override default onTap behaviour
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (Platform.isIOS) {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext builder) {
                return Container(
                    height: MediaQuery.of(builder).copyWith().size.height / 3,
                    child: iosDatePicker(builder));
              });
        }
        if (Platform.isAndroid) {
          _showDatePickerAndSelectDate(context);
        }
      },
      child: IgnorePointer(
        ignoring: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              controller: _dateTextController,
              decoration: PrevozStyles.searchFormCardInputDecoration(
                  AppLocalizations.of(context).datum, _dateTextController),
            ),
          ],
        ),
      ),
    );
  }

  searchRides({@required RideSearchRequest searchRequest}) async {
    {
      //* check connectivity
      if (!await connectivityAvailable()) {
        return;
      }
      //* check form
      if (!isFormValid(searchRequest)) {
        return;
      }
      //* add to SearchHistory
      BlocProvider.of<HomePageBloc>(context).addToSearchHistory(searchRequest);
      //* push to search page
      Navigator.of(context, rootNavigator: true)
          .push(new CupertinoPageRoute<bool>(
              builder: (BuildContext context) {
                return SearchResultsPage(searchRequest);
              },
              settings: RouteSettings(name: "SearchResultsPage")));
    }
  }

  bool isFormValid(RideSearchRequest request) {
    if (request.route.locationTo == null ||
        request.route.locationFrom == null) {
      _showSelectLocationsWarning();
      return false;
    }

    return true;
  }

  Widget iosDatePicker(BuildContext context) {
    return CupertinoDatePicker(
      initialDateTime: _selectedDate,
      onDateTimeChanged: (DateTime newdate) {
        if (newdate.isAfter(DateTimeUtils.fortyFiveDaysFromNow())) {
          return;
        }

        if (DateTimeUtils.isSelectedDateBeforeCurrentDate(newdate)) {
          return;
        }
        MyApp.eventBus.fire(DatePickerSelection(newdate));
      },
      maximumDate: DateTime.now().add(Duration(days: 45)),
      minimumYear: DateTime.now().year,
      maximumYear: DateTime.now().year + 1,
      mode: CupertinoDatePickerMode.date,
    );
  }

  Future<Null> _showDatePickerAndSelectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      selectableDayPredicate: (DateTime val) {
        return DateTimeUtils.selectableDatesForDatePicker(val);
      },
      initialDate: _selectedDate,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
    );

    if (picked != null && picked != _selectedDate) {
      //* had to do it this way because of issue 105
      MyApp.eventBus.fire(DatePickerSelection(picked));
    }
  }

  void _showSelectLocationsWarning() {
    final snackBar = SnackBar(
      content: Text(AppLocalizations.of(context).selectLocationsWarning),
      action: SnackBarAction(
        label: "OK",
        onPressed: () {},
      ),
    );
    widget.scaffoldState.currentState.showSnackBar(snackBar);
  }

  Future<bool> connectivityAvailable() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.none) {
      final snackBar = SnackBar(
        content: Text(AppLocalizations.of(context).notConnectedToInternet),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {},
        ),
      );
      widget.scaffoldState.currentState.showSnackBar(snackBar);
      return false;
    }
    return true;
  }
}
