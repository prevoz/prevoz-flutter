import 'package:flutter/material.dart';

class ProgressIndicatorForClickableDay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 28,
        width: 28,
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: CircularProgressIndicator(),
        ));
  }
}
