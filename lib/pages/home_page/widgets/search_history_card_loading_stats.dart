import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';
import 'package:prevoz_org/pages/home_page/widgets/progress_indicator_for_clickable_day.dart';

class SearchHistoryCardLoadingStats extends StatefulWidget {
  final PrevozRoute route;

  const SearchHistoryCardLoadingStats({
    @required this.route,
  });

  @override
  _SearchHistoryCardLoadingStatsState createState() =>
      _SearchHistoryCardLoadingStatsState();
}

class _SearchHistoryCardLoadingStatsState
    extends State<SearchHistoryCardLoadingStats> {
  @override
  Widget build(BuildContext context) {
    //? check if route statistics fetch in progress
    //? if route statistics fetch is in progress, check if this route is being searched for
    if (widget.route == null) {
      return Container();
    }
    return Card(
      child: Column(
        children: <Widget>[_buildRouteRow(), _buildStatisticRow()],
      ),
    );
  }

  Widget _buildLoadingStatsBox(DateTime datetime, {bool isToday = false}) {
    String dayName = DateTimeUtils.getDayName(datetime, context);
    return Row(
      children: <Widget>[
        Expanded(
          flex: 100,
          child: InkWell(
            onTap: () {},
            child: Column(
              children: <Widget>[
                ProgressIndicatorForClickableDay(),
                SizedBox(
                  height: 2,
                ),
                Text(
                  dayName,
                  style: PrevozStyles.srcHisCardDayTxtStyle,
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: _buildHorizontalDivider(),
          flex: 1,
        )
      ],
    );
  }

  Container _buildRouteRow() {
    return Container(
      color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(width: 1.0, color: HexColor("#D8D8D8")))),
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            //* ROUTE
            child: _setupLocationDisplay()),
      ),
    );
  }

  // if locations
  Widget _setupLocationDisplay() {
    if (PrevozRoute.isRouteStringLong(
        widget.route, MediaQuery.of(context).size.width)) {
      return _twoRowsForRouteTitle();
    }
    return _singleRowForRouteTitle();
  }

  /// business as usual for locations that dont have long strings
  Widget _singleRowForRouteTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        GestureDetector(
          onTap: () {},
          child: Container(
            child: Row(
              children: <Widget>[
                Text(
                  widget.route.locationFrom,
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: "InterBlack",
                      color: HexColor("#4D4D4D")),
                ),
                SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 16,
                  color: HexColor("#4D4D4D"),
                ),
                SizedBox(
                  width: 4,
                ),
                Text(
                  widget.route.locationTo,
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: "InterBlack",
                      color: HexColor("#4D4D4D")),
                ),
              ],
            ),
          ),
        ),
        //! todo - remove comment, implement swap feature on card scope
        GestureDetector(
            onTap: () {
              setState(() {
                String from = widget.route.locationFrom;
                widget.route.locationFrom = widget.route.locationTo;
                widget.route.countryFrom = widget.route.countryTo;
                widget.route.locationTo = from;
                widget.route.countryTo = widget.route.countryFrom;
                //widget.onRouteSwapTap(widget.route);
              });
            },
            child: Icon(
              Icons.swap_horiz,
              size: 25,
              color: HexColor("#4D4D4D"),
            )),
      ],
    );
  }

  /// _singleRowForRouteTitle is not enough for locations with longer string
  /// therefor we have a seperate UI that includes two rows for locations with longer strings
  Widget _twoRowsForRouteTitle() {
    return Container(
        height: 40,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              child: GestureDetector(
                onTap: () {},
                child: Text(
                  widget.route.locationFrom,
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: "InterBlack",
                      color: HexColor("#4D4D4D")),
                ),
              ),
            ),
            //! todo - implement swap feature on card scope
            Positioned(
              right: 0,
              top: 15,
              child: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.swap_horiz,
                    size: 25,
                    color: HexColor("#4D4D4D"),
                  )),
            ),
            Positioned(
              bottom: 0,
              right: 40,
              child: GestureDetector(
                onTap: () {},
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.subdirectory_arrow_right,
                      size: 22,
                      color: HexColor("#4D4D4D"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      widget.route.locationTo,
                      style: TextStyle(
                          fontSize: 18,
                          fontFamily: "InterBlack",
                          color: HexColor("#4D4D4D")),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }

  RouteStatistics getStatsForCurrentCard(
      List<RouteStatistics> statsForAllRoutes) {
    RouteStatistics statsForCurrentCard;
    statsForAllRoutes.forEach((RouteStatistics singleRouteStats) {
      if (singleRouteStats.route.locationFrom == widget.route.locationFrom &&
          singleRouteStats.route.locationTo == widget.route.locationTo) {
        statsForCurrentCard = singleRouteStats;
      }
    });

    return statsForCurrentCard;
  }

  Widget _buildStatisticRow() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          //*TODAY
          Expanded(
              flex: 1,
              child: _buildLoadingStatsBox(DateTime.now(), isToday: true)),

          //* TOMMORROW
          Expanded(
              flex: 1,
              child:
                  _buildLoadingStatsBox(DateTime.now().add(Duration(days: 1)))),

          //* DAY AFTER TOMMORROW
          Expanded(
              flex: 1,
              child:
                  _buildLoadingStatsBox(DateTime.now().add(Duration(days: 2)))),
        ],
      ),
    );
  }

  Widget _buildHorizontalDivider() {
    return SizedBox(
      width: 1,
      child: Container(
        width: 1,
        height: 65,
        color: Colors.grey.shade200,
      ),
    );
  }

  /// 2019-08-22:
  /// key for accessing number of rides in json
  String getMapKeyForDay(DateTime dateTime) {
    String year = dateTime.year.toString();
    String month = dateTime.month.toString();
    String day = dateTime.day.toString();
    if (dateTime.month < 10) {
      month = "0" + month;
    }
    if (dateTime.day < 10) {
      day = "0" + day;
    }
    return "$year-$month-$day";
  }
}
