import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/blocs/home_page/home_page_state.dart';
import 'package:prevoz_org/pages/home_page/widgets/progress_indicator_for_clickable_day.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/data/blocs/home_page/route_statistics/bloc.dart';
import 'package:prevoz_org/data/blocs/home_page/route_statistics/routestatistics_state.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class SearchHistoryCard extends StatefulWidget {
  final PrevozRoute route;
  final Function(RideSearchRequest) onHistoryCardTap;
  final StatsForAllRoutesLoaded statsForAllRoutesLoaded;
  final int searchHistoryIndex;

  const SearchHistoryCard(
      {@required this.route,
      @required this.onHistoryCardTap,
      @required this.statsForAllRoutesLoaded,
      @required this.searchHistoryIndex});

  @override
  _SearchHistoryCardState createState() => _SearchHistoryCardState();
}

class _SearchHistoryCardState extends State<SearchHistoryCard> {
  bool isLoading = false;
  StatsForAllRoutesLoaded changeableStats;
  List<PrevozRoute> allRoutes;
  List<RouteStatistics> allRouteStatistics;

  FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {
    super.initState();
    allRoutes = widget.statsForAllRoutesLoaded.routes;
    allRouteStatistics =
        widget.statsForAllRoutesLoaded.routeStatsResult.routeStatistics;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.route == null) {
      return Container();
    }
    return Card(
      child: Column(
        children: <Widget>[
          _buildRouteRow(),
          _buildStatisticRow(widget.searchHistoryIndex)
        ],
      ),
    );
  }

  //* This is where the magic happens
  //* This widget must be aware of
  Widget _buildClickableDay(DateTime datetime,
      {bool isToday = false, String day}) {
    String dateString = getDateString(datetime);

    String dayName = getDayName(datetime, isToday);

    return InkWell(
      onTap: () {
        _searchRidesForTappedDay(datetime);
        analytics.logEvent(name: "quick_search_click_$day");
      },
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 100,
            child: Column(
              children: <Widget>[
                BlocBuilder(
                    cubit: BlocProvider.of<RouteStatisticsBloc>(context),
                    builder:
                        (BuildContext context, RouteStatisticsState state) {
                      //? Maybe I could significantly simplify the code for single route stats fetches
                      //? by just checking the cards index???

                      if (state is LoadingSingleRouteStats) {
                        //!check if this current route is loading
                        if (isCurrentRouteProcessed(
                            state.route,
                            widget.searchHistoryIndex,
                            state.indexInSearchHistory)) {
                          return ProgressIndicatorForClickableDay();
                        }
                      }

                      if (state is LoadedSingleRouteStats) {
                        _updateRoutes(state);

                        //! check if this current route has loaded
                        if (isCurrentRouteProcessed(
                            state.route,
                            widget.searchHistoryIndex,
                            state.indexInSearchHistory)) {
                          List<RouteStatistics> updatedStats =
                              allRouteStatistics;
                          LoadedSingleRouteStats loadedRouteStats = state;
                          RouteStatistics newStatsForThisCard;

                          if (loadedRouteStats != null &&
                              loadedRouteStats.result != null &&
                              loadedRouteStats.result.routeStatistics != null &&
                              loadedRouteStats.result.routeStatistics.length >
                                  0) {
                            newStatsForThisCard =
                                loadedRouteStats.result.routeStatistics[0];

                            for (int i = 0; i < updatedStats.length; i++) {
                              PrevozRoute route = updatedStats[i].route;
                              if (PrevozRoute.isOpositeRoute(
                                  route, state.route)) {
                                updatedStats[i] = newStatsForThisCard;
                              }
                            }
                            allRouteStatistics = updatedStats;
                          }

                          return _statsText(newStatsForThisCard, dateString);
                        }
                      }

                      if (state is ErrorLoadingSingleRoute) {
                        //! check if this current route has error
                        if (isCurrentRouteProcessed(
                            state.route,
                            widget.searchHistoryIndex,
                            state.indexInSearchHistory)) {
                          return Container(
                            child: Center(
                                child: Icon(
                              Icons.error,
                            )),
                          );
                        }
                      }

                      RouteStatistics routeStatsForThisCard =
                          _getRouteStatsForThisCard();

                      return _statsText(routeStatsForThisCard, dateString);
                    }),
                SizedBox(
                  height: 2,
                ),
                Text(
                  dayName,
                  style: PrevozStyles.srcHisCardDayTxtStyle,
                ),
              ],
            ),
          ),
          Expanded(
            child: day != "day_after_tomorrow"
                ? _buildHorizontalDivider()
                : Container(
                    height: 65,
                  ),
            flex: 1,
          )
        ],
      ),
    );
  }

  /// swaps the newly loaded route in the [List<PrevozRoute> allRoutes]
  void _updateRoutes(LoadedSingleRouteStats state) {
    List<PrevozRoute> updatedRoutes = allRoutes;

    //* update previous route statiscis with new result
    for (int i = 0; i < updatedRoutes.length; i++) {
      if (PrevozRoute.isOpositeRoute(updatedRoutes[i], state.route)) {
        if (widget.searchHistoryIndex == state.indexInSearchHistory) {
          updatedRoutes[i] = state.route;
        }
      }
    }
    allRoutes = updatedRoutes;
  }

  Text _statsText(RouteStatistics routeStatsForThisCard, String dateString) {
    if (routeStatsForThisCard != null &&
        routeStatsForThisCard.datesWithCount != null) {
      String numOfRidesString = "-";
      String clickableDayKey = "clickableDay";

      if (routeStatsForThisCard != null &&
          routeStatsForThisCard.datesWithCount != null &&
          routeStatsForThisCard.route != null) {
        try {
          routeStatsForThisCard.datesWithCount
              .forEach((DateWithCount dateWithCount) {
            if (dateWithCount.date == dateString) {
              numOfRidesString = dateWithCount.count.toString();
            }
          });
          //* key name used for integration test "home_results_details.dart"
          String tomorrowDateString =
              getDateString(DateTime.now().add(Duration(days: 1)));
          if (routeStatsForThisCard.route.locationTo == "Neum" &&
              dateString == tomorrowDateString) {
            clickableDayKey = "clickableDayNeum";
          }
          //logger.i("Route stats ok");
        } catch (e) {
          Injector.getInjector().get<SentryErrorReporter>().logger.i("error is... $e");
          Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("_statsText(), SearchHistoryCard",
              StackTrace.fromString(e.toString()));
        }
      } else {
        Injector.getInjector().get<SentryErrorReporter>().logger.i("Route stats not fit for display");
        Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
            "Route stats not fit for display: $routeStatsForThisCard", null);
      }

      return Text(
        numOfRidesString,
        key: Key(clickableDayKey),
        style: PrevozStyles.srcHisCardCounterTxtStyle,
      );
    }

    return Text("-", style: PrevozStyles.srcHisCardCounterTxtStyle);
  }

  bool isCurrentRouteProcessed(PrevozRoute routeBeingProcessed,
      int indexFromWidget, int indexFromState) {
    try {
      if (PrevozRoute.areRoutesTheSame(widget.route, routeBeingProcessed)) {
        if (indexFromWidget == indexFromState) {
          return true;
        }
      }
    } catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("isCurrentRouteProcessed(), SearchHistoryCard",
          StackTrace.fromString(e.toString()));
    }

    return false;
  }

  /// gets route stats fetched for search history
  RouteStatistics _getRouteStatsForThisCard() {
    PrevozRoute currentRoute = widget.route;

    List<RouteStatistics> allRouteStats =
        widget.statsForAllRoutesLoaded.routeStatsResult.routeStatistics;

    RouteStatistics currentRouteStats;
    allRouteStats.forEach((RouteStatistics routeStats) {
      if (PrevozRoute.areRoutesTheSame(routeStats.route, currentRoute)) {
        currentRouteStats = routeStats;
      }
    });

    return currentRouteStats;
  }

  Container _buildRouteRow() {
    return Container(
      decoration: BoxDecoration(
          color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(4), topLeft: Radius.circular(4))),
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(width: 1.0, color: HexColor("#D8D8D8")))),
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            //* ROUTE
            child: _setupLocationDisplay()),
      ),
    );
  }

  // if locations
  Widget _setupLocationDisplay() {
    if (PrevozRoute.isRouteStringLong(
        widget.route, MediaQuery.of(context).size.width)) {
      return _twoRowsForRouteTitle();
    }
    return _singleRowForRouteTitle();
  }

  /// business as usual for locations that dont have long strings
  Widget _singleRowForRouteTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            RideSearchRequest request =
                RideSearchRequest(route: widget.route, date: DateTime.now());

            widget.onHistoryCardTap(request);
          },
          child: Container(
            child: Row(
              children: <Widget>[
                Text(
                  widget.route.locationFrom,
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: "InterBlack",
                      color: HexColor("#4D4D4D")),
                ),
                SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 16,
                  color: HexColor("#4D4D4D"),
                ),
                SizedBox(
                  width: 4,
                ),
                Text(
                  widget.route.locationTo,
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: "InterBlack",
                      color: HexColor("#4D4D4D")),
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
            onTap: () {
              PrevozRoute swappedRoute =
                  PrevozRoute.getSwapedRoute(widget.route);
              analytics.logEvent(name: "quick_search_reverse");
              if (_swapedRouteAlreadyExistsInTheHistoryList(swappedRoute)) {
                return;
              }
              setState(() {
                PrevozRoute beforeRoute = widget.route;
                String from = beforeRoute.locationFrom;
                String countryFrom = beforeRoute.countryFrom;
                widget.route.locationFrom = widget.route.locationTo;
                widget.route.countryFrom = widget.route.countryTo;
                widget.route.locationTo = from;
                widget.route.countryTo = countryFrom;

                BlocProvider.of<RouteStatisticsBloc>(context)
                    .fetchSingleRoute(widget.route, widget.searchHistoryIndex);
              });
            },
            child: Icon(
              Icons.swap_horiz,
              size: 25,
              color: HexColor("#4D4D4D"),
            )),
      ],
    );
  }

  /// _singleRowForRouteTitle is not enough for locations with longer string
  /// therefor we have a seperate UI that includes two rows for locations with longer strings
  Widget _twoRowsForRouteTitle() {
    return Container(
        height: 40,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              child: GestureDetector(
                onTap: () {
                  RideSearchRequest request = RideSearchRequest(
                      route: widget.route, date: DateTime.now());

                  widget.onHistoryCardTap(request);
                },
                child: Text(
                  widget.route.locationFrom,
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: "InterBlack",
                      color: HexColor("#4D4D4D")),
                ),
              ),
            ),
            Positioned(
              right: 0,
              top: 15,
              child: GestureDetector(
                  onTap: () {
                    PrevozRoute swappedRoute =
                        PrevozRoute.getSwapedRoute(widget.route);

                    analytics.logEvent(name: "quick_search_reverse");
                    if (_swapedRouteAlreadyExistsInTheHistoryList(
                        swappedRoute)) {
                      return;
                    }

                    setState(() {
                      PrevozRoute beforeRoute = widget.route;
                      String from = beforeRoute.locationFrom;
                      String countryFrom = beforeRoute.countryFrom;

                      widget.route.locationFrom = widget.route.locationTo;
                      widget.route.countryFrom = widget.route.countryTo;
                      widget.route.locationTo = from;
                      widget.route.countryTo = countryFrom;
                      BlocProvider.of<RouteStatisticsBloc>(context)
                          .fetchSingleRoute(
                              widget.route, widget.searchHistoryIndex);
                    });
                  },
                  child: Icon(
                    Icons.swap_horiz,
                    size: 25,
                    color: HexColor("#4D4D4D"),
                  )),
            ),
            Positioned(
              bottom: 0,
              right: 40,
              child: GestureDetector(
                onTap: () {
                  RideSearchRequest request = RideSearchRequest(
                      route: widget.route, date: DateTime.now());

                  widget.onHistoryCardTap(request);
                },
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.subdirectory_arrow_right,
                      size: 22,
                      color: HexColor("#4D4D4D"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      widget.route.locationTo,
                      style: TextStyle(
                          fontSize: 18,
                          fontFamily: "InterBlack",
                          color: HexColor("#4D4D4D")),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }

  RouteStatistics getStatsForCurrentCard(
      List<RouteStatistics> statsForAllRoutes) {
    RouteStatistics statsForCurrentCard;
    statsForAllRoutes.forEach((RouteStatistics singleRouteStats) {
      if (singleRouteStats.route.locationFrom == widget.route.locationFrom &&
          singleRouteStats.route.locationTo == widget.route.locationTo) {
        statsForCurrentCard = singleRouteStats;
      }
    });

    return statsForCurrentCard;
  }

  bool _swapedRouteAlreadyExistsInTheHistoryList(PrevozRoute swappedRoute) {
    List<PrevozRoute> historyRoutes = widget.statsForAllRoutesLoaded.routes;

    for (int i = 0; i < historyRoutes.length; i++) {
      if (PrevozRoute.areRoutesTheSame(historyRoutes[i], swappedRoute)) {
        //* when users trys to swap a route that already has its counter part in the results list
        //* we scroll to that position

        return true;
      }
    }

    return false;
  }

  Widget _buildStatisticRow(int searchHistoryIndex) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          //*TODAY
          Expanded(
              flex: 1,
              child: _buildClickableDay(DateTime.now(),
                  isToday: true, day: "today")),
          //* TOMMORROW
          Expanded(
              //? this key is used for an integration test

              flex: 1,
              child: _buildClickableDay(DateTime.now().add(Duration(days: 1)),
                  day: "tomorrow")),
          //* DAY AFTER TOMMORROW
          Expanded(
              flex: 1,
              child: _buildClickableDay(DateTime.now().add(Duration(days: 2)),
                  day: "day_after_tomorrow")),
        ],
      ),
    );
  }

  _searchRidesForTappedDay(DateTime dateTime) {
    RideSearchRequest request =
        RideSearchRequest(route: widget.route, date: dateTime);
    widget.onHistoryCardTap(request);
  }

  Widget _buildHorizontalDivider() {
    return SizedBox(
      width: 1,
      child: Container(
        width: 1,
        height: 65,
        color: Colors.grey.shade200,
      ),
    );
  }

  /// 2019-08-22:
  /// key for accessing number of rides in json
  String getDateString(DateTime dateTime) {
    String year = dateTime.year.toString();
    String month = dateTime.month.toString();
    String day = dateTime.day.toString();
    if (dateTime.month < 10) {
      month = "0" + month;
    }
    if (dateTime.day < 10) {
      day = "0" + day;
    }
    return "$year-$month-$day";
  }

  String getDayName(DateTime dateTime, bool isToday) {
    return DateTimeUtils.getDayName(dateTime, context);
  }
}
