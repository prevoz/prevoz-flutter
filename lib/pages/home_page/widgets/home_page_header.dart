import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class HomePageHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.only(top: 20),
        child: Column(
          children: <Widget>[
            Image(
              fit: BoxFit.fitHeight,
              height: 45,
              image: AssetImage(
                'assets/icon/app_icon_foreground.png',
              ),
            ),
            Text(
              "Kam greš danes?",
              style: TextStyle(
                  fontSize: 29,
                  fontFamily: "InterBlack",
                  color: HexColor("333333")),
            ),
          ],
        ),
      ),
    );
  }
}
