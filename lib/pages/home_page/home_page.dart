import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_event.dart';
import 'package:prevoz_org/pages/home_page/widgets/search_history_card_loading_stats.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_bloc.dart';
import 'package:prevoz_org/utils/app_config.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/blocs/home_page/bloc.dart';
import 'package:prevoz_org/widgets/cards/error_card.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/search_results/search_results_page.dart';
import 'package:prevoz_org/pages/home_page/widgets/home_page_header.dart';
import 'package:prevoz_org/pages/home_page/widgets/search_form_card.dart';
import 'package:prevoz_org/data/blocs/home_page/route_statistics/bloc.dart';
import 'package:prevoz_org/pages/home_page/widgets/search_history_card.dart';
import 'package:prevoz_org/data/blocs/home_page/route_statistics/routestatistics_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Logger logger = Logger(printer: CustomLogPrinter("HomePage"));
  HomePageBloc _homePageBloc;
  static const int NUM_OF_STATIC_LIST_ELEMENTS =
      2; //? SearchFormCard, BottomPadding

  ScrollController _historyResultsScrollController;
  final _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _historyResultsScrollController = ScrollController();
    _homePageBloc = BlocProvider.of<HomePageBloc>(context);
    _checkIfFirstTimeInApp();
    Future.delayed(Duration.zero, () {
      _checkExistingAuthData(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          PrevozBackground(),
          _buildHomePageScaffold(),
        ],
      ),
    );
  }

  Widget _buildHomePageScaffold() {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: PrevozAppBar(
        context: context,
        isBackIcon: false,
        isNavIconShown: false,
        pageTitle: AppLocalizations.of(context).whereAreYouHeadingToday,
      ),
      body: _buildHomePageBlocBuilder(),
    );
  }

  /// Placed on top of the stack as foreground.
  /// 1) loads search history from DB
  /// 2) fetches route stats in  _buildSearchHistoryLoaded
  Widget _buildHomePageBlocBuilder() {
    return Padding(
      padding: EdgeInsets.only(
          top: 25, left: 20, right: 20), // PrevozStyles.DESERT_SKY_PADDING,
      child: BlocBuilder(
        cubit: _homePageBloc,
        builder: (BuildContext context, HomePageState state) {
          if (_isSearchHistoryLoading(state)) {
            return _buildSearchHistoryLoading(state);
          }

          if (state is LoadingRouteStats) {
            return _buildLoadingStats(state);
          }

          if (state is StatsForAllRoutesLoaded) {
            if (state.routes != null &&
                state.routeStatsResult != null &&
                state.routes.length > 0 &&
                state.routeStatsResult.routeStatistics.length > 0) {
              return _buildStatsLoadedScenario(state);
            }
          }

          if (state is ErrorLoadingSearchHistory ||
              state is ErrorLoadingRoutesStats) {
            return _buildErrorLoadingSearchHistory();
          }

          //todo handle dis
          return Container();
        },
      ),
    );
  }

  /// Displays search history ListView with loading progress for route stats
  Widget _buildFetchingRouteStats(LoadingRouteStats loadingRoutesStats) {
    return ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return _listViewSeperator(index);
        },
        itemCount: NUM_OF_STATIC_LIST_ELEMENTS +
            loadingRoutesStats.routesSearchHistory.length,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return SearchFormCard(scaffoldState: _scaffoldState);
          }

          if (_isLastItemInList(
              index, loadingRoutesStats.routesSearchHistory)) {
            return _bottomPadding();
          }

          return SearchHistoryCardLoadingStats(
            route: loadingRoutesStats.routesSearchHistory[index - 1],
          );
        });
  }

  Widget _bottomPadding() {
    return Padding(
      padding: EdgeInsets.only(bottom: 25),
    );
  }

  bool _isLastItemInList(int index, List list) {
    if (index == (NUM_OF_STATIC_LIST_ELEMENTS + list.length - 1)) {
      return true;
    }

    return false;
  }

  Widget _listViewSeperator(int index) {
    double seperatorHeight = 35;
    if (index == 0) {
      seperatorHeight = 40;
    }
    return SizedBox(
      height: seperatorHeight,
    );
  }

  /// builds a list that contains a static header, a static input form and the dynamic search history list
  /// it get built after both search history have been fetched from DB and ride statistics from API
  Widget _buildStatsLoadedScenario(StatsForAllRoutesLoaded statsForAllRoutes) {
    int itemCount =
        NUM_OF_STATIC_LIST_ELEMENTS + statsForAllRoutes.routes.length;
    return ListView.separated(
        key: Key("_homePageListView"),
        controller: _historyResultsScrollController,
        separatorBuilder: (BuildContext context, int index) {
          return _listViewSeperator(index);
        },
        itemCount: itemCount, // 9,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return SearchFormCard(scaffoldState: _scaffoldState);
          }

          if (_isLastItemInList(index, statsForAllRoutes.routes)) {
            return _bottomPadding();
          }

          PrevozRoute route = statsForAllRoutes.routes[index - 1];
          int searchHistoryIndex = statsForAllRoutes.routes.length - 1;

          return SearchHistoryCard(
              onHistoryCardTap: (RideSearchRequest request) {
                _goToSearchResults(request);
              },
              statsForAllRoutesLoaded: statsForAllRoutes,
              route: route,
              searchHistoryIndex: searchHistoryIndex);
        });
  }

  bool _isSearchHistoryLoading(HomePageState state) {
    return (state is InitialHomePageState ||
        state is LoadingSearchHistory ||
        state is InitialisingSearchHistory);
  }

  Widget _buildSearchHistoryLoading(HomePageState state) {
//* both states return a loading screen, InitialHomePageState aditionaly fetches history
    if (state is InitialHomePageState) {
      _homePageBloc.loadSearchHistory();
    }

    return _buildLoadingSearchHistoryFromDb();
  }

  Widget _buildLoadingStats(HomePageState state) {
    //* Search history loaded, but we still have to get route stats
    LoadingRouteStats loadingRoutesStats = state;

    //! this can occur on some rare circumstances of which im not yet aware of
    if (loadingRoutesStats.routesSearchHistory == null ||
        loadingRoutesStats.routesSearchHistory.length == 0) {
      return _buildLoadingSearchHistoryFromDb();
    }

    return _buildFetchingRouteStats(loadingRoutesStats);
  }

  Widget _buildLoadingSearchHistoryFromDb() {
    return Column(
      children: <Widget>[
        SearchFormCard(
          scaffoldState: _scaffoldState,
        ),
        SizedBox(
          height: 25,
        ),
        SizedBox(
          height: 60,
          width: 60,
          child: CircularProgressIndicator(),
        )
      ],
    );
  }

  Widget _buildErrorLoadingSearchHistory() {
    ScrollController errorScrollController = ScrollController();

    Future.delayed(Duration(milliseconds: 30), () {
      errorScrollController.animateTo(
          errorScrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 600),
          curve: Curves.easeIn);
    });

    return ListView.separated(
        controller: errorScrollController,
        separatorBuilder: (BuildContext context, int index) {
          return _listViewSeperator(index);
        },
        itemCount: 2,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return HomePageHeader();
          }

          if (index == 1) {
            return Column(
              children: <Widget>[
                SearchFormCard(
                  scaffoldState: _scaffoldState,
                ),
                SizedBox(
                  height: 25,
                ),
                ErrorCard(
                    errorText:
                        AppLocalizations.of(context).errorLoadingSearchHistory,
                    onErrorButtonPress: () {
                      _homePageBloc.loadSearchHistory();
                    }),
                //* for footer padding when scrolling all the way down
                SizedBox(
                  height: 80,
                ),
              ],
            );
          }

          return Container();
        });
  }

  Widget noItemsFound(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.not_listed_location,
            size: 30,
          ),
          SizedBox(
            width: 15,
          ),
          Text(AppLocalizations.of(context).noLocationsFound),
        ],
      ),
    );
  }

  /// When running the app for the first time
  /// We have to listen and wait for DatabaseHelper to create the DB
  /// and to prepopulate it with data
  void handleFirstTimeInApp() {
    AppConfig config = AppConfig.of(context);

    var db = config.appDatabase;

    //* Migration of notifications
    if (Platform.isAndroid) {
      db.migratedAndroidNotificationsBroadcastController.stream
          .listen((onData) {}, onDone: () {
        BlocProvider.of<NotificationSubscriptionBloc>(context)
            .androidNotificationMigration();
      }, onError: (err) {});
    }
  }

  //* currently we are fetching route stats in two places
  //* 1) when search history gets loaded
  //* 2) when routes are swaped in S.H.C. route row
  //? where should I start the refactor??
  //? when search history is loaded and the way its loaded??
  //? or maybe I could just check if all the existing routes in search history are already loaded in the existing
  //? state and therefor wouldnt have to fetch?
  //?
  fetchRouteStats(List<PrevozRoute> routes) {
    //todo: refactor all dispatches to Bloc methods
    BlocProvider.of<RouteStatisticsBloc>(context)
        .add(FetchStatsForAllRoutes(routes: routes));
  }

  _checkIfFirstTimeInApp() {
    SharedPreferences.getInstance().then((prefRef) {
      if (!prefRef.containsKey(MyConstants.notTheFirstTimeInApp)) {
        handleFirstTimeInApp();
        prefRef.setBool(MyConstants.notTheFirstTimeInApp, true);
      } else {
        // check if DB exists
        BlocProvider.of<NotificationSubscriptionBloc>(context)
            .loadNotificationsOnStartup();
      }
    });
  }

  void _goToSearchResults(RideSearchRequest request) {
    _homePageBloc.addToSearchHistory(request);
    // push to search page
    Navigator.of(context, rootNavigator: true)
        .push(new CupertinoPageRoute<bool>(
            builder: (BuildContext context) {
              return SearchResultsPage(request);
            },
            settings: RouteSettings(name: "SearchResultsPage")))
        .then((onValue) {
      //* when navigating back from SearchResults, reload search history

      BlocProvider.of<HomePageBloc>(context).loadSearchHistory();
    });
  }

  void _checkExistingAuthData(BuildContext context) {
    BlocProvider.of<AuthenticationBloc>(context).add(AppStarted());
  }
}
