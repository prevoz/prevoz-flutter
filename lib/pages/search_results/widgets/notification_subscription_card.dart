import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/helpers/event_bus/database_initialisation.dart';
import 'package:prevoz_org/data/helpers/push_notifications/notification_helper.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_bloc.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_state.dart';
import 'package:prevoz_org/utils/app_config.dart';

class NotificationSubscriptionCard extends StatefulWidget {
  final PrevozRoute route;
  final DateTime date;
  const NotificationSubscriptionCard({Key key, this.route, this.date})
      : super(key: key);

  @override
  _NotificationSubscriptionCardState createState() =>
      _NotificationSubscriptionCardState();
}

class _NotificationSubscriptionCardState
    extends State<NotificationSubscriptionCard> {
  NotificationSubscriptionBloc _notificationSubscriptionBloc;

  FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {
    super.initState();

    _notificationSubscriptionBloc =
        BlocProvider.of<NotificationSubscriptionBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    //* dont show subscription UI if fcmToken not present or if pushEnabled config is false
    AppConfig config = AppConfig.of(context);
    if (NotificationHelper.fcmToken == null ||
        config.pushEnabled == false ||
        DbInitState.isInitInProgress()) {
      return Container();
    }

    return BlocBuilder(
      cubit: _notificationSubscriptionBloc,
      builder: (BuildContext context, NotificationSubscriptionsState state) {
        //* 1. Check if route in subscriptions list
        bool isRouteAlreadyInSubscriptionList = _notificationSubscriptionBloc
            .isRouteInSubsList(route: widget.route, date: widget.date);

        if (state is SuccessSavingSubscription) {
          if (state.deleted) {
            print("_______ state is SuccessSavingSubscription");
            isRouteAlreadyInSubscriptionList = false;
          }
        }

        if (state is SavingSubscription) {
          return InkWell(
            onTap: () {},
            child: Container(
              padding: EdgeInsets.only(left: 18, right: 18, bottom: 11),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Center(
                        child: SizedBox(
                            height: 21,
                            width: 21,
                            child: CircularProgressIndicator()),
                      ),
                      Expanded(
                        child: Text(
                          isRouteAlreadyInSubscriptionList
                              ? AppLocalizations.of(context).stopSubscribing
                              : AppLocalizations.of(context).startSubscribing,
                          maxLines: 3,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }

        return InkWell(
          onTap: () {
            _notificationSubscriptionBloc.saveSubscription(
                route: widget.route, date: widget.date);

            analytics.logEvent(
                name: "notification_subscription",
                parameters: {"date": widget.date.toIso8601String()});
          },
          child: Container(
            padding: EdgeInsets.only(left: 18, right: 18, bottom: 11),
            child: Row(
              children: <Widget>[
                Icon(isRouteAlreadyInSubscriptionList
                    ? Icons.notifications_off
                    : Icons.notifications),
                Expanded(
                  child: Text(
                    isRouteAlreadyInSubscriptionList
                        ? AppLocalizations.of(context).stopSubscribing
                        : AppLocalizations.of(context).startSubscribing,
                    maxLines: 3,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
