import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/home_page/bloc.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';

class ScaffoldWithAppBarAndProgressIndicator extends StatelessWidget {
  final String appBarTitle;

  const ScaffoldWithAppBarAndProgressIndicator({Key key, this.appBarTitle})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: PrevozAppBar(
          isBackIcon: true,
          isNavIconShown: true,
          context: context,
          pageTitle: appBarTitle,
          onBackPress: () {
            BlocProvider.of<HomePageBloc>(context)
                .add(HomePageLoadSearchHistoryEvent());
          },
        ),
        body: Padding(
            padding: const EdgeInsets.only(top: 30, bottom: 20),
            child: Center(
              child: CircularProgressIndicator(),
            )));
  }
}
