import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/rides_for_route.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/search_results/widgets/search_results_ride_row.dart';
import 'package:prevoz_org/pages/search_results/widgets/zero_rides_row.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class RidesForRouteCard extends StatelessWidget {
  final RidesForRoute ridesForRoute;

  RidesForRouteCard({@required this.ridesForRoute})
      : assert(ridesForRoute != null);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: _buildHeaderAndRows(context),
      ),
    );
  }

  List<Widget> _buildHeaderAndRows(BuildContext context) {
    List<Widget> columnItems = [];

    columnItems.add(_header(context));

    if (ridesForRoute.rides.length == 0) {
      columnItems
          .add(ZeroRidesRow(AppLocalizations.of(context).noRidesForRoute));
    } else {
      columnItems.addAll(_buildRidesForRoute(context));
    }

    return columnItems;
  }

  List<Widget> _buildRidesForRoute(BuildContext context) {
    List<Widget> rows = [];

    for (var i = 0; i < ridesForRoute.rides.length; i++) {
      RideModel ride = ridesForRoute.rides[i];

      bool isLastInList = i == (ridesForRoute.rides.length - 1);

      SearchResultRideRow rowCard = SearchResultRideRow(
        ride: ride,
        isLastInList: isLastInList,
      );

      rows.add(rowCard);
    }

    return rows;
  }

  Container _header(BuildContext context) {
    if (PrevozRoute.isRouteStringLong(
        ridesForRoute.route, MediaQuery.of(context).size.width)) {
      return _twoRowsForRouteTitle();
    }
    return _singleRowForRouteTitle();
  }

  Widget _twoRowsForRouteTitle() {
    //* uses nested containers to be able to use both border and borderRadius properties
    return Container(
      decoration: _outerContainerDecoration(),
      child: Container(
        decoration: _innerContainerDecoration(),
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: 11, horizontal: 18),
            child: Container(
                height: 40,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 0,
                      left: 0,
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          ridesForRoute.route.locationFrom,
                          style: _routeTitleStyle(),
                        ),
                      ),
                    ),
                    Positioned(
                        right: 0, top: 15, child: _buildNumOfLocations()),
                    Positioned(
                      bottom: 0,
                      right: 70,
                      child: GestureDetector(
                        onTap: () {},
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.subdirectory_arrow_right,
                              size: 22,
                              color: HexColor("#4D4D4D"),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              ridesForRoute.route.locationTo,
                              style: _routeTitleStyle(),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ))),
      ),
    );
  }

  TextStyle _routeTitleStyle() {
    return TextStyle(
      fontSize: 18,
      fontFamily: "InterBlack",
    );
  }

  Widget _singleRowForRouteTitle() {
    //* uses nested containers to be able to use both border and borderRadius properties
    return Container(
      decoration: _outerContainerDecoration(),
      child: Container(
        decoration: _innerContainerDecoration(),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 11, horizontal: 18),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Text(
                      ridesForRoute.route.locationFrom,
                      style: _routeTitleStyle(),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.grey.shade700,
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Text(
                      ridesForRoute.route.locationTo,
                      style: _routeTitleStyle(),
                    ),
                  ],
                ),
              ),
              _buildNumOfLocations()
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNumOfLocations() {
    return Container(
      child: Row(
        children: <Widget>[
          Text(
            ridesForRoute.rides.length.toString(),
            style: TextStyle(
                fontFamily: "InterRegular",
                fontSize: 16,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 5,
          ),
          Image.asset(
            'assets/icon/camel_gray_small.png',
            height: 15,
          )
        ],
      ),
    );
  }

  BoxDecoration _outerContainerDecoration() {
    return BoxDecoration(
      color: HexColor("FAF7F2"),
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(4), topRight: Radius.circular(4)),
    );
  }

  BoxDecoration _innerContainerDecoration() {
    return BoxDecoration(
        border:
            Border(bottom: BorderSide(width: 1.0, color: HexColor("DFDFDF"))));
  }
}
