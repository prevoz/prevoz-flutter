import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_state.dart';
import 'package:prevoz_org/data/blocs/ride_details/ridedetails_bloc.dart';
import 'package:prevoz_org/data/blocs/ride_details/ridedetails_event.dart';
import 'package:prevoz_org/data/blocs/ride_details/ridedetails_state.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/my_rides/blocs/my_rides_list_bloc.dart/my_rides_list_bloc.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class RideDetailsAppBar extends StatelessWidget implements PreferredSizeWidget {
  final RideModel ride;
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  RideDetailsAppBar({this.ride});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.transparent,
        height: 80,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
          child: Stack(
            children: <Widget>[
              _buildNav(context),
              _buildFav(context),
              _buildTitle(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFavIcon(BuildContext context) {
    if (BlocProvider.of<AuthenticationBloc>(context).state
        is AuthStateAuthenticated) {
      return _buildFavWithBlocBuilder(context);
    }

    return Container();
  }

  Widget _buildNav(BuildContext context) {
    return Positioned(
      left: 0,
      child: _navIcon(context),
    );
  }

  Widget _buildFav(BuildContext context) {
    return Positioned(
      right: 0,
      child: _buildFavIcon(context),
    );
  }

  Widget _buildTitle(BuildContext context) {
    String title = DateTimeUtils.dateTimeToDisplayableDate(
      ride.dateIso8601,
      context,
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 40,
          alignment: AlignmentDirectional.center,
          child: Text(
            title,
            style: TextStyle(
                fontSize: title.length > 19 ? 20 : 29,
                fontFamily: "InterBlack",
                color: HexColor("333333")),
          ),
        ),
      ],
    );
  }

  Widget _buildFavWithBlocBuilder(BuildContext buildContext) {
    return BlocBuilder(
      cubit: BlocProvider.of<RideDetailsBloc>(buildContext),
      builder: (BuildContext context, RideDetailsState state) {
        //LOADING SPINNER
        if (state is SavingBookmark) {
          return CircularProgressIndicator();
        }

        if (state is BookmarkSaved) {
          _handleBookmarkEvent(buildContext);
        }

        if (state is ErrorSavingBookmark) {
          _displayErrorSnackbar(buildContext);
        }

        return SizedBox(
          height: 40,
          width: 40,
          child: Container(
            child: InkWell(
              child: ride.bookmark == MyConstants.BOOKMARK
                  ? Icon(Icons.favorite,
                      size: 35, color: HexColor(PrevozStyles.BOOKMARK_ACTIVE))
                  : Icon(Icons.favorite_border,
                      size: 35,
                      color: HexColor(PrevozStyles.BOOKMARK_UNACTIVE)),
              onTap: () {
                analytics.logEvent(name: "ride_details_bookmark_clicked");
                BlocProvider.of<RideDetailsBloc>(context).add(
                    RideDetailsSetBookmark(
                        _getNewBookmarkState(ride), ride.id.toString()));
              },
            ),
          ),
        );
      },
    );
  }

  String _getNewBookmarkState(RideModel ride) {
    return ride.bookmark == MyConstants.BOOKMARK ? null : MyConstants.BOOKMARK;
  }

  void _handleBookmarkEvent(BuildContext context) {
    if (ride.bookmark != null &&
        (ride.bookmark == MyConstants.BOOKMARK ||
            ride.bookmark == MyConstants.GOING_WITH)) {
      // UNFAVOURITED
      ride.bookmark = null;
      BlocProvider.of<MyRidesListBloc>(context).removeBookmarksResult(ride.id);
    } else {
      // FAVOURITED
      ride.bookmark = MyConstants.BOOKMARK;
      BlocProvider.of<MyRidesListBloc>(context).addToBookmarksResult(ride);
    }
  }

  void _displayErrorSnackbar(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final snackBar = SnackBar(
        content: Text(AppLocalizations.of(context).errorSavingBookmark),
        duration: Duration(milliseconds: 1500),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {},
        ),
      );

      Scaffold.of(context).showSnackBar(snackBar);
    });

    BlocProvider.of<RideDetailsBloc>(context).resetToInitialState();
  }

  Widget _navIcon(BuildContext context) {
    return InkResponse(
      onTap: () {
        Navigator.of(context).pop();
      },
      child: new Container(
        width: 40,
        height: 40,
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: new Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(120);
}
