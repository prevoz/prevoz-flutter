import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/helpers/auth/auth_helper.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/details/ride_details_page.dart';
import 'package:prevoz_org/pages/my_rides/blocs/my_rides_list_bloc.dart/bloc.dart';
import 'package:prevoz_org/pages/search_results/bloc/riderowbookmark_bloc.dart';
import 'package:prevoz_org/pages/search_results/bloc/riderowboomark_state.dart';
import 'package:prevoz_org/pages/search_results/widgets/fav_icon_placeholder.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/utils/other_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

///Single row for a ride in the RidesForRouteCard, that's displayed in SearchResultsPage
class SearchResultRideRow extends StatefulWidget {
  final RideModel ride;
  final bool isLastInList;

  const SearchResultRideRow({Key key, this.ride, this.isLastInList})
      : super(key: key);

  @override
  _SearchResultRideRowState createState() => _SearchResultRideRowState();
}

class _SearchResultRideRowState extends State<SearchResultRideRow> {
  FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: _addBottomBorder(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          _rowData(context),
          _favIcon(context),
        ],
      ),
    );
  }

  Expanded _rowData(BuildContext context) {
    return Expanded(
      flex: 1,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              CupertinoPageRoute(
                  settings: RouteSettings(name: "RideDetailsPage"),
                  builder: (context) => RideDetailsPage(
                        ride: widget.ride,
                      )));
        },
        child: Container(
          padding: EdgeInsets.only(left: 18, top: 11, bottom: 11),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.ride.time.replaceAll("ob ", ""),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  //* Miha | Lj Bezigrad - Mb Center
                  Row(
                    children: <Widget>[
                      widget.ride.businessName != null
                          ? Padding(
                              padding: EdgeInsets.only(right: 8),
                              child: Icon(Icons.airport_shuttle),
                            )
                          : Container(),
                      (widget.ride.author != null &&
                              widget.ride.author.length > 0)
                          ? CardSubText(OtherUtils.capitaliseFirstLetter(
                              widget.ride.author))
                          : Text(
                              "-",
                              style: TextStyle(color: HexColor("808080")),
                            ),
                      widget.ride.fromSublocation != null ||
                              widget.ride.toSublocation != null
                          ? CardSubText("  |  ")
                          : Container(),
                      widget.ride.fromSublocation != null
                          ? CardSubText(widget.ride.fromSublocation)
                          : Container(),
                      widget.ride.fromSublocation != null &&
                              widget.ride.toSublocation != null
                          ? CardSubText(" -> ")
                          : Container(),
                      widget.ride.toSublocation != null
                          ? CardSubText(widget.ride.toSublocation)
                          : Container()
                    ],
                  )

                  //Row()
                ],
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    widget.ride.price != null
                        ? Text(
                            OtherUtils.removeZerosFromPrice(widget.ride.price) +
                                "€",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          )
                        : Text("-"),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  InkWell _favIcon(BuildContext context) {
    return InkWell(
      onTap: () {
        if (BlocProvider.of<AuthenticationBloc>(context).isUserLoggedIn()) {
          BlocProvider.of<RideRowBookmarkBloc>(context)
              .setBookmark(widget.ride);

          analytics.logEvent(name: "search_results_bookmark_click_logged_in");
        } else {
          analytics.logEvent(name: "src_results_bookmark_click_not_logged_in");
          AuthHelper.showSigninDialog(context);
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 22),
        child: Center(
          child: BlocBuilder(
              cubit: BlocProvider.of<RideRowBookmarkBloc>(context),
              builder:
                  (BuildContext context, RideRowBookmarkState bookmarkState) {
                //LOADING SPINNER
                if (bookmarkState is RideRowSavingState) {
                  if (bookmarkState.ride.id == widget.ride.id) {
                    return SizedBox(
                        height: 21,
                        width: 24,
                        child: CircularProgressIndicator());
                  }
                }

                if (bookmarkState is RideRowSavedState) {
                  //* Reset results in MyRidesListBloc so that these
                  if (widget.ride.bookmark != null &&
                      (widget.ride.bookmark == MyConstants.BOOKMARK ||
                          widget.ride.bookmark == MyConstants.GOING_WITH)) {
                    // UNFAVOURITED
                    if (bookmarkState.ride.id == widget.ride.id) {
                      widget.ride.bookmark = null;
                      //*Update MyRidesListBloc so we dont have to refetch when navigating to MyRides
                      BlocProvider.of<MyRidesListBloc>(context)
                          .removeBookmarksResult(widget.ride.id);
                    }
                  } else {
                    // FAVOURITED
                    if (bookmarkState.ride.id == widget.ride.id) {
                      widget.ride.bookmark = MyConstants.BOOKMARK;
                      //*Update MyRidesListBloc so we dont have to refetch when navigating to MyRides
                      BlocProvider.of<MyRidesListBloc>(context)
                          .addToBookmarksResult(widget.ride);
                    }
                  }

                  if (bookmarkState.ride.id == widget.ride.id) {
                    BlocProvider.of<RideRowBookmarkBloc>(context)
                        .resetToInitialState();
                  }
                }

                if (bookmarkState is RideRowErrorSavingState) {
                  //* show snackbar on error
                  if (bookmarkState.ride.id == widget.ride.id) {
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      final snackBar = SnackBar(
                        content: Text(
                            AppLocalizations.of(context).errorSavingBookmark),
                        duration: Duration(milliseconds: 1500),
                        action: SnackBarAction(
                          label: "OK",
                          onPressed: () {},
                        ),
                      );

                      Scaffold.of(context).showSnackBar(snackBar);
                    });
                    BlocProvider.of<RideRowBookmarkBloc>(context)
                        .resetToInitialState();
                  }
                }

                Icon iconPlaceholder;

                if (widget.ride.bookmark == null) {
                  iconPlaceholder = Icon(
                    Icons.favorite_border,
                    color: HexColor(PrevozStyles.BOOKMARK_UNACTIVE),
                  );
                } else {
                  iconPlaceholder = Icon(Icons.favorite,
                      color: HexColor(PrevozStyles.BOOKMARK_ACTIVE));
                }
                return FavIconPlaceholder(
                  icon: iconPlaceholder,
                );
              }),
        ),
      ),
    );
  }

  ///adds bottom border to all RidesForRoute items except the last one
  BoxDecoration _addBottomBorder() {
    if (widget.isLastInList) {
      return BoxDecoration();
    }

    return BoxDecoration(
        border:
            Border(bottom: BorderSide(width: 1, color: HexColor("D8D8D8"))));
  }
}

class CardSubText extends StatelessWidget {
  final String cardText;

  const CardSubText(this.cardText);

  @override
  Widget build(BuildContext context) {
    return Text(cardText,
        style: TextStyle(
            fontSize: 16,
            color: HexColor("808080"),
            fontFamily: "InterRegular",
            fontWeight: FontWeight.normal));
  }
}
