import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/widgets/cards/error_card.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';

class ErrorLoadingScreen extends StatelessWidget {
  final RideSearchRequest searchRequest;
  final OnErrorButtonPress onErrorButtonPress;
  final String errorText;
  const ErrorLoadingScreen(
      {Key key, this.searchRequest, this.onErrorButtonPress, this.errorText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          PrevozBackground(),
          Scaffold(
              backgroundColor: Colors.transparent,
              appBar: PrevozAppBar(
                context: context,
                isBackIcon: true,
                isNavIconShown: true,
                pageTitle: DateTimeUtils.dateTimeToDayNameWithDate(
                  searchRequest.date,
                  context,
                ),
              ),
              body: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: ListView.builder(
                    itemCount: 2,
                    itemBuilder: (BuildContext context, int index) {
                      if (index == 0) {
                        return SizedBox(
                          height: 50,
                        );
                      }
                      if (index == 1) {
                        return ErrorCard(
                          errorText: errorText,
                          onErrorButtonPress: () {
                            onErrorButtonPress();
                          },
                        );
                      }

                      return Container();
                    }),
              ))

          //_scrollableForeground(context, rides.fetchedRides,
          //widget.searchRequest.route),
        ],
      ),
    );
  }
}
