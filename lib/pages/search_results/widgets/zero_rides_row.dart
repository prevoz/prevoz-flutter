import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_event.dart';
import 'package:prevoz_org/data/models/app_tab.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

class ZeroRidesRow extends StatelessWidget {
  final String displayText;
  final bool showButton;
  ZeroRidesRow(this.displayText, {this.showButton = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18, vertical: 11),
      child: Column(
        children: <Widget>[
          Text(
            displayText,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            textAlign: TextAlign.center,
          ),
          this.showButton
              ? Container(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 12,
                      ),
                      _buildGoToAddRide(context),
                    ],
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  Widget _buildGoToAddRide(BuildContext context) {
    return SizedBox(
      width: 220,
      child: RaisedButton(
        splashColor: Theme.of(context).accentColor,
        color: HexColor(PrevozStyles.PREVOZ_ORANGE_HEX),
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(4.0)),
        child: Container(
          height: 40,
          width: 280,
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(AppLocalizations.of(context).addRide,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 17)),
                SizedBox(
                  width: 4,
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.white, size: 15),
              ],
            ),
          ),
        ),
        onPressed: () {
          BlocProvider.of<BottomNavigationBloc>(context)
              .add(UpdateTabBottomNavEvent(AppTab.addride));
        },
      ),
    );
  }
}
