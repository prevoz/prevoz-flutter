import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/home_page/bloc.dart';
import 'package:prevoz_org/data/blocs/rides/rides_bloc.dart';
import 'package:prevoz_org/data/blocs/rides/rides_events.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/data/models/rides_for_route.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/pages/search_results/widgets/notification_subscription_card.dart';
import 'package:prevoz_org/pages/search_results/widgets/rides_for_route_card.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';

class SearchResultsListScreen extends StatelessWidget {
  final RideSearchRequest searchRequest;
  final List<RideModel> fetchedRides;

  SearchResultsListScreen({Key key, this.searchRequest, this.fetchedRides})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (BuildContext context) {
      return RefreshIndicator(
        key: new GlobalKey<RefreshIndicatorState>(),
        onRefresh: () {
          return _fetchRides(context, this.searchRequest);
        },
        child: Scaffold(
          body: Stack(
            fit: StackFit.passthrough,
            alignment: Alignment.topCenter,
            children: <Widget>[
              PrevozBackground(),
              _buildResultListWithScaffold(context, this.fetchedRides,
                  searchRequest.route, this.searchRequest)
            ],
          ),
        ),
      );
    });
  }

  Widget _buildResultListWithScaffold(
      BuildContext context,
      List<RideModel> allFetchedRides,
      PrevozRoute route,
      RideSearchRequest searchRequest) {
    List<RidesForRoute> ridesGroupedByRoutes =
        getRidesGroupedByRoutes(allFetchedRides);

    //* check if RidesForRoute contain the exact searched route.
    //* if not, add .insert(0, new RidesForRoute(route, [] empty list))
    //* then further down, do some special magic if first route has empty rides list
    if (!_isSearchedRoutePresentInResults(ridesGroupedByRoutes)) {
      RidesForRoute searchedRouteWithEmptyRides =
          RidesForRoute(route: searchRequest.route, rides: []);
      ridesGroupedByRoutes.insert(0, searchedRouteWithEmptyRides);
    }

    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: _buildPrevozAppbar(context),
        body: Padding(
          padding: const EdgeInsets.only(top: 25),
          child: ListView.separated(
            itemCount: ridesGroupedByRoutes.length + 1, //* +1 is for the footer
            key: PageStorageKey("_ridesResultsList"),
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 25,
              );
            },
            itemBuilder: (BuildContext context, int index) {
              //* footer spacing
              if (index == ridesGroupedByRoutes.length) {
                return SizedBox(
                  height: 35,
                );
              }

              RidesForRoute currentRidesForRoute = ridesGroupedByRoutes[index];

              return _buildRidesForRoute(currentRidesForRoute, index);
            },
            //+1 is for the header
          ),
        ));

    //return Sample2();
  }

  Widget _buildRidesForRoute(RidesForRoute ridesForRoute, int index) {
    return Padding(
      padding: PrevozStyles.CARDS_OUTTER_PADDING,
      child: index == 0
          ? Column(
              children: <Widget>[
                NotificationSubscriptionCard(
                  date: this.searchRequest.date,
                  route: this.searchRequest.route,
                ),
                RidesForRouteCard(ridesForRoute: ridesForRoute),
              ],
            )
          : RidesForRouteCard(ridesForRoute: ridesForRoute),
    );
  }

  Widget _buildPrevozAppbar(BuildContext context) {
    return PrevozAppBar(
      isBackIcon: true,
      isNavIconShown: true,
      context: context,
      pageTitle: DateTimeUtils.dateTimeToDisplayableDate(
        searchRequest.date,
        context,
      ),
      onBackPress: () {
        BlocProvider.of<HomePageBloc>(context)
            .add(HomePageLoadSearchHistoryEvent());
      },
    );
  }

  List<RidesForRoute> getRidesGroupedByRoutes(List<RideModel> fetchedRides) {
    if (fetchedRides == null) {
      return [];
    }

    List<String> allRoutesStrings = [];
    List<PrevozRoute> allRoutes = [];
    List<RidesForRoute> ridesForRoutes = [];

    //* get all different routes from fetch result
    fetchedRides.forEach((RideModel ride) {
      PrevozRoute currentRoute = PrevozRoute();
      currentRoute.locationTo = ride.to;
      currentRoute.locationFrom = ride.from;

      if (!allRoutesStrings.contains(
          currentRoute.locationFrom + "-" + currentRoute.locationTo)) {
        allRoutesStrings
            .add(currentRoute.locationFrom + "-" + currentRoute.locationTo);

        allRoutes.add(currentRoute);
      }
    });

    //* initialize RidesForRoute models
    for (var i = 0; i < allRoutes.length; i++) {
      ridesForRoutes.add(
          RidesForRoute(route: allRoutes[i], rides: new List<RideModel>()));
    }

    //* add rides to their belonging RidesForRoute model
    for (var i = 0; i < fetchedRides.length; i++) {
      RideModel currentRide = fetchedRides[i];
      for (var j = 0; j < ridesForRoutes.length; j++) {
        if (currentRide.route.locationFrom ==
                ridesForRoutes[j].route.locationFrom &&
            currentRide.route.locationTo ==
                ridesForRoutes[j].route.locationTo) {
          ridesForRoutes[j].rides.add(currentRide);
        }
      }
    }

    //* if results contain rides for exact searched route
    //* make sure that that route is displayed on the top
    for (var i = 0; i < ridesForRoutes.length; i++) {
      if (PrevozRoute.areRoutesTheSame(
          searchRequest.route, ridesForRoutes[i].route)) {
        if (i != 0) {
          // searched route isnt the first one in list, replace it
          RidesForRoute firstRidesForRoute = ridesForRoutes[0];
          ridesForRoutes[0] = ridesForRoutes[i];
          ridesForRoutes[i] = firstRidesForRoute;

          break;
        }
      }
    }
    //* if searched route is sublocation, make sure that the second in results is the locations main route
    if (searchRequest.route.containsSublocation()) {
      PrevozRoute mainRoute = new PrevozRoute(
          locationFrom: searchRequest.route.locationFrom,
          locationTo: searchRequest.route.locationTo,
          countryFrom: searchRequest.route.countryFrom,
          countryTo: searchRequest.route.countryTo);

      if (mainRoute.locationFrom.contains("(")) {
        mainRoute.locationFrom = mainRoute.locationFrom.split(" (")[0];
      }
      if (mainRoute.locationTo.contains("(")) {
        mainRoute.locationTo = mainRoute.locationTo.split(" (")[0];
      }

      for (var i = 0; i < ridesForRoutes.length; i++) {
        if (PrevozRoute.areRoutesTheSame(ridesForRoutes[i].route, mainRoute)) {
          if (i != 0) {
            RidesForRoute secondRidesForRoute = ridesForRoutes[0];
            ridesForRoutes[0] = ridesForRoutes[i];
            ridesForRoutes[i] = secondRidesForRoute;
          }
        }
      }
    }
    //* sort by time
    for (var i = 0; i < ridesForRoutes.length; i++) {
      ridesForRoutes[i].rides.sort((firstRide, secondRide) =>
          DateTimeUtils.timeStringToSeconds(firstRide.time)
              .compareTo(DateTimeUtils.timeStringToSeconds(secondRide.time)));
    }
    return ridesForRoutes;
  }

  bool _isSearchedRoutePresentInResults(
      List<RidesForRoute> ridesGroupedByRoutes) {
    for (var i = 0; i < ridesGroupedByRoutes.length; i++) {
      if (PrevozRoute.areRoutesTheSame(
          searchRequest.route, ridesGroupedByRoutes[i].route)) {
        return true;
      }
    }

    return false;
  }

  Future<Null> _fetchRides(
      BuildContext context, RideSearchRequest searchRequest) {
    try {
      BlocProvider.of<RideBloc>(context)
          .add(FetchRidesEvent(searchRequest: searchRequest));
    } catch (e) {
      handleError(
          error: e, message: "SearchResultsPage, initState -> fetching rides");
    }
    return null;
  }
}
