import 'package:flutter/material.dart';

/// a simple wrapper that helps integration tests use .findByType() and test fav icon clicks
class FavIconPlaceholder extends StatelessWidget {
  final Icon icon;
  const FavIconPlaceholder({Key key, this.icon}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: icon,
    );
  }
}
