import 'package:equatable/equatable.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';

abstract class NotificationSubscriptionsState extends Equatable {
  const NotificationSubscriptionsState();
}

class InitialNotificationState extends NotificationSubscriptionsState {
  const InitialNotificationState();
  @override
  String toString() => "InitialNotificationState";
  @override
  List<Object> get props => [];
}

class LoadingSubscriptions extends NotificationSubscriptionsState {
  const LoadingSubscriptions();
  @override
  String toString() => "LoadingSubscriptions";
  @override
  List<Object> get props => [];
}

class DeletingOldSubscriptions extends NotificationSubscriptionsState {
  const DeletingOldSubscriptions();
  @override
  String toString() => "DeletingOldSubscriptions";
  @override
  List<Object> get props => [];
}

class ErrorLoadingSubscriptions extends NotificationSubscriptionsState {
  const ErrorLoadingSubscriptions();
  @override
  String toString() => "ErrorLoadingSubscriptions";
  @override
  List<Object> get props => [];
}

class SubscriptionsLoaded extends NotificationSubscriptionsState {
  final List<NotificationSubscription> subscriptions;
  const SubscriptionsLoaded(this.subscriptions);
  @override
  String toString() => "SubscriptionsLoaded";
  @override
  List<Object> get props => [subscriptions];
}

class SavingSubscription extends NotificationSubscriptionsState {
  final NotificationSubscription notificationSubscription;
  const SavingSubscription(this.notificationSubscription);
  @override
  String toString() => "SavingSubscription";
  @override
  List<Object> get props => [notificationSubscription];
}

//ErrorSavingNewSubscription
class ErrorSavingSubscription extends NotificationSubscriptionsState {
  const ErrorSavingSubscription();
  @override
  String toString() => "ErrorSavingSubscription";
  @override
  List<Object> get props => [];
}

class SuccessSavingSubscription extends NotificationSubscriptionsState {
  final bool deleted;
  const SuccessSavingSubscription({this.deleted});
  @override
  String toString() => "SuccessSavingSubscription";
  @override
  List<Object> get props => [deleted];
}

class DeletingSingleSubscription extends NotificationSubscriptionsState {
  final NotificationSubscription subscription;
  const DeletingSingleSubscription(this.subscription);
  @override
  String toString() => "DeletingSingleSubscription";
  @override
  List<Object> get props => [subscription];
}

class SingleSubSuccessfullyDeleted extends NotificationSubscriptionsState {
  const SingleSubSuccessfullyDeleted();
  @override
  String toString() => "SingleSubSuccessfullyDeleted";
  @override
  List<Object> get props => [];
}

class ErrorDeletingSingleSub extends NotificationSubscriptionsState {
  const ErrorDeletingSingleSub();
  @override
  String toString() => "ErrorDeletingSingleSub";
  @override
  List<Object> get props => [];
}

class OldSubscriptionsDeleted extends NotificationSubscriptionsState {
  const OldSubscriptionsDeleted();
  @override
  String toString() => "OldSubscriptionsDeleted";
  @override
  List<Object> get props => [];
}
