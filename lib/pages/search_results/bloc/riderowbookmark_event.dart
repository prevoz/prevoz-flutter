import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class RideRowBookmarkEvent extends Equatable {
  const RideRowBookmarkEvent();
}

class RideRowSetBookmark extends RideRowBookmarkEvent {
  final String state;
  final String id;
  final RideModel ride;
  const RideRowSetBookmark(this.state, this.id, this.ride);
  @override
  String toString() => 'RideRowSetBookmark';
  @override
  List<Object> get props => [state, id, ride];
}

class RideRowResetState extends RideRowBookmarkEvent {
  const RideRowResetState();
  @override
  String toString() => 'RideRowResetState';
  @override
  List<Object> get props => [];
}
