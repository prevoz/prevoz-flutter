import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/pages/search_results/bloc/riderowbookmark_event.dart';
import 'package:prevoz_org/pages/search_results/bloc/riderowboomark_state.dart';
import 'package:prevoz_org/utils/my_constants.dart';

class RideRowBookmarkBloc
    extends Bloc<RideRowBookmarkEvent, RideRowBookmarkState> {
  final RidesRepository ridesRepo;
  RideRowBookmarkBloc({this.ridesRepo}) : super(InitialRideRowState());

  @override
  Stream<RideRowBookmarkState> mapEventToState(
    RideRowBookmarkEvent event,
  ) async* {
    if (event is RideRowSetBookmark) {
      try {
        yield RideRowSavingState(event.ride);

        Response response =
            await ridesRepo.setRideBookmark(event.id, event.state);

        if (response == null) {
          throw Exception("null response");
        }
        if (response != null &&
            response.data != null &&
            response.data["error"] != null) {
          throw Exception(response.data["error"]);
        }

        yield RideRowSavedState(event.state, event.ride);
      } catch (e) {
        print("___ error saving bookmark: " + e.toString());
        yield RideRowErrorSavingState(event.ride);
      }
    }

    if (event is RideRowResetState) {
      yield InitialRideRowState();
    }
  }

  void resetToInitialState() {
    add(RideRowResetState());
  }

  void setBookmark(RideModel ride) {
    String newBookmarkState = _getNewBookmarkState(ride);
    String rideId = ride.id.toString();
    add(RideRowSetBookmark(newBookmarkState, rideId, ride));
  }

  String _getNewBookmarkState(RideModel ride) {
    return ride.bookmark == MyConstants.BOOKMARK ? null : MyConstants.BOOKMARK;
  }
}
