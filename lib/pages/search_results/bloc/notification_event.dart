import 'package:equatable/equatable.dart';

import 'package:prevoz_org/data/models/registered_notification.dart';

abstract class NotificationEvent extends Equatable {
  const NotificationEvent();
}

class LoadNotificationSubsEvent extends NotificationEvent {
  const LoadNotificationSubsEvent();
  @override
  String toString() => 'LoadNotificationSubsEvent';
  @override
  List<Object> get props => [];
}

/// used when migrating old android db
class AndroidNotificationMigration extends NotificationEvent {
  const AndroidNotificationMigration();
  @override
  String toString() => 'AndroidNotificationMigration';
  @override
  List<Object> get props => [];
}

class DeleteOldNotificationSubscriptionsEvent extends NotificationEvent {
  const DeleteOldNotificationSubscriptionsEvent();
  @override
  String toString() => 'DeleteOldNotificationSubscriptionsEvent';
  @override
  List<Object> get props => [];
}

class LoadNotificationsOnStartupEvent extends NotificationEvent {
  const LoadNotificationsOnStartupEvent();
  @override
  String toString() => 'LoadNotificationsOnStartupEvent';
  @override
  List<Object> get props => [];
}

class ConfigureFirebaseMessagingEvent extends NotificationEvent {
  const ConfigureFirebaseMessagingEvent();
  @override
  String toString() => 'ConfigureFirebaseMessagingEvent';
  @override
  List<Object> get props => [];
}

class DeleteNotificationSubscriptionEvent extends NotificationEvent {
  final NotificationSubscription notification;
  const DeleteNotificationSubscriptionEvent(this.notification);
  @override
  String toString() => 'DeleteNotificationSubscriptionEvent';
  @override
  List<Object> get props => [notification];
}

class SaveSubscriptionEvent extends NotificationEvent {
  final NotificationSubscription notification;
  const SaveSubscriptionEvent(this.notification);
  @override
  String toString() => 'SaveSubscriptionEvent';
  @override
  List<Object> get props => [notification];
}
