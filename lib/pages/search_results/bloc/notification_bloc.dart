import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/data/repositories/notification_subscription_repository.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_event.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_state.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';

class NotificationSubscriptionBloc
    extends Bloc<NotificationEvent, NotificationSubscriptionsState> {
  final NotificationSubscriptionRepository repository;

  Logger logger = Logger(printer: CustomLogPrinter("NotificationsBloc"));

  NotificationSubscriptionBloc({this.repository}) : super(InitialNotificationState());

  List<NotificationSubscription> allSubscriptions = [];

  @override
  Stream<NotificationSubscriptionsState> mapEventToState(
    NotificationEvent event,
  ) async* {
    if (event is LoadNotificationSubsEvent) {
      yield* _mapGetAllToState();
    }

    if (event is AndroidNotificationMigration) {
      yield* _onAndroidNotificationMigration();
    }

    if (event is DeleteOldNotificationSubscriptionsEvent) {
      yield* _deleteOldToState();
    }

    if (event is DeleteNotificationSubscriptionEvent) {
      yield* _mapDeleteSingleToState(event.notification);
    }

    if (event is LoadNotificationsOnStartupEvent) {
      yield* _onAppStartup();
    }

    if (event is SaveSubscriptionEvent) {
      yield* _mapSaveToState(event.notification);
    }

    //* doesnt change state, just fires the chain for configuring FCM
    if (event is ConfigureFirebaseMessagingEvent) {
      _configureNotifications();
    }
  }

  Stream<NotificationSubscriptionsState>
      _onAndroidNotificationMigration() async* {
    try {
      yield LoadingSubscriptions();
      List<NotificationSubscription> subscriptions =
          await repository.getAllNotificationSubscriptions();

      repository.registerMigratedSubscriptions(subscriptions);

      allSubscriptions = subscriptions;

      yield SubscriptionsLoaded(subscriptions);
    } catch (e) {}
  }

  Stream<NotificationSubscriptionsState> _onAppStartup() async* {
    try {
      logger.i("OnAppStartup");
      //* 1) check if notification table exists
      //? notification table might not exist at app starup when onUpgrade() from DB V1 to DB V2
      bool notificationTableExist =
          await repository.checkIfNotificationsTableExists();

      if (notificationTableExist) {
        //* 2) delete old subscriptions
        yield DeletingOldSubscriptions();
        await repository.deleteOldNotificationSubscriptions();
        yield OldSubscriptionsDeleted();

        yield LoadingSubscriptions();
        List<NotificationSubscription> subscriptions =
            await repository.getAllNotificationSubscriptions();
        allSubscriptions = subscriptions;
        logger.i("Loaded subs, " + subscriptions.length.toString());

        yield SubscriptionsLoaded(subscriptions);
      }
    } catch (e) {
      handleError(error: e, message: "notifications bloc, _onAppStartup()");
      repository.checkDBSanity();
    }
  }

  Stream<NotificationSubscriptionsState> _mapSaveToState(
      NotificationSubscription notificationSubscription) async* {
    try {
      yield SavingSubscription(notificationSubscription);

      //* 1) handle in repo (DB&API calls)
      int result = await repository.saveSubscription(notificationSubscription);
      //* 2) save to global state
      bool deleted = false;
      if (result == -1) {
        deleted = true;
        removeSubscriptionFromAllSubs(notificationSubscription);
      } else {
        allSubscriptions.add(notificationSubscription);
      }
      yield SuccessSavingSubscription(deleted: deleted);
      //* 3) reset to inital state
      yield InitialNotificationState();
    } catch (e) {
      repository.checkDBSanity();
      yield ErrorSavingSubscription();
    }
  }

  //_mapDeleteSingleToState
  Stream<NotificationSubscriptionsState> _mapDeleteSingleToState(
      NotificationSubscription notificationSubscription) async* {
    try {
      yield DeletingSingleSubscription(notificationSubscription);

      await repository.deleteNotificationSubscription(notificationSubscription);

      yield SingleSubSuccessfullyDeleted();
    } catch (e) {
      repository.checkDBSanity();
      yield ErrorDeletingSingleSub();
    }
  }

  Stream<NotificationSubscriptionsState> _mapGetAllToState() async* {
    try {
      yield LoadingSubscriptions();
      List<NotificationSubscription> subscriptions =
          await repository.getAllNotificationSubscriptions();
      allSubscriptions = subscriptions;
      print("____...._____..._ALL SUBS: " + allSubscriptions.length.toString());
      yield SubscriptionsLoaded(subscriptions);
    } catch (e) {
      repository.checkDBSanity();
      yield ErrorLoadingSubscriptions();
    }
  }

  Stream<NotificationSubscriptionsState> _deleteOldToState() async* {
    try {
      yield DeletingOldSubscriptions();

      await repository.deleteOldNotificationSubscriptions();

      yield OldSubscriptionsDeleted();
    } catch (e) {
      handleError(error: e, message: "_deleteOldToState");
    }
  }

  _configureNotifications() {
    repository.configureFirebaseMessaging();
  }

  removeSubscriptionFromAllSubs(
      NotificationSubscription notificationSubscription) {
    print("_______removeSubscriptionFromAllSubs");
    for (var i = 0; i < allSubscriptions.length; i++) {
      if (PrevozRoute.areRoutesTheSame(
          allSubscriptions[i].route, notificationSubscription.route)) {
        if (DateTimeUtils.areDatesTheSame(
            allSubscriptions[i].rideDate, notificationSubscription.rideDate)) {
          print("______ FOUND IT AND REMOVING IT");
          allSubscriptions.removeAt(i);
        }
      }
    }
  }

  bool isRouteInSubsList({PrevozRoute route, DateTime date}) {
    print("_______ all subs in bloc: " + allSubscriptions.length.toString());

    if (allSubscriptions == null) {
      return false;
    }
    for (var i = 0; i < allSubscriptions.length; i++) {
      if (PrevozRoute.areRoutesTheSame(allSubscriptions[i].route, route)) {
        if (allSubscriptions[i].rideDate.year == date.year &&
            allSubscriptions[i].rideDate.month == date.month &&
            allSubscriptions[i].rideDate.day == date.day) {
          return true;
        }
      }
    }

    return false;
  }

  void configureFirebaseMessaging() {
    add(ConfigureFirebaseMessagingEvent());
  }

  void saveSubscription(
      {@required PrevozRoute route, @required DateTime date}) {
    add(SaveSubscriptionEvent(
        NotificationSubscription(route: route, rideDate: date)));
  }

  void loadAllSubscriptions() {
    add(LoadNotificationSubsEvent());
  }

  /// deletes old subscriptions, loads all active ones, saves them to state
  void loadNotificationsOnStartup() {
    add(LoadNotificationsOnStartupEvent());
  }

  //RegisterMigratedSubscriptions
  void androidNotificationMigration() {
    add(AndroidNotificationMigration());
  }
}
