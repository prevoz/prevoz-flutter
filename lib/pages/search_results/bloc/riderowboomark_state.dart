import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class RideRowBookmarkState extends Equatable {
  const RideRowBookmarkState();
}

class InitialRideRowState extends RideRowBookmarkState {
  const InitialRideRowState();
  @override
  String toString() => "InitialRideRowState";
  @override
  List<Object> get props => [];
}

class RideRowSavingState extends RideRowBookmarkState {
  final RideModel ride;
  const RideRowSavingState(this.ride);
  @override
  String toString() => "RideRowSavingState";
  @override
  List<Object> get props => [ride];
}

class RideRowSavedState extends RideRowBookmarkState {
  final String state;
  final RideModel ride;
  RideRowSavedState(this.state, this.ride);
  @override
  String toString() => "RideRowSavedState";
  @override
  List<Object> get props => [state, ride];
}

class RideRowErrorSavingState extends RideRowBookmarkState {
  final RideModel ride;
  RideRowErrorSavingState(this.ride);
  @override
  String toString() => "RideRowErrorSavingState";
  @override
  List<Object> get props => [ride];
}
