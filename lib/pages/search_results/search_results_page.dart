import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/data/blocs/rides/rides_bloc.dart';
import 'package:prevoz_org/data/blocs/rides/rides_state.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/search_results/widgets/error_loading_screen.dart';
import 'package:prevoz_org/pages/search_results/widgets/search_results_list_screen.dart';
import 'package:prevoz_org/pages/search_results/widgets/scaffold_with_app_bar_and_progress_indicator.dart';

class SearchResultsPage extends StatefulWidget {
  final RideSearchRequest searchRequest;
  SearchResultsPage(this.searchRequest);

  @override
  _SearchResultsPageState createState() => _SearchResultsPageState();
}

class _SearchResultsPageState extends State<SearchResultsPage> {

  @override
  void initState() {
    super.initState();
    _fetchRides();
    //* used to handle logins from within this page
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<AuthenticationBloc>(context),
      builder: (BuildContext context, AuthenticationState state) {
        if (state is AuthStateLoading) {
          return _buildLoadingAuthentication();
        }
        return _buildRidesBlocBuilder();
      },
    );
  }

  Widget _buildRidesBlocBuilder() {
    return BlocBuilder(
        cubit: BlocProvider.of<RideBloc>(context),
        builder: (BuildContext context, RidesState state) {
          if (state is RidesLoading) {
            return _buildProgressIndicator();
          }
          if (state is RidesLoaded) {
            return _buildRidesLoaded(state);
          }

          if (state is ErrorLoadingRides) {
            return _buildErrorLoadingScreen();
          }
          return Container();
        });
  }

  Widget _buildRidesLoaded(RidesLoaded state) {
    return SearchResultsListScreen(
      searchRequest: widget.searchRequest,
      fetchedRides: state.fetchedRides,
    );
  }

  Widget _buildErrorLoadingScreen() {
    return ErrorLoadingScreen(
      searchRequest: widget.searchRequest,
      errorText: AppLocalizations.of(context).errorLoadingRidesTryAgain,
      onErrorButtonPress: () {
        _fetchRides();
      },
    );
  }

  Widget _buildLoadingAuthentication() {
    return Scaffold(
      body: Stack(
        fit: StackFit.passthrough,
        alignment: Alignment.topCenter,
        children: <Widget>[
          PrevozBackground(),
          Center(
            child: CircularProgressIndicator(),
          )
        ],
      ),
    );
  }

  Widget _buildProgressIndicator() {
    return Scaffold(
      body: Stack(
        fit: StackFit.passthrough,
        alignment: Alignment.topCenter,
        children: <Widget>[
          PrevozBackground(),
          ScaffoldWithAppBarAndProgressIndicator(
            appBarTitle: DateTimeUtils.dateTimeToDisplayableDate(
              widget.searchRequest.date,
              context,
            ),
          ),
        ],
      ),
    );
  }

  _fetchRides() {
    BlocProvider.of<RideBloc>(context).fetchRides(widget.searchRequest);
  }
}
