import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/home_page/home_page_bloc.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/profile/widgets/notify_card.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_bloc.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_state.dart';
import 'package:prevoz_org/pages/search_results/search_results_page.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/other_utils.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  NotificationSubscriptionBloc _notificationSubscriptionBloc;

  final FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {
    super.initState();
    _notificationSubscriptionBloc =
        BlocProvider.of<NotificationSubscriptionBloc>(context);
  }

  final _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<NotificationSubscriptionBloc>(context),
      builder: (BuildContext context, NotificationSubscriptionsState state) {
        return Scaffold(
          key: _scaffoldState,
          body: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              PrevozBackground(),
              Scaffold(
                  backgroundColor: Colors.transparent,
                  appBar: PrevozAppBar(
                      isBackIcon: false,
                      isNavIconShown: false,
                      context: context,
                      pageTitle: AppLocalizations.of(context).notifications),
                  body: _buildBody())
            ],
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    if (_notificationSubscriptionBloc.allSubscriptions == null ||
        _notificationSubscriptionBloc.allSubscriptions.length == 0) {
      return Padding(
        padding: EdgeInsets.only(top: 25),
        child: NotifyCard(
          cardText: AppLocalizations.of(context).noSubscriptions,
          imgSrc: "assets/images/my_rides_not_logged_in.png",
          showButton: false,
          pageName: "NotificationsPage",
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 30),
      child: ListView.builder(
        itemCount: _notificationSubscriptionBloc.allSubscriptions.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildItem(context, index,
              _notificationSubscriptionBloc.allSubscriptions[index]);
        },
      ),
    );
  }

  Widget _buildItem(
      BuildContext context, int index, NotificationSubscription subscription) {
    if (index == _notificationSubscriptionBloc.allSubscriptions.length + 1) {
      return SizedBox(
        height: 40,
      );
    }

    return Container(
      decoration: OtherUtils.getCardsBoxDecoration(context,
          index == _notificationSubscriptionBloc.allSubscriptions.length - 1),
      child: Material(
        color: Colors.transparent,
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 19, vertical: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    searchRides(notificationSubscription: subscription);
                  },
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(DateTimeUtils.dateWithDayName(
                            subscription.rideDate, context)),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          subscription.route.locationFrom +
                              " > " +
                              subscription.route.locationTo,
                          style: TextStyle(fontFamily: "InterRegular"),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                    key: Key("deleteNotificationBtn"),
                    onTap: () {
                      showRemoveSubDialog(subscription);
                    },
                    child: Container(child: Icon(Icons.delete)))
              ],
            )),
      ),
    );
  }

  searchRides(
      {@required NotificationSubscription notificationSubscription}) async {
    {
      //* check connectivity
      if (!await connectivityAvailable()) {
        return;
      }

      RideSearchRequest rideSearchRequest = RideSearchRequest(
          date: notificationSubscription.rideDate,
          route: notificationSubscription.route);

      //* add to SearchHistory
      BlocProvider.of<HomePageBloc>(context)
          .addToSearchHistory(rideSearchRequest);
      //* push to search page
      Navigator.of(context, rootNavigator: true)
          .push(new CupertinoPageRoute<bool>(
              builder: (BuildContext context) {
                return SearchResultsPage(rideSearchRequest);
              },
              settings: RouteSettings(name: "SearchResultsPage")));
    }
  }

  Future<bool> connectivityAvailable() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.none) {
      final snackBar = SnackBar(
        content: Text(AppLocalizations.of(context).notConnectedToInternet),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {},
        ),
      );
      _scaffoldState.currentState.showSnackBar(snackBar);
      return false;
    }
    return true;
  }

  void showRemoveSubDialog(NotificationSubscription subscription) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            AppLocalizations.of(context).removeSubscriptionDialogTitle,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: new Text(
              AppLocalizations.of(context).removeSubscriptionDialogContent),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: Text(
                AppLocalizations.of(context).cancel.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(
                AppLocalizations.of(context).remove.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () async {
                analytics.logEvent(
                    name: "notification_page_remove_subscription");
                _notificationSubscriptionBloc.saveSubscription(
                    route: subscription.route, date: subscription.rideDate);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
