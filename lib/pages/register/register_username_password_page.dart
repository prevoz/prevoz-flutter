
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/login/login_bloc.dart';
import 'package:prevoz_org/data/blocs/registration/registration_bloc.dart';
import 'package:prevoz_org/data/blocs/registration/registration_event.dart';
import 'package:prevoz_org/data/blocs/registration/registration_state.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/login/login_username_password_page.dart';
import 'package:prevoz_org/pages/profile_edit/widgets/text_field_with_title.dart';
import 'package:prevoz_org/utils/form_validators.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/google_sign_in_button.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';


class RegisterUsernamePasswordPage extends StatefulWidget {

  RegisterUsernamePasswordPage();

  @override
  _RegisterUsernamePasswordPageState createState() => _RegisterUsernamePasswordPageState();
}

class _RegisterUsernamePasswordPageState extends State<RegisterUsernamePasswordPage> {

  RegistrationBloc _registrationBloc;
  TextEditingController _usernameController;
  TextEditingController _emailController;
  TextEditingController _passwordController;
  Map<String, dynamic> _errorsFromApi;
  bool _showLoading = false;
  bool _showGoogleSignIn = false;
  bool _showEmailDelivered = false;
  String _generalError;

  @override
  void initState() {
    super.initState();
    _usernameController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _registrationBloc = BlocProvider.of<RegistrationBloc>(context);
    return WillPopScope(
      onWillPop: _handleBackNavigation,
      child: Stack(
        children: <Widget>[
          PrevozBackground(),
          BlocListener<AuthenticationBloc, AuthenticationState>(
            listener: (context, authState) {
              if (authState is AuthStateAuthenticated) {
                _showLoading = false;
                _errorsFromApi = null;
                _generalError = null;
                _showGoogleSignIn = false;
                _showEmailDelivered = false;
                Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
              }
            },
            child: BlocBuilder(
                cubit: _registrationBloc,
                builder: (BuildContext buildContext, RegistrationState registrationState) {
                  if (registrationState is RegistrationErrorGmail) {
                    _showLoading = false;
                    _showEmailDelivered = false;
                    _errorsFromApi = null;
                    _generalError = AppLocalizations.of(context).loginPageGmailError;
                    _showGoogleSignIn = true;
                  }
                  else if (registrationState is ErrorSubmittingRegistration) {
                    _showLoading = false;
                    _showEmailDelivered = false;
                    _showGoogleSignIn = registrationState.wasFromGoogle;
                    _handleSubmitError(registrationState);
                  } else if (registrationState is SubmittingRegistration) {
                    _showLoading = true;
                    _errorsFromApi = null;
                    _generalError = null;
                    _showGoogleSignIn = false;
                    _showEmailDelivered = false;
                  } else if (registrationState is SuccessfullySubmittedRegistration) {
                    _showLoading = false;
                    _showEmailDelivered = true;
                    _showGoogleSignIn = false;
                    _generalError = null;
                    _errorsFromApi = null;
                  }

                  return Scaffold(
                    backgroundColor: Colors.transparent,
                    appBar: _buildAppBar(),
                    body: _buildBody(),
                  );
                }
            ),
          )
        ],
      ),
    );
  }

  Widget _buildAppBar() {
    return PrevozAppBar(
      isBackIcon: true,
      isNavIconShown: true,
      context: context,
      pageTitle: AppLocalizations.of(context).registrationPageTitle,
      onBackPress: _handleToolbarBackClick
    );
  }

  Widget _buildBody() {
    return ListView(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 20, top: 25, right: 20),
          child: Card(
            color: HexColor(PrevozStyles.PREVOZ_CARD_GREY),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
              child: _showEmailDelivered ? _buildEmailSentInfo(context) : _buildNormalBody(context),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildEmailSentInfo(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 20,),
          Icon(
            Icons.check_circle,
            color:Colors.green,
            size: 70,
          ),
          SizedBox(height: 15,),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: Text(
              AppLocalizations.of(context).registrationPageCheckEmail,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black
              ),
            ),
          )
        ],
      )
    );
  }

  Widget _buildNormalBody(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 28,),
        TextFieldWithTitle(
          fieldKey: "username",
          title: AppLocalizations.of(context).registrationPageUsernameTitle,
          controller: _usernameController,
          error: FormValidators.usernameValidator(_usernameController.text, _errorsFromApi, context),
        ),
        SizedBox(height: 18,),
        TextFieldWithTitle(
          inputType: TextInputType.emailAddress,
          fieldKey: "email",
          title: AppLocalizations.of(context).registrationPageEmailTitle,
          controller: _emailController,
          error: FormValidators.emailValidator(_emailController.text, _errorsFromApi, context),
        ),
        SizedBox(height: 18,),
        TextFieldWithTitle(
          fieldKey: "password",
          title: AppLocalizations.of(context).registrationPagePasswordTitle,
          obscureText: true,
          controller: _passwordController,
          error: FormValidators.passwordValidator(_passwordController.text, _errorsFromApi, context),
        ),
        SizedBox(height: 28,),
        _buildSubmitButton(context),
        _buildGeneralError(context),
        _buildGoogleSignInButtonIfNeeded(context),
        SizedBox(height: 38,),
        _buildNavigationToLogin(context),
        SizedBox(height: 10,)
      ],
    );
  }

  Widget _buildSubmitButton(BuildContext context) {
    if (_showLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_showEmailDelivered) {
      return _buildEmailSentInfo(context);
    }

    return Container(
        width: double.infinity,
        child: RaisedButton(
            padding: EdgeInsets.only(
                top: 10, bottom: 10, left: 22, right: 22),
            child: Text(
              AppLocalizations.of(context).registrationPageRegisterButton,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            colorBrightness: Brightness.light,
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4)),
            splashColor: Theme.of(context).accentColor,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              _submitRegistration();
            })
    );
  }

  Widget _buildNavigationToLogin(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      child: Center(
        child: FlatButton(
          child: Text(
              AppLocalizations.of(context).registrationPageGoToLoginPage
          ),
          onPressed: () async {
            await _navigateToLoginPage() ;
          },
        ),
      ),
    );
  }

  Widget _buildGeneralError(BuildContext context) {
    if (_generalError == null) {
      return Container();
    }

    return Column(
      key: Key("_editProfilePageAddress_generalError"),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 20,),
        Icon(
          Icons.error,
          color: Colors.red,
          size: 40,
        ),
        SizedBox(height: 4,),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Text(
            _generalError,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
    );
  }

  Widget _buildGoogleSignInButtonIfNeeded(context) {
    if (_showGoogleSignIn) {
      return Column(
        children: <Widget>[
          SizedBox(height: 10,),
          GoogleSignInButton(onClick: _submitGoogleSignIn)
        ],
      );
    }

    return Container();
  }

  Future<bool> _handleBackNavigation() {
    if (_showEmailDelivered) {
      Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
      return Future.value(false);
    }

    return Future.value(true);
  }

  void _handleToolbarBackClick() {
    if (_showEmailDelivered) {
      Navigator.of(context).pop();
    }
  }

  void _handleSubmitError(ErrorSubmittingRegistration errorState) {
    Response response = errorState.response;
    if (_isValidApiError(response)) {
      _generalError = null;
      _errorsFromApi = errorState.response.data["error"];
    } else if (_isValidSpecificError(response)) {
      _errorsFromApi = null;
      _generalError = errorState.response.data["detail"];
    } else {
      _errorsFromApi = null;
      _generalError = AppLocalizations.of(context).authErrorLoggingIn;
    }
  }

  bool _isValidApiError(Response response) {
    if (response == null) return false;
    if (response.data == null) return false;
    if (response.data["error"] == null) return false;
    if (!(response.data["error"] is Map)) return false;

    for (var value in (response.data["error"] as Map).values) {
      if (!(value is List)) return false;
    }

    return true;
  }

  bool _isValidSpecificError(Response response) {
    if (response == null) return false;
    if (response.data == null) return false;
    if (response.data["detail"] == null) return false;
    if (!(response.data["detail"] is String)) return false;

    return true;
  }

  void _submitRegistration() {
    String username = _usernameController.text.toString();
    String email = _emailController.text.toString();
    String password = _passwordController.text.toString();
    FocusScope.of(context).unfocus();
    _registrationBloc.add(SubmitRegistration(
        username: username,
        email: email,
        password: password
    ));
  }

  void _submitGoogleSignIn() {
    _registrationBloc.add(SubmitGoogleLogin());
  }

  Future _navigateToLoginPage() async {
    await Navigator.of(context, rootNavigator: true)
        .pushReplacement(new CupertinoPageRoute<bool>(
      builder: (BuildContext context) {
        return BlocProvider<LoginBloc>(
          create: (context) => LoginBloc(
              RepositoryProvider.of<AuthRepository>(context),
              BlocProvider.of<AuthenticationBloc>(context)
          ),
          child: LoginUsernamePasswordPage(),
        );
      },
    ));
  }
}