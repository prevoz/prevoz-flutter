import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import './bloc.dart';

class MyRidesListBloc extends Bloc<MyRidesListEvent, MyRidesListState> {
  final RidesRepository _ridesRepository;

  MyRidesListBloc(this._ridesRepository) : super(InitialMyRidesListBlocState());

  RidesSearchResult bookmarksResult;
  RidesSearchResult addedRidesResult;

  void fetchMyRides() {
    if (!(state is MyRidesLoading)) {
      //* dont fetch if already fetching
      if (bookmarksResult == null || addedRidesResult == null) {
        add(FetchMyRidesFromApi());
      } else {
        add(GetMyRidesFromBloc());
      }
    }
  }

  void addToBookmarksResult(RideModel ride) {
    if (bookmarksResult != null) {
      bookmarksResult.addRide(ride);
    }
  }

  void removeBookmarksResult(int rideId) {
    if (bookmarksResult != null) {
      bookmarksResult.removeRide(rideId);
    }
  }

  void removeRide(RideModel ride) {
    if (ride != null && ride.id != null) {
      addedRidesResult.removeRide(ride.id);
    }
  }

  void addRide(RideModel ride) {
    if (ride != null) {
      if (addedRidesResult == null) {
        addedRidesResult = RidesSearchResult();
      }

      addedRidesResult.addRide(ride);
    }
  }

  void resetResults() {
    bookmarksResult = null;
    addedRidesResult = null;
  }

  @override
  Stream<MyRidesListState> mapEventToState(
    MyRidesListEvent event,
  ) async* {
    if (event is FetchMyRidesFromApi) {
      yield* _mapFetchToState();
    }

    if (event is GetMyRidesFromBloc) {
      yield MyRidesLoaded(
          bookmarks: bookmarksResult, addedRides: addedRidesResult);
    }

    if (event is ResetStateMyRides) {
      yield InitialMyRidesListBlocState();
    }
  }

  Stream<MyRidesListState> _mapFetchToState() async* {
    try {
      yield MyRidesLoading();

      bookmarksResult = await this._ridesRepository.fetchUsersBookmarkedRides();
      print("_____ bookmarks");
      addedRidesResult = await this._ridesRepository.fetchUsersAddedRides();
      print("_____ addedRides");

      yield MyRidesLoaded(
          bookmarks: bookmarksResult, addedRides: addedRidesResult);
    } catch (e) {
      yield MyRidesError();
    }
  }

  void resetState() {
    add(ResetStateMyRides());
  }
}
