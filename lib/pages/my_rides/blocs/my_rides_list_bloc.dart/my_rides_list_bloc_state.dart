import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class MyRidesListState extends Equatable {
  const MyRidesListState();
}

class InitialMyRidesListBlocState extends MyRidesListState {
  const InitialMyRidesListBlocState();
  @override
  String toString() => 'InitialMyRidesListBlocState';
  @override
  List<Object> get props => [];
}

// request for deleting in progress
class MyRidesBookmarksDeleting extends MyRidesListState {
  const MyRidesBookmarksDeleting();
  @override
  String toString() => 'MyRidesBookmarksDeleting';
  @override
  List<Object> get props => [];
}

// request for deleting in progress
class MyRidesLoading extends MyRidesListState {
  const MyRidesLoading();
  @override
  String toString() => 'MyRidesLoading';
  @override
  List<Object> get props => [];
}

// request for deleting in progress
class MyRidesLoaded extends MyRidesListState {
  final RidesSearchResult bookmarks;
  final RidesSearchResult addedRides;
  const MyRidesLoaded({@required this.bookmarks, @required this.addedRides});
  @override
  String toString() => 'MyRidesLoaded';
  @override
  List<Object> get props => [bookmarks, addedRides];
}

// request for deleting in progress
class MyRidesError extends MyRidesListState {
  const MyRidesError();
  @override
  String toString() => 'MyRidesError';
  @override
  List<Object> get props => [];
}
