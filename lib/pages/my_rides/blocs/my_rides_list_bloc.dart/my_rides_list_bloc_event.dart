import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MyRidesListEvent extends Equatable {
  const MyRidesListEvent();
}

class FetchMyRidesFromApi extends MyRidesListEvent {
  const FetchMyRidesFromApi();
  @override
  String toString() => 'FetchMyRidesFromApi';
  @override
  List<Object> get props => [];
}

class GetMyRidesFromBloc extends MyRidesListEvent {
  const GetMyRidesFromBloc();
  @override
  String toString() => 'GetMyRidesFromBloc';
  @override
  List<Object> get props => [];
}

class ResetStateMyRides extends MyRidesListEvent {
  const ResetStateMyRides();
  @override
  String toString() => 'ResetStateMyRides';
  @override
  List<Object> get props => [];
}

class MyRidesDeleteBookmarkEvent extends MyRidesListEvent {
  final String id;
  const MyRidesDeleteBookmarkEvent({@required this.id});
  @override
  String toString() => 'MyRidesDeleteBookmarkEvent';
  @override
  List<Object> get props => [id];
}
