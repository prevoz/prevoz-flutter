import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import './bloc.dart';

class BookmarksCardBloc extends Bloc<BookmarksCardEvent, BookmarksCardState> {
  final RidesRepository ridesRepo;

  BookmarksCardBloc({@required this.ridesRepo}) : super(InitialBookmarkCardState());

  @override
  Stream<BookmarksCardState> mapEventToState(
    BookmarksCardEvent event,
  ) async* {
    // TODO: Add Logic

    if (event is BookmarksCardResetState) {
      yield InitialBookmarkCardState();
    }

    if (event is BookmarkSetNewStateEvent) {
      try {
        yield BookmarksCardLoading();

        Response response =
            await ridesRepo.setRideBookmark(event.id, event.state);

        if (response == null) {
          throw Exception("null response");
        }
        if (response != null &&
            response.data != null &&
            response.data["error"] != null) {
          throw Exception(response.data["error"]);
        }

        print("___ bookmark response was fine");

        yield BookmarkSuccessfulySaved();
      } catch (e) {
        print("___ error saving bookmark: " + e.toString());
        yield BookmarkCardErrorDeleting();
      }
    }

    if (event is SaveBookMarkEvent) {
      try {
        yield BookmarksCardLoading();

        Response response =
            await ridesRepo.setRideBookmark(event.id, "bookmark");

        if (response == null) {
          throw Exception("null response");
        }
        if (response != null &&
            response.data != null &&
            response.data["error"] != null) {
          throw Exception(response.data["error"]);
        }

        print("___ bookmark response was fine");

        yield BookmarkSuccessfulySaved();
      } catch (e) {
        print("___ error saving bookmark: " + e.toString());
        yield BookmarkCardErrorDeleting();
      }
    }

    if (event is BookmarksCardDeleteEvent) {
      try {
        yield BookmarksCardLoading();

        Response response = await ridesRepo.setRideBookmark(event.id, null);

        if (response == null) {
          throw Exception("null response");
        }
        if (response != null &&
            response.data != null &&
            response.data["error"] != null) {
          throw Exception(response.data["error"]);
        }

        print("___ bookmark response was fine");

        yield BookmarkCardDeleted();
      } catch (e) {
        print("___ error saving bookmark: " + e.toString());
        yield BookmarkCardErrorDeleting();
      }
    }

    if (event is BookmarkCardResetState) {
      yield InitialBookmarkCardState();
    }
  }
}
