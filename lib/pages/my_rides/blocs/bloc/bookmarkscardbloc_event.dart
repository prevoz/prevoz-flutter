import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BookmarksCardEvent extends Equatable {
  const BookmarksCardEvent();
}

class BookmarksCardDeleteEvent extends BookmarksCardEvent {
  final String id;
  const BookmarksCardDeleteEvent(this.id);
  @override
  String toString() => 'BookmarksCardDeleteEvent';
  @override
  List<Object> get props => [id];
}

class BookmarksCardResetState extends BookmarksCardEvent {
  const BookmarksCardResetState();
  @override
  String toString() => 'BookmarksCardResetState';
  @override
  List<Object> get props => [];
}

class BookmarkSetNewStateEvent extends BookmarksCardEvent {
  final String state;
  final String id;
  const BookmarkSetNewStateEvent(this.state, this.id);
  @override
  String toString() => "BookmarkSetNewStateEvent";
  @override
  List<Object> get props => [state, id];
}

class SaveBookMarkEvent extends BookmarksCardEvent {
  final String id;
  const SaveBookMarkEvent(this.id);
  @override
  String toString() => 'SaveBookMarkEvent';
  @override
  List<Object> get props => [id];
}

class BookmarkCardResetState extends BookmarksCardEvent {
  const BookmarkCardResetState();
  @override
  String toString() => 'BookmarkCardResetState';
  @override
  List<Object> get props => [];
}
