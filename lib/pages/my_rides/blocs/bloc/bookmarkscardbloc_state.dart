import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BookmarksCardState extends Equatable {
  const BookmarksCardState();
}

class InitialBookmarkCardState extends BookmarksCardState {
  const InitialBookmarkCardState();
  @override
  String toString() => "BookmarksCardLoading";
  @override
  List<Object> get props => [];
}

class BookmarksCardLoading extends BookmarksCardState {
  const BookmarksCardLoading();
  @override
  String toString() => "BookmarksCardLoading";
  @override
  List<Object> get props => [];
}

class BookmarkCardDeleted extends BookmarksCardState {
  const BookmarkCardDeleted();
  @override
  String toString() => "BookmarkCardDeleted";
  @override
  List<Object> get props => [];
}

class BookmarkSuccessfulySaved extends BookmarksCardState {
  const BookmarkSuccessfulySaved();
  @override
  String toString() => "BookmarkSuccessfulySaved";
  @override
  List<Object> get props => [];
}

class BookmarkCardErrorSaving extends BookmarksCardState {
  const BookmarkCardErrorSaving();
  @override
  String toString() => "BookmarkCardErrorSaving";
  @override
  List<Object> get props => [];
}

class BookmarkCardErrorDeleting extends BookmarksCardState {
  const BookmarkCardErrorDeleting();
  @override
  String toString() => "BookmarkCardErrorDeleting";
  @override
  List<Object> get props => [];
}
