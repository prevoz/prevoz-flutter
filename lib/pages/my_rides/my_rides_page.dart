import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/pages/profile/widgets/notify_card.dart';
import 'package:prevoz_org/widgets/cards/auth_error_card.dart';
import 'package:prevoz_org/widgets/other/ad_view.dart';

import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/my_rides/widgets/my_rides_list.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_bloc.dart';
import 'package:prevoz_org/pages/my_rides/blocs/my_rides_list_bloc.dart/my_rides_list_bloc.dart';

class MyRidesPage extends StatefulWidget {
  final String accessToken;
  final RideFormChange rideFormChange;

  MyRidesPage(this.accessToken, {this.rideFormChange});

  @override
  _MyRidesPageState createState() => _MyRidesPageState();
}

class _MyRidesPageState extends State<MyRidesPage> {
  String username;
  String codeFromOAuthIntent;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<AuthenticationBloc>(context),
      builder: (BuildContext context, AuthenticationState state) {

        if (state is AuthStateAuthenticated) {
          username = state.email;
          //* Fetch my rides
          BlocProvider.of<MyRidesListBloc>(context).fetchMyRides();
          return MyRidesList(rideFormChange: widget.rideFormChange);
        }

        if (state is AuthStateLoading) {
          return Center(child: CircularProgressIndicator());
        }

        if (state is AuthStateError) {
          return _authErrorScreen();
        }

        return _loginScreen(context);
      },
    );
  }

  Widget _loginScreen(BuildContext context) {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: PrevozAppBar(
            context: context,
            pageTitle: AppLocalizations.of(context).myRides,
            isNavIconShown: false,
            isBackIcon: false,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 25),
              NotifyCard(
                  cardText: AppLocalizations.of(context).myRidesLoginText,
                  imgSrc: "assets/images/my_rides_not_logged_in.png",
                  pageName: "MyRidesPage"),
              SizedBox(height: 25,),
              AdView()
            ],
          )
        )
      ],
    );
  }

  Widget _authErrorScreen() {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: PrevozAppBar(
            context: context,
            pageTitle: AppLocalizations.of(context).myRides,
            isBackIcon: false,
            isNavIconShown: false),
          body: Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: AuthErrorCard()
            )
          ),
        )
      ],
    );
  }
}
