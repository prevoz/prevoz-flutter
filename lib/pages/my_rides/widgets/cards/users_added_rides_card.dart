import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/utils/other_utils.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bloc.dart';

class UsersAddedRidesCard extends StatelessWidget {
  final RideModel ride;
  final bool isLastInList;
  const UsersAddedRidesCard({@required this.ride, @required this.isLastInList});

  @override
  Widget build(BuildContext context) {
    String timeAndDate = ride.time.replaceAll("ob ", "") +
        ", " +
        DateTimeUtils.dateWithDayName(ride.dateIso8601, context).toLowerCase();

    String priceForDisplay;
    if (ride.price == null) {
      priceForDisplay = "-";
    } else {
      priceForDisplay = OtherUtils.removeZerosFromPrice(ride.price) +
          "€"; //? append € in util funtcion?
    }

    return Container(
      decoration: OtherUtils.getCardsBoxDecoration(context, isLastInList),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            BlocProvider.of<BottomNavigationBloc>(context)
                .add(GoToEditRideNavEvent(ride));
          },
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 19, vertical: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(timeAndDate),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          PrevozRoute.getRouteStringFromRide(ride),
                          style: TextStyle(fontFamily: "InterRegular"),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Text(priceForDisplay),
                        SizedBox(
                          width: 16,
                        ),
                        Image(
                          width: 20,
                          height: 20,
                          image: AssetImage('assets/icon/edit_btn.png'),
                        ),
                      ],
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
