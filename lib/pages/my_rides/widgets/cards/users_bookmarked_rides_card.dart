import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/other_utils.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/pages/details/ride_details_page.dart';
import 'package:prevoz_org/pages/my_rides/blocs/bloc/bookmarkscardbloc_bloc.dart';
import 'package:prevoz_org/pages/my_rides/blocs/bloc/bookmarkscardbloc_event.dart';
import 'package:prevoz_org/pages/my_rides/blocs/bloc/bookmarkscardbloc_state.dart';
import 'package:prevoz_org/data/blocs/users_bookmarked_rides/users_bookmarked_rides_state.dart';
import 'package:prevoz_org/widgets/other/centered_circular_progress_indicator.dart';

typedef DeleteBookmarkCallback = void Function(bool isSuccessfullyDeleted);

class UsersBookmarkedCard extends StatefulWidget {
  final RideModel ride;
  final bool isLastInList;
  final DeleteBookmarkCallback onDeleteBookmark;

  const UsersBookmarkedCard(
      {Key key, this.ride, this.isLastInList, this.onDeleteBookmark})
      : super(key: key);

  @override
  _UsersBookmarkedCardState createState() => _UsersBookmarkedCardState();
}

class _UsersBookmarkedCardState extends State<UsersBookmarkedCard> {
  BookmarksCardBloc _bookmarksCardBloc;

  @override
  void initState() {
    super.initState();
    _bookmarksCardBloc = BlocProvider.of<BookmarksCardBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    String timeAndDate = _getTimeDate();
    String priceForDisplay = _getPriceForDisplay();

    return Container(
        decoration:
            OtherUtils.getCardsBoxDecoration(context, widget.isLastInList),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _buildLeftClickableArea(timeAndDate),
            _buildRightClickableArea(priceForDisplay),
          ],
        ));
  }

  Widget _buildLeftClickableArea(String timeAndDate) {
    return Expanded(
      flex: 1,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            goToDetailsPage();
          },
          child: Container(
            padding: EdgeInsets.only(left: 19, top: 12, bottom: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(timeAndDate),
                SizedBox(
                  height: 5,
                ),
                Text(
                  widget.ride.from + " > " + widget.ride.to,
                  style: TextStyle(fontFamily: "InterRegular"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void goToDetailsPage() {
    Navigator.push(
            context,
            CupertinoPageRoute(
                settings: RouteSettings(name: "RideDetailsPage"),
                builder: (context) => RideDetailsPage(ride: widget.ride)))
        .then((onValue) {});
  }

  Widget _buildRightClickableArea(String priceForDisplay) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        child: Container(
          padding: EdgeInsets.only(right: 19, top: 12, bottom: 12),
          child: Row(
            children: <Widget>[
              _buildPrice(priceForDisplay),
              _buildSpacing(),
              _buildFavBloc(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPrice(String priceForDisplay) {
    return GestureDetector(
        onTap: () {
          goToDetailsPage();
        },
        child:
            Container(height: 30, child: Center(child: Text(priceForDisplay))));
  }

  Widget _buildSpacing() {
    return GestureDetector(
      onTap: () {
        goToDetailsPage();
      },
      child: SizedBox(
        width: 10,
        height: 30,
      ),
    );
  }

  Widget _buildFavBloc() {
    return Padding(
      padding: const EdgeInsets.only(left: 0),
      child: BlocBuilder(
        cubit: _bookmarksCardBloc,
        builder: (BuildContext context, BookmarksCardState state) {
          if (state is BookmarksDeleting) {
            return CenteredCircularProgressIndicator();
          }
          if (state is BookmarkCardErrorDeleting) {
            //todo
          }
          if (state is BookmarkCardDeleted) {
            //todo
          }

          return _buildFavIcon();
        },
      ),
    );
  }

  Widget _buildFavIcon() {
    return GestureDetector(
        onTap: () {
          _bookmarksCardBloc
              .add(BookmarksCardDeleteEvent(widget.ride.id.toString()));
          widget.onDeleteBookmark(true);
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 0.0),
          child: Icon(
            Icons.favorite,
            color: HexColor(PrevozStyles.BOOKMARK_ACTIVE),
          ),
        ));
  }

  String _getTimeDate() {
    return widget.ride.time.replaceAll("ob ", "") +
        ", " +
        DateTimeUtils.dateWithDayName(widget.ride.dateIso8601, context)
            .toLowerCase();
  }

  String _getPriceForDisplay() {
    String priceForDisplay;

    if (widget.ride.price == null) {
      priceForDisplay = "-";
    } else {
      priceForDisplay = OtherUtils.removeZerosFromPrice(widget.ride.price) +
          "€"; //? append € in util funtcion?
    }

    return priceForDisplay;
  }
}
