
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_state.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/my_rides/blocs/my_rides_list_bloc.dart/bloc.dart';
import 'package:prevoz_org/pages/my_rides/widgets/cards/users_added_rides_card.dart';
import 'package:prevoz_org/pages/my_rides/widgets/cards/users_bookmarked_rides_card.dart';
import 'package:prevoz_org/pages/search_results/widgets/zero_rides_row.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/cards/error_card.dart';
import 'package:prevoz_org/widgets/other/ad_view.dart';
import 'package:prevoz_org/widgets/other/prevoz_app_bar.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';

class MyRidesList extends StatefulWidget {
  final RideFormChange rideFormChange;
  MyRidesList({this.rideFormChange});
  @override
  _MyRidesListState createState() => _MyRidesListState();
}

class _MyRidesListState extends State<MyRidesList> {
  List<RideModel> bookmarks;

  @override
  Widget build(BuildContext context) {
    //* 1) New Bloc - load added rides and bookmarks at the same time...
    //* 2) States: MyRidesLoaded(boomarks, added), MyRidesError, MyRidesLoading

    return Stack(
      children: <Widget>[
        PrevozBackground(),
        Scaffold(
            backgroundColor: Colors.transparent,
            appBar: PrevozAppBar(
              context: context,
              pageTitle: AppLocalizations.of(context).myRides,
              isNavIconShown: false,
              isBackIcon: false,
            ),
            body: Builder(
              builder: (BuildContext context) {
                _displaySuccessSnackbar(context);
                return BlocBuilder(
                  cubit: BlocProvider.of<MyRidesListBloc>(context),
                  builder: (BuildContext context, MyRidesListState state) {
                    if (state is MyRidesLoading) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    if (state is MyRidesLoaded) {
                      print("______ my rides iz loaded");
                      //* list display similar to SearchResultsPage
                      List<RideModel> addedRides = [];
                      if (state.addedRides != null) {
                        addedRides = state.addedRides.getRides;
                      }
                      if (state.bookmarks != null) {
                        bookmarks = state.bookmarks.getRides;
                      }

                      return Padding(
                        padding: EdgeInsets.only(top: 25, left: 20, right: 20),
                        child: ListView.separated(
                          key: PageStorageKey("_myRidesList"),
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: 25,
                            );
                          },
                          itemCount: 3, // upper padding, bookmarks, added
                          itemBuilder: (BuildContext context, int index) {
                            //* ADDED RIDES
                            if (index == 0) {
                              return Card(
                                child:
                                    Column(children: _addedRides(addedRides)),
                              );
                            }
                            //* BOOKMARX
                            if (index == 1) {
                              return Card(
                                child: Column(
                                  children: _bookmarks(bookmarks),
                                ),
                              );
                            }

                            if (index == 2) {
                              return Center(
                                child: Padding(
                                  padding: EdgeInsets.only(bottom: 40),
                                  child: AdView(),
                                ),
                              );
                            }

                            return Padding(
                                padding: PrevozStyles.CARDS_OUTTER_PADDING,
                                child: Container());
                          },
                        ),
                      );
                    }

                    if (state is MyRidesError) {
                      return _ridesErrorWidget();
                    }

                    return Container();
                  },
                );
              },
            )),
      ],
    );
  }

  Widget _ridesErrorWidget() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: ErrorCard(
          errorText: AppLocalizations.of(context).errorFetchingDataFromServer,
          onErrorButtonPress: () {
            BlocProvider.of<MyRidesListBloc>(context).fetchMyRides();
          },
        )
      )
    );
  }

  List<Widget> _buildBookmarkRows(List<RideModel> bookmarks) {
    List<Widget> rows = [];

    if (bookmarks == null || bookmarks.length == 0) {
      rows.add(ZeroRidesRow(
        AppLocalizations.of(context).noBookmarkedRides,
      ));

      return rows;
    }

    for (var i = 0; i < bookmarks.length; i++) {
      RideModel ride = bookmarks[i];

      bool isLastInList = i == (bookmarks.length - 1);

      rows.add(UsersBookmarkedCard(
        ride: ride,
        isLastInList: isLastInList,
        onDeleteBookmark: (bool isSuccessfullyDeleted) {
          if (isSuccessfullyDeleted) {
            setState(() {
              bookmarks.removeAt(i);
            });
          } else {
            //todo: figure out why you cant call the parent scaffold and trigger a snackbar
          }
        },
      ));
    }

    return rows;
  }

  List<Widget> _buildAddedRidesRows(List<RideModel> addedRides) {
    List<Widget> rows = [];

    if (addedRides == null || addedRides.length == 0) {
      rows.add(ZeroRidesRow(AppLocalizations.of(context).noRidesAdded,
          showButton: true));

      return rows;
    }

    for (var i = 0; i < addedRides.length; i++) {
      RideModel ride = addedRides[i];

      bool isLastInList = i == (addedRides.length - 1);

      rows.add(UsersAddedRidesCard(
        ride: ride,
        isLastInList: isLastInList,
      ));
    }

    return rows;
  }

  List<Widget> _addedRides(List<RideModel> addedRides) {
    List<Widget> columnItems = [];

    columnItems
        .add(_cardTitle(AppLocalizations.of(context).added, addedRides.length));

    columnItems.addAll(_buildAddedRidesRows(addedRides));

    return columnItems;
  }

  List<Widget> _bookmarks(List<RideModel> bookmarks) {
    List<Widget> columnItems = [];

    if (bookmarks != null) {
      columnItems.add(
          _cardTitle(AppLocalizations.of(context).bookmarks, bookmarks.length));

      columnItems.addAll(_buildBookmarkRows(bookmarks));
    }

    return columnItems;
  }

  Widget _cardTitle(String title, int numOfRides) {
    return Container(
      decoration: BoxDecoration(
          color: HexColor("FAF7F2"),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(4), topRight: Radius.circular(4))),
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(width: 1.0, color: HexColor("DFDFDF")))),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 11, horizontal: 18),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: PrevozStyles.cardTitleStyle,
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Text(
                      numOfRides.toString(),
                      style:
                          TextStyle(fontFamily: "InterRegular", fontSize: 16),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    title == AppLocalizations.of(context).bookmarks
                        ? Icon(Icons.favorite,
                            size: 18, color: HexColor("4D4D4D"))
                        : Image.asset(
                            'assets/icon/camel_gray_small.png',
                            height: 15,
                          )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// a succesfull CRUD operation on AddRideForm redirect the user to AddedRidesTab
  /// and in that case we have to display a SnackBar to inform the user of the operations success.
  /// To do this we must use a PostFrameCallback that gets fired after everything gets renderd
  /// For more info on this technique refer to: https://www.didierboelens.com/faq/week2/
  _displaySuccessSnackbar(BuildContext context) {
    String snackbarContent;
    if (BlocProvider.of<BottomNavigationBloc>(context).state
        is RideSuccessfullyAdded) {
      snackbarContent = AppLocalizations.of(context).rideSuccessfullyAdded;
    }

    if (BlocProvider.of<BottomNavigationBloc>(context).state
        is RideSuccessfullyUpdated) {
      snackbarContent = AppLocalizations.of(context).rideSuccessfullyUpdated;
    }

    if (BlocProvider.of<BottomNavigationBloc>(context).state
        is RideSuccessfullyDeleted) {
      snackbarContent = AppLocalizations.of(context).rideSuccessfullyDeleted;
    }

    if (snackbarContent != null && snackbarContent.length > 0) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        final snackBar = SnackBar(
          content: Text(snackbarContent),
          duration: Duration(milliseconds: 1500),
          action: SnackBarAction(
            label: "OK",
            onPressed: () {},
          ),
        );

        Scaffold.of(context).showSnackBar(snackBar);
        BlocProvider.of<BottomNavigationBloc>(context)
            .add(ChangeStateAfterSnackbarEvent());
      });
    }
  }
}
