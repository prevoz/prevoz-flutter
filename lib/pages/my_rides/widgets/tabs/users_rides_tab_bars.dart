import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/prevoz_background.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/pages/my_rides/widgets/tabs/added_tab.dart';
import 'package:prevoz_org/pages/my_rides/widgets/tabs/bookmarks_tab.dart';

class UsersRidesTabBars extends StatefulWidget {
  final RideFormChange rideFormChange;
  UsersRidesTabBars({this.rideFormChange});

  @override
  _UsersRidesTabBarsState createState() => _UsersRidesTabBarsState();
}

class _UsersRidesTabBarsState extends State<UsersRidesTabBars>
    with SingleTickerProviderStateMixin {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        PrevozBackground(),
        SafeArea(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            key: _scaffoldKey,
            appBar: TabBar(
              labelPadding: EdgeInsets.only(
                  top:
                      16), //todo add padding depending on device height. add more padding to bigger devices.
              labelColor: Colors.black,
              unselectedLabelColor: HexColor(PrevozStyles.GREY_TEXT_HEX),
              indicatorColor: HexColor(PrevozStyles.PREVOZ_ORANGE_HEX),
              indicatorWeight: 4,
              indicatorSize: TabBarIndicatorSize.label,
              labelStyle: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              unselectedLabelStyle:
                  TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
              controller: _tabController,
              onTap: (tabNum) {
                print("___ tab tap, tab num");
              },
              tabs: <Widget>[
                Tab(
                  text: "Objavljeni", //todor: localize
                ),
                Tab(
                  text: "Zaznamki", //todo: localize
                )
              ],
            ),
            body: TabBarView(
              controller: _tabController,
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                  child: AddedTab(
                    parentScaffoldKey: this._scaffoldKey,
                    rideFormChange: widget.rideFormChange,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                  child: BookmarksTab(parentScaffoldKey: this._scaffoldKey),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
