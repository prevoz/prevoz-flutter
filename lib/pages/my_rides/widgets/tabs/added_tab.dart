import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/error_screen.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/my_rides/widgets/cards/users_added_rides_card.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_state.dart';
import 'package:prevoz_org/data/blocs/users_added_rides/users_added_rides_bloc.dart';
import 'package:prevoz_org/data/blocs/users_added_rides/users_added_rides_event.dart';
import 'package:prevoz_org/data/blocs/users_added_rides/users_added_rides_state.dart';

class AddedTab extends StatefulWidget {
  final RideFormChange rideFormChange;
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  AddedTab({this.parentScaffoldKey, this.rideFormChange});

  @override
  _AddedTabState createState() => _AddedTabState();
}

class _AddedTabState extends State<AddedTab> {
  UsersAddedRidesBloc _usersAddedRidesBloc;

  @override
  void initState() {
    super.initState();

    _usersAddedRidesBloc = BlocProvider.of<UsersAddedRidesBloc>(context);
    _fetchAddedRides();
    //_usersAddedRidesBloc.add(FetchAddedRidesEvent());
    print("____init state dispatching fetch users added rides event");
    //!TODO - APP WIDE WAY TO HANDLE ALL KINDS OF MESSAGES
    //_handleUpdatesFromAddRidePage();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        _displaySuccessSnackbar(context);

        return BlocBuilder(
          cubit: _usersAddedRidesBloc,
          builder: (BuildContext context, UsersAddedRidesState state) {
            if (state is AddedRidesLoading) {
              _dispatchErrorAfterFiveSeconds();
              return _buildCircularProgressIndicator();
            }
            if (state is AddedRidesLoaded) {
              AddedRidesLoaded rides = state;
              if (rides.fetchedRides.length < 1) {
                return _buildNoRidesFound();
              }
              return _buildUsersRidesList(rides);
            }

            if (state is AddedRidesFailedLoading) {
              return ErrorScreen(
                  errorText:
                      AppLocalizations.of(context).errorLoadingAddedRides,
                  onRetryPressed: () {
                    _fetchAddedRides();
                    //_usersAddedRidesBloc.add(FetchAddedRidesEvent());
                  });
            }

            return Center(
              child: Text(AppLocalizations.of(context).unexpectedError),
            );
          },
        );
      },
    );
  }

  _buildCircularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _buildNoRidesFound() {
    return Center(
      child: Text(AppLocalizations.of(context).noRidesAdded,
          textAlign: TextAlign.center,
          style: PrevozStyles.commonUserNoticeTextStyle),
    );
  }

  _buildUsersRidesList(AddedRidesLoaded rides) {
    return Column(
      children: <Widget>[
        Expanded(
          child: RefreshIndicator(
            key: new GlobalKey<RefreshIndicatorState>(),
            onRefresh: _fetchAddedRides,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(4)),
              child: ListView.builder(
                  itemCount: rides.fetchedRides.length,
                  itemBuilder: (BuildContext context, int index) {
                    return UsersAddedRidesCard(
                      ride: rides.fetchedRides[index],
                      isLastInList: true,
                    );
                  }),
            ),
          ),
        ),
      ],
    );
  }
  /*
  _tryAgain() {
    print("_______tryAgain dispatching");
    _usersAddedRidesBloc.add(FetchAddedRidesEvent());
  }*/

  Future<void> _fetchAddedRides() {
    print("____fetchAddedRides dispatching");
    _usersAddedRidesBloc.add(FetchAddedRidesEvent());
    //todo: return added to satisfy type expectation... find better way to do this
    return Future.delayed(Duration(microseconds: 1));
  }

  /// if rides are being loaded for more than 5 seconds, add the error loading
  _dispatchErrorAfterFiveSeconds() {
    Future.delayed(Duration(seconds: 5), () {
      if (_usersAddedRidesBloc.state == AddedRidesLoading()) {
        _usersAddedRidesBloc.add(AddedRidesFailedLoadingEvent());
      }
    });
  }

  /// a succesfull CRUD operation on AddRideForm redirect the user to AddedRidesTab
  /// and in that case we have to display a SnackBar to inform the user of the operations success.
  /// To do this we must use a PostFrameCallback that gets fired after everything gets renderd
  /// For more info on this technique refer to: https://www.didierboelens.com/faq/week2/
  _displaySuccessSnackbar(BuildContext context) {
    String snackbarContent;
    if (BlocProvider.of<BottomNavigationBloc>(context).state
        is RideSuccessfullyAdded) {
      snackbarContent = AppLocalizations.of(context).rideSuccessfullyAdded;
    }

    if (BlocProvider.of<BottomNavigationBloc>(context).state
        is RideSuccessfullyUpdated) {
      snackbarContent = AppLocalizations.of(context).rideSuccessfullyUpdated;
    }

    if (BlocProvider.of<BottomNavigationBloc>(context).state
        is RideSuccessfullyDeleted) {
      snackbarContent = AppLocalizations.of(context).rideSuccessfullyDeleted;
    }

    if (snackbarContent != null && snackbarContent.length > 0) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        final snackBar = SnackBar(
          content: Text(snackbarContent),
          duration: Duration(milliseconds: 1500),
          action: SnackBarAction(
            label: "OK",
            onPressed: () {},
          ),
        );

        Scaffold.of(context).showSnackBar(snackBar);
      });
    }
  }
}
