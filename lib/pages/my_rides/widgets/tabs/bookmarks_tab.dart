import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/users_bookmarked_rides/bookmarks_tab_bloc.dart';
import 'package:prevoz_org/data/blocs/users_bookmarked_rides/users_bookmarked_rides_event.dart';
import 'package:prevoz_org/data/blocs/users_bookmarked_rides/users_bookmarked_rides_state.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/my_rides/widgets/cards/users_bookmarked_rides_card.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/error_screen.dart';

class BookmarksTab extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  BookmarksTab({this.parentScaffoldKey});

  @override
  _BookmarksTabState createState() => _BookmarksTabState();
}

class _BookmarksTabState extends State<BookmarksTab> {
  BookmarksTabBloc _usersBookmarksBloc;

  @override
  void initState() {
    super.initState();
    _usersBookmarksBloc = BlocProvider.of<BookmarksTabBloc>(context);
    _fetchBookmarks();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: _usersBookmarksBloc,
      builder: (BuildContext context, UsersBookmarksState state) {
        if (state is BookmarksDeleting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state is BookmarksLoaded) {
          BookmarksLoaded rides = state;

          return _bookmarksLoaded(rides);
        }

        if (state is BookmarksFailedLoading) {
          return ErrorScreen(
              errorText: AppLocalizations.of(context).bookmarksSearchError,
              onRetryPressed: _fetchBookmarks);
        }

        return Container();
      },
    );
  }

  Future<void> _fetchBookmarks() {
    _usersBookmarksBloc.add(FetchBookmarksEvent());
    //todo: return added to satisfy type expectation... find better way to do this
    return Future.delayed(Duration(microseconds: 1));
  }

  Widget _bookmarksLoaded(BookmarksLoaded rides) {
    //* NO BOOKMARKS
    if (rides.fetchedRides.length < 1) {
      return Center(
        child: Text(AppLocalizations.of(context).noBookmarkedRides,
            textAlign: TextAlign.center,
            style: PrevozStyles.commonUserNoticeTextStyle),
      );
    }

    return Column(
      children: <Widget>[
        Expanded(
          child: RefreshIndicator(
            key: new GlobalKey<RefreshIndicatorState>(),
            onRefresh: _fetchBookmarks,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(4)),
              child: ListView.builder(
                  itemCount: rides.fetchedRides.length,
                  itemBuilder: (BuildContext context, int index) {
                    return UsersBookmarkedCard(
                      ride: rides.fetchedRides[index],
                      isLastInList: true,
                      onDeleteBookmark: (bool isSuccessfullyDeleted) {
                        if (isSuccessfullyDeleted) {
                          setState(() {
                            rides.fetchedRides.removeAt(index);
                          });
                        } else {
                          //todo: figure out why you cant call the parent scaffold and trigger a snackbar
                        }
                      },
                    );
                  }),
            ),
          ),
        ),
      ],
    );
  }
}
