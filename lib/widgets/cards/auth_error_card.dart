
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_event.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

import 'error_card.dart';

class AuthErrorCard extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return ErrorCard(
      errorText: AppLocalizations.of(context).errorFetchingDataFromServer,
      onErrorButtonPress: () {
        BlocProvider.of<AuthenticationBloc>(context).add(AuthenticationRefreshAccountStatus());
      },
    );
  }
}