import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

typedef OnErrorButtonPress = void Function();

class ErrorCard extends StatelessWidget {
  final String errorText;
  final OnErrorButtonPress onErrorButtonPress;

  const ErrorCard(
      {Key key, @required this.errorText, @required this.onErrorButtonPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.error,
                size: 40,
                color: Colors.red,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                errorText,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: HexColor(PrevozStyles.GREY_TEXT_HEX),
                    fontSize: 18,
                    fontFamily: "Inter"),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: ButtonTheme(
                  minWidth: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 22, right: 22),
                      child: Text(
                        AppLocalizations.of(context).tryAgain,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      colorBrightness: Brightness.light,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(4)),
                      splashColor: Theme.of(context).accentColor,
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        onErrorButtonPress();
                        //AuthUtils.launchBrowsersForAuthorisation();
                      }),
                ),
              )
            ],
          ),
        )),
      ],
    );
  }
}
