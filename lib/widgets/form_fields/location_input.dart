import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

//* Flutter note
//* disables any possibility of TextField having focus
class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

class LocationInput extends StatelessWidget {
  final Location location;
  final TextEditingController controller;
  final VoidCallback onInputTap;
  final bool isFromLocation;
  final Key key;

  LocationInput(
      {@required this.location,
      @required this.isFromLocation,
      @required this.onInputTap,
      @required this.key,
      this.controller});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onTap: onInputTap,
      focusNode: AlwaysDisabledFocusNode(),
      style: TextStyle(
        fontSize: 16,
        fontFamily: "Inter",
        color: HexColor("333333"),
      ),
      decoration: PrevozStyles.searchFormCardInputDecoration(
          isFromLocation ? "Od" : "Do", controller),
    );
  }
}
