import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

/// Used to prevent the keyboard of TextField to pop-up
class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

/// //! SearchInput is deprecated
//todo - remove this
class SearchInput extends StatelessWidget {
  final Location location;
  final TextEditingController controller;
  final VoidCallback onInputTap;
  final VoidCallback onSuffixClickedCallback;

  final bool
      isFromLocation; // true if used for "From location", false if used for "To location"

  SearchInput(
      {@required this.location,
      @required this.isFromLocation,
      @required this.onInputTap,
      @required this.onSuffixClickedCallback,
      this.controller});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 32.0),
        child: Material(
            elevation: 5.0,
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            child: TextField(
                enableInteractiveSelection: false,
                focusNode: AlwaysDisabledFocusNode(),
                key: isFromLocation
                    ? Key(MyConstants.fromLocationKey)
                    : Key(MyConstants.toLocationKey),
                onTap: onInputTap,
                controller: controller,
                style: PrevozStyles.textFieldStyle,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: PrevozStyles.textFieldNormalPadding,
                    prefixIcon: isFromLocation
                        ? Icon(Icons.home,
                            color:
                                location == null ? Colors.grey : Colors.black)
                        : Icon(Icons.directions_car,
                            color:
                                location == null ? Colors.grey : Colors.black),
                    suffixIcon: location != null
                        ? IconButton(
                            icon: Icon(Icons.remove),
                            onPressed: onSuffixClickedCallback,
                          )
                        : IconButton(
                            icon: Icon(Icons.adb),
                            iconSize: 0.0,
                            onPressed: () {},
                          ),
                    hintText: isFromLocation
                        ? AppLocalizations.of(context).departureLocation
                        : AppLocalizations.of(context).arrivalLocation,
                    hintStyle: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.italic,
                        //shadows:
                        color: Colors.black.withOpacity(0.6))))));
  }
}
