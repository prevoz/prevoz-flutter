
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class AdView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Random random = new Random();
    int randomNumber = random.nextInt(1000);
    var html = '''
    <div>
        <a href='https://go.legit.si/2AU' target='_blank'><img src='https://ad.legit.si/www/delivery/avw.php?zoneid=17&amp;cb=''' + randomNumber.toString() + '''&amp;n=ab31a84a' border='0' alt='' /></a>
    </div>
    ''';

    return HtmlWidget(
      html,
    );
  }
}