import 'package:flutter/material.dart';

class DefaultPrevozButton extends StatelessWidget {
  final String buttonText;
  //final Function _onPressedHandler;
  final Function onPressedHandler;
  final double width;

  DefaultPrevozButton({this.buttonText, this.onPressedHandler, this.width});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 7, right: 7),
      child: ButtonTheme(
        minWidth: width == null ? MediaQuery.of(context).size.width / 3 : width,
        child: RaisedButton(
            elevation: 3,
            child: Text(
              buttonText,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            colorBrightness: Brightness.light,
            shape: StadiumBorder(),
            splashColor: Theme.of(context).accentColor,
            color: Theme.of(context)
                .primaryColor, //UIColor(red:0.93, green:0.44, blue:0, alpha:1)
            onPressed: () {
              if (onPressedHandler != null) {
                onPressedHandler();
              }
            }),
      ),
    );
  }
}
