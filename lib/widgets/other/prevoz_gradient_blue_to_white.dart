import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class PrevozGradientBlueToWhite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height,
        ),
        //* #2 - PrevozDetailsAppBar needs to be transparent
        //*      but we put this container before it as background
        //*      to achieve seemless transition of gradient from appbar to page body
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [
              HexColor("#ffffff"), //Background Gradient od: #ffffff do #C3E9F4
              HexColor("#C3E9F4")
            ], //UIColor(red:0.76, green:0.91, blue:0.96, alpha:1).cgColor
            begin: Alignment.bottomLeft,
            end: Alignment.topLeft,
          )),
          height: 140,
          width: MediaQuery.of(context).size.width,
        ),
      ],
    );
  }
}
