import 'package:flutter/material.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:prevoz_org/widgets/other/default_prevoz_button.dart';

class ErrorScreen extends StatelessWidget {
  final String errorText;
  final Function onRetryPressed;

  ErrorScreen({this.errorText, this.onRetryPressed});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            errorText,
            textAlign: TextAlign.center,
            style: PrevozStyles.commonUserNoticeTextStyle,
          ),
          SizedBox(
            height: 15,
          ),
          DefaultPrevozButton(
            buttonText: AppLocalizations.of(context).tryAgain,
            onPressedHandler: () {
              if (onRetryPressed != null) {
                onRetryPressed();
              }
            },
          )
        ],
      ),
    );
  }
}
