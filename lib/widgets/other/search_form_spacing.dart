import 'package:flutter/material.dart';

class SearchFormSpacingHelper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    double formSpacing = 10;
    if (deviceHeight > 680) {
      formSpacing = 25;
    }

    return SizedBox(
      height: formSpacing,
    );
  }
}
