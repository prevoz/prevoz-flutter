
import 'package:flutter/material.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

class GoogleSignInButton extends StatelessWidget {

  final Function onClick;

  GoogleSignInButton({@required this.onClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: 48,
      child: FlatButton(
          splashColor: Colors.grey,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage("assets/icon/google_logo.png"), height: 22.0),
              SizedBox(width: 10,),
              Flexible(
                child: Text(
                  AppLocalizations.of(context).signInWithGoogleButton,
                  style: TextStyle(
                      inherit: false,
                      fontFamily: '.SF Pro Text',
                      letterSpacing: -0.41,
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      color: Colors.black),
                ),
              )
            ],
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.0),
              side: BorderSide(color: Colors.black)
          ),
          onPressed: onClick
      ),
    );
  }
}