import 'package:flutter/material.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/hex_color.dart';

typedef AppBarAction = void Function();

class ActionableAppBar extends StatelessWidget implements PreferredSizeWidget {
  @required
  final BuildContext context;
  @required
  final String pageTitle;
  final bool isBackIcon;
  final bool isNavIconShown;
  final AppBarAction onAppBarAction;
  final IconData iconData;

  ActionableAppBar(
      {this.context,
      this.pageTitle,
      this.isBackIcon,
      this.isNavIconShown,
      this.onAppBarAction,
      this.iconData});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.transparent,
        height: 80,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
          child: Stack(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 40,
                    alignment: AlignmentDirectional.center,
                    child: Text(
                      pageTitle,
                      style: TextStyle(
                          fontSize: 29,
                          fontFamily: "InterBlack",
                          color: HexColor("333333")),
                    ),
                  ),
                ],
              ),
              Positioned(
                  right: 0, child: _quickRidesActionIcon(isBackIcon, context)),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(120);

  Widget _quickRidesActionIcon(bool isBackIcon, BuildContext context) {
    return InkResponse(
      key: Key("_ActionableAppBarNavIcon"),
      onTap: () {
        //Navigator.of(context).pop();
        onAppBarAction();
      },
      child: new Container(
        width: 40,
        height: 40,
        decoration: new BoxDecoration(
          color: Colors.transparent,
          shape: BoxShape.circle,
        ),
        child: new Icon(
          iconData,
          color: Colors.black,
          size: 35,
        ),
      ),
    );
  }

  // IconData _setupIconData(bool isBackIcon) {
  //   if (!isBackIcon) {
  //     return Icons.close;
  //   }

  //   return Icons.arrow_back_ios;
  // }
}

class ActionableAppBarWithRaisedButton extends StatelessWidget
    implements PreferredSizeWidget {
  @required
  final BuildContext context;
  @required
  final String pageTitle;
  final bool isBackIcon;
  final bool isNavIconShown;
  final AppBarAction onAppBarAction;
  final IconData iconData;

  ActionableAppBarWithRaisedButton(
      {this.context,
      this.pageTitle,
      this.isBackIcon,
      this.isNavIconShown,
      this.onAppBarAction,
      this.iconData});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.transparent,
        height: 80,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 25),
          child: Stack(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 40,
                    alignment: AlignmentDirectional.center,
                    child: Text(
                      "Dodaj Prevoz",
                      style: TextStyle(
                          fontSize: 29,
                          fontFamily: "InterBlack",
                          color: HexColor("333333")),
                    ),
                  ),
                ],
              ),
              Positioned(
                  right: 0,
                  child: InkWell(
                      key: Key("_ActionableAppBarNavIcon"),
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 12, right: 6, top: 2, bottom: 2),
                        decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: HexColor("#a8b0b4"),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(6))),
                        child: Text(
                          AppLocalizations.of(context).quickRidesNewLine,
                          textAlign: TextAlign.end,
                        ),
                      ),
                      onTap: () {
                        onAppBarAction();
                      })),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(120);

  // IconData _setupIconData(bool isBackIcon) {
  //   if (!isBackIcon) {
  //     return Icons.close;
  //   }

  //   return Icons.arrow_back_ios;
  // }
}
