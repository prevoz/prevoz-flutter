import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class PrevozAppBar extends StatelessWidget implements PreferredSizeWidget {
  @required
  final BuildContext context;
  @required
  final String pageTitle;
  final bool isBackIcon;
  final bool isNavIconShown;
  final Function onBackPress;

  PrevozAppBar(
      {this.context,
      this.pageTitle,
      this.isBackIcon,
      this.isNavIconShown,
      this.onBackPress});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.transparent,
        height: 80,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 15),
          child: Stack(
            children: <Widget>[
              Positioned(
                left: 0,
                child: isNavIconShown
                    ? _navIcon(isBackIcon, context)
                    : Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 40,
                    alignment: AlignmentDirectional.center,
                    child: Text(
                      pageTitle,
                      style: TextStyle(
                          fontSize: pageTitle.length > 19 ? 25 : 29,
                          fontFamily: "InterBlack",
                          color: HexColor("333333")),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(120);

  Widget _navIcon(bool isBackIcon, BuildContext context) {
    return InkResponse(
      key: Key("_prevozAppBarNavIcon"),
      onTap: () {
        if (onBackPress != null) {
          onBackPress();
        }
        Navigator.of(context).pop();
      },
      child: new Container(
        width: 40,
        height: 40,
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: new Icon(
          _setupIconData(isBackIcon),
          color: Colors.black,
        ),
      ),
    );
  }

  IconData _setupIconData(bool isBackIcon) {
    if (!isBackIcon) {
      return Icons.close;
    }

    return Icons.arrow_back_ios;
  }
}
