import 'package:flutter/material.dart';
import 'package:prevoz_org/utils/hex_color.dart';

class PrevozBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        //_buildGradientContainerForBackground(context),
        _backgroundLinearGradient(),
        _buildBackgroundUi(context),
      ],
    );
  }

  Widget _backgroundLinearGradient() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [
          HexColor("#FFFFFF"),
          HexColor("C3E9F4"),
        ],
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
      )),
    );
  }

  Widget _buildBackgroundUi(BuildContext context) {
    double topPaddingForDesert = 105;

    double deviceHeight = MediaQuery.of(context).size.height;

    if (deviceHeight < 745) {
      topPaddingForDesert = 85;
    }

    if (deviceHeight < 680) {
      topPaddingForDesert = 75;
    }

    return Padding(
      padding: EdgeInsets.only(top: topPaddingForDesert),
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.png"),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: 60,
            bottom: -10,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/footerBackground.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
