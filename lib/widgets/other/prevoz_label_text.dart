import 'package:flutter/material.dart';

class PrevozLabelText extends StatelessWidget {
  final String textLabel;
  PrevozLabelText({Key key, this.textLabel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      textLabel,
      style: TextStyle(fontFamily: "InterNormal"),
    );
  }
}
