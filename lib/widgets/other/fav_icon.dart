import 'package:flutter/material.dart';

class FavIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(8),
        child: Icon(
          Icons.favorite,
          color: Theme.of(context).primaryColor,
          size: 32,
        ));
  }
}
