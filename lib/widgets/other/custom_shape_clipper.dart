import 'package:flutter/material.dart';

class CustomShapeClipper extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    final Path path = Path();

    /* Working with path is actually pretty easy 

      You're just drawing on the screen with programmin. 
      Here's a cool article about it: 
      
      https://medium.com/flutter-community/paths-in-flutter-a-visual-guide-6c906464dcd0
     */

    path.lineTo(0.0, size.height);

    // we'll draw two curves. each curve gets an endpoint and controlpoint
    Offset firstEndPoint = Offset(size.width * 0.5, size.height - 40);
    Offset firstControlPoint = Offset(size.width * 0.3, size.height - 50);

    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    Offset secondEndPoint = Offset(size.width, size.height - 80);
    Offset secondControlPoint = Offset(size.width, size.height - 20);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    //path.lineTo(size.width, size.height); we remove this cuz we already reach the right side of the curve.
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) => true;
}
