import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/blocs/locations/locations_bloc.dart';
import 'package:prevoz_org/data/blocs/locations/locations_events.dart';
import 'package:prevoz_org/data/blocs/locations/locations_state.dart';
import 'package:prevoz_org/data/helpers/event_bus/location_selected_event.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/enum_from_to.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/widgets/other/country_search_delegate.dart';
import 'package:prevoz_org/widgets/other/error_screen.dart';

class LocationsSearchDelegate extends SearchDelegate {
  final SearchType searchType;
  LocationsBloc _locationsBloc;
  Country _newlySelectedCountry;
  final Location _searchLocation;

  LocationsSearchDelegate(this.searchType, this._searchLocation);

  @override
  Widget buildLeading(BuildContext context) {
    _locationsBloc = BlocProvider.of<LocationsBloc>(context);
    return _buildNavigateBackIcon(context);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      _buildCountryButton(context),
      _buildCancelQueryButton(),
    ];
  }

  @override
  Widget buildResults(BuildContext context) {
    //_locationsBloc = BlocProvider.of<LocationsBloc>(context);
    return _buildResultsBlocBuilder(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    _fetchLocations(context);
    return _buildResultsBlocBuilder(context);
  }

  Widget _buildQueryResults(BuildContext context, List<Location> locations) {
    return ListView.separated(
        itemCount: locations.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildLocationCard(context, locations, index);
        },
        separatorBuilder: (BuildContext context, int itemcount) {
          return Divider(
            height: 1,
          );
        });
  }

  _fetchLocations(BuildContext context) {
    String countryCode;
    if (_searchLocation != null) {
      countryCode = _searchLocation.country;
    }

    if (_newlySelectedCountry != null) {
      countryCode = _newlySelectedCountry.countryCode;
    }

    if (countryCode == null || countryCode.length == 0) {
      countryCode = "SI";
    }

    //! do not fetch when locations are loaded and the same query is made
    if (_isTheSameQuery(context, query, countryCode)) {
      return;
    }
    BlocProvider.of<LocationsBloc>(context)
        .add(FetchLocations(query, countryCode));
  }

  BlocBuilder _buildResultsBlocBuilder(BuildContext context) {
    return BlocBuilder(
      cubit: BlocProvider.of<LocationsBloc>(context),
      builder: (BuildContext context, dynamic state) {
        if (state is LocationsLoading ||
            state is SanitisingLocations ||
            state is InitialisingLocations) {
          return _buildCircularProgressIndicator();
        }

        if (state is SuccessLoadingLocations) {
          List<Location> locations = state.fetchedLocations;

          if (locations == null) {
            Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
                "SearchLocationDelegate: locations == null", null);
            return _buildErrorScreen(context);
          }
          if (locations.length < 1) {
            return _buildNoResultsForThisQuery(context);
          }

          return _buildQueryResults(context, locations);
        }
        if (state is ErrorLoadingLocations) {
          return ErrorScreen(
            errorText: AppLocalizations.of(context).erroLoadingLocations,
            onRetryPressed: () {
              _fetchLocations(context);
            },
          );
        }
        return Container();
      },
    );
  }

  Widget _buildNoResultsForThisQuery(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).noResultsForThisQuery,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 12,
          ),
          Icon(Icons.not_listed_location, size: 45)
        ],
      ),
    );
  }

  Widget _buildCircularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  String _getCountryText() {
    try {
      if (_newlySelectedCountry != null) {
        return _newlySelectedCountry.name;
      }

      if (_searchLocation != null) {
        return _getCountryNameBasedOnCountryCode(_searchLocation.country);
      }

      return 'Slovenija';
    } catch (e) {
      handleError(error: e, message: "_getCountryText");
      return 'Slovenija';
    }
  }

  String _getCountryNameBasedOnCountryCode(String countryCode) {
    print("______ contry code " + countryCode.toString());
    try {
      List<Country> allCountries = MyConstants.countriesList;

      print("_getCountryNameBasedOnCountryCode(), " + countryCode.toString());

      Country currentCountry = allCountries
          .singleWhere((c) => c.countryCode == countryCode, orElse: null);

      print("_getCountryNameBasedOnCountryCode(), current country: " +
          currentCountry.toString());

      if (currentCountry != null) {
        return currentCountry.name;
      }
      print(
          "_getCountryNameBasedOnCountryCode(), bout to return regular country code " +
              currentCountry.toString());
      return countryCode;
    } catch (e) {
      handleError(
          error: e, message: "LocSrcDelg, _getCountryNameBasedOnCountryCode()");
      return "";
    }
  }

  ErrorScreen _buildErrorScreen(BuildContext context) {
    return ErrorScreen(
      errorText: AppLocalizations.of(context).erroLoadingLocations,
      onRetryPressed: () {
        _fetchLocations(context);
      },
    );
  }

  InkWell _buildLocationCard(
      BuildContext context, List<Location> locations, int index) {
    return InkWell(
        onTap: () {
          BlocProvider.of<LocationsBloc>(context).resetStateToInitial();

          //* EventBus is used here because the SearchFormCard widget on HomePage
          //* can get rebuilt while the user is selecting the location.
          //* This produces an error because when the SearchDelegate Future completes,
          //* the widget that started it and awaits for Future result, no longer exists.
          if (searchType == SearchType.SearchRideFrom) {
            MyApp.eventBus.fire(
                LocationSelectedEvent(LocationType.FROM, locations[index]));
          }

          if (searchType == SearchType.SearchRideTo) {
            MyApp.eventBus
                .fire(LocationSelectedEvent(LocationType.TO, locations[index]));
          }

          close(context, locations[index]);
        },
        child: ListTile(
          leading: Icon(Icons.location_city),
          title: Text(locations[index].name),
        ));
  }

  IconButton _buildNavigateBackIcon(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        BlocProvider.of<LocationsBloc>(context).resetStateToInitial();
        close(context, null);
      },
    );
  }

  InkWell _buildCountryButton(BuildContext buildContext) {
    return InkWell(
      onTap: () {
        CountriesSearchDelegate _countriesSearchDelegate =
            CountriesSearchDelegate();

        showSearch(context: buildContext, delegate: _countriesSearchDelegate)
            .then((onValue) {
          query = "";
          try {
            if (onValue is Country) {
              //todo: what happens if I just press back in country search
              //todo: what happens if I already had a selected country previously
              if (_newlySelectedCountry == null) {
                _newlySelectedCountry = onValue;
                _locationsBloc.add(FetchLocations("", onValue.countryCode));
              } else if (_newlySelectedCountry.name != onValue.name) {
                _newlySelectedCountry = onValue;
                _locationsBloc.add(FetchLocations("", onValue.countryCode));
              }
            }
          } catch (e) {
            handleError(error: e, message: "_buildCountryButton");
          }
        });
      },
      child: Chip(
        avatar: Icon(Icons.arrow_drop_down_circle),
        label: Text(_getCountryText()),
      ),
    );
  }

  Widget _buildCancelQueryButton() {
    if (query.length == 0) {
      return SizedBox(
        width: 16,
      );
    }

    return IconButton(
      icon: Icon(Icons.cancel),
      onPressed: () {
        query = '';
      },
    );
  }

  /// this is a temporary solution for multiple unnecessary builds of
  /// buildSuggestions() which results in unnecessary fetches of locations from DB
  //todo: find a better, "Flutter way of doing things" aprroach for this problem
  //* https://medium.com/saugo360/flutter-my-futurebuilder-keeps-firing-6e774830bc2
  bool _isTheSameQuery(BuildContext context, String query, String country) {
    if (BlocProvider.of<LocationsBloc>(context).state
        is SuccessLoadingLocations) {
      SuccessLoadingLocations locLoadedState =
          BlocProvider.of<LocationsBloc>(context).state;

      if (locLoadedState.country.toLowerCase() == country.toLowerCase() &&
          locLoadedState.query.toLowerCase() == query.toLowerCase()) {
        return true;
      }
    }
    return false;
  }
}
