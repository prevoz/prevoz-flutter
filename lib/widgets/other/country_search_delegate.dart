import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/data/blocs/countries/countries_bloc.dart';
import 'package:prevoz_org/data/blocs/countries/countries_state.dart';
import 'package:prevoz_org/data/blocs/locations/locations_state.dart';
import 'package:prevoz_org/data/blocs/countries/countries_events.dart';

class CountriesSearchDelegate extends SearchDelegate {
  CountriesSearchDelegate();

  @override
  List<Widget> buildActions(BuildContext context) {
    return [_cancelQueryButton()];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.length == 0 &&
        BlocProvider.of<CountriesBloc>(context).state is CountriesLoaded) {
      return recentCountries();
    }

    return BlocBuilder(
      cubit: BlocProvider.of<CountriesBloc>(context),
      builder: (BuildContext context, CountriesState state) {
        if (state is CountriesLoading) {
          return _circularProgressIndicator();
        } else if (state is InitialLocationsState) {
          return recentCountries();
        } else if (state is CountriesLoaded) {
          List<Country> countries = state.fetchedCountries;

          if (countries.length < 1) {
            return _noResultsForThisQuery(context);
          }

          return _queryResultCountries(context, countries);
        }

        return Container();
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.length > 0) {
      BlocProvider.of<CountriesBloc>(context).add(FetchCountries(query));
    }

    if (query.length == 0) {
      BlocProvider.of<CountriesBloc>(context).add(GetAllCountries());
    }

    return BlocBuilder(
      cubit: BlocProvider.of<CountriesBloc>(context),
      builder: (BuildContext context, CountriesState state) {
        if (state is CountriesLoading || state is InitialisingCountries) {
          return _circularProgressIndicator();
        } else if (state is InitialLocationsState) {
          return recentCountries();
        } else if (state is CountriesLoaded) {
          List<Country> countries = state.fetchedCountries;
          if (countries.length < 1) {
            return _noResultsForThisQuery(context);
          }
          return _queryResultCountries(context, countries);
        } else if (state is ErrorLoadingCountries) {
          return Center(
            child: Text(AppLocalizations.of(context).errorLoadingCountries),
          );
        }

        return Container();
      },
    );
  }

  Widget _queryResultCountries(BuildContext context, List<Country> countries) {
    return ListView.separated(
        itemCount: countries.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
              onTap: () {
                close(context, countries[index]);
              },
              child: ListTile(
                leading: Icon(Icons.outlined_flag),
                title: Text(countries[index].name),
              ));
        },
        separatorBuilder: (BuildContext context, int itemcount) {
          return Divider(
            height: 1,
          );
        });
  }

  Widget _noResultsForThisQuery(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).noResultsForThisQuery,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 12,
          ),
          Icon(Icons.not_listed_location, size: 45)
        ],
      ),
    );
  }

  //todo; refactor and remove recent countries from this delegate
  Widget recentCountries() {
    return Container();
  }

  Widget _cancelQueryButton() {
    if (query.length == 0) {
      return Container();
    }

    return IconButton(
      icon: Icon(Icons.cancel),
      onPressed: () {
        query = '';
      },
    );
  }

  Widget _circularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
