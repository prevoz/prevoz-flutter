import 'dart:async';
import 'package:event_bus/event_bus.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/profile/profile_bloc.dart';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/helpers/event_bus/invalid_token_event.dart';
import 'package:prevoz_org/data/repositories/user_profile_repository.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/bloc/quick_entries_bloc.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';
import 'package:uni_links/uni_links.dart';
import 'data/blocs/add_ride_profile/add_ride_profile_bloc.dart';
import 'data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/navigation_delegation_page.dart';
import 'package:prevoz_org/data/blocs/rides/rides_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bloc.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_bloc.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/data/blocs/locations/locations_bloc.dart';
import 'package:prevoz_org/data/blocs/countries/countries_bloc.dart';
import 'package:prevoz_org/data/blocs/home_page/home_page_bloc.dart';
import 'package:prevoz_org/data/repositories/locations_repository.dart';
import 'package:prevoz_org/data/repositories/countries_repository.dart';
import 'package:prevoz_org/data/blocs/ride_details/ridedetails_bloc.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_bloc.dart';
import 'package:prevoz_org/data/repositories/search_history_repository.dart';
import 'package:prevoz_org/pages/search_results/bloc/riderowbookmark_bloc.dart';
import 'package:prevoz_org/pages/my_rides/blocs/bloc/bookmarkscardbloc_bloc.dart';
import 'package:prevoz_org/data/blocs/users_added_rides/users_added_rides_bloc.dart';
import 'package:prevoz_org/data/blocs/users_bookmarked_rides/bookmarks_tab_bloc.dart';
import 'package:prevoz_org/data/repositories/notification_subscription_repository.dart';
import 'package:prevoz_org/data/blocs/home_page/route_statistics/routestatistics_bloc.dart';
import 'package:prevoz_org/data/blocs/ride_details_redesign/ride_details_redesign_bloc.dart';
import 'package:prevoz_org/pages/my_rides/blocs/my_rides_list_bloc.dart/my_rides_list_bloc.dart';

class MyApp extends StatefulWidget {
  final BaseDatabaseHelper appDatabase;
  final AuthRepository _authRepository;
  final RidesRepository _ridesRepository;
  final LocationsRepository _locationsRepository;
  final CountriesRepository _countriesRepository;
  final SearchHistoryRepository _searchHistoryRepository;
  final NotificationSubscriptionRepository _notificationSubscriptionRepository;
  final UserProfileRepository _userProfileRepository;
  static final EventBus eventBus = EventBus();

  MyApp(
    this.appDatabase,
    this._authRepository,
    this._ridesRepository,
    this._locationsRepository,
    this._countriesRepository,
    this._searchHistoryRepository,
    this._notificationSubscriptionRepository,
    this._userProfileRepository
  );

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  RideBloc _ridesBloc;
  AddRideBloc _addRideBloc;
  HomePageBloc _homePageBloc;
  LocationsBloc _locationsBloc;
  CountriesBloc _countriesBloc;
  MyRidesListBloc _myRidesListBloc;
  RideDetailsBloc _rideDetailsBloc;
  BookmarksCardBloc _bookmarksCardBloc;
  QuickEntriesBloc _quickEntriesBloc;
  AuthenticationBloc _authenticationBloc;
  UsersAddedRidesBloc _usersAddedRidesBloc;
  RouteStatisticsBloc _routeStatisticsBloc;
  RideRowBookmarkBloc _rideRowBookmarkBloc;
  BookmarksTabBloc _usersBookmarkedRidesBloc;
  BottomNavigationBloc _bottomNavigationBloc;
  RideDetailsRedesignBloc _rideDetailsRedesignBloc;
  NotificationSubscriptionBloc _notificationSubscriptionBloc;
  AddRideProfileBloc _addRideProfileBloc;
  ProfileBloc _profileBloc;
  EditProfileBloc _editProfileBloc;

  StreamSubscription _sub;

  /// _accessTokenForOlderAndroidVersions is used for login flow on Android with API lower than 25

  static FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await _initializeDB();
      await initUniLinks();
    });

    super.initState();
  }

  @override
  void dispose() {
    disposeBlocs();
    _sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    initializeAllBlocs();

    MyApp.eventBus.on<InvalidTokenEvent>().listen((event) {
      _authenticationBloc.add(AuthenticationInvalidToken());
    });

    return RepositoryProvider(
      create: (context) => widget._authRepository,
      child: MultiBlocProvider(
        providers: _allBlocProviders(),
        child: MaterialApp(
          home: NavigationDelegationPage(
            accessTokenFromOnCreate:
            "", //todo - this is deprecated, can be removed.
          ),
          localizationsDelegates: _localizationDelegates(),
          navigatorObservers: [
            FirebaseAnalyticsObserver(analytics: analytics)
          ],
          supportedLocales: [Locale('sl', "SI")],
          onGenerateTitle: (BuildContext context) =>
          AppLocalizations.of(context).appTitle,
          theme: PrevozStyles.appThemeData(context),
        ),
      ),
    );
  }

  List<LocalizationsDelegate<dynamic>> _localizationDelegates() {
    return [
      AppLocalizationsDelegate(),
      GlobalMaterialLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate
    ];
  }

  List<BlocProvider> _allBlocProviders() {
    return [
      BlocProvider<RideBloc>(
          create: (BuildContext context) =>
              RideBloc(ridesRepo: widget._ridesRepository)),
      BlocProvider<LocationsBloc>(
          create: (BuildContext context) =>
              LocationsBloc(locationsRepo: widget._locationsRepository)),
      BlocProvider<RideDetailsBloc>(
          create: (BuildContext context) =>
              RideDetailsBloc(widget._ridesRepository)),
      BlocProvider<BookmarksTabBloc>(
          create: (BuildContext context) =>
              BookmarksTabBloc(widget._ridesRepository)),
      BlocProvider<BottomNavigationBloc>(
          create: (BuildContext context) => BottomNavigationBloc()),
      BlocProvider<RouteStatisticsBloc>(
          create: (BuildContext context) =>
              RouteStatisticsBloc(ridesRepo: widget._ridesRepository)),
      BlocProvider<BookmarksCardBloc>(
          create: (BuildContext context) =>
              BookmarksCardBloc(ridesRepo: widget._ridesRepository)),
      BlocProvider<HomePageBloc>(
          create: (BuildContext context) => HomePageBloc(
              ridesRepo: widget._ridesRepository,
              searchHistoryRepository: widget._searchHistoryRepository)),
      BlocProvider<AddRideBloc>(
          create: (BuildContext context) => AddRideBloc(
              widget._ridesRepository, widget._searchHistoryRepository)),
      BlocProvider<CountriesBloc>(
          create: (BuildContext context) =>
              CountriesBloc(widget._countriesRepository)),
      BlocProvider<AuthenticationBloc>(
          create: (BuildContext context) {
            _authenticationBloc = AuthenticationBloc(authRepository: widget._authRepository, analytics: FirebaseAnalytics());
            return _authenticationBloc;
          }),
      BlocProvider<RideRowBookmarkBloc>(
          create: (BuildContext context) =>
              RideRowBookmarkBloc(ridesRepo: widget._ridesRepository)),
      BlocProvider<UsersAddedRidesBloc>(
          create: (BuildContext context) =>
              UsersAddedRidesBloc(widget._ridesRepository)),
      BlocProvider<RideDetailsRedesignBloc>(
          create: (BuildContext context) =>
              RideDetailsRedesignBloc(ridesRepo: widget._ridesRepository)),
      BlocProvider<MyRidesListBloc>(
          create: (BuildContext context) =>
              MyRidesListBloc(widget._ridesRepository)),
      BlocProvider<NotificationSubscriptionBloc>(
          create: (BuildContext context) => NotificationSubscriptionBloc(
              repository: widget._notificationSubscriptionRepository)),
      BlocProvider<QuickEntriesBloc>(
          create: (BuildContext context) =>
              QuickEntriesBloc(widget._ridesRepository)),
      BlocProvider<AddRideProfileBloc>(
          create: (BuildContext context) =>
              AddRideProfileBloc(widget._userProfileRepository)),
      BlocProvider<ProfileBloc>(
          create: (BuildContext context) =>
              ProfileBloc(widget._userProfileRepository)),
      BlocProvider<EditProfileBloc>(
          create: (BuildContext context) =>
              EditProfileBloc(widget._userProfileRepository))
    ];
  }

  void disposeBlocs() {
    _ridesBloc.close();
    _addRideBloc.close();
    _homePageBloc.close();
    _countriesBloc.close();
    _locationsBloc.close();
    _rideDetailsBloc.close();
    _myRidesListBloc.close();
    _bookmarksCardBloc.close();
    _authenticationBloc.close();
    _quickEntriesBloc.close();
    _routeStatisticsBloc.close();
    _rideRowBookmarkBloc.close();
    _usersAddedRidesBloc.close();
    _bottomNavigationBloc.close();
    _rideDetailsRedesignBloc.close();
    _usersBookmarkedRidesBloc.close();

    _notificationSubscriptionBloc.close();
    _addRideProfileBloc.close();
    _profileBloc.close();
    _editProfileBloc.close();
  }

  void initializeAllBlocs() {
    _rideRowBookmarkBloc =
        RideRowBookmarkBloc(ridesRepo: widget._ridesRepository);
    _bottomNavigationBloc = BottomNavigationBloc();
    _ridesBloc = RideBloc(ridesRepo: widget._ridesRepository);
    _countriesBloc = CountriesBloc(widget._countriesRepository);
    _rideDetailsBloc = RideDetailsBloc(widget._ridesRepository);
    _bookmarksCardBloc = BookmarksCardBloc(ridesRepo: widget._ridesRepository);
    _routeStatisticsBloc =
        RouteStatisticsBloc(ridesRepo: widget._ridesRepository);
    _quickEntriesBloc = QuickEntriesBloc(widget._ridesRepository);
    _homePageBloc = HomePageBloc(
        ridesRepo: widget._ridesRepository,
        searchHistoryRepository: widget._searchHistoryRepository);
    _myRidesListBloc = MyRidesListBloc(widget._ridesRepository);
    _usersAddedRidesBloc = UsersAddedRidesBloc(widget._ridesRepository);
    _usersBookmarkedRidesBloc = BookmarksTabBloc(widget._ridesRepository);
    _locationsBloc = LocationsBloc(locationsRepo: widget._locationsRepository);
    _addRideBloc =
        AddRideBloc(widget._ridesRepository, widget._searchHistoryRepository);

    _notificationSubscriptionBloc = NotificationSubscriptionBloc(
        repository: widget._notificationSubscriptionRepository);

    _rideDetailsRedesignBloc =
        RideDetailsRedesignBloc(ridesRepo: widget._ridesRepository);

    _addRideProfileBloc =
        AddRideProfileBloc(widget._userProfileRepository);

    _profileBloc =
        ProfileBloc(widget._userProfileRepository);

    _editProfileBloc =
        EditProfileBloc(widget._userProfileRepository);
  }

  Future<void> _initializeDB() async {
    var db = widget.appDatabase;
    await db.initDB();
  }

  Future<Null> initUniLinks() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      String initialLink = await getInitialLink();
      _handleUniLink(initialLink);

      // Attach a listener to the stream
      _sub = getLinksStream().listen((String link) {
        _handleUniLink(link);
      }, onError: (err) {
       print(err);
      });
    } on PlatformException {
      // Handle exception by warning the user their action did not succeed
      // return?
    } catch (e) {
      print(e);
    }
  }

  void _handleUniLink(String link) {
    if (link == null) return;

    if (link.startsWith("https://prevoz.org/accounts/activate/")) {
      String activationToken = link.split("https://prevoz.org/accounts/activate/")[1].split("/")[0];
      _authenticationBloc.add(AuthenticationActivateAccount(activationToken: activationToken));
    }
  }
}
