
import 'dart:async';

import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:prevoz_org/data/helpers/auth/auth_helper.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:prevoz_org/utils/sign_in_with_apple_wrapper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppModule {

  Future<Injector> initialise(Injector injector) async {
    injector.map<SentryErrorReporter>((sentryErrorReporter) => SentryErrorReporter(), isSingleton: true);
    injector.map<BaseAuthHelper>((authHelper) => AuthHelper(), isSingleton: true);
    injector.map<SignInWithAppleWrapper>((signWithAppleWrapper) => SignInWithAppleWrapper(), isSingleton: true);
    injector.map<GoogleSignIn>((googleSignIn) => GoogleSignIn(scopes: <String>['email'], hostedDomain: ""), isSingleton: true);
    SharedPreferences sp = await SharedPreferences.getInstance();
    injector.map<SharedPreferences>((sharedPreferences) => sp, isSingleton: true);

    return injector;
  }
}