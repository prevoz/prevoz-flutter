

import 'dart:async';

import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/mocks/mock_google_sign_in.dart';
import 'package:prevoz_org/mocks/mock_sentry_error_reporter.dart';
import 'package:prevoz_org/mocks/mock_sign_in_with_apple_wrapper.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:prevoz_org/utils/sign_in_with_apple_wrapper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MockAppModule {

  Future<Injector> initialise(
      Injector injector,
      BaseAuthHelper baseAuthHelper,
      MockSignInWithAppleWrapper mockSignInWithAppleWrapper,
      MockGoogleSignIn mockGoogleSignIn
    ) async {
    injector.map<SentryErrorReporter>((sentryErrorReporter) => MockSentryErrorReporter(), isSingleton: true);
    injector.map<BaseAuthHelper>((authHelper) => baseAuthHelper, isSingleton: true);
    injector.map<SignInWithAppleWrapper>((signWithAppleWrapper) => mockSignInWithAppleWrapper, isSingleton: true);
    injector.map<GoogleSignIn>((googleSignIn) => mockGoogleSignIn, isSingleton: true);
    SharedPreferences sp = await SharedPreferences.getInstance();
    injector.map<SharedPreferences>((sharedPreferences) => sp, isSingleton: true);

    return injector;
  }
}