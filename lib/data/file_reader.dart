import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/data/models/location.dart';

class FileReader {
  /// Assumes the given path is a text-file-asset.
  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  Future<List<Location>> getLocationsFromTxtFile() async {
    String data = await getFileData("assets/locations.txt");
    List<Location> locations = [];
    List<String> locationsFromTxtFile = data.split('\n');

    for (String locationString in locationsFromTxtFile) {
      //51208,Murska Sobota,Murska Sobota,SI,46.66250000000000000 [lat],16.16639000000000000 [lng]
      List<String> locationSplitStrings = locationString.split(";");

      if (locationSplitStrings.length == 6) {
        Location location = Location(
          sortNumber: int.parse(locationSplitStrings[0]),
          name: locationSplitStrings[1],
          nameAscii: locationSplitStrings[2],
          country: locationSplitStrings[3],
          lat: double.parse(locationSplitStrings[4]),
          lng: double.parse(locationSplitStrings[5]),
        );

        locations.add(location);
      }
    }

    return locations;
  }

  Future<List<Country>> getCountriesFromTxtFile() async {
    String data = await getFileData("assets/countries.txt");
    List<Country> countries = [];
    List<String> countriesFromTxtFile = data.split('\n');

    for (String countryString in countriesFromTxtFile) {
      List<String> countrySplitStrings = countryString.split(",");

      if (countrySplitStrings.length == 3) {
        Country country = Country(
            countryCode: countrySplitStrings[0],
            name: countrySplitStrings[1],
            language: countrySplitStrings[2]);

        countries.add(country);
      }
    }

    return countries;
  }

  Future<String> get _localPath async {
    final directory = await getTemporaryDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    print(path);
    //assets/countries.txt"
    //return File('$path/countries.txt');
    return File('assets/countries.txt ');
  }

  Future<String> readFileAsString() async {
    String contents = "";
    final file = await _localFile;
    if (file.existsSync()) {
      //Must check or error is thrown
      //debugPrint("File exists");
      contents = await file.readAsString();
    }
    return contents;
  }

  Future<Null> writeFile(String text) async {
    final file = await _localFile;

    IOSink sink = file.openWrite(mode: FileMode.append);
    sink.add(utf8.encode('$text\n')); //Use newline as the delimiter
    await sink.flush();
    await sink.close();
  }
}
