import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';

abstract class BasePrevozApi {
  static BaseAuthHelper baseAuthHelper;

  void setApiToken(String apiToken);

  Future<dynamic> deleteRide(String id);

  Future<dynamic> addRide(RideModel ride);

  Future<dynamic> setFull(int rideId, String state);

  Future<dynamic> setRideBookmark(String id, String state);

  Future<RidesSearchResult> fetchRides(
      {@required RideSearchRequest searchRequest});

  Future<RouteStatisticsResult> fetchRouteStatistics(
      RouteStatisticsRequest request);

  Future<RidesSearchResult> fetchUsersAddedRides();

  Future<RidesSearchResult> fetchUsersBookmarkedRides();

  Future<List<RideModel>> getQuickRides();

  Future getAccessToken(String grantType, String clientId, String clientSecret,
      String code, String redirectUri);

  Future getAccountStatus();

  Future getUserProfile();

  Future postUserProfile(Map<String, dynamic> data);

  Future getRefreshedToken(String grantType, String refreshToken,
      String clientId, String clientSecret, String scope);

  Future<Response> exchangeBearerToken(String bearer);

  Future<Response> login(String username, String password);

  Future<Response> register({
    @required String username,
    @required String email,
    @required String password
  });

  Future<Response> activateAccount({@required String activationToken});

  Future<Response> uploadGoogleToken({@required String idToken});

  Future<Response> uploadAppleCode({@required String code});

  void configureFirebaseMessaging();

  Future setSubscriptionStatus(PrevozRoute route, DateTime date, String status);
}
