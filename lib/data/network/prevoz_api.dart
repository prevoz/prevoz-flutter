import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/helpers/event_bus/invalid_token_event.dart';
import 'package:prevoz_org/data/helpers/push_notifications/notification_helper.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/quick_ride.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';

import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';
import 'package:prevoz_org/data/network/handle_error.dart';

import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';

import '../../my_app.dart';

class PrevozApi implements BasePrevozApi {
  static const String BASE_URL = const String.fromEnvironment("BASE_URL", defaultValue: "https://prevoz.org");
  static const String LOGIN = "/api/auth/login/";
  static const String REGISTER = "/api/auth/register/";
  static const String ACTIVATE_ACCOUNT = "/api/auth/activate/";
  static const String SOCIAL_LOGIN = "/api/accounts/social/knox_user/";
  static const String SEARCH_SHARES = "/api/search/shares/?";
  static const String OTH_ACC_TOKEN = "/oauth2/access_token/";
  static const String AUTH_STATUS = "/api/accounts/status/";
  static const String REFRESH_TOKEN = "/oauth2/token/";
  static const String USERS_RIDES = "/api/carshare/list/";
  static const String USERS_BOOKMARKED_RIDES = "/api/search/bookmarks/";
  static const String ADD_RIDE = "/api/carshare/create/";
  static const String DELETE_RIDE = "/api/carshare/delete/";
  static const String SET_BOOKMARK = "/api/carshare/bookmark/";
  static const String USER_PROFILE = "/api/accounts/profile/";
  static const String ROUTES_STATS = "/api/search/aggregate/";
  static const String QUICK_RIDES = "/api/carshare/suggest/";
  static const String REGISTER_SUBSCRIPTION = "/api/c2dm/register/";
  static const String SET_FULL = "/api/carshare/full/";
  static const String CLIENT_ID = "QTwUBJLA8ZngFS5iK2h2kcV68qAftyLIi2gjXkIy";
  static const String CLIENT_SECRET =
      "qcPKCeIScAU8Ca009BwokX4xW86AhSaPZu1rqu2ZCygMPhsrG57sF1gMcryzCBqf2qwSuZpGFVkurl0ZtiVwLi62B3pLYoawTk2z0qX2PcSePZvVkrjGsntxSbxOroSc";
  static const String redirectUrl = "prevoz://auth/done/";

  Dio dio;
  Logger logger = Logger(printer: CustomLogPrinter("PrevozApi"));
  String _apiToken;

  PrevozApi() {
    BaseOptions baseOptions = BaseOptions()..baseUrl = BASE_URL;
    dio = Dio(baseOptions);

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      options.headers.addEntries([MapEntry("User-Agent", "Prevoz Flutter")]);
      if (_apiToken != null) {
        var entries = [
          MapEntry("Authorization", "Token $_apiToken"),
        ];

        options.headers.addEntries(entries);
      }
      return options;
    }, onResponse: (Response response) {
          if (response != null && response.statusCode == 401 && _apiToken != null) {
            MyApp.eventBus.fire(InvalidTokenEvent());
          }
      return response;
    }, onError: (DioError error) {
      if (error.response != null && error.response.statusCode == 401 && _apiToken != null) {
        MyApp.eventBus.fire(InvalidTokenEvent());
      }
      return error;
    }));
  }

  void setApiToken(String apiToken) {
    _apiToken = apiToken;
  }

  Future<RidesSearchResult> fetchRides(
      {@required RideSearchRequest searchRequest}) async {
    String url = _getFetchRidesQueryString(searchRequest: searchRequest);
    try {
      url = Uri.encodeFull(url);
      //await Future.delayed(Duration(seconds: 2));

      final response = await dio.get(url);

      if (response != null && response.statusCode == 200) {
        String jsonResult = json.encode(response.data);

        return RidesSearchResult.fromJson(json.decode(jsonResult));
      }
      throw Exception('fetchRides');
    } catch (e) {
      return handleApiError(error: e, message: "Fetching rides", apiToken: _apiToken);
    }
  }

  Future<RouteStatisticsResult> fetchRouteStatistics(
      RouteStatisticsRequest request) async {
    try {
      var routeStatsRequestJson = json.encode(request);

      Response response = await dio.post("$BASE_URL$ROUTES_STATS",
          data: routeStatsRequestJson,
          options:
              new Options(contentType: "application/json"));

      if (response.data != null) {
        RouteStatisticsResult result =
            RouteStatisticsResult.fromJson(response.data);

        //? Uncomment bellow to partially simulate issue #95
        // result.routeStatistics.removeLast();
        // if (result.routeStatistics.length > 3) {
        //   result.routeStatistics.removeAt(1);
        //   result.routeStatistics.removeAt(2);
        // }

        return result;
      }

      return null;
    } catch (e) {
      return handleApiError(error: e, message: "Fetching route statitistics", apiToken: _apiToken);
    }
  }

  Future<Response> deleteRide(String id) async {
    try {
      Response response = await dio.delete("$DELETE_RIDE$id/");
      return response;
    } catch (e) {
      return handleApiError(error: e, message: 'deleting ride', apiToken: _apiToken);
    }
  }

  Future<Response> addRide(RideModel ride) async {
    try {
      var rideJson = json.encode(ride);
      Response response = await dio.post("$BASE_URL$ADD_RIDE",
          data: rideJson,
          options:
              new Options(contentType: "application/json"));

      logger.i("add ride repsonse $response");
      return response;
    } catch (e) {
      return handleApiError(error: e, message: "adding ride", apiToken: _apiToken);
    }
  }

  Future<Response> setFull(int rideId, String state) async {
    try {
      var data = {"state": state};
      Response response = await dio.post("$BASE_URL$SET_FULL$rideId/",
          data: data,
          options: new Options(
              contentType: "application/x-www-form-urlencoded"));

      return response;
    } catch (e) {
      return handleApiError(error: e, message: "setting ride to full", apiToken: _apiToken);
    }
  }

  Future<Response> setSubscriptionStatus(
      PrevozRoute route, DateTime date, String status) async {
    var match = {
      "registration_id": NotificationHelper.fcmToken,
      "from": route.locationFrom,
      "fromcountry": route.countryFrom,
      "to": route.locationTo,
      "tocountry": route.countryTo,
      "date": DateTimeUtils.dateToStringForApiCalls(date),
      "action": status,
      "flutter": true
      //todo - add this when backend is ready... "flutterApp": true
    };

    try {
      logger.i(match.toString());
      logger.i(
          "_______ API call subscription, FCM token = {$NotificationHelper.fcmToken}");
      Response response = await dio.post("$BASE_URL$REGISTER_SUBSCRIPTION",
          data: match,
          options: new Options(
              contentType: "application/x-www-form-urlencoded"));

      logger.i(response.toString());

      return response;
    } catch (e) {
      return handleApiError(error: e, message: "setSubscriptionStatus(), PrevozAPI", apiToken: _apiToken);
    }
  }

  Future<Response> setRideBookmark(String id, String state) async {
    try {
      if (state == null) state = "erase";
      Map<String, String> stateMap = Map();
      stateMap['state'] = state;

      Response response = await dio.post("$SET_BOOKMARK$id/",
          data: json.encode(stateMap),
          options:
              new Options(contentType: "application/json"));

      return response;
    } catch (e) {
      return handleApiError(error: e, message: "setting ride bookmark", apiToken: _apiToken);
    }
  }

  Future<RidesSearchResult> fetchUsersAddedRides() async {
    try {
      final response = await dio.get("$USERS_RIDES");
      if (response != null) {
        String jsonResult = json.encode(response.data);
        return RidesSearchResult.fromJson(json.decode(jsonResult));
      }
      throw Exception('fetchUsersAddedRides');
    } catch (e) {
      return handleApiError(error: e, message: "Fetching users added rides", apiToken: _apiToken);
    }
  }

  Future<Response> exchangeBearerToken(String bearer) {
    try {
      dio.options.headers["Authorization"] =  "Bearer $bearer";
      return dio.post(LOGIN).catchError((error) {
        if (error is DioError) {
          return error.response;
        }
        throw Exception("Napaka pri menjavi žetona");
      });
    } catch (e) {
      return handleApiError(error: e, message: "Exchanging old bearer token", apiToken: _apiToken);
    }
  }

  Future<Response> login(String username, String password) {
    try {
      String credentials = "$username:$password";
      Codec<String, String> stringToBase64Url = utf8.fuse(base64Url);
      String encoded = stringToBase64Url.encode(credentials);
      dio.options.headers["Authorization"] =  "Basic $encoded";
      return dio.post(LOGIN).catchError((error) {
        if (error is DioError) {
          return error.response;
        }
        throw Exception("Napaka pri prijavi");
      });
    } catch (e) {
      return handleApiError(error: e, message: "Performing login", apiToken: _apiToken);
    }
  }

  Future<Response> register({
    @required String username,
    @required String email,
    @required String password
  }) {
    try {
      var data = {
        "username": username,
        "email": email,
        "password1": password,
        "password2": password
      };

      return dio.post(REGISTER, data: data, options: Options(
          contentType: "application/x-www-form-urlencoded")
      ).catchError((error) {
        if (error is DioError) {
          return error.response;
        }
        throw Exception("Napaka pri registraciji");
      });
    } catch (e) {
      return handleApiError(error: e, message: "Performing registration", apiToken: _apiToken);
    }
  }

  Future<Response> activateAccount({@required String activationToken}) {
    try {
      return dio.get("$ACTIVATE_ACCOUNT$activationToken/").catchError((error) {
        if (error is DioError) {
          return error.response;
        }
        throw Exception("Napaka pri aktiviranju računa");
      });
    } catch (e) {
      return handleApiError(error: e, message: "Activating account", apiToken: _apiToken);
    }
  }

  Future<Response> uploadGoogleToken({@required String idToken}) async {
    try {
      var data = {
        "provider": "google-plus",
        "id_token": idToken
      };

      return dio.post(SOCIAL_LOGIN, data: data, options: Options(
          contentType: "application/x-www-form-urlencoded")
      ).catchError((error) {
        if (error is DioError) {
          return error.response;
        }
        throw Exception("Napaka pri pošiljanju Google token");
      });
    } catch (e) {
      return handleApiError(error: e, message: "Uploading google id token", apiToken: _apiToken);
    }
  }

  Future<Response> uploadAppleCode({@required String code}) {
    try {
      var data = {
        "provider": "apple-id",
        "code": code
      };
      return dio.post(SOCIAL_LOGIN, data: data, options: Options(
          contentType: "application/x-www-form-urlencoded")
      ).catchError((error) {
        if (error is DioError) {
          return error.response;
        }
        throw Exception("Napaka pri pošiljanju Apple code");
      });
    } catch (e) {
      return handleApiError(error: e, message: "Uploading apple code", apiToken: _apiToken);
    }
  }

  //fetchUsersBookmarkedRides
  Future<RidesSearchResult> fetchUsersBookmarkedRides() async {
    try {
      final response = await dio.get("$USERS_BOOKMARKED_RIDES" +
          "?nocache=" +
          DateTime.now().millisecondsSinceEpoch.toString());
      if (response != null) {
        String jsonResult = json.encode(response.data);

        return RidesSearchResult.fromJson(json.decode(jsonResult));
      }
      throw Exception('Napaka pri nalaganju prevozov');
    } catch (e) {
      return handleApiError(error: e, message: "Fetching users bookmarked rides", apiToken: _apiToken);
    }
  }

  Future getAccessToken(String grantType, String clientId, String clientSecret,
      String code, String redirectUri) async {
    var match = {
      "grant_type": grantType,
      "client_id": clientId,
      "client_secret": clientSecret,
      "code": code,
      "redirect_uri": redirectUri
    };

    try {
      return dio.post("$BASE_URL$OTH_ACC_TOKEN",
          data: match,
          options: new Options(
              contentType: "application/x-www-form-urlencoded"));
    } catch (e) {
      return handleApiError(error: e, message: "Getting access token", apiToken: _apiToken);
    }
  }

  Future getAccountStatus() {
    try {
      return dio.get(AUTH_STATUS, options: Options());
    } catch (e) {
      return handleApiError(error: e, message: "Getting account status", apiToken: _apiToken);
    }
  }

  Future getUserProfile() async {
    try {
      return dio.get(USER_PROFILE, options: Options());
    } catch (e) {
      return handleApiError(error: e, message: "Getting users profile", apiToken: _apiToken);
    }
  }

  @override
  Future postUserProfile(Map<String, dynamic> data) {
    try {
      return dio.post(USER_PROFILE, data: data, options: Options(
          contentType: "application/x-www-form-urlencoded"));
    } catch (e) {
      return handleApiError(error: e, message: "Posting users profile", apiToken: _apiToken);
    }
  }

  Future getRefreshedToken(String grantType, String refreshToken,
      String clientId, String clientSecret, String scope) {
    var match = {
      "grant_type": grantType,
      "client_id": clientId,
      "client_secret": clientSecret,
      "refresh_token": refreshToken,
      "scope": scope
    };

    try {
      return dio.post("$BASE_URL$REFRESH_TOKEN",
          data: match,
          options: new Options(
              contentType: "application/x-www-form-urlencoded"));
    } catch (e) {
      return handleApiError(error: e, message: "Getting refreshed token", apiToken: _apiToken);
    }
  }

  String _getFetchRidesQueryString(
      {@required RideSearchRequest searchRequest}) {
    bool exact = searchRequest.exact == null ? false : searchRequest.exact;
    bool intl = searchRequest.intl == null ? false : searchRequest.intl;
    String from = searchRequest.route.locationFrom;
    String to = searchRequest.route.locationTo;
    String fc = searchRequest.route.countryFrom;
    String tc = searchRequest.route.countryTo;
    String date = DateTimeUtils.dateToStringForApiCalls(searchRequest.date);

    return "$BASE_URL$SEARCH_SHARES" +
        (searchRequest.route.locationFrom != null ? "&f=$from" : "") +
        (searchRequest.route.countryFrom != null ? "&fc=$fc" : "") +
        (searchRequest.route.locationTo != null ? "&t=$to" : "") +
        (searchRequest.route.countryTo != null ? "&tc=$tc" : "") +
        (searchRequest.date != null ? "&d=$date" : "") +
        ("&exact=$exact") +
        ("&intl=$intl") +
        "&nocache=" +
        DateTime.now().millisecondsSinceEpoch.toString();
  }

  @override
  void configureFirebaseMessaging() {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      logger.i("_______push notification: onMessage");
      logger.i(message.toString());
    }, onResume: (Map<String, dynamic> message) async {
      logger.i("_______push notification: onResume");
      logger.i(message.toString());
    }, onLaunch: (Map<String, dynamic> message) async {
      logger.i("_______push notification: onLaunch");
      logger.i(message.toString());
    });

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));

    _firebaseMessaging.getToken().then((token) {
      logger.i("______ FCM token: $token");
      NotificationHelper.fcmToken = token;
    });
  }

  @override
  Future<List<RideModel>> getQuickRides() async {
    try {
      final response = await dio.get("$QUICK_RIDES");
      if (response != null) {
        QuickRideResponse quickRidesResponse =
            QuickRideResponse.fromJson(response.data);
        return quickRidesResponse.getRides;
      }
      throw Exception('fetchQuickRides');
    } catch (e) {
      return handleApiError(error: e, message: "fetching quick rides", apiToken: _apiToken);
    }
  }
}
