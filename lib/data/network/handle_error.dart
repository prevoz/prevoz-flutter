import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/helpers/event_bus/invalid_token_event.dart';
import 'package:prevoz_org/my_app.dart';


handleError({@required dynamic error, @required String message, String apiToken}) {
  String exceptionMessage = 'Error occured';

  if (error is DioError) {
    if (error.response != null) {
      if (apiToken != null && error.response.statusCode == 401) {
        MyApp.eventBus.fire(InvalidTokenEvent());
      }

      if (error.response.data['message'] != null) {
        exceptionMessage = error.response.data['message'];
      } else {
        exceptionMessage =
            'Error occured in server while $message' + error.toString();
      }
    } else {
      exceptionMessage = 'Error connecting to server';
    }
  } else {
    if (error != null) {
      String errTxt = error.toString();
      exceptionMessage = 'Error $errTxt occured while $message';
    } else {
      exceptionMessage = 'Error occured while $message';
    }
  }

  print("$exceptionMessage");
  throw Exception(exceptionMessage);
}

handleApiError({@required dynamic error, @required String message, @required String apiToken}) {
  handleError(error: error, message: message, apiToken: apiToken);
}