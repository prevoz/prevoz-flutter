import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

/// a search result can contain many different routes, here we group them together
class RidesForRoute {
  final PrevozRoute route;
  final List<RideModel> rides;

  RidesForRoute({@required this.route, @required this.rides})
      : assert(route != null, rides != null);

  static bool areRoutesEqual(PrevozRoute route1, PrevozRoute route2) {
    String firstRoute = route1.locationFrom + "-" + route1.locationTo;
    String secondRoute = route2.locationFrom + "-" + route2.locationTo;
    if (firstRoute == secondRoute) {
      return true;
    }

    return false;
  }
}
