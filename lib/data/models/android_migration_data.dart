import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';

class AndroidMigrationData {
  List<PrevozRoute> searchHistory;
  List<NotificationSubscription> notifications;
  String dbPath;
}
