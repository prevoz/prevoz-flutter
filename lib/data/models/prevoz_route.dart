import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class PrevozRoute {
  int id;
  String locationFrom;
  String countryFrom;
  String locationTo;
  String countryTo;
  String searchDate;

  PrevozRoute(
      {this.id,
      this.locationFrom,
      this.locationTo,
      this.countryFrom,
      this.countryTo,
      this.searchDate});

  PrevozRoute.fromDb(Map map)
      : id = map["id"],
        locationFrom = map["locationFrom"],
        locationTo = map["locationTo"],
        countryFrom = map["countryFrom"],
        countryTo = map["countryTo"],
        searchDate = map["searchDate"];

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['locationFrom'] = locationFrom;
    map['locationTo '] = locationTo;
    map['countryFrom'] = countryFrom;
    map['countryTo'] = countryTo;
    map['searchDate'] = searchDate;

    return map;
  }

  PrevozRoute.fromOldAndroidDb(Map map)
      : locationFrom = map["l_from"],
        locationTo = map["l_to"],
        countryFrom = map["c_from"],
        countryTo = map["c_to"],
        searchDate =
            DateTime.fromMillisecondsSinceEpoch(map["date"]).toString();

  @override
  String toString() {
    return "$locationFrom -> $locationTo";
  }

  static bool areRoutesTheSame(
      PrevozRoute firstRoute, PrevozRoute secondRoute) {
    if (firstRoute.locationTo == secondRoute.locationTo &&
        firstRoute.locationFrom == secondRoute.locationFrom) {
      return true;
    }

    return false;
  }

  static bool isOpositeRoute(PrevozRoute firstRoute, PrevozRoute secondRoute) {
    try {
      if (firstRoute.locationTo == secondRoute.locationFrom &&
          firstRoute.locationFrom == secondRoute.locationTo) {
        return true;
      }
    } catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
          "isOppositeRoute(), PrevozRoute: $firstRoute - $secondRoute",
          StackTrace.fromString(e.toString()));
    }

    return false;
  }

  getRouteName() {
    try {
      return "From " + this.locationFrom + " -> to " + this.locationTo;
    } catch (e) {
      return "err, " + e.toString();
    }
  }

  bool containsSublocation() {
    if (locationFrom.contains("(") || locationTo.contains("(")) {
      return true;
    }
    return false;
  }

  static String getRouteStringFromRide(RideModel ride) {
    String from = ride.from;

    if (ride.isFromSubLocation) {
      from = from + " (" + ride.fromSublocation + ")";
    }

    String to = ride.to;

    if (ride.isToSubLocation) {
      to = to + " (" + ride.toSublocation + ")";
    }

    return from + " > " + to;
  }

  static bool isRouteStringLong(PrevozRoute route, double deviceWidth) {
    try {
      String routeString = route.locationFrom + route.locationTo;

      int stringTooLongMeasure = 22;

      if (deviceWidth < 340) {
        stringTooLongMeasure = 18;
      }

      if (routeString.length > stringTooLongMeasure) {
        return true;
      }

      return false;
    } catch (e) {
      handleError(error: e, message: "PrevozRoute, isRouteStringLong");
      return false;
    }
  }

  static bool isOneLocationEmpty(PrevozRoute route) {
    if (route.locationFrom == null ||
        route.locationFrom == "" ||
        route.locationFrom.length < 2) {
      return true;
    }

    if (route.locationTo == null ||
        route.locationTo == "" ||
        route.locationTo.length < 2) {
      return true;
    }

    return false;
  }

  static PrevozRoute getSwapedRoute(PrevozRoute beforeRoute) {
    try {
      return PrevozRoute(
          locationFrom: beforeRoute.locationTo,
          locationTo: beforeRoute.locationFrom,
          countryFrom: beforeRoute.countryTo,
          countryTo: beforeRoute.countryFrom);
    } catch (e) {
      handleError(error: e, message: "error getting swaped route");
      return beforeRoute;
    }
  }
}
