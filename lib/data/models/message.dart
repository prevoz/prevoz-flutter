import 'package:meta/meta.dart';

class Message {
  final String title;
  final String body;

  Message({@required this.title, @required this.body});
}
