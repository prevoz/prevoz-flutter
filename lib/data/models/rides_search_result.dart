import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/network/handle_error.dart';

class RidesSearchResult {
  List<RideModel> _rides = [];

  RidesSearchResult();

  RidesSearchResult.fromJson(Map<String, dynamic> parsedJson) {
    print(parsedJson["carshare_list"].length);

    List<RideModel> temp = [];
    for (int i = 0; i < parsedJson["carshare_list"].length; i++) {
      RideModel ride = RideModel(parsedJson["carshare_list"][i]);
      temp.add(ride);
    }
    _rides = temp;
  }

  List<RideModel> get getRides => _rides;

  void addRide(RideModel ride) {
    _rides.add(ride);
  }

  void removeRide(int rideId) {
    for (var i = 0; i < _rides.length; i++) {
      if (_rides[i].id == rideId) {
        _rides.removeAt(i);
      }
    }
  }

  /*String getLocalizedFrom() {
    //? add localized city names in the future??
  }

  String getLocalizedTo() {
    //? add localized city names in the future??
  }*/
}

class RideModel {
  int type = 0; // 0 - share, 1 - seek
  int id, numPeople;
  int scorePositive;
  int scoreNegative;
  double price;
  double drajvScore;
  String from;
  String fromSublocation; // (Center), (Šiška), (Merkur)
  String fromCountry;
  String to;
  String toSublocation; // (Center), (Šiška), (Merkur)
  String toCountry;
  String contact;
  String author;
  String carInfo;
  String bookmark;
  String comment;
  String localizedFromCity;
  String localizedToCity;
  String time;
  String businessName;
  bool full,
      confirmedContact,
      insured,
      isAuthor,
      businessActive,
      picksMidway,
      isFromSubLocation, // ex.: Ljubljana (Center)
      isToSubLocation; // ex.: Ljubljana (Center)
  DateTime dateIso8601, added;
  String dateTextForDisplay;
  String priceForDisplay;
  String date;
  PrevozRoute route;
  // Added verification fields
  String firstName;
  String lastName;
  String address;
  bool extended = false;

  RideModel.fromQuickRideResult(Map<String, dynamic> mappedJson) {
    this.from = mappedJson["transpfrom"];
    this.fromCountry = mappedJson["transpfromcountry"];
    this.to = mappedJson["transpto"];
    this.toCountry = mappedJson["transptocountry"];
    this.price = mappedJson["transpprice"];
    this.numPeople = mappedJson["transpppl"];
    this.type = mappedJson["transptype"];
    this.time = mappedJson["transptime"];
    this.contact = mappedJson["transpphone"];
    this.comment = mappedJson["transpdescr"];
    this.carInfo = mappedJson["transpcarinfo"];
    //todo bool insured = mappedJson[""];
    //todo bool picksMidway = mappedJson[""];
  }

  RideModel.fromCreateForm(
      String from,
      String fromCountry,
      String to,
      String toCountry,
      double price,
      int numOfPeople,
      int type,
      DateTime added,
      String date,
      String time,
      String contact,
      bool insured,
      bool picksMidway,
      String comment,
      String author) {
    this.price = price;
    this.numPeople = numOfPeople;
    this.confirmedContact = true;
    this.isAuthor = true;
    this.from = from;
    this.to = to;
    this.fromCountry = fromCountry;
    this.toCountry = toCountry;
    this.added = added;
    this.date = date;
    this.time = time;
    this.contact = contact;
    this.insured = insured;
    this.picksMidway = picksMidway;
    this.comment = comment;
    this.type = type;
    this.author = author;
  }

  RideModel(rideJson) {
    try {
      if (rideJson["type"] is String) {
        rideJson["type"] = int.parse(rideJson["type"]);
      }
      type = rideJson["type"]; // 0 - share, 1 - seek

      if (rideJson["id"] is String) {
        rideJson["id"] = int.parse(rideJson["id"]);
      }
      id = rideJson["id"];

      if (rideJson["num_people"] is double) {
        double numPpl = rideJson["num_people"];
        numPeople = numPpl.round();
      } else {
        numPeople = rideJson["num_people"];
      }

      if (rideJson["price"] != null) {
        price = double.parse(rideJson["price"].toString());
      }

      if (rideJson["score_negative"] != null) {
        scoreNegative = rideJson["score_negative"];
      }

      if (rideJson["score_positive"] != null) {
        scorePositive = rideJson["score_positive"];
      }

      if (rideJson["drajv_score"] != null) {
        drajvScore = double.parse(rideJson["drajv_score"].toString());
      }

      from = rideJson["from"];
      fromCountry = rideJson["from_country"];
      to = rideJson["to"];
      toCountry = rideJson["to_country"];
      contact = rideJson["contact"];
      author = rideJson["author"];
      carInfo = rideJson["car_info"];
      bookmark = rideJson["bookmark"];
      time = rideJson["time"];
      comment = rideJson["comment"];
      localizedFromCity = rideJson["_localizedFromCity"];
      localizedToCity = rideJson["_localizedToCity"];
      date = rideJson["date"];
      full = rideJson["full"] == "true" ? true : false;
      confirmedContact = rideJson["confirmed_contact"] == "true" ? true : false;
      insured = rideJson["insured"] == "true" ? true : false;
      picksMidway = rideJson["picks_midway"];
      isAuthor = rideJson["is_author"] == "true" ? true : false;

      dateIso8601 = DateTime.parse(rideJson["date_iso8601"]);
      added = DateTime.parse(rideJson["added"]);

      /**/
      businessName =
          rideJson["business_name"] != null ? rideJson["business_name"] : null;

      businessActive = (rideJson["business_active"] != null &&
              rideJson["business_active"] == "true")
          ? true
          : false; /**/

      //* turn sublocations to locations for better result grouping
      //* and add sublocation to a seperate field
      if (from.contains("(")) {
        try {
          isFromSubLocation = true;
          fromSublocation = from.split("(")[1];
          fromSublocation = fromSublocation.replaceAll(")", "");
          from = from.split(" (")[0];
        } catch (e) {
          isFromSubLocation = false;
          fromSublocation = null;
        }
      } else {
        isFromSubLocation = false;
        fromSublocation = null;
      }

      //* turn sublocations to locations for better result grouping
      //* and add sublocation to a seperate field
      if (to.contains("(")) {
        try {
          isToSubLocation = true;
          toSublocation = to.split("(")[1];
          toSublocation = toSublocation.replaceAll(")", "");
          to = to.split(" (")[0];
        } catch (e) {
          isToSubLocation = false;
          toSublocation = null;
        }
      } else {
        isToSubLocation = false;
        toSublocation = null;
      }

      route = PrevozRoute(locationFrom: from, locationTo: to);
    } catch (e) {
      handleError(error: e, message: "while transformin json to RideModel");
    }
  }

  factory RideModel.setupRideAfterPosting(RideModel rideModel) {
    if (rideModel.from.contains("(")) {
      try {
        rideModel.isFromSubLocation = true;
        rideModel.fromSublocation = rideModel.from.split("(")[1];
        rideModel.fromSublocation =
            rideModel.fromSublocation.replaceAll(")", "");
        rideModel.from = rideModel.from.split(" (")[0];
      } catch (e) {
        rideModel.isFromSubLocation = false;
        rideModel.fromSublocation = null;
      }
    } else {
      rideModel.isFromSubLocation = false;
      rideModel.fromSublocation = null;
    }

    if (rideModel.to.contains("(")) {
      try {
        rideModel.isToSubLocation = true;
        rideModel.toSublocation = rideModel.to.split("(")[1];
        rideModel.toSublocation = rideModel.toSublocation.replaceAll(")", "");
        rideModel.to = rideModel.to.split(" (")[0];
      } catch (e) {
        rideModel.isToSubLocation = false;
        rideModel.toSublocation = null;
      }
    } else {
      rideModel.isToSubLocation = false;
      rideModel.toSublocation = null;
    }
    return rideModel;
  }

  static DateTime buildDateTimeIsoFromStrings(String dateTime, String time) {
    try {
      List<String> yearMonthDay = dateTime.split("-");
      List<String> hourMinute = time.split(":");

      return DateTime(
          int.parse(yearMonthDay[0]),
          int.parse(yearMonthDay[1]),
          int.parse(yearMonthDay[2]),
          int.parse(hourMinute[0]),
          int.parse(hourMinute[1]));
    } catch (e) {
      return DateTime.now();
    }
  }

  RideModel.empty();

  bool isCommentPresent() {
    if (comment != null && comment.length > 0) {
      return true;
    }

    return false;
  }

  Map<String, dynamic> toJson() => {
        'price': price,
        'num_people': numPeople,
        'from': from,
        'to': to,
        'from_country': fromCountry,
        'to_country': toCountry,
        'added': added != null ? added.toIso8601String() : "",
        'date': date,
        'contact': contact,
        'insured': insured,
        'picks_midway': picksMidway,
        'full': full,
        'comment': comment,
        'type': type,
        'time': time,
        'id': id ?? null,
        'car_info': carInfo,
        'first_name': firstName,
        'last_name': lastName,
        'address': address,
        'extended': extended
      };

  /// includes sublocation if present
  static String getFullFromName(RideModel ride) {
    String from = ride.from;
    if (ride.isFromSubLocation) {
      from = from + " (" + ride.fromSublocation + ")";
    }

    return from;
  }

  /// includes sublocation if present
  static String getFullToName(RideModel ride) {
    String to = ride.to;
    if (ride.isToSubLocation) {
      to = to + " (" + ride.toSublocation + ")";
    }

    return to;
  }
}
