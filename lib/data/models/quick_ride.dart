import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

class QuickRideResponse {
  List<RideModel> _rides = [];
  List<RideModel> get getRides => _rides;

  QuickRideResponse();

  QuickRideResponse.fromJson(Map<String, dynamic> parsedJson) {
    print(parsedJson["results"].length);

    List<RideModel> temp = [];
    for (int i = 0; i < parsedJson["results"].length; i++) {
      Map<String, dynamic> ride = parsedJson["results"][i];
      temp.add(RideModel.fromQuickRideResult(ride));
    }
    _rides = temp;
  }
}

class QuickRide {
  final PrevozRoute route;
  final DateTime date;

  QuickRide({this.route, this.date}) : assert(route != null, date != null);
}
