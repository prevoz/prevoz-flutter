import 'package:meta/meta.dart';
import 'package:prevoz_org/data/helpers/database/database_commons.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';

class NotificationSubscription {
  PrevozRoute route;
  DateTime rideDate;

  NotificationSubscription({@required this.route, @required this.rideDate});

  NotificationSubscription.fromDb(Map map) {
    try {
      this.route = PrevozRoute(
          locationFrom: map[DatabaseCommons.notificationLocationFromColumn],
          countryFrom: map[DatabaseCommons.notificationCountrFromColumn],
          locationTo: map[DatabaseCommons.notificationLocationToColumn],
          countryTo: map[DatabaseCommons.notificationCountryToColumn]);
      this.rideDate = DateTime.fromMillisecondsSinceEpoch(
          map[DatabaseCommons.notificationRideDateColumn]);
    } catch (e) {
      handleError(error: e, message: "ReigstedNotification.fromDB");
    }
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map[DatabaseCommons.notificationLocationFromColumn] = route.locationFrom;
    map[DatabaseCommons.notificationLocationToColumn] = route.locationTo;
    map[DatabaseCommons.notificationCountrFromColumn] = route.countryFrom;
    map[DatabaseCommons.notificationCountryToColumn] = route.countryTo;
    map[DatabaseCommons.notificationRideDateColumn] =
        DateTimeUtils.startOfDayToMilisecSinceEpoch(rideDate);
    return map;
  }

  NotificationSubscription.fromOldAndroidDb(Map map) {
    try {
      this.route = PrevozRoute(
          locationFrom: map['l_from'],
          countryFrom: map['c_from'],
          locationTo: map['l_to'],
          countryTo: map['c_to']);
      this.rideDate = DateTime.fromMillisecondsSinceEpoch(map["date"]);
    } catch (e) {
      handleError(error: e, message: "ReigstedNotification.fromDB");
    }
  }
}
