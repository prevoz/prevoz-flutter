class Location {
  int id;
  int sortNumber;
  String name;
  String nameAscii;
  String country;
  double lat;
  double lng;

  Location(
      {this.id,
      this.sortNumber,
      this.name,
      this.nameAscii,
      this.country,
      this.lat,
      this.lng});

  Location.fromDb(Map map)
      : id = map["id"],
        sortNumber = map["sortNumber"],
        name = map["name"],
        nameAscii = map["nameAscii"],
        country = map["country"],
        lat = map["lat"],
        lng = map["lng"];

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['sortNumber'] = sortNumber;
    map['name '] = name;
    map['nameAscii'] = nameAscii;
    map['country'] = country;
    map['lat'] = lat;
    map['lng'] = lng;

    return map;
  }
}
