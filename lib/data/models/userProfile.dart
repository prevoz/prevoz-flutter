
import 'package:dio/dio.dart';

class UserProfile {
  String username;
  String email;
  String firstName;
  String lastName;
  String address;
  // TODO move phoneNumbers here maybe?

  UserProfile({
    this.username,
    this.email,
    this.firstName,
    this.lastName,
    this.address,
  });

  @override
  String toString() {
    return "$username, $email, $firstName, $lastName, $address";
  }

  UserProfile.fromResponse(Response userProfileResponse) { // TODO add safety check
    username = userProfileResponse.data["username"];
    email = userProfileResponse.data["email"];
    firstName = userProfileResponse.data["first_name"];
    lastName = userProfileResponse.data["last_name"];
    address = userProfileResponse.data["address"];
  }

  Map<String, dynamic> toJson() => {
    "username": username,
    "email": email,
    "first_name": firstName,
    "last_name": lastName,
    "address": address
  };

}