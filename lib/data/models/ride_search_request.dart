import 'package:prevoz_org/data/models/prevoz_route.dart';

class RideSearchRequest {
  final PrevozRoute route;
  final DateTime date;
  bool exact = false;
  bool intl = false;

  RideSearchRequest({this.route, this.date, this.exact, this.intl})
      : assert(route != null, date != null);
}
