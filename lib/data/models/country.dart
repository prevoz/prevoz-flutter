class Country {
  String countryCode;
  String language;
  String name;
  String searchDate;

  Country({this.countryCode, this.language, this.name, this.searchDate});

  Map<String, dynamic> toMapSearchHistory() {
    var map = Map<String, dynamic>();
    map['countryCode'] = countryCode;
    map['language '] = language;
    map['name'] = name;
    map['searchDate'] = searchDate;

    return map;
  }

  Map<String, dynamic> toMapRegular() {
    var map = Map<String, dynamic>();
    map['countryCode'] = countryCode;
    map['language '] = language;
    map['name'] = name;

    return map;
  }

  Country.fromDbSearchHistory(Map map)
      : countryCode = map["countryCode"],
        language = map["language"],
        name = map["name"],
        searchDate = map["searchDate"];

  Country.fromDbRegular(Map map)
      : countryCode = map["countryCode"],
        language = map["language"],
        name = map["name"];
}
