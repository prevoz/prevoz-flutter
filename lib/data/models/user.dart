import 'package:meta/meta.dart';

class User {
  String username;
  String accessToken;
  String refreshToken;
  int expiryTimestamp;
  int lastLoginTimestamp;
  bool oauth2;
  List<dynamic> phoneNumbers;

  User(
      {@required this.username,
      this.accessToken,
      this.refreshToken,
      this.lastLoginTimestamp,
      this.expiryTimestamp,
      this.oauth2,
      this.phoneNumbers});

  @override
  String toString() {
    return "$username, $accessToken, $refreshToken, $expiryTimestamp, $lastLoginTimestamp, $oauth2, $phoneNumbers";
  }
}
