enum AddRideInputType {
  location,
  date,
  time,
  numOfPpl,
  price,
  carDescription,
  phoneNumber,
  comment
}
