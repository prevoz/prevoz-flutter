import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class DateWithCount {
  String date;
  int count;

  DateWithCount.fromJson(json) {
    try {
      date = json['date'];
      count = json['count'];
    } catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
          "DateWithCount, fromJson()", StackTrace.fromString(e.toString()));
    }
  }

  @override
  String toString() {
    return "Date: $date, count: $count";
  }
}

class RouteStatistics {
  PrevozRoute route;
  List<DateWithCount> datesWithCount = [];

  RouteStatistics({@required this.route, @required this.datesWithCount});

  RouteStatistics.fromJson(json) {
    //{from: Zasavje, from_country: SI, to: Koper, to_country: SI, dates: {2019-08-22: 0, 2019-08-23: 0, 2019-08-24: 0}}
    try {
      route = PrevozRoute(
          locationFrom: json['from'],
          countryFrom: json['from_country'],
          locationTo: json['to'],
          countryTo: json['to_country']);

      List<dynamic> dwc = json['dates'];
      dwc.forEach((dejtWCount) {
        DateWithCount dateWithCount = DateWithCount.fromJson(dejtWCount);
        datesWithCount.add(dateWithCount);
      });
    } catch (e) {
      print("____RouteStatistics.fromJson: " + e.toString());
    }
  }

  @override
  String toString() {
    int datesWithCountLength = datesWithCount.length;
    return "RouteStatistics: route = $route ... dates with count: length =  $datesWithCountLength ... ";
  }
}

class RouteStatisticsResult {
  List<RouteStatistics> routeStatistics;

  RouteStatisticsResult.fromJson(Map<String, dynamic> parsedJson) {
    try {
      List<Object> results = parsedJson["results"];
      routeStatistics = [];

      if (results != null) {
        results.forEach((routeStatsMap) {
          RouteStatistics routeStats = RouteStatistics.fromJson(routeStatsMap);
          routeStatistics.add(routeStats);
        });
      }
    } catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
          "Error parsing route stats", StackTrace.fromString(e.toString()));
    }
  }

  /*  
  Returns:
    * `results`: [
        {'from': ..,
         'from_country': ..,
         'to': ..,
         'to_country': ..,
         dates: {
                    'YYYY-MM-DD': 50,
                    'YYYY-MM-DD': 15,
                    'YYYY-MM-DD': 5
         }
       },
       { .. }
       ..
    ]
  */

}

class RouteStatisticsRequest {
  final List<PrevozRoute> routes;
  final List<DateTime> dates;

  RouteStatisticsRequest({@required this.routes, @required this.dates});

  /// compares all routes of two seperate requests
  static bool isLatestRequestEqualToNewOne(
      {RouteStatisticsRequest latestRequest,
      RouteStatisticsRequest newRequest}) {
    if ((latestRequest == null && newRequest != null) ||
        (newRequest == null && latestRequest != null)) {
      return false;
    }

    if (latestRequest.routes.length != newRequest.routes.length) {
      return false;
    }

    int routeLength = latestRequest.routes.length;

    List<PrevozRoute> latestRoutes = latestRequest.routes;
    List<PrevozRoute> newRoutes = newRequest.routes;

    for (int i = 0; i < routeLength; i++) {
      //check each routes location
      if (latestRoutes[i].locationFrom != newRoutes[i].locationFrom ||
          latestRoutes[i].locationTo != newRoutes[i].locationTo) {
        return false;
      }
    }

    return true;
  }

  List<Object> routesToLocationObjects() {
    try {
      List<Object> locations = [];

      routes.forEach((PrevozRoute route) {
        locations.add({
          'f': route.locationFrom,
          'fc': route.countryFrom,
          't': route.locationTo,
          'tc': route.countryTo
        });
      });

      return locations;
    } catch (e) {
      print("____routesToLocationObjects, " + e.toString());
      return null;
    }
  }

  List<String> datesToStrings() {
    try {
      List<String> stringdates = [];
      dates.forEach((DateTime datetime) {
        String stringForApiCall = ""; //2019-08-22:
        String year = datetime.year.toString();
        String month = datetime.month.toString();
        String day = datetime.day.toString();
        if (datetime.month < 10) {
          month = "0" + month.toString();
        }
        if (datetime.day < 10) {
          day = "0" + day.toString();
        }
        stringForApiCall = "$year-$month-$day";
        stringdates.add(stringForApiCall);
      });

      return stringdates;
    } catch (e) {
      print("_____datesToStrings, " + e.toString());
      return null;
    }

    //['2019-08-22', '2019-08-23', '2019-08-24']
  }

  Map<String, dynamic> toJson() => {
        'locations': routesToLocationObjects(),
        'dates': datesToStrings(),

        /*
        `locations`: [ {'f': .., 'fc': .. , 't': .. 'tc': .. }, { .. } ]
        `dates`: [ 'YYYY-MM-DD', 'YYYY-MM-DD', 'YYYY-MM-DD' ]
        Returns:
        `results`: [
            {'from': ..,
            'from_country': ..,
            'to': ..,
            'to_country': ..,
            dates: {
                        'YYYY-MM-DD': 50,
                        'YYYY-MM-DD': 15,
                        'YYYY-MM-DD': 5
            }
          },
          { .. }
          ..
        ]
        */
      };
}
