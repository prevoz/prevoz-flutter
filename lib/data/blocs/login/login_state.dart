
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class InitialLoginState extends LoginState {
  const InitialLoginState();
  @override
  String toString() => 'InitialLoginState';
  @override
  List<Object> get props => [];
}

class LoginErrorGmail extends LoginState {
  const LoginErrorGmail();
  @override
  String toString() => 'LoginErrorGmail';
  @override
  List<Object> get props => [];
}

class SubmittingLogin extends LoginState {
  const SubmittingLogin();
  @override
  String toString() => 'SubmittingLogin';
  @override
  List<Object> get props => [];
}

class SuccessfullySubmittedLogin extends LoginState {
  final String token;
  const SuccessfullySubmittedLogin(this.token);
  @override
  String toString() => 'SuccessfullySubmittedLogin';
  @override
  List<Object> get props => [token];
}

class ErrorSubmittingLogin extends LoginState {
  final Response response;
  final bool wasFromGoogle;
  const ErrorSubmittingLogin(this.response, this.wasFromGoogle);
  @override
  String toString() => 'ErrorSubmittingLogin';
  @override
  List<Object> get props => [response, wasFromGoogle];
}