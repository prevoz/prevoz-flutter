import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/login/login_event.dart';
import 'package:prevoz_org/data/blocs/login/login_state.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {

  final AuthRepository _authRepository;
  final AuthenticationBloc _authenticationBloc;

  LoginBloc(this._authRepository, this._authenticationBloc): super(InitialLoginState());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is SubmitLogin) {
      yield* _mapSubmitLoginToState(event);
    }
    else if (event is SubmitGoogleLogin) {
      yield* _mapSubmitGoogleLoginToState(event);
    }
  }

  Stream<LoginState> _mapSubmitLoginToState(SubmitLogin event) async* {
    try {
      String username = event.username;
      String password = event.password;
      
      // Check if username is actually a gmail
      if (username != null && username.contains("@gmail.com")) {
        yield LoginErrorGmail();
        return;
      }

      // Check for empty fields
      bool emptyUsername = ["", null].contains(username);
      bool emptyPassword = ["", null].contains(password);
      if (emptyUsername || emptyPassword) {
        Response errResp = Response();
        Map<String, dynamic> errorMap = {};
        if (emptyUsername) errorMap.putIfAbsent("username", () => ["Polje ne sme biti prazno"]);
        if (emptyPassword) errorMap.putIfAbsent("password", () => ["Polje ne sme biti prazno"]);
        errResp.data = {
          "error": errorMap
        };
        yield ErrorSubmittingLogin(errResp, false);

        return;
      }

      yield SubmittingLogin();

      Response response = await _authRepository.login(username, password);
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null) {
        yield ErrorSubmittingLogin(response, false);
      } else {
        String token = response.data["token"];
        _authenticationBloc.add(AuthenticationLoggedIn(token: token));
      }
    } catch (e) {
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorSubmittingLogin(errResp, false);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("LoginBloc, _mapSubmitLoginToState()",
          StackTrace.fromString(e.toString()));
    }
  }

  Stream<LoginState> _mapSubmitGoogleLoginToState(SubmitGoogleLogin event) async* {
    try {
      yield SubmittingLogin();

      Response response = await _authRepository.signInWithGoogle();
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null) {
        yield ErrorSubmittingLogin(response, true);
      } else {
        String token = response.data["token"];
        _authenticationBloc.add(AuthenticationLoggedIn(token: token));
      }
    } catch (e) {
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorSubmittingLogin(errResp, true);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("LoginBloc, _mapSubmitGoogleLoginToState()",
          StackTrace.fromString(e.toString()));
    }
  }
}