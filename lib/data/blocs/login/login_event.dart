import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class SubmitLogin extends LoginEvent {
  final String username;
  final String password;
  const SubmitLogin({
    @required this.username,
    @required this.password
  });
  String toString() => 'SubmitLogin';
  @override
  List<Object> get props => [username, password];
}

class SubmitGoogleLogin extends LoginEvent {
  const SubmitGoogleLogin();
  String toString() => 'SubmitGoogleLogin';
  @override
  List<Object> get props => [];
}