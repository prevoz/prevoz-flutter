import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UsersAddedRidesEvent extends Equatable {
  const UsersAddedRidesEvent();
}

class FetchAddedRidesEvent extends UsersAddedRidesEvent {
  const FetchAddedRidesEvent();
  @override
  String toString() => 'FetchAddedRidesEvent';
  @override
  List<Object> get props => [];
}

class AddedRidesFailedLoadingEvent extends UsersAddedRidesEvent {
  @override
  String toString() => 'AddedRidesFailedLoadingEvent';
  @override
  List<Object> get props => [];
}
