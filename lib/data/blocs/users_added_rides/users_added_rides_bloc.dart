import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:prevoz_org/data/blocs/users_added_rides/users_added_rides_event.dart';
import 'package:prevoz_org/data/blocs/users_added_rides/users_added_rides_state.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';

class UsersAddedRidesBloc
    extends Bloc<UsersAddedRidesEvent, UsersAddedRidesState> {
  final RidesRepository _ridesRepository;

  UsersAddedRidesBloc(this._ridesRepository) : super(AddedRidesLoading());

  @override
  Stream<UsersAddedRidesState> mapEventToState(
    UsersAddedRidesEvent event,
  ) async* {
    if (event is FetchAddedRidesEvent) {
      yield* _mapFetchEventToState();
    }

    if (event is AddedRidesFailedLoadingEvent) {
      yield AddedRidesFailedLoading();
    }
  }

  Stream<UsersAddedRidesState> _mapFetchEventToState() async* {
    try {
      yield AddedRidesLoading();
      RidesSearchResult results =
          await this._ridesRepository.fetchUsersAddedRides();
      yield AddedRidesLoaded(results.getRides);
    } catch (e) {
      print(e);
      yield AddedRidesFailedLoading();
    }
  }
}
