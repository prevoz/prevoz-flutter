import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class UsersAddedRidesState extends Equatable {
  const UsersAddedRidesState();
}

class AddedRidesLoading extends UsersAddedRidesState {
  const AddedRidesLoading();
  @override
  String toString() => 'AddedRidesLoading';
  @override
  List<Object> get props => [];
}

class AddedRidesLoaded extends UsersAddedRidesState {
  final List<RideModel> fetchedRides;
  const AddedRidesLoaded(this.fetchedRides);
  @override
  String toString() => 'AddedRidesLoaded';
  @override
  List<Object> get props => [fetchedRides];
}

class AddedRidesFailedLoading extends UsersAddedRidesState {
  const AddedRidesFailedLoading();
  @override
  String toString() => 'AddedRidesFailedLoading';
  @override
  List<Object> get props => [];
}
