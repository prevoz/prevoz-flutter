import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UsersBookmarksEvent extends Equatable {
  const UsersBookmarksEvent();
}

class FetchBookmarksEvent extends UsersBookmarksEvent {
  const FetchBookmarksEvent();
  @override
  String toString() => 'FetchBookmarksEvent';
  @override
  List<Object> get props => [];
}

class DeleteBookmarkEvent extends UsersBookmarksEvent {
  final String id;
  const DeleteBookmarkEvent({@required this.id});
  @override
  String toString() => 'DeleteBookmarkEvent';
  @override
  List<Object> get props => [id];
}

//todo: will i need this?
// used to resset state to initial... there's probably a better way to do it
class BookmarksResetEvent extends UsersBookmarksEvent {
  const BookmarksResetEvent();
  @override
  String toString() => 'BookmarksResetEvent';
  @override
  List<Object> get props => [];
}

class FailedLoadingBookmarksEvent extends UsersBookmarksEvent {
  const FailedLoadingBookmarksEvent();
  @override
  String toString() => 'FailedLoadingBookmarksEvent';
  @override
  List<Object> get props => [];
}
