import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:prevoz_org/data/blocs/users_bookmarked_rides/users_bookmarked_rides_event.dart';
import 'package:prevoz_org/data/blocs/users_bookmarked_rides/users_bookmarked_rides_state.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';

class BookmarksTabBloc extends Bloc<UsersBookmarksEvent, UsersBookmarksState> {
  final RidesRepository _ridesRepository;
  BookmarksTabBloc(this._ridesRepository) : super(BookmarksDeleting());

  @override
  Stream<UsersBookmarksState> mapEventToState(
    UsersBookmarksEvent event,
  ) async* {
    if (event is FetchBookmarksEvent) {
      yield* _mapFetchEventToState();
    }

    if (event is FailedLoadingBookmarksEvent) {
      yield BookmarksFailedLoading();
    }
  }

  Stream<UsersBookmarksState> _mapFetchEventToState() async* {
    try {
      yield BookmarksDeleting();

      RidesSearchResult results =
          await this._ridesRepository.fetchUsersBookmarkedRides();
      yield BookmarksLoaded(results.getRides);
    } catch (e) {
      yield BookmarksFailedLoading();
    }
  }
}
