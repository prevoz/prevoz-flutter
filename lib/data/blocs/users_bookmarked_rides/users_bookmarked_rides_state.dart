import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class UsersBookmarksState extends Equatable {
  const UsersBookmarksState();
}

class InitialBookmarksState extends UsersBookmarksState {
  const InitialBookmarksState();
  @override
  String toString() => 'InitialBookmarksState';
  @override
  List<Object> get props => [];
}

// request for deleting in progress
class BookmarksDeleting extends UsersBookmarksState {
  const BookmarksDeleting();
  @override
  String toString() => 'BookmarksDeleting';
  @override
  List<Object> get props => [];
}

class BookmarksLoaded extends UsersBookmarksState {
  final List<RideModel> fetchedRides;
  const BookmarksLoaded(this.fetchedRides);
  @override
  String toString() => 'BookmarksLoaded';
  @override
  List<Object> get props => [fetchedRides];
}

class BookmarksFailedLoading extends UsersBookmarksState {
  const BookmarksFailedLoading();
  @override
  String toString() => 'BookmarksFailedLoading';
  @override
  List<Object> get props => [];
}
