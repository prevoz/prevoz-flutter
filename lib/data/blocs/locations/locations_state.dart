import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:prevoz_org/data/models/location.dart';

@immutable
abstract class LocationsState extends Equatable {
  const LocationsState();
}

class InitialLocationsState extends LocationsState {
  const InitialLocationsState();
  @override
  String toString() => 'InitialLocationsState';
  @override
  List<Object> get props => [];
}

class LocationsLoading extends LocationsState {
  const LocationsLoading();
  @override
  String toString() => 'LocationsLoading';
  @override
  List<Object> get props => [];
}

class SuccessLoadingLocations extends LocationsState {
  final List<Location> fetchedLocations;
  final String country;
  final String query;
  const SuccessLoadingLocations(
      this.fetchedLocations, this.country, this.query);
  @override
  String toString() => 'SuccessLoadingLocations';
  @override
  List<Object> get props => [fetchedLocations, country, query];
}

class ErrorLoadingLocations extends LocationsState {
  final Object error;
  const ErrorLoadingLocations(this.error);
  @override
  String toString() => 'ErrorLoadingLocations';
  @override
  List<Object> get props => [error];
}

class SanitisingLocations extends LocationsState {
  const SanitisingLocations();
  @override
  String toString() => 'SanitisingLocations';
  @override
  List<Object> get props => [];
}

/// initial DB insert upon DB creation
class InitialisingLocations extends LocationsState {
  const InitialisingLocations();
  @override
  String toString() => 'InitialisingLocations';
  @override
  List<Object> get props => [];
}

class NoLocationsFound extends LocationsState {
  const NoLocationsFound();
  @override
  String toString() => 'NoLocationsFound';
  @override
  List<Object> get props => [];
}
