import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/blocs/locations/locations_events.dart';
import 'package:prevoz_org/data/blocs/locations/locations_state.dart';
import 'package:prevoz_org/data/helpers/event_bus/database_initialisation.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/data/repositories/locations_repository.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/utils/prevoz_exceptions.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class LocationsBloc extends Bloc<LocationsEvent, LocationsState> {
  String latestLocationQuery;
  Logger logger = Logger(printer: CustomLogPrinter("LocationsBloc"));
  final LocationsRepository locationsRepo;

  LocationsBloc({@required this.locationsRepo}) : super(InitialLocationsState()) {
    _listenToDbInitialisationEvents();
  }

  /// This prevents odd states when initialising the database
  /// wait for DB to get initialised, only then trigger a FetchLocations event
  /// while DB is in initialising process, trigger an InitialisingEvent
  void _listenToDbInitialisationEvents() {
    MyApp.eventBus
        .on<DatabaseInitialisationEvent>()
        .listen((DatabaseInitialisationEvent event) {
      if (DbInitState.isInitInProgress()) {
        add(LocationsDbInitEvent());
      } else {
        add(FetchLocations("", "SI"));
      }
    });
  }

  @override
  Stream<LocationsState> mapEventToState(LocationsEvent event) async* {
    if (event is LocationsDbInitEvent) {
      yield InitialisingLocations();
    }

    if (event is FetchLocations) {
      if (!DbInitState.isInitInProgress()) {
        yield* _mapFetchLocationsEventToState(event);
      }
    }

    if (event is SanitizeLocationsTable) {
      yield* _mapFetchSanitizeToState(event);
    }

    if (event is ResetLocationsBlocState) {
      yield InitialLocationsState();
    }
  }

  @override
  onTransition(Transition<LocationsEvent, LocationsState> transition) {
    super.onTransition(transition);
  }

  @override
  onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print("ERROR ridesbloc");
    print(error);
    print(stacktrace);
  }

  //_mapFetchSanitizeToState
  Stream<LocationsState> _mapFetchSanitizeToState(
      SanitizeLocationsTable event) async* {
    logger.i("Fetch sanitize event to state");
    try {
      yield SanitisingLocations();

      //* 1) SANITIZE
      await locationsRepo.sanitizeLocationsTable();
      int locationsCount = await locationsRepo.countLocations();
      if (locationsCount != MyConstants.NUM_OF_LOCATIONS_IN_DATABASE) {
        throw LocationsCountException(
            "SearchLocDelegate, wrong locations count. Got $locationsCount instead of 1214");
      }

      logger.i("DB locations table after sanitizing: $locationsCount");

      //* 2) FETCH LOCATIONS AFTER SANITIZING
      List<Location> results =
          await locationsRepo.queryLocations(event.query, event.country);
      logger.i("after sanitizing results: " + results.toString());
      latestLocationQuery = event.query;

      if (latestLocationQuery == '' && results.length < 1) {
        //* this should return results, therefor check DB
        logger.i(
            "Sanitize, .... latestLocationQuery == '' && results.length < 1");
        logger.i("Event query, country and sub loc search: " +
            event.query.toString() +
            "  " +
            event.country.toString());
        throw Exception();
      }
      yield SuccessLoadingLocations(results, event.country, event.query);
    } on LocationsCountException catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
          "LocationsCountException: $e", StackTrace.fromString(e.toString()));
      _checkDbSanity();
      yield ErrorLoadingLocations(
          "Error while sanitizing locations, locations count");
    } catch (e) {
      logger.i("sanitize, exception " + e.toString());
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
          "error while sanitizing locations: " + e.toString(), null);
      _checkDbSanity();
      yield ErrorLoadingLocations("Error while sanitizing locations");
    }
  }

  Stream<LocationsState> _mapFetchLocationsEventToState(
      FetchLocations event) async* {
    try {
      yield LocationsLoading();
      //* 1) Check if all locations in database
      int locationsCount = await locationsRepo.countLocations();

      if (locationsCount != MyConstants.NUM_OF_LOCATIONS_IN_DATABASE) {
        logger.e(
            "SearchLocDelegate, wrong locations count. Got $locationsCount instead of 1214");
        throw LocationsCountException("Wrong locations count: $locationsCount");
      }
      //* 2) query
      List<Location> results =
          await locationsRepo.queryLocations(event.query, event.country);
      latestLocationQuery = event.query;

      if (latestLocationQuery == '' && results.length < 1) {
        //* this should return results
        throw Exception();
      }

      yield SuccessLoadingLocations(results, event.country, event.query);
    } on LocationsCountException catch (e) {
      logger.i("LocationsCountException");

      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(
          "LocationsCountException: $e", StackTrace.fromString(e.toString()));
      sanitizeLocationsTable(event);
    } catch (e) {
      logger.i("loc bloc exception");
      _checkDbSanity();
      yield ErrorLoadingLocations(e);
    }
  }

  void _checkDbSanity() async {
    await locationsRepo.checkDBSanity();
  }

  void sanitizeLocationsTable(FetchLocations event) {
    add(SanitizeLocationsTable(event.query, event.country));
  }

  void resetStateToInitial() {
    add(ResetLocationsBlocState());
  }
}
