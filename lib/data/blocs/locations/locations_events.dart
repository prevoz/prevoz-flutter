import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LocationsEvent extends Equatable {
  const LocationsEvent();
}

class LoadingLocations extends LocationsEvent {
  const LoadingLocations();
  @override
  String toString() => 'LoadingLocations';
  @override
  List<Object> get props => [];
}

class LocationsDbInitEvent extends LocationsEvent {
  const LocationsDbInitEvent();
  @override
  String toString() => 'LocationsDbInitEvent';
  @override
  List<Object> get props => [];
}

/// deletes all locations and refills them.
class SanitizeLocationsTable extends LocationsEvent {
  final String query;
  final String country;
  const SanitizeLocationsTable(this.query, this.country);
  @override
  String toString() => 'SanitizeLocationsTable';
  @override
  List<Object> get props => [
        query,
        country,
      ];
}

class FetchLocations extends LocationsEvent {
  final String query;
  final String country;
  const FetchLocations(this.query, this.country);
  @override
  String toString() => 'FetchLocations';
  @override
  List<Object> get props => [query, country];
}

class ResetLocationsBlocState extends LocationsEvent {
  const ResetLocationsBlocState();
  @override
  String toString() => 'ResetLocationsBlocState';
  @override
  List<Object> get props => [];
}
