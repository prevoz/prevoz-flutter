import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class RidesState extends Equatable {
  const RidesState();
}

class RidesBookmarkedLoading extends RidesState {
  const RidesBookmarkedLoading();
  @override
  String toString() => 'RidesBookmarkedLoading';
  @override
  List<Object> get props => [];
}

class RidesLoading extends RidesState {
  const RidesLoading();
  @override
  String toString() => 'RidesLoading';
  @override
  List<Object> get props => [];
}

class RidesLoaded extends RidesState {
  final List<RideModel> fetchedRides;
  const RidesLoaded(this.fetchedRides);
  @override
  String toString() => 'RidesLoaded';
  @override
  List<Object> get props => [fetchedRides];
}

class ErrorLoadingRides extends RidesState {
  const ErrorLoadingRides();
  @override
  String toString() => 'ErrorLoadingRides';
  @override
  List<Object> get props => [];
}
