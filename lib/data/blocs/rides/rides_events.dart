import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';

@immutable
abstract class RidesEvent extends Equatable {
  const RidesEvent();
}

class FetchRidesEvent extends RidesEvent {
  final RideSearchRequest searchRequest;

  const FetchRidesEvent({@required this.searchRequest})
      : assert(searchRequest != null);
  @override
  String toString() => 'FetchRidesEvent';
  @override
  List<Object> get props => [searchRequest];
}

class ErrorFetchingRidesEvent extends RidesEvent {
  const ErrorFetchingRidesEvent();
  @override
  String toString() => 'ErrorFetchingRidesEvent';
  @override
  List<Object> get props => [];
}
