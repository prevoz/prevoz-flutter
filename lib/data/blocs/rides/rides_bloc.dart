import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:prevoz_org/data/blocs/rides/rides_state.dart';
import 'package:prevoz_org/data/blocs/rides/rides_events.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';

class RideBloc extends Bloc<RidesEvent, RidesState> {
  final RidesRepository ridesRepo;

  RideBloc({@required this.ridesRepo}) : super(RidesLoading());

  @override
  Stream<RidesState> mapEventToState(RidesEvent event) async* {
    if (event is FetchRidesEvent) {
      yield* _mapFetchRidesEventToState(event);
    }

    if (event is ErrorFetchingRidesEvent) {
      yield* _mapErrorLoadingEventToState();
    }
  }

  @override
  onTransition(Transition<RidesEvent, RidesState> transition) {
    super.onTransition(transition);
    //print("TRANSITION: current state: " + currentState.toString());
  }

  @override
  onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print("ERROR ridesbloc");
    print(error);
    print(stacktrace);
  }

  Stream<RidesState> _mapFetchRidesEventToState(FetchRidesEvent event) async* {
    try {
      //every time we're using yield we're pushing that piece of data trough the stream
      yield RidesLoading();
      RidesSearchResult results = await this.ridesRepo.fetchRides(event);
      yield RidesLoaded(results.getRides);
    } catch (e) {
      print(e);
      yield ErrorLoadingRides();
    }
  }

  Stream<RidesState> _mapErrorLoadingEventToState() async* {
    yield ErrorLoadingRides();
  }

  void fetchRides(RideSearchRequest searchRequest) {
    add(FetchRidesEvent(searchRequest: searchRequest));
  }

  void errorLoading() {
    add(ErrorFetchingRidesEvent());
  }
}
