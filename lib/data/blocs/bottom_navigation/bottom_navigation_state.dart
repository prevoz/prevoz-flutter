import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class BottomNavigationState extends Equatable {
  const BottomNavigationState();
}

class HomePageActive extends BottomNavigationState {
  const HomePageActive();
  @override
  String toString() => 'HomePageActive';
  @override
  List<Object> get props => [];
}

class MyRidesPageActive extends BottomNavigationState {
  const MyRidesPageActive();
  @override
  String toString() => 'MyRidesPageActive';
  @override
  List<Object> get props => [];
}

class AddRidePageActive extends BottomNavigationState {
  const AddRidePageActive();
  @override
  String toString() => 'AddRidePageActive';
  @override
  List<Object> get props => [];
}

class ProfilePageActive extends BottomNavigationState {
  const ProfilePageActive();
  @override
  String toString() => 'ProfilePageActive';
  @override
  List<Object> get props => [];
}

class NotificationsPageActive extends BottomNavigationState {
  const NotificationsPageActive();
  @override
  String toString() => 'NotificationsPageActive';
  @override
  List<Object> get props => [];
}

class RideSuccessfullyAdded extends BottomNavigationState {
  const RideSuccessfullyAdded();
  @override
  String toString() => 'RideSuccessfullyAdded';
  @override
  List<Object> get props => [];
}

class RideSuccessfullyDeleted extends BottomNavigationState {
  const RideSuccessfullyDeleted();
  @override
  String toString() => 'RideSuccessfullyDeleted';
  @override
  List<Object> get props => [];
}

class RideSuccessfullyUpdated extends BottomNavigationState {
  const RideSuccessfullyUpdated();
  @override
  String toString() => 'RideSuccessfullyUpdated';
  @override
  List<Object> get props => [];
}

class EditRidePageActive extends BottomNavigationState {
  final RideModel ride;
  const EditRidePageActive(this.ride);
  @override
  String toString() => 'EditRidePageActive';
  @override
  List<Object> get props => [ride];
}

/// For dipslaying SnackBars in UsersRidesTabBars after editing, deleting or adding a new ride in AddRideForm
class TabUpdatedAfterRideFormChange extends BottomNavigationState {
  final RideFormChange rideFormChange;
  const TabUpdatedAfterRideFormChange(this.rideFormChange);
  @override
  String toString() => 'TabUpdatedAfterRideFormChange';
  @override
  List<Object> get props => [rideFormChange];
}
