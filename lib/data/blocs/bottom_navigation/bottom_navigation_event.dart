import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'package:prevoz_org/data/models/app_tab.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class BottomNavigationEvent extends Equatable {
  const BottomNavigationEvent();
}

class UpdateTabBottomNavEvent extends BottomNavigationEvent {
  final AppTab appTab;
  const UpdateTabBottomNavEvent(this.appTab);
  @override
  String toString() => 'UpdateTabBottomNavEvent';
  @override
  List<Object> get props => [];
}

class ChangeStateAfterSnackbarEvent extends BottomNavigationEvent {
  const ChangeStateAfterSnackbarEvent();
  @override
  String toString() => 'ChangeStateAfterSnackbarEvent';
  @override
  List<Object> get props => [];
}

class AddRideBottomNavEvent extends BottomNavigationEvent {
  final RideFormChange rideFormChange;
  const AddRideBottomNavEvent(this.rideFormChange);
  @override
  String toString() => 'AddRideBottomNavEvent';
  @override
  List<Object> get props => [rideFormChange];
}

class GoToEditRideNavEvent extends BottomNavigationEvent {
  final RideModel ride;
  GoToEditRideNavEvent(this.ride);
  @override
  String toString() => 'GoToEditRideNavEvent';
  @override
  List<Object> get props => [ride];
}
