import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_state.dart';
import 'package:prevoz_org/data/models/app_tab.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import './bloc.dart';

//* Cool example of yielding a different data type as state.
class BottomNavigationBloc
    extends Bloc<BottomNavigationEvent, BottomNavigationState> {

  BottomNavigationBloc() : super(HomePageActive());

  @override
  Stream<BottomNavigationState> mapEventToState(
    BottomNavigationEvent event,
  ) async* {
    if (event is UpdateTabBottomNavEvent) {
      switch (event.appTab) {
        case AppTab.home:
          yield HomePageActive();
          break;
        case AppTab.myrides:
          yield MyRidesPageActive();
          break;
        case AppTab.addride:
          yield AddRidePageActive();
          break;
        case AppTab.profile:
          yield ProfilePageActive();
          break;
        case AppTab.notifications:
          yield NotificationsPageActive();
          break;
        default:
          yield HomePageActive();
      }
    }

    if (event is AddRideBottomNavEvent) {
      switch (event.rideFormChange) {
        case RideFormChange.rideAdded:
          yield RideSuccessfullyAdded();
          break;
        case RideFormChange.rideDeleted:
          print("_____yield success deleted state, current state izzzz " +
              state.toString());
          yield RideSuccessfullyDeleted();

          break;
        case RideFormChange.rideUpdated:
          yield RideSuccessfullyUpdated();
          break;

        default:
      }
    }

    if (event is ChangeStateAfterSnackbarEvent) {
      yield MyRidesPageActive();
    }

    if (event is GoToEditRideNavEvent) {
      print("______ GoToEditRideNavEvent");
      yield EditRidePageActive(event.ride);
    }
  }
}
