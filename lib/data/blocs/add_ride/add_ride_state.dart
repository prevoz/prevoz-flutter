import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class AddRideState extends Equatable {
  const AddRideState();
}

class InitialAddRideState extends AddRideState {
  const InitialAddRideState();
  @override
  String toString() => 'InitialAddRideState';
  @override
  List<Object> get props => [];
}

class SubmitingRide extends AddRideState {
  const SubmitingRide();
  @override
  String toString() => 'SubmitingRide';
  @override
  List<Object> get props => [];
}

class SuccessfullySubmitedNewRide extends AddRideState {
  final RideModel ride;
  const SuccessfullySubmitedNewRide(this.ride);
  @override
  String toString() => 'SuccessfullySubmitedNewRide';
  @override
  List<Object> get props => [ride];
}

class SuccessfullyDeletedRide extends AddRideState {
  final RideModel ride;
  const SuccessfullyDeletedRide(this.ride);
  @override
  String toString() => 'SuccessfullyDeletedRide';
  @override
  List<Object> get props => [ride];
}

class SuccessfullyEditedRide extends AddRideState {
  final RideModel ride;
  const SuccessfullyEditedRide(this.ride);
  @override
  String toString() => 'SuccessfullyEditedRide';
  @override
  List<Object> get props => [ride];
}

class SubmittingSetFull extends AddRideState {
  const SubmittingSetFull();
  @override
  String toString() => 'SubmittingSetFull';
  @override
  List<Object> get props => [];
}

class ErrorSettingFull extends AddRideState {
  final RideModel ride;
  final Response response;
  const ErrorSettingFull(this.ride, this.response);
  @override
  String toString() => 'ErrorSettingFull';
  @override
  List<Object> get props => [ride, response];
}

class ErrorSubmitingRide extends AddRideState {
  final RideModel ride;
  final Response response;
  const ErrorSubmitingRide(this.ride, this.response);
  @override
  String toString() => 'ErrorSubmitingNewRide';
  @override
  List<Object> get props => [ride, response];
}
