import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

@immutable
abstract class AddRideEvent extends Equatable {
  const AddRideEvent();
}

class PostRide extends AddRideEvent {
  final RideModel ride;
  final bool isEditMode;
  const PostRide(this.ride, this.isEditMode);
  @override
  String toString() => 'PostRide';
  @override
  List<Object> get props => [ride, isEditMode];
}

class SetRideFull extends AddRideEvent {
  final RideModel ride;
  final String state;
  const SetRideFull(this.ride, this.state);
  @override
  String toString() => 'SetRideFull';
  @override
  List<Object> get props => [ride, state];
}

class DeleteRide extends AddRideEvent {
  final RideModel ride;
  const DeleteRide(this.ride);
  @override
  String toString() => 'DeleteRide';
  @override
  List<Object> get props => [ride];
}

class EditRide extends AddRideEvent {
  final String id;
  const EditRide(this.id);
  @override
  String toString() => 'EditRide';
  @override
  List<Object> get props => [id];
}

class ResetToInitialState extends AddRideEvent {
  const ResetToInitialState();
  @override
  String toString() => 'ResetToInitialState';
  @override
  List<Object> get props => [];
}
