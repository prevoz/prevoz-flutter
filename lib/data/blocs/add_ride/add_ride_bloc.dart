import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';

import 'package:flutter/material.dart';

import 'package:logger/logger.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_event.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_state.dart';
import 'package:prevoz_org/data/helpers/event_bus/ride_submited_event.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';

import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/data/repositories/search_history_repository.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';

import '../../../my_app.dart';

class AddRideBloc extends Bloc<AddRideEvent, AddRideState> {
  final RidesRepository ridesRepo;
  final SearchHistoryRepository searchHistoryRepo;
  Logger logger = Logger(printer: CustomLogPrinter("AddRideBloc"));

  AddRideBloc(this.ridesRepo, this.searchHistoryRepo) : super(InitialAddRideState());

  void resetStateToInitial() {
    add(ResetToInitialState());
  }

  @override
  Stream<AddRideState> mapEventToState(
    AddRideEvent event,
  ) async* {
    if (event is PostRide) {
      yield* _mapRidePostToState(event);
    }

    if (event is ResetToInitialState) {
      print("____BLOC, resset to initla state");
      yield InitialAddRideState();
    }

    if (event is DeleteRide) {
      yield* _mapDeleteRideToState(event);
    }

    if (event is SetRideFull) {
      yield* _mapSetRideFullToState(event);
    }
  }

  Stream<AddRideState> _mapDeleteRideToState(DeleteRide event) async* {
    yield SubmitingRide();

    try {
      Response response = await ridesRepo.deleteRide(event.ride.id.toString());

      if (response.data != null && response.data["error"] != null) {
        print("_____NEW ERROR FROM API " + response.data["error"].toString());
        yield ErrorSubmitingRide(null, response);
      }
      if (response != null &&
          response.data != null &&
          response.statusCode == 200 &&
          response.data["error"] == null) {
        yield SuccessfullyDeletedRide(event.ride);
      }
    } catch (e) {
      yield ErrorSubmitingRide(null, e);
    }
  }

  Stream<AddRideState> _mapSetRideFullToState(SetRideFull event) async* {
    try {
      yield SubmittingSetFull();
      Response response = await ridesRepo.setFull(event.ride.id, event.state);

      if (response.data != null && response.data["error"] != null) {
        throw Exception();
      }

      if (response != null &&
          response.data != null &&
          response.data["error"] == null) {
        yield SuccessfullyEditedRide(event.ride);
      } else {
        throw Exception();
      }
    } catch (e) {
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      logger.e("error setting full ...." + errResp.statusMessage.toString());
      yield ErrorSettingFull(event.ride, errResp);
    }
  }

  Stream<AddRideState> _mapRidePostToState(PostRide event) async* {
    try {
      yield SubmitingRide();

      Response response = await ridesRepo.addRide(event.ride);

      if (response.data != null && response.data["error"] != null) {
        logger.e("Error submiting ride:  " + response.data["error"].toString());

        yield ErrorSubmitingRide(event.ride, response);
      }
      if (response != null &&
          response.data != null &&
          response.statusCode == 200 &&
          response.data["error"] == null) {
        if (event.isEditMode) {
          yield SuccessfullyEditedRide(event.ride);
        } else {
          event.ride.id = response.data["id"];
          logger.i("Successfully submited new ride, " + event.ride.toString());

          MyApp.eventBus.fire(RideSubmitedEvent(event.ride));

          yield SuccessfullySubmitedNewRide(
              RideModel.setupRideAfterPosting(event.ride));
        }
      } else {
        yield ErrorSubmitingRide(event.ride, response);
      }
    } catch (e) {
      debugPrint("____RideBLOc, error adding ride");
      debugPrint(e.toString());
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorSubmitingRide(event.ride, errResp);
    }
  }
}
