
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

abstract class RegistrationState extends Equatable {
  const RegistrationState();
}

class InitialRegistrationState extends RegistrationState {
  const InitialRegistrationState();
  @override
  String toString() => 'InitialRegistrationState';
  @override
  List<Object> get props => [];
}

class RegistrationErrorGmail extends RegistrationState {
  const RegistrationErrorGmail();
  @override
  String toString() => 'RegistrationErrorGmail';
  @override
  List<Object> get props => [];
}

class SubmittingRegistration extends RegistrationState {
  const SubmittingRegistration();
  @override
  String toString() => 'SubmittingRegistration';
  @override
  List<Object> get props => [];
}

class SuccessfullySubmittedRegistration extends RegistrationState {
  const SuccessfullySubmittedRegistration();
  @override
  String toString() => 'SuccessfullySubmittedRegistration';
  @override
  List<Object> get props => [];
}

class ErrorSubmittingRegistration extends RegistrationState {
  final Response response;
  final bool wasFromGoogle;
  const ErrorSubmittingRegistration(this.response, this.wasFromGoogle);
  @override
  String toString() => 'ErrorSubmittingRegistration';
  @override
  List<Object> get props => [response, wasFromGoogle];
}