
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegistrationEvent extends Equatable {
  const RegistrationEvent();
}

class SubmitRegistration extends RegistrationEvent {
  final String username;
  final String email;
  final String password;
  const SubmitRegistration({
    @required this.username,
    @required this.email,
    @required this.password
  });
  String toString() => 'SubmitRegistration';
  @override
  List<Object> get props => [username, email, password];
}

class SubmitGoogleLogin extends RegistrationEvent {
  const SubmitGoogleLogin();
  String toString() => 'SubmitGoogleLogin';
  @override
  List<Object> get props => [];
}
