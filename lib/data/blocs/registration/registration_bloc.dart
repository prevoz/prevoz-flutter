

import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/registration/registration_event.dart';
import 'package:prevoz_org/data/blocs/registration/registration_state.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {

  final AuthRepository _authRepository;
  final AuthenticationBloc _authenticationBloc;

  RegistrationBloc(this._authRepository, this._authenticationBloc): super(InitialRegistrationState());

  @override
  Stream<RegistrationState> mapEventToState(RegistrationEvent event) async* {
    if (event is SubmitRegistration) {
      yield* _mapSubmitRegistrationToState(event);
    }
    else if (event is SubmitGoogleLogin) {
      yield* _mapSubmitGoogleLoginToState(event);
    }
  }

  Stream<RegistrationState> _mapSubmitRegistrationToState(SubmitRegistration event) async* {
    try {
      String username = event.username;
      String email = event.email;
      String password = event.password;

      // Check if gmail
      if (email != null && email.contains("@gmail.com")) {
        yield RegistrationErrorGmail();
        return;
      }

      // Check for empty fields
      bool emptyUsername = ["", null].contains(username);
      bool emptyEmail = ["", null].contains(email);
      bool emptyPassword = ["", null].contains(password);
      if (emptyUsername || emptyPassword) {
        Response errResp = Response();
        Map<String, dynamic> errorMap = {};
        if (emptyUsername) errorMap.putIfAbsent("username", () => ["Polje ne sme biti prazno"]);
        if (emptyEmail) errorMap.putIfAbsent("email", () => ["Polje ne sme biti prazno"]);
        if (emptyPassword) errorMap.putIfAbsent("password", () => ["Polje ne sme biti prazno"]);
        errResp.data = {
          "error": errorMap
        };
        yield ErrorSubmittingRegistration(errResp, false);

        return;
      }

      yield SubmittingRegistration();

      Response response = await _authRepository.register(
        username: username,
        email: email,
        password: password
      );
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null) {
        yield ErrorSubmittingRegistration(response, false);
      } else {
        yield SuccessfullySubmittedRegistration();
      }

    } catch (e) {
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorSubmittingRegistration(errResp, false);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("RegistrationBloc, _mapSubmitRegistrationToState()",
          StackTrace.fromString(e.toString()));
    }
  }

  Stream<RegistrationState> _mapSubmitGoogleLoginToState(SubmitGoogleLogin event) async* {
    try {
      yield SubmittingRegistration();

      Response response = await _authRepository.signInWithGoogle();
      yield ErrorSubmittingRegistration(response, true);
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null) {
        yield ErrorSubmittingRegistration(response, true);
      } else {
        String token = response.data["token"];
        _authenticationBloc.add(AuthenticationLoggedIn(token: token));
      }
    } catch (e) {
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorSubmittingRegistration(errResp, true);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("RegistrationBloc, _mapSubmitGoogleLoginToState()",
          StackTrace.fromString(e.toString()));
    }
  }
}