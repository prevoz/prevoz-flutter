import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:prevoz_org/data/blocs/profile/profile_event.dart';
import 'package:prevoz_org/data/blocs/profile/profile_state.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/data/repositories/user_profile_repository.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final UserProfileRepository _profileRepository;

  ProfileBloc(this._profileRepository) : super(InitialProfileState());

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is GetUserProfile) {
      yield* _mapGetProfileToState();
    }
    else if (event is StopFetchingProfile) {
      yield StoppedFetchingProfile();
    }
    else if (event is ResetProfileToInitialState) {
      yield InitialProfileState();
    }
  }

  Stream<ProfileState> _mapGetProfileToState() async* {
    UserProfile storedProfile;
    try {
      storedProfile = await _profileRepository.getProfileFromSharedPrefs();
    } catch (e) {
      debugPrint("____ProfileBLOc, error getting stored profile");
      debugPrint(e.toString());
    }
    try {
      yield FetchingProfile(storedProfile);
      Response response = await _profileRepository.fetchProfile();
      if (response.statusCode != 200 || response.data["error"] != null) {
        yield ErrorFetchingProfile(response, storedProfile);
      } else {
        yield SuccessfullyFetchedProfile(UserProfile.fromResponse(response));
      }
    } catch (e) {
      debugPrint("____ProfileBLOc, error fetching profile");
      debugPrint(e.toString());
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorFetchingProfile(errResp, storedProfile);
    }
  }
}