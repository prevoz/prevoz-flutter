
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileEvent extends Equatable {
  const ProfileEvent();
}

class GetUserProfile extends ProfileEvent {
  const GetUserProfile();
  @override
  String toString() => 'GetUserProfile';
  @override
  List<Object> get props => [];
}

class StopFetchingProfile extends ProfileEvent {
  const StopFetchingProfile();
  @override
  String toString() => 'StopFetchingProfile';
  @override
  List<Object> get props => [];
}

class ResetProfileToInitialState extends ProfileEvent {
  const ResetProfileToInitialState();
  @override
  String toString() => 'ResetProfileToInitialState';
  @override
  List<Object> get props => [];
}