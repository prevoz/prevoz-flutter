
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/userProfile.dart';

@immutable
abstract class ProfileState extends Equatable {
  const ProfileState();
}

class InitialProfileState extends ProfileState {
  const InitialProfileState();
  @override
  String toString() => 'InitialProfileState';
  @override
  List<Object> get props => [];
}

class FetchingProfile extends ProfileState {
  final UserProfile storedProfile;
  const FetchingProfile(this.storedProfile);
  @override
  String toString() => 'FetchingProfile';
  @override
  List<Object> get props => [storedProfile];
}

class SuccessfullyFetchedProfile extends ProfileState {
  final UserProfile userProfile;
  const SuccessfullyFetchedProfile(this.userProfile);
  @override
  String toString() => 'SuccessfullyFetchedProfile';
  @override
  List<Object> get props => [userProfile];
}

class StoppedFetchingProfile extends ProfileState {
  const StoppedFetchingProfile();
  @override
  String toString() => 'StoppedFetchingProfile';
  @override
  List<Object> get props => [];
}

class ErrorFetchingProfile extends ProfileState {
  final Response response;
  final UserProfile storedProfile;
  const ErrorFetchingProfile(this.response, this.storedProfile);
  @override
  String toString() => 'ErrorFetchingProfile';
  @override
  List<Object> get props => [response, storedProfile];
}