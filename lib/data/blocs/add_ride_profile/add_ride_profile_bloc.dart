import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:prevoz_org/data/blocs/add_ride_profile/add_ride_profile_event.dart';
import 'package:prevoz_org/data/blocs/add_ride_profile/add_ride_profile_state.dart';

import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/data/repositories/user_profile_repository.dart';

class AddRideProfileBloc extends Bloc<AddRideProfileEvent, AddRideProfileState> {
  final UserProfileRepository _profileRepository;

  AddRideProfileBloc(this._profileRepository) : super(InitialAddRideProfileState());

  @override
  Stream<AddRideProfileState> mapEventToState(AddRideProfileEvent event) async* {
    if (event is GetUserAddRideProfile) {
      yield* _mapGetProfileToState();
    }
    else if (event is StopFetchingAddRideProfile) {
      yield StoppedFetchingAddRideProfile();
    }
    else if (event is ResetAddRideProfileToInitialState) {
      yield InitialAddRideProfileState();
    }
  }

  Stream<AddRideProfileState> _mapGetProfileToState() async* {
    UserProfile storedProfile;
    try {
      storedProfile = await _profileRepository.getProfileFromSharedPrefs();
    } catch (e) {
      debugPrint("____ProfileBLOc, error getting stored profile");
      debugPrint(e.toString());
    }
    try {
      yield FetchingAddRideProfile(storedProfile);
      Response response = await _profileRepository.fetchProfile();
      if (response.statusCode != 200 || response.data["error"] != null) {
        yield ErrorFetchingAddRideProfile(response, storedProfile);
      } else {
        yield SuccessfullyFetchedAddRideProfile(UserProfile.fromResponse(response));
      }
    } catch (e) {
      debugPrint("____ProfileBLOc, error fetching profile");
      debugPrint(e.toString());
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorFetchingAddRideProfile(errResp, storedProfile);
    }
  }
}