
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/userProfile.dart';

@immutable
abstract class AddRideProfileState extends Equatable {
  const AddRideProfileState();
}

class InitialAddRideProfileState extends AddRideProfileState {
  const InitialAddRideProfileState();
  @override
  String toString() => 'InitialAddRideProfileState';
  @override
  List<Object> get props => [];
}

class FetchingAddRideProfile extends AddRideProfileState {
  final UserProfile storedProfile;
  const FetchingAddRideProfile(this.storedProfile);
  @override
  String toString() => 'FetchingAddRideProfile';
  @override
  List<Object> get props => [storedProfile];
}

class SuccessfullyFetchedAddRideProfile extends AddRideProfileState {
  final UserProfile userProfile;
  const SuccessfullyFetchedAddRideProfile(this.userProfile);
  @override
  String toString() => 'SuccessfullyFetchedAddRideProfile';
  @override
  List<Object> get props => [userProfile];
}

class StoppedFetchingAddRideProfile extends AddRideProfileState {
  const StoppedFetchingAddRideProfile();
  @override
  String toString() => 'StoppedFetchingAddRideProfile';
  @override
  List<Object> get props => [];
}

class ErrorFetchingAddRideProfile extends AddRideProfileState {
  final Response response;
  final UserProfile storedProfile;
  const ErrorFetchingAddRideProfile(this.response, this.storedProfile);
  @override
  String toString() => 'ErrorFetchingAddRideProfile';
  @override
  List<Object> get props => [response, storedProfile];
}