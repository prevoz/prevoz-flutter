
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AddRideProfileEvent extends Equatable {
  const AddRideProfileEvent();
}

class GetUserAddRideProfile extends AddRideProfileEvent {
  const GetUserAddRideProfile();
  @override
  String toString() => 'GetUserAddRideProfile';
  @override
  List<Object> get props => [];
}

class StopFetchingAddRideProfile extends AddRideProfileEvent {
  const StopFetchingAddRideProfile();
  @override
  String toString() => 'StopFetchingAddRideProfile';
  @override
  List<Object> get props => [];
}

class ResetAddRideProfileToInitialState extends AddRideProfileEvent {
  const ResetAddRideProfileToInitialState();
  @override
  String toString() => 'ResetAddRideProfileToInitialState';
  @override
  List<Object> get props => [];
}