import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CountriesEvent extends Equatable {
  const CountriesEvent();
}

class LoadingCountries extends CountriesEvent {
  const LoadingCountries();
  @override
  String toString() => 'LoadingCountries';
  @override
  List<Object> get props => [];
}

class CountriesDbInitEvent extends CountriesEvent {
  const CountriesDbInitEvent();
  @override
  String toString() => 'CountriesDbInitEvent';
  @override
  List<Object> get props => [];
}

//CountriesDbInitEvent

class GetAllCountries extends CountriesEvent {
  const GetAllCountries();
  @override
  String toString() => 'GetAllCountries';
  @override
  List<Object> get props => [];
}

class FetchCountries extends CountriesEvent {
  final String query;
  FetchCountries(this.query);
  @override
  String toString() => 'FetchCountries';
  @override
  List<Object> get props => [query];
}
