import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:prevoz_org/data/models/country.dart';

@immutable
abstract class CountriesState extends Equatable {
  const CountriesState();
}

//EnterSearchQuery
class EnterSearchQueryCountry extends CountriesState {
  const EnterSearchQueryCountry();
  @override
  String toString() => 'EnterSearchQueryCountry';
  @override
  List<Object> get props => [];
}

class InitialisingCountries extends CountriesState {
  const InitialisingCountries();
  @override
  String toString() => 'InitialisingCountries';
  @override
  List<Object> get props => [];
}

class CountriesLoading extends CountriesState {
  const CountriesLoading();
  @override
  String toString() => 'CountriesLoading';
  @override
  List<Object> get props => [];
}

class CountriesLoaded extends CountriesState {
  final List<Country> fetchedCountries;
  const CountriesLoaded(this.fetchedCountries);
  @override
  String toString() => 'CountriesLoaded';
  @override
  List<Object> get props => [fetchedCountries];
}

class ErrorLoadingCountries extends CountriesState {
  const ErrorLoadingCountries();
  @override
  String toString() => 'ErrorLoadingCountries';
  @override
  List<Object> get props => [];
}

class NoCountriesFound extends CountriesState {
  const NoCountriesFound();
  @override
  String toString() => 'NoCountriesFound';
  @override
  List<Object> get props => [];
}
