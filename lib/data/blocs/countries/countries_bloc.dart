import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:prevoz_org/data/blocs/countries/countries_state.dart';
import 'package:prevoz_org/data/blocs/countries/countries_events.dart';
import 'package:prevoz_org/data/helpers/event_bus/database_initialisation.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/data/repositories/countries_repository.dart';
import 'package:prevoz_org/my_app.dart';

class CountriesBloc extends Bloc<CountriesEvent, CountriesState> {
  String countryQuery;
  final CountriesRepository _countriesRepository;
  CountriesBloc(this._countriesRepository): super(EnterSearchQueryCountry()) {
    _listenToDbInitialisationEvents();
  }

  void _listenToDbInitialisationEvents() {
    MyApp.eventBus
        .on<DatabaseInitialisationEvent>()
        .listen((DatabaseInitialisationEvent event) {
      if (DbInitState.isInitInProgress()) {
        add(CountriesDbInitEvent());
      } else {
        add(FetchCountries(""));
      }
    });
  }

  @override
  Stream<CountriesState> mapEventToState(CountriesEvent event) async* {
    if (event is CountriesDbInitEvent) {
      yield InitialisingCountries();
    }
    if (event is FetchCountries &&
        event.query.length == 0 &&
        state is CountriesLoaded) {
      yield* _mapFetchCountriesEventToState(event);
    }
    // query checking stops multiple calls on same query, that would otherwise be triggered by buildSuggestions() in the SearchDelegate
    if (event is FetchCountries && event.query != countryQuery) {
      yield* _mapFetchCountriesEventToState(event);
    }

    if (event is GetAllCountries) {
      yield* _mapGetAllCountriesToState();
    }
  }

  Stream<CountriesState> _mapGetAllCountriesToState() async* {
    if (DbInitState.isInitInProgress()) {
      return;
    }
    try {
      yield CountriesLoading();
      List<Country> results = await _countriesRepository.getAllCountries();

      yield CountriesLoaded(results);
    } catch (e) {
      print(e);
      _countriesRepository.checkDBSanity();
      yield ErrorLoadingCountries();
    }
  }

  Stream<CountriesState> _mapFetchCountriesEventToState(
      FetchCountries event) async* {
    if (DbInitState.isInitInProgress()) {
      return;
    }
    try {
      yield CountriesLoading();

      List<Country> results =
          await _countriesRepository.queryCountries(event.query);

      countryQuery = event.query;

      yield CountriesLoaded(results);
    } catch (e) {
      print(e);
      yield ErrorLoadingCountries();
    }
  }
}
