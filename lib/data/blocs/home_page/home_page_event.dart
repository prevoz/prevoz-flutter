import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';

@immutable
abstract class HomePageEvent extends Equatable {
  const HomePageEvent();
}

class HomePageLoadSearchHistoryEvent extends HomePageEvent {
  const HomePageLoadSearchHistoryEvent();
  @override
  String toString() => 'HomePageLoadSearchHistoryEvent';
  @override
  List<Object> get props => [];
}

class HomePageErrorLoadingSearchHistoryEvent extends HomePageEvent {
  const HomePageErrorLoadingSearchHistoryEvent();
  @override
  String toString() => 'HomePageErrorLoadingSearchHistoryEvent';
  @override
  List<Object> get props => [];
}

class HomepageDbInitEvent extends HomePageEvent {
  const HomepageDbInitEvent();
  @override
  List<Object> get props => [];
}

class HomePageAddSearchHistoryEvent extends HomePageEvent {
  final PrevozRoute route;
  const HomePageAddSearchHistoryEvent(this.route);
  @override
  String toString() => 'HomePageAddSearchHistoryEvent';
  @override
  List<Object> get props => [route];
}

//? lahko simuliram, da je v search history tako
//? da se dela vsak fetch posebaj,
//? cetudi se dela bundle fetch...
class HomePageAggregateFetch extends HomePageEvent {
  final PrevozRoute route;
  const HomePageAggregateFetch(this.route);
  @override
  String toString() => 'HomePageAggregateFetch';
  @override
  List<Object> get props => [route];
}

class HomePageSingleFetch extends HomePageEvent {
  const HomePageSingleFetch();
  @override
  String toString() => 'HomePageSingleFetch';
  @override
  List<Object> get props => [];
}
