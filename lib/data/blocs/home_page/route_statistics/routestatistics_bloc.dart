import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import './bloc.dart';

class RouteStatisticsBloc
    extends Bloc<RouteStatisticsEvent, RouteStatisticsState> {
  final RidesRepository ridesRepo;
  RouteStatisticsBloc({@required this.ridesRepo}) : super(InitialRoutestatisticsState());

  RouteStatisticsRequest latestStatsRequest;
  RouteStatisticsResult latestStatsResult;

  @override
  Stream<RouteStatisticsState> mapEventToState(
    RouteStatisticsEvent event,
  ) async* {
    // TODO: Add Logic

    if (event is FetchStatsForASingleRoute) {
      yield* _mapFetchStatsForSingleRoute(event);
    }
  }

  Stream<RouteStatisticsState> _mapFetchStatsForSingleRoute(
      FetchStatsForASingleRoute event) async* {
    try {
      RouteStatisticsRequest singleRouteRequest = RouteStatisticsRequest(
          routes: [event.route],
          dates: DateTimeUtils.todayTomorrowDayAfterTomorrow());

      yield LoadingSingleRouteStats(
          route: event.route, indexInSearchHistory: event.indexInSearchHistory);

      RouteStatisticsResult newResult =
          await ridesRepo.fetchRouteStatistics(singleRouteRequest);

      yield LoadedSingleRouteStats(
          result: newResult,
          route: event.route,
          indexInSearchHistory: event.indexInSearchHistory);
    } catch (e) {
      ErrorLoadingSingleRoute(
          route: event.route, indexInSearchHistory: event.indexInSearchHistory);
    }
  }

  void fetchSingleRoute(PrevozRoute route, int indexInSearchHistory) {
    FetchStatsForASingleRoute event = FetchStatsForASingleRoute(
        route: route, indexInSearchHistory: indexInSearchHistory);

    add(event);
  }

  bool blocHasData() {
    print("_______bloc has data --> " +
        (latestStatsRequest != null && latestStatsResult != null).toString());
    return latestStatsRequest != null && latestStatsResult != null;
  }

  bool isSameRequest(RouteStatisticsRequest newRequest) {
    print("______is same request --> " +
        RouteStatisticsRequest.isLatestRequestEqualToNewOne(
                latestRequest: latestStatsRequest, newRequest: newRequest)
            .toString());

    return RouteStatisticsRequest.isLatestRequestEqualToNewOne(
        latestRequest: latestStatsRequest, newRequest: newRequest);
  }
}
