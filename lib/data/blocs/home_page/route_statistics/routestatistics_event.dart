import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';

@immutable
abstract class RouteStatisticsEvent extends Equatable {
  const RouteStatisticsEvent();
}

class FetchStatsForAllRoutes extends RouteStatisticsEvent {
  const FetchStatsForAllRoutes({@required this.routes});
  final List<PrevozRoute> routes;
  @override
  List<Object> get props => [routes];
}

class FetchStatsForASingleRoute extends RouteStatisticsEvent {
  final PrevozRoute route;
  final int indexInSearchHistory;
  FetchStatsForASingleRoute(
      {@required this.route, @required this.indexInSearchHistory});
  @override
  List<Object> get props => [route, indexInSearchHistory];
}
