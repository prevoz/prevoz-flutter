import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';

@immutable
abstract class RouteStatisticsState extends Equatable {
  const RouteStatisticsState();
}

class InitialRoutestatisticsState extends RouteStatisticsState {
  const InitialRoutestatisticsState();
  @override
  String toString() => 'InitialRoutestatisticsState';
  @override
  List<Object> get props => [];
}

class LoadingSingleRouteStats extends RouteStatisticsState {
  final PrevozRoute route;
  final int indexInSearchHistory;
  const LoadingSingleRouteStats(
      {@required this.route, @required this.indexInSearchHistory});
  @override
  String toString() => 'LoadingSingleRouteStats';
  @override
  List<Object> get props => [route, indexInSearchHistory];
}

class LoadedSingleRouteStats extends RouteStatisticsState {
  final PrevozRoute route;
  final RouteStatisticsResult result;
  final int indexInSearchHistory;
  const LoadedSingleRouteStats(
      {@required this.result, @required this.route, this.indexInSearchHistory});
  @override
  String toString() => 'LoadedSingleRouteStats';
  @override
  List<Object> get props => [route, result, indexInSearchHistory];
}

class ErrorLoadingSingleRoute extends RouteStatisticsState {
  final PrevozRoute route;
  final int indexInSearchHistory;
  const ErrorLoadingSingleRoute(
      {@required this.route, @required this.indexInSearchHistory});

  @override
  String toString() => 'ErrorLoadingSingleRoute';
  @override
  List<Object> get props => [route, indexInSearchHistory];
}
