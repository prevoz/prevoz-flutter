import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/helpers/event_bus/database_initialisation.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/data/repositories/search_history_repository.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import './bloc.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  final RidesRepository ridesRepo;
  final SearchHistoryRepository searchHistoryRepository;

  //* SearchFormCard is placed inside a ListView. This means that when its scrolled out of position
  //* it loses its state and data on its inputs... we handle this with the bellow fields inside this bloc
  Location fromLocation;
  Location toLocation;
  DateTime selectedDate = DateTime.now();

  HomePageBloc(
      {@required this.ridesRepo, @required this.searchHistoryRepository}) : super(InitialHomePageState()) {
    _listenToDbInitialisationEvents();
  }

  void _listenToDbInitialisationEvents() {
    MyApp.eventBus
        .on<DatabaseInitialisationEvent>()
        .listen((DatabaseInitialisationEvent event) {
      if (DbInitState.isInitInProgress()) {
        add(HomepageDbInitEvent());
      } else {
        add(HomePageLoadSearchHistoryEvent());
      }
    });
  }

  @override
  Stream<HomePageState> mapEventToState(
    HomePageEvent event,
  ) async* {
    if (event is HomePageLoadSearchHistoryEvent) {
      print("______!!!!_____ loading src historz");
      yield* _mapLoadSearchHistoryEventToState();
    }

    if (event is HomepageDbInitEvent) {
      yield InitialisingSearchHistory();
    }

    if (event is HomePageAddSearchHistoryEvent) {
      _mapAddSearchHistoryEvent(event.route);
    }
  }

  _mapAddSearchHistoryEvent(PrevozRoute route) {
    try {
      searchHistoryRepository.addRouteToSearchHistory(route);
    } catch (e) {
      handleError(
          error: e, message: "errror while adding new route to search history");
    }
  }

  //TODO: add if (blocHasData() && isSameRequest(newRequest)) for route stats
  //TODO taken from route stats bloc...
  Stream<HomePageState> _mapLoadSearchHistoryEventToState() async* {
    try {
      yield LoadingSearchHistory();
      final List<PrevozRoute> searchHistoryRoutes =
          await this.searchHistoryRepository.getSearchHistoryRoutes(6);
      //* search history loaded, start loading stats from API
      yield LoadingRouteStats(searchHistoryRoutes);
      if (searchHistoryRoutes != null) {
        RouteStatisticsRequest statsRequestForAllRoute = RouteStatisticsRequest(
            routes: searchHistoryRoutes,
            dates: DateTimeUtils.todayTomorrowDayAfterTomorrow());

        RouteStatisticsResult statsResultForAllRoutes =
            await ridesRepo.fetchRouteStatistics(statsRequestForAllRoute);

        if (statsResultForAllRoutes == null) {
          throw APIErrorStats("Route stats result was null");
        }

        yield StatsForAllRoutesLoaded(
            statsResultForAllRoutes, searchHistoryRoutes);
      } else {
        throw DBErrorHistory("Search history not loaded from DB");
      }
    } on APIErrorStats {
      yield ErrorLoadingRoutesStats();
    } on DBErrorHistory {
      _checkDbSanity();
      yield ErrorLoadingSearchHistory();
    } catch (_) {
      _checkDbSanity();
      yield ErrorLoadingSearchHistory();
    }
  }

  void addToSearchHistory(RideSearchRequest request) {
    toLocation = Location(
        name: request.route.locationTo, country: request.route.countryTo);
    fromLocation = Location(
        name: request.route.locationFrom, country: request.route.countryFrom);
    selectedDate = request.date;
    add(HomePageAddSearchHistoryEvent(request.route));
  }

  void loadSearchHistory() {
    add(HomePageLoadSearchHistoryEvent());
  }

  _checkDbSanity() {
    this.searchHistoryRepository.checkDBSanity();
  }
}

class DBErrorHistory implements Exception {
  String cause;
  DBErrorHistory(this.cause);
}

class APIErrorStats implements Exception {
  String cause;
  APIErrorStats(this.cause);
}
