import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';

@immutable
abstract class HomePageState extends Equatable {
  const HomePageState();
}

class InitialHomePageState extends HomePageState {
  const InitialHomePageState();
  @override
  String toString() => 'InitialHomePageState';
  @override
  List<Object> get props => [];
}

class InitialisingSearchHistory extends HomePageState {
  const InitialisingSearchHistory();
  @override
  String toString() => 'InitialisingSearchHistory';
  @override
  List<Object> get props => [];
}

class LoadingSearchHistory extends HomePageState {
  const LoadingSearchHistory();
  @override
  String toString() => 'LoadingSearchHistory';
  @override
  List<Object> get props => [];
}

class LoadingRouteStats extends HomePageState {
  final List<PrevozRoute> routesSearchHistory;
  const LoadingRouteStats(this.routesSearchHistory);
  @override
  String toString() => 'LoadingRouteStats';
  @override
  List<Object> get props => [routesSearchHistory];
}

// loads from DB
class SearchHistoryLoaded extends HomePageState {
  final List<PrevozRoute> routesSearchHistory;
  const SearchHistoryLoaded(this.routesSearchHistory);
  @override
  String toString() => 'SearchHistoryLoaded';
  @override
  List<Object> get props => [routesSearchHistory];
}

/// stats from API for all routes
class StatsForAllRoutesLoaded extends HomePageState {
  final RouteStatisticsResult routeStatsResult;
  final List<PrevozRoute> routes;
  const StatsForAllRoutesLoaded(this.routeStatsResult, this.routes);
  @override
  String toString() => 'StatsForAllRoutesLoaded';
  @override
  List<Object> get props => [routeStatsResult, routes];
}

class ErrorLoadingSearchHistory extends HomePageState {
  const ErrorLoadingSearchHistory();
  String toString() => 'ErrorLoadingSearchHistory';
  @override
  List<Object> get props => [];
}

class ErrorLoadingRoutesStats extends HomePageState {
  const ErrorLoadingRoutesStats();
  String toString() => 'ErrorLoadingRoutesStats';
  @override
  List<Object> get props => [];
}
