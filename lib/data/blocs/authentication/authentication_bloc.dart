import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/auth_error_type.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:sentry/sentry.dart';
import 'authentication_event.dart';
import 'authentication_state.dart';
import 'package:prevoz_org/data/models/user.dart' as User;
import 'package:prevoz_org/data/repositories/auth_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthRepository authRepository;
  Logger logger = Logger(printer: CustomLogPrinter("AuthBloc"));
  FirebaseAnalytics analytics;

  AuthenticationBloc({@required this.authRepository, this.analytics})
      : assert(authRepository != null), super(AuthenticationUninitialized());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      String bearerToken = await authRepository.getApiBearer();
      if (bearerToken != null) {
        yield* _exchangeBearerTokenOnAppStarted(bearerToken);
      } else {
        yield* _mapAppStartedToState(event);
      }
    }

    if (event is AuthenticationInvalidToken) {
      yield* _mapAuthenticationInvalidTokenToState(event);
    }

    if (event is AuthenticationGoogleSignIn) {
      yield* _mapAuthenticationGoogleSignInToState(event);
    }

    if (event is AuthenticationAppleSignIn) {
      yield* _mapAuthenticationAppleSignInToState(event);
    }

    if (event is AuthenticationLoggedIn) {
      yield* _mapAuthenticationLoggedInToState(event);
    }

    if (event is AuthenticationActivateAccount && !this.isUserLoggedIn()) {
      yield* _mapAuthenticationActivateAccountToState(event);
    }

    if (event is AuthenticationRefreshAccountStatus) {
      yield* _mapAuthenticationRefreshAccountStatusToState(event);
    }

    else if (event is LogoutEvent) {
      yield AuthStateLoading();
      await authRepository.logout();
      yield AuthStateUnauthenticated();
    }
  }

  Stream<AuthenticationState> _exchangeBearerTokenOnAppStarted(String bearerToken) async* {
    try {
      await authRepository.clearApiBearer();
      yield AuthStateLoading();
      Response response = await authRepository.exchangeBearerToken(bearerToken);
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null) {
        await authRepository.logout();
        yield AuthStateUnauthenticated();
      } else {
        String token = response.data["token"];
        yield* _mapAuthenticationLoggedInToState(AuthenticationLoggedIn(token: token));
      }
    } catch (e) {
      await authRepository.logout();
      yield AuthStateUnauthenticated();
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthenticationBloc, _exchangeBearerTokenOnAppStarted()",
          StackTrace.fromString(e.toString()));
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState(AppStarted event) async* {
    try {
      yield AuthStateLoading();
      final User.User user = await authRepository.getUserFromSharedPrefs();
      logger.d("User data, $user");
      if (user != null) {
        // We need to check the current account status
        Response accountStatusResponse = await authRepository.getAccountStatus();
        //* if 12 hours have passed since last login, check backend if user has updated phone numbers
        if (user.lastLoginTimestamp + (12 * 3600 * 1000) <
            DateTime.now().millisecondsSinceEpoch) {
          //* 1) fetch user profile
          Response userProfileResponse = await authRepository.getUserProfile();
          //* 2) get users phone numbers
          List<dynamic> phoneNumbers;
          if (userProfileResponse.data["phone_numbers"] != null) {
            phoneNumbers = userProfileResponse.data["phone_numbers"];
            logger.d("Saving user phone numbers, $phoneNumbers");
            user.phoneNumbers = phoneNumbers;
            await authRepository.savePhoneNumbersToSharedPrefs(phoneNumbers);
          }
        }

        yield AuthStateAuthenticated(user.username, user.phoneNumbers, accountStatusResponse.data["needs_verification"]);
      } else {
        authRepository..logout();
        yield AuthStateUnauthenticated();
      }
    } catch (e) {
      if (e is DioError && e.type == DioErrorType.DEFAULT && e.error is SocketException) {
        yield AuthStateError(type: AuthErrorType.CONNECTION_ERROR);
      }
      else {
        authRepository..logout();
        yield AuthStateUnauthenticated();
        Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthenticationBloc, _mapAppStartedToState()",
            StackTrace.fromString(e.toString()));
      }
    }
  }

  Stream<AuthenticationState> _mapAuthenticationInvalidTokenToState(AuthenticationInvalidToken event) async* {
    authRepository.logout();
    yield AuthStateError(type: AuthErrorType.INVALID_TOKEN_ERROR);
    Injector.getInjector().get<SentryErrorReporter>().submitMessageToSentry(
      message: "Api token was invalid",
      level: SeverityLevel.info
    );
  }

  Stream<AuthenticationState> _mapAuthenticationGoogleSignInToState(AuthenticationGoogleSignIn event) async* {
    try {
      yield AuthStateLoading();
      Response response = await authRepository.signInWithGoogle();
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null) {
        authRepository.logout();
        yield AuthStateError(type: AuthErrorType.SIGN_IN_ERROR);
      } else {
        String token = response.data["token"];
        yield* _mapAuthenticationLoggedInToState(AuthenticationLoggedIn(token: token));
      }
    } catch (e) {
      await authRepository.saveAuthFailToSharedPrefs();
      authRepository.logout();
      yield AuthStateError(type: AuthErrorType.SIGN_IN_ERROR);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthBloc, _mapAuthenticationGoogleSignInToState()",
          StackTrace.fromString(e.toString()));
    }
  }

  Stream<AuthenticationState> _mapAuthenticationAppleSignInToState(event) async* {
    try {
      yield AuthStateLoading();
      Response response = await authRepository.signInWithApple();
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null) {
        authRepository.logout();
        yield AuthStateError(type: AuthErrorType.SIGN_IN_ERROR);
      } else {
        String token = response.data["token"];
        yield* _mapAuthenticationLoggedInToState(AuthenticationLoggedIn(token: token));
      }
    } catch (e) {
      await authRepository.saveAuthFailToSharedPrefs();
      authRepository.logout();
      yield AuthStateError(type: AuthErrorType.SIGN_IN_ERROR);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthBloc, _mapAuthenticationAppleSignInToState()",
          StackTrace.fromString(e.toString()));
    }
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedInToState(AuthenticationLoggedIn event) async* {
    try {
      yield AuthStateLoading();
      String token = event.token;
      // save the token
      await authRepository.saveToken(event.token);
      // fetch the account status
      Response accountStatusResponse = await authRepository.getAccountStatus();
      String username = accountStatusResponse.data["username"];
      // fetch user profile
      Response userProfileResponse = await authRepository.getUserProfile();
      // get users phone numbers
      List<dynamic> phoneNumbers;
      if (userProfileResponse.data["phone_numbers"] != null) {
        phoneNumbers = userProfileResponse.data["phone_numbers"];
      }
      if (accountStatusResponse.data["is_authenticated"] == "true") {
        //* 6. save auth and profile data to shared preferences
        await authRepository.saveAuthData(token: token, username: username);
        await authRepository.savePhoneNumbersToSharedPrefs(phoneNumbers);
        //* 7. successfull authentication
        analytics?.logLogin();
        yield AuthStateAuthenticated(
            userProfileResponse.data["email"] != null
                ? userProfileResponse.data["email"]
                : accountStatusResponse.data["username"],
            phoneNumbers,
            accountStatusResponse.data["needs_verification"]);
      } else {
        throw new Exception("User account not confirmed");
      }
    } catch (e) {
      authRepository.logout();
      await authRepository.saveAuthFailToSharedPrefs();
      yield AuthStateError(type: AuthErrorType.SIGN_IN_ERROR);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthBloc, _mapAuthenticationLoggedInToState()",
          StackTrace.fromString(e.toString()));
    }
  }

  Stream<AuthenticationState> _mapAuthenticationActivateAccountToState(AuthenticationActivateAccount event) async* {
    try {
      yield AuthStateLoading();
      Response response = await authRepository.activateAccount(activationToken: event.activationToken);
      print(response);
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null || response.data["status"] == "error") {
        authRepository.logout();
        yield AuthStateError(type: AuthErrorType.ACCOUNT_ACTIVATION_ERROR);
      }
      else {
        print("Successful activation");
        String token = response.data["token"];
        yield* _mapAuthenticationLoggedInToState(AuthenticationLoggedIn(token: token));
      }
    } catch (e) {
      authRepository.logout();
      yield AuthStateError(type: AuthErrorType.ACCOUNT_ACTIVATION_ERROR);
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthBloc, _mapAuthenticationActivateAccountToState()",
          StackTrace.fromString(e.toString()));
    }
  }

  Stream<AuthenticationState> _mapAuthenticationRefreshAccountStatusToState(AuthenticationRefreshAccountStatus event) async* {
    try {
      yield AuthStateLoading();
      Response response = await authRepository.getAccountStatus();
      if (response.statusCode != 200 || response.data["error"] != null || response.data["detail"] != null || response.data["status"] == "error") {
        authRepository.logout();
        yield AuthStateError(type: AuthErrorType.INVALID_TOKEN_ERROR);
      } else {
        final User.User user = await authRepository.getUserFromSharedPrefs();
        yield AuthStateAuthenticated(user.username, user.phoneNumbers, response.data["needs_verification"]);
      }
    } catch (e) {
      if (e is DioError && e.type == DioErrorType.DEFAULT && e.error is SocketException) {
        yield AuthStateError(type: AuthErrorType.CONNECTION_ERROR);
      }
      else {
        authRepository..logout();
        yield AuthStateUnauthenticated();
        Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthBloc, _mapAuthenticationSilentlyRefreshAccountStatusToState()",
            StackTrace.fromString(e.toString()));
      }
    }
  }

  bool isUserLoggedIn() {
    return state is AuthStateAuthenticated;
  }
}
