import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:prevoz_org/data/blocs/authentication/auth_error_type.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();
}

class AuthenticationUninitialized extends AuthenticationState {
  const AuthenticationUninitialized();
  @override
  String toString() => 'AuthenticationUninitialized';

  @override
  List<Object> get props => [];
}

class AuthStateAuthenticated extends AuthenticationState {
  final String email;
  final List<dynamic> phoneNumbers;
  final bool needsVerification;
  const AuthStateAuthenticated(this.email, this.phoneNumbers, this.needsVerification);

  @override
  String toString() => 'AuthStateAuthenticated';

  @override
  List<Object> get props => [email, phoneNumbers, needsVerification];
}

//class AuthStateReceivedAccessToken extends AuthenticationState {
//  const AuthStateReceivedAccessToken();
//
//  @override
//  String toString() => 'AuthStateReceivedAccessToken';
//
//  @override
//  List<Object> get props => [];
//}

class AuthStateError extends AuthenticationState {
  final AuthErrorType type;
  final String authError;
  const AuthStateError({
    @required this.type,
    this.authError
  });
  @override
  String toString() => 'AuthStateError';
  @override
  List<Object> get props => [type, authError];
}

class AuthStateUnauthenticated extends AuthenticationState {
  const AuthStateUnauthenticated();
  @override
  String toString() => 'AuthStateUnauthenticated';
  @override
  List<Object> get props => [];
}

class AuthStateLoading extends AuthenticationState {
  const AuthStateLoading();
  @override
  String toString() => 'AuthStateLoading';
  @override
  List<Object> get props => [];
}
