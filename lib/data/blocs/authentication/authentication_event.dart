import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();
}

class AppStarted extends AuthenticationEvent {
  const AppStarted();
  @override
  String toString() => 'AppStarted';
  @override
  List<Object> get props => [];
}

class AuthenticationRefreshAccountStatus extends AuthenticationEvent {
  const AuthenticationRefreshAccountStatus();
  @override
  String toString() => 'AuthenticationSilentlyRefreshAccountStatus';
  @override
  List<Object> get props => [];
}

class AuthenticationAuthenticatedEvent extends AuthenticationEvent {
  const AuthenticationAuthenticatedEvent();
  @override
  String toString() => 'AuthenticationAuthenticatedEvent';
  @override
  List<Object> get props => [];
}

class AuthenticationGoogleSignIn extends AuthenticationEvent {
  const AuthenticationGoogleSignIn();
  @override
  String toString() => 'AuthenticationGoogleSignIn';
  @override
  List<Object> get props => [];
}

class AuthenticationAppleSignIn extends AuthenticationEvent {
  const AuthenticationAppleSignIn();
  @override
  String toString() => 'AuthenticationAppleSignIn';
  @override
  List<Object> get props => [];
}

class AuthenticationLoggedIn extends AuthenticationEvent {
  final String token;
  const AuthenticationLoggedIn({@required this.token});
  String toString() => 'AuthenticationLoggedIn';
  @override
  List<Object> get props => [token];
}

class AuthenticationActivateAccount extends AuthenticationEvent {
  final String activationToken;
  const AuthenticationActivateAccount({@required this.activationToken});
  String toString() => 'AuthenticationActivateAccount';
  @override
  List<Object> get props => [activationToken];
}

class AuthenticationInvalidToken extends AuthenticationEvent {
  const AuthenticationInvalidToken();
  String toString() => 'AuthenticationInvalidToken';
  @override
  List<Object> get props => [];
}

class LogoutEvent extends AuthenticationEvent {
  const LogoutEvent();
  @override
  String toString() => 'LogoutEvent';
  @override
  List<Object> get props => [];
}


