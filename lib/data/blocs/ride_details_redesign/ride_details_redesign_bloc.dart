import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:prevoz_org/data/blocs/ride_details_redesign/ride_details_redesign_event.dart';
import 'package:prevoz_org/data/blocs/ride_details_redesign/ride_details_redesign_state.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';

class RideDetailsRedesignBloc
    extends Bloc<RideDetailsRedesignEvent, RideDetailsRedesignState> {
  final RidesRepository ridesRepo;
  RideDetailsRedesignBloc({this.ridesRepo}) : super(InitialRideDetailsRedesignState());

  @override
  Stream<RideDetailsRedesignState> mapEventToState(
    RideDetailsRedesignEvent event,
  ) async* {
    // TODO: Add Logic

    if (event is RideDetailsRedesignSetBookmark) {
      print("_____ event is SetBookmark");
      try {
        print(
            "_____ event is SetBookmark, state is gonna be SavingBookmarkState, current state is " +
                state.toString());

        yield RideDetailsRedesignSavingBookmarkState();

        print(
            "_____state should be SavingBookmarkState ... " + state.toString());

        Response response =
            await ridesRepo.setRideBookmark(event.id, event.state);

        if (response == null) {
          throw Exception("null response");
        }
        if (response != null &&
            response.data != null &&
            response.data["error"] != null) {
          throw Exception(response.data["error"]);
        }

        print("___ bookmark response was fine: " + response.toString());

        yield RideDetailsRedesignBookmarkSavedState(event.state);

        print(
            "_____state should be BookmarkSavedState ... " + state.toString());
      } catch (e) {
        print("___ error saving bookmark: " + e.toString());
        yield RideDetailsRedesignErrorSavingBookmarkState();
      }
    }

    if (event is RideDetailsRedesignResetState) {
      yield InitialRideDetailsRedesignState();
    }
  }

  void resetToInitialState() {
    add(RideDetailsRedesignResetState());
  }
}
