import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RideDetailsRedesignEvent extends Equatable {
  const RideDetailsRedesignEvent();
}

class RideDetailsRedesignSetBookmark extends RideDetailsRedesignEvent {
  final String state;
  final String id;
  const RideDetailsRedesignSetBookmark(this.state, this.id);
  @override
  String toString() => 'RideDetailsRedesignSetBookmark';
  @override
  List<Object> get props => [state, id];
}

class RideDetailsRedesignResetState extends RideDetailsRedesignEvent {
  const RideDetailsRedesignResetState();
  @override
  String toString() => 'RideDetailsRedesignResetState';
  @override
  List<Object> get props => [];
}
