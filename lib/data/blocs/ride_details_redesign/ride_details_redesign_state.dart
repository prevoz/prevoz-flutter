import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RideDetailsRedesignState extends Equatable {
  const RideDetailsRedesignState();
}

class InitialRideDetailsRedesignState extends RideDetailsRedesignState {
  const InitialRideDetailsRedesignState();
  @override
  String toString() => "InitialRideDetailsRedesignState";
  @override
  List<Object> get props => [];
}

class RideDetailsRedesignSavingBookmarkState extends RideDetailsRedesignState {
  const RideDetailsRedesignSavingBookmarkState();
  @override
  String toString() => "RideDetailsRedesignSavingBookmarkState";
  @override
  List<Object> get props => [];
}

class RideDetailsRedesignBookmarkSavedState extends RideDetailsRedesignState {
  final String state;
  const RideDetailsRedesignBookmarkSavedState(this.state);
  @override
  String toString() => "RideDetailsRedesignBookmarkSavedState";
  @override
  List<Object> get props => [state];
}

class RideDetailsRedesignErrorSavingBookmarkState
    extends RideDetailsRedesignState {
  const RideDetailsRedesignErrorSavingBookmarkState();
  @override
  String toString() => "RideDetailsRedesignErrorSavingBookmarkState";
  @override
  List<Object> get props => [];
}
