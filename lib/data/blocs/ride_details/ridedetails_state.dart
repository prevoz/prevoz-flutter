import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RideDetailsState extends Equatable {
  const RideDetailsState();
}

class InitialRideDetailsState extends RideDetailsState {
  const InitialRideDetailsState();
  @override
  String toString() => "InitialRideDetailsState";
  @override
  List<Object> get props => [];
}

class SavingBookmark extends RideDetailsState {
  const SavingBookmark();
  @override
  String toString() => "SavingBookmark";
  @override
  List<Object> get props => [];
}

class BookmarkSaved extends RideDetailsState {
  final String state;
  const BookmarkSaved(this.state);
  @override
  String toString() => "BookmarkSaved";
  @override
  List<Object> get props => [state];
}

class ErrorSavingBookmark extends RideDetailsState {
  const ErrorSavingBookmark();
  @override
  String toString() => "ErrorSavingBookmark";
  @override
  List<Object> get props => [];
}
