import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:prevoz_org/data/blocs/ride_details/ridedetails_event.dart';
import 'package:prevoz_org/data/blocs/ride_details/ridedetails_state.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';

class RideDetailsBloc extends Bloc<RideDetailsEvent, RideDetailsState> {
  final RidesRepository ridesRepo;

  RideDetailsBloc(this.ridesRepo) : super(InitialRideDetailsState());

  @override
  Stream<RideDetailsState> mapEventToState(
    RideDetailsEvent event,
  ) async* {
    if (event is RideDetailsSetBookmark) {
      try {
        yield SavingBookmark();

        Response response =
            await ridesRepo.setRideBookmark(event.id, event.state);

        if (response == null) {
          throw Exception("null response");
        }
        if (response != null &&
            response.data != null &&
            response.data["error"] != null) {
          throw Exception(response.data["error"]);
        }

        print("___ bookmark response was fine");

        yield BookmarkSaved(event.state);
      } catch (e) {
        print("___ error saving bookmark: " + e.toString());
        yield ErrorSavingBookmark();
      }
    }

    if (event is RideDetailsResetState) {
      yield InitialRideDetailsState();
    }
  }

  void resetToInitialState() {
    add(RideDetailsResetState());
  }
}
