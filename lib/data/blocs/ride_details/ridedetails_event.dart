import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RideDetailsEvent extends Equatable {
  const RideDetailsEvent();
}

class RideDetailsSetBookmark extends RideDetailsEvent {
  final String state;
  final String id;
  const RideDetailsSetBookmark(this.state, this.id);

  @override
  String toString() => 'RideDetailsSetBookmark';

  @override
  List<Object> get props => [state, id];
}

class RideDetailsResetState extends RideDetailsEvent {
  const RideDetailsResetState();
  @override
  String toString() => 'RideDetailsResetState';
  @override
  List<Object> get props => [];
}
