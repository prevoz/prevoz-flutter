

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class EditProfileEvent extends Equatable {
  const EditProfileEvent();
}

class PostFirstAndLastName extends EditProfileEvent {
  final String firstName;
  final String lastName;
  const PostFirstAndLastName({this.firstName, this.lastName});
  @override
  String toString() => 'PostProfile';
  @override
  List<Object> get props => [firstName, lastName];
}

class PostAddress extends EditProfileEvent {
  final String address;
  const PostAddress({this.address});
  @override
  String toString() => 'PostAddress';
  @override
  List<Object> get props => [address];
}

class Reset extends EditProfileEvent {
  const Reset();
  @override
  String toString() => 'ResetState';
  @override
  List<Object> get props => [];
}