import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_event.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_state.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/data/repositories/user_profile_repository.dart';

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  final UserProfileRepository _profileRepository;

  EditProfileBloc(this._profileRepository) : super(InitialEditProfileState());

  @override
  Stream<EditProfileState> mapEventToState(EditProfileEvent event) async* {
    if (event is PostFirstAndLastName) {
      yield* _mapPostNameToState(event);
    }
    else if (event is PostAddress) {
      yield* _mapPostAddressToState(event);
    }
    else if (event is Reset) {
      yield InitialEditProfileState();
    }
  }

  Stream<EditProfileState> _mapPostNameToState(PostFirstAndLastName event) async* {
    try {
      yield SubmittingProfile();
      var data = {
        "first_name": event.firstName,
        "last_name": event.lastName
      };
      Response response = await _profileRepository.postProfile(data);
      if (response.statusCode != 200 || response.data["error"] != null) {
        yield ErrorSubmittingProfile(response);
      } else {
        UserProfile userProfile = UserProfile.fromResponse(response);
        await _profileRepository.saveToSharedPrefs(userProfile);
        yield SuccessfullySubmittedProfile(userProfile);
      }
    } catch (e) {
      debugPrint("____EditProfileBLOc, error posting profile");
      debugPrint(e.toString());
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorSubmittingProfile(errResp);
    }
  }

  Stream<EditProfileState> _mapPostAddressToState(PostAddress event) async* {
    try {
      yield SubmittingProfile();
      var data = {
        "address": event.address
      };
      Response response = await _profileRepository.postProfile(data);
      if (response.statusCode != 200 || response.data["error"] != null) {
        yield ErrorSubmittingProfile(response);
      } else {
        UserProfile userProfile = UserProfile.fromResponse(response);
        await _profileRepository.saveToSharedPrefs(userProfile);
        yield SuccessfullySubmittedProfile(userProfile);
      }
    } catch (e) {
      debugPrint("____EditProfileBLOc, error posting profile");
      debugPrint(e.toString());
      Response errResp = Response();
      errResp.statusMessage = e.toString();
      yield ErrorSubmittingProfile(errResp);
    }
  }
}