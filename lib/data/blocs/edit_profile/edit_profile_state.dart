
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/models/userProfile.dart';

@immutable
abstract class EditProfileState extends Equatable {
  const EditProfileState();
}

class InitialEditProfileState extends EditProfileState {
  const InitialEditProfileState();
  @override
  String toString() => 'InitialEditProfileState';
  @override
  List<Object> get props => [];
}

class SubmittingProfile extends EditProfileState {
  const SubmittingProfile();
  @override
  String toString() => 'SubmittingProfile';
  @override
  List<Object> get props => [];
}

class SuccessfullySubmittedProfile extends EditProfileState {
  final UserProfile userProfile;
  const SuccessfullySubmittedProfile(this.userProfile);
  @override
  String toString() => 'SuccessfullySubmittedProfile';
  @override
  List<Object> get props => [userProfile];
}

class ErrorSubmittingProfile extends EditProfileState {
  final Response response;
  const ErrorSubmittingProfile(this.response);
  @override
  String toString() => 'ErrorSubmittingProfile';
  @override
  List<Object> get props => [response];
}