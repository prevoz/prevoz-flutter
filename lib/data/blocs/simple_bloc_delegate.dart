import 'package:bloc/bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/utils/app_config.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class SimpleBlocObserver extends BlocObserver {
  final AppConfig appConfig;
  SimpleBlocObserver(this.appConfig);
  Logger logger = Logger(printer: CustomLogPrinter("BlocDelegate"));

  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);

    if (appConfig.debugMode) {
      logger.i(event.toString());
    }
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);

    if (appConfig.debugMode) {
      logger.d(transition.toString());
    }
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stacktrace) {
    super.onError(cubit, error, stacktrace);
    logger.e(error.toString());
    Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("Bloc delegate error: " + error.toString(), stacktrace);
  }
}
