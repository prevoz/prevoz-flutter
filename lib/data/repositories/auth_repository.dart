import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/models/user.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';
import 'package:dio/dio.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:prevoz_org/utils/sign_in_with_apple_wrapper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AuthRepository {
  final BasePrevozApi api;
  final BaseAuthHelper baseAuthHelper;
  final SignInWithAppleWrapper signInWithAppleWrapper;
  final GoogleSignIn googleSignIn;
  final SharedPreferences sharedPreferences;
  Logger logger;

  AuthRepository({
    @required this.api,
    @required this.baseAuthHelper,
    @required this.signInWithAppleWrapper,
    @required this.googleSignIn,
    @required this.sharedPreferences,
    @required this.logger
  });

  Future<String> getApiBearer() async {
    return baseAuthHelper.getApiBearer();
  }

  Future<void> clearApiBearer() async {
    await sharedPreferences.setString(MyConstants.PREF_KEY_BEARER_TOKEN, null);
  }

  void _setApiToken(String token) {
    api.setApiToken(token);
  }

  Future<void> saveToken(String token) async {
    _setApiToken(token);
    await sharedPreferences.setString(MyConstants.PREF_KEY_API_TOKEN, token);
  }

  Future getAccountStatus() async {
    return await this.api.getAccountStatus();
  }

  Future getUserProfile() async {
    return await this.api.getUserProfile();
  }

  //todo: the repository should be using only an interface for local storage data
  //todo: refactor this and make it testable
  /// gets localy stored user data
  Future<User> getUserFromSharedPrefs() async {
    try {
      User authenticatedUser = await baseAuthHelper.getUserFromSharedPrefs();
      if (authenticatedUser == null) {
        return null;
      }

      //todo: check if access token has expired before setting api bearer
      _setApiToken(authenticatedUser.accessToken);

      return authenticatedUser;
    } catch (e) {
      _setApiToken(null);
      print("Error while checking authentication");
      print(e.toString());
      return null;
    }
  }

  Future<void> saveAuthFailToSharedPrefs() async {
    await sharedPreferences.setInt(MyConstants.KEY_ERROR_CODE,
        9); // 9 = from android, AccountManager.ERROR_CODE_BAD_AUTHENTICATION
    await sharedPreferences.setString(
        MyConstants.KEY_ERROR_MSG, "Failed to confirm authentication.");
  }

  Future<void> savePhoneNumbersToSharedPrefs(
      List<dynamic> apiPhoneNumbers) async {
    if (apiPhoneNumbers != null) {
      await sharedPreferences.setStringList(MyConstants.USERS_PHONE_NUMBERS,
          List<String>.from(apiPhoneNumbers));
    }
  }

  Future<void> saveAuthData({
    @required String token,
    @required String username
  }) async {
      await sharedPreferences.setString(MyConstants.PREF_KEY_API_TOKEN, token);
      await sharedPreferences.setString(MyConstants.PREF_KEY_USERNAME, username);
      await sharedPreferences.setInt(
          MyConstants.LAST_LOGIN, DateTime.now().millisecondsSinceEpoch);
  }

  Future<void> logout() async {
    api.setApiToken(null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_USERNAME, null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_EMAIL, null);
    await sharedPreferences.setString(MyConstants.USERS_PHONE_NUMBERS, null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_FIRST_NAME, null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_LAST_NAME, null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_ADDRESS, null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_API_TOKEN, null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_BEARER_TOKEN, null);
    await sharedPreferences.setString(MyConstants.PREF_KEY_REFRESH_TOKEN, null);
    await sharedPreferences.setString(
        MyConstants.KEY_ACCOUNT_TYPE, MyConstants.TYPE_PREVOZ_ACCOUNT);

    await sharedPreferences.setBool(MyConstants.KEY_OAUTH2, true);

    await sharedPreferences.setInt(MyConstants.KEY_EXPIRES, null);
    googleSignIn.signOut();
  }

  Future<Response> exchangeBearerToken(String bearer) async {
    return await api.exchangeBearerToken(bearer);
  }

  Future<Response> login(String username, String password) async {
    return await api.login(username, password);
  }

  Future<Response> signInWithGoogle() async {
    try {
      final GoogleSignInAccount googleUser = await googleSignIn.signIn();
      // The documentation specifies null for canceled sign in
      if (googleUser == null) {
        Response errorResponse = Response();
        errorResponse.statusCode = 400;

        return errorResponse;
      }

      final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

      return await api.uploadGoogleToken(idToken: googleAuth.idToken);
    } catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthRepo, signInWithGoogle()",
          StackTrace.fromString(e.toString()));

      Response errorResponse = Response();
      errorResponse.statusCode = 400;

      return errorResponse;
    }
  }

  Future<Response> signInWithApple() async {
    try {
      final credential = await signInWithAppleWrapper.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      );

      return await api.uploadAppleCode(code: credential.authorizationCode);
    } catch (e) {
      // If the exception is caused by user interruption, then don't log to Sentry
      if (!(e is SignInWithAppleAuthorizationException) || e.code != AuthorizationErrorCode.canceled) {
        Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry("AuthRepo, signInWithApple()",
            StackTrace.fromString(e.toString()));
      }

      Response errorResponse = Response();
      errorResponse.statusCode = 400;

      return errorResponse;
    }
  }

  Future<Response> register({
    @required String username,
    @required String email,
    @required String password
  }) async {
    return await api.register(username: username, email: email, password: password);
  }

  Future<Response> activateAccount({@required String activationToken}) async {
    return await api.activateAccount(activationToken: activationToken);
  }
}
