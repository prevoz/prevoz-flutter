import 'dart:async';
import 'package:dio/dio.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProfileRepository {
  final BasePrevozApi api;

  UserProfileRepository(this.api);

  Future<Response> fetchProfile() async {
    Response response = await api.getUserProfile();
    try {
      await saveToSharedPrefs(UserProfile.fromResponse(response));
    } catch (e) {
      print("Error while saving profile to shared pref.");
      print(e.toString());
    }

    return response;
  }

  Future<Response> postProfile(Map<String, dynamic> data) async {
    return await api.postUserProfile(data);
  }

  Future<UserProfile> getProfileFromSharedPrefs() async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      return UserProfile(
        username: sharedPreferences.getString(MyConstants.PREF_KEY_PROFILE_USERNAME),
        email: sharedPreferences.getString(MyConstants.PREF_KEY_PROFILE_EMAIL),
        firstName: sharedPreferences.getString(MyConstants.PREF_KEY_PROFILE_FIRST_NAME),
        lastName: sharedPreferences.getString(MyConstants.PREF_KEY_PROFILE_LAST_NAME),
        address: sharedPreferences.getString(MyConstants.PREF_KEY_PROFILE_ADDRESS)
      );
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<bool> saveToSharedPrefs(UserProfile userProfile) async {
    if (userProfile == null) {
      return false;
    }

    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString(MyConstants.PREF_KEY_USERNAME, userProfile.username);
      sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_EMAIL, userProfile.email);
      sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_FIRST_NAME, userProfile.firstName);
      sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_LAST_NAME, userProfile.lastName);
      sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_ADDRESS, userProfile.address);
      return true;
    } catch (e) {
      print("Error while saving profile to shared pref.");
      print(e.toString());
      return false;
    }
  }
}