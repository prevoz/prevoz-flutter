import 'dart:async';
import 'package:dio/dio.dart';
import 'package:prevoz_org/data/blocs/rides/rides_events.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';

class RidesRepository {
  final BasePrevozApi api;
  static List<RidesSearchResult> usersAddedRides;
  static List<RidesSearchResult> usersBookmarkedRides;
  RidesRepository(this.api);

  Future<RidesSearchResult> fetchRides(FetchRidesEvent event) {
    return api.fetchRides(searchRequest: event.searchRequest);
  }

  Future<Response> addRide(RideModel ride) async {
    return await api.addRide(ride);
  }

  Future<Response> setRideBookmark(String id, String state) async {
    return await api.setRideBookmark(id, state);
  }

  Future<Response> deleteRide(String id) async {
    return await api.deleteRide(id);
  }

  Future<RidesSearchResult> fetchUsersAddedRides() {
    return api.fetchUsersAddedRides();
  }

  Future<Response> setFull(int rideId, String state) {
    return api.setFull(rideId, state);
  }

  Future<RidesSearchResult> fetchUsersBookmarkedRides() {
    return api.fetchUsersBookmarkedRides();
  }

  Future<RouteStatisticsResult> fetchRouteStatistics(
      RouteStatisticsRequest request) {
    return api.fetchRouteStatistics(request);
  }

  Future<List<RideModel>> getQuickRides() async {
    return await api.getQuickRides();
  }

  /* 
  * `locations`: [ {'f': .., 'fc': .. , 't': .. 'tc': .. }, { .. } ]
    * `dates`: [ 'YYYY-MM-DD', 'YYYY-MM-DD', 'YYYY-MM-DD' ]

  */
}
