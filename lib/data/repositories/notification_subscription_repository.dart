import 'dart:async';

import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/helpers/database/database_commons.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';

class NotificationSubscriptionRepository {
  Logger logger = Logger(printer: CustomLogPrinter("NotifSubRepository"));
  final BaseDatabaseHelper db;
  final BasePrevozApi api;

  NotificationSubscriptionRepository(this.db, this.api);

  void configureFirebaseMessaging() {
    api.configureFirebaseMessaging();
  }

  Future<List<NotificationSubscription>>
      getAllNotificationSubscriptions() async {
    return await db.getAllNotificationSubscriptions();
  }

  Future<int> deleteNotificationSubscription(
      NotificationSubscription notification) async {
    return await db.deleteNotificationSubscription(notification);
  }

  Future<int> deleteOldNotificationSubscriptions() async {
    return await db.deleteOldNotificationSubscriptions();
  }

  Future<int> saveSubscription(NotificationSubscription notification) async {
    //* 1) check if notification already in DB
    bool isAlreadySubscribed =
        await db.isNotificationAlreadySubscribed(notification);

    //* 2) API call
    Response response = await api.setSubscriptionStatus(
        notification.route,
        notification.rideDate,
        isAlreadySubscribed == true ? "unsubscribe" : "subscribe");
    //* 3) Save in DB (if API response successfull)
    if (response.statusCode == 200 &&
        response.data.toString().contains("success")) {
      return await db.saveSubscription(notification);
    }

    return 0;
  }

  /// registers a list of subscriptions that were migrated from old Android DB
  Future<void> registerMigratedSubscriptions(
      List<NotificationSubscription> notifications) async {
    if (notifications != null && notifications.length > 0) {
      notifications
          .forEach((NotificationSubscription notificationSubscription) async {
        Response response = await api.setSubscriptionStatus(
            notificationSubscription.route,
            notificationSubscription.rideDate,
            "subscribe");
        logger
            .i("Register migratied subscription: " + response.data.toString());
      });
    }
  }

  Future<bool> checkIfNotificationsTableExists() async {
    return db.checkIfTableExists(DatabaseCommons.notificationTable);
  }

  checkDBSanity() async {
    await db.checkDBSanity(DatabaseCommons.notificationTable);
  }
}
