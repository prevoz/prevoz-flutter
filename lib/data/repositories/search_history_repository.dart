import 'dart:async';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/helpers/database/database_commons.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';

class SearchHistoryRepository {
  final BaseDatabaseHelper db;

  SearchHistoryRepository(this.db);

  Future<List<PrevozRoute>> getSearchHistoryRoutes(int numberOfRoutes) async {
    List<PrevozRoute> routes = await db.getSearchHistoryRoutes(numberOfRoutes);

    return routes;
  }

  checkDBSanity() async {
    await db.checkDBSanity(DatabaseCommons.searchHistoryTable);
  }

  addRouteToSearchHistory(PrevozRoute route) async {
    return await db.addRouteToSearchHistory(route);
  }
}
