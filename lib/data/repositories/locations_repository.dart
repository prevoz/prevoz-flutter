import 'dart:async';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/helpers/database/database_commons.dart';

import 'package:prevoz_org/data/models/location.dart';

class LocationsRepository {
  final BaseDatabaseHelper db;
  LocationsRepository(this.db);

  Future<List<Location>> queryLocations(String location, String country) async {
    List<Location> res = await db.queryLocations(location, country);
    return res;
  }

  Future<void> sanitizeLocationsTable() async {
    await db.sanitizeLocationsTable();
  }

  Future<int> countLocations() async {
    return await db.countLocations();
  }

  checkDBSanity() async {
    await db.checkDBSanity(DatabaseCommons.locationsTable);
  }
}
