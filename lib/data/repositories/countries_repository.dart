import 'dart:async';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/helpers/database/database_commons.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/utils/mock_data.dart';

class CountriesRepository {
  final BaseDatabaseHelper db;
  CountriesRepository(this.db);

  Future<List<Country>> queryCountries(String country) async {
    List<Country> res = await db.queryCountries(country);
    return res;
  }

  Future<List<Country>> getAllCountries() async {
    return MockData.allCountries();
  }

  checkDBSanity() async {
    await db.checkDBSanity(DatabaseCommons.countriesTable);
  }
}
