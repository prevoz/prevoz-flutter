import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/android_migration_data.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:prevoz_org/data/helpers/database/old_android/old_android_database.dart';

class AndroidMigrationHelper {
  static Future<AndroidMigrationData> getDataFromPreviousAndroidApp() async {
    try {
      AndroidMigrationData oldAndroidDbData = AndroidMigrationData();
      String oldAndroidDBPath = await getOldAndroidDbPath();
      oldAndroidDbData.dbPath = oldAndroidDBPath;

      OldAndroidDatabase oldAndroidDatabase = OldAndroidDatabase();
      await oldAndroidDatabase.initDB();

      List<NotificationSubscription> notifications =
          await oldAndroidDatabase.getAndroidNotifications();

      oldAndroidDbData.notifications = notifications;
      oldAndroidDbData.searchHistory =
          await oldAndroidDatabase.getSearchHistory();

      //* Remove routes with empty locations
      for (var i = 0; i < oldAndroidDbData.searchHistory.length; i++) {
        if (PrevozRoute.isOneLocationEmpty(oldAndroidDbData.searchHistory[i])) {
          oldAndroidDbData.searchHistory.removeAt(i);
        }
      }

      return oldAndroidDbData;
    } catch (e) {
      return null;
    }
  }

  static Future<String> getOldAndroidDbPath() async {
    var oldAndroidDBDir = await getDatabasesPath();
    return join(oldAndroidDBDir, "dataprovider");
  }
}
