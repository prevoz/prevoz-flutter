import 'dart:async';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class OldAndroidDatabase {
  Logger logger = Logger(printer: CustomLogPrinter("OldAndroidDatabase"));
  //DB itself
  OldAndroidDatabase._privateConstructor();
  static final OldAndroidDatabase _instance =
      OldAndroidDatabase._privateConstructor();
  factory OldAndroidDatabase() => _instance;
  static Database _database;
  Future<Database> get database async {
    if (_database != null && _database.isOpen) {
      return _database;
    }
    _database = await initDB();
    return _database;
  }

  Future<String> getOldAndroidDbPath() async {
    var oldAndroidDBDir = await getDatabasesPath();
    return join(oldAndroidDBDir, "dataprovider");
  }

  Future<Database> initDB() async {
    String path = await getOldAndroidDbPath();

    return await openDatabase(
      path,
      version: 1,
    );
  }

  Future<List<NotificationSubscription>> getAndroidNotifications() async {
    List<NotificationSubscription> oldNotifications = [];
    try {
      var db = await database;
      List<Map> androidNotifications =
          await db.rawQuery("SELECT * FROM notification");

      androidNotifications.forEach((Map map) {
        NotificationSubscription notification =
            NotificationSubscription.fromOldAndroidDb(map);
        oldNotifications.add(notification);
      });
      return oldNotifications;
    } catch (e) {
      handleError(
          error: e,
          message: "AndroidDB migration helper, getAndroidNotifications()");
      return null;
    }
  }

  Future<List<PrevozRoute>> getSearchHistory() async {
    List<PrevozRoute> oldSearchHistoryRoutes = [];
    try {
      var db = await database;

      List<Map> androidSearchHistory = await db.rawQuery(
          "SELECT * FROM searchhistoryitem WHERE length(l_from) > 0 AND length(l_to) > 0");

      logger.i("Old Android Search history: $androidSearchHistory");

      if (androidSearchHistory != null && androidSearchHistory.length > 0) {
        androidSearchHistory.forEach((Map map) {
          try {
            PrevozRoute route = PrevozRoute.fromOldAndroidDb(map);
            oldSearchHistoryRoutes.add(route);
          } catch (e) {
            handleError(
                error: e, message: "mapping old android search history");
          }
        });
      }

      return oldSearchHistoryRoutes;
    } catch (e) {
      handleError(
          error: e, message: "AndroidDB migration helper, getSearchHistory()");
    }

    return oldSearchHistoryRoutes;
  }
}
