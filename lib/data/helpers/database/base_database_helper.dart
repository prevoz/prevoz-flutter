import 'dart:async';

import 'package:prevoz_org/data/helpers/database/database_commons.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:sqflite/sqflite.dart';

abstract class BaseDatabaseHelper extends DatabaseCommons {
  // prepopulate DB and utils
  void insertLocationsIntoDb();

  void insertDefaultSearchHistoryIntoDb();

  void insertCountriesIntoDb();

  Future<Database> initDB();

  Future<bool> checkIfTableExists(String tableName);

  Future<int> insertLocation(Location location);

  List<PrevozRoute> defaultPopularRoutes();

  Future<int> insertRoute(PrevozRoute route);

  Future<int> insertCountry(Country country);

  Future<void> checkDBSanity(String tableName);

  //* querying locations and countries

  Future<List<Country>> queryCountries(String country);

  Future<void> sanitizeLocationsTable();
  Future<List<Location>> queryLocations(String location, String countryCode);

  //* search history
  Future<int> addRouteToSearchHistory(PrevozRoute route);

  Future<List<PrevozRoute>> getSearchHistoryRoutes(int numberOfRoutes);

  //* notifications
  Future<List<NotificationSubscription>> getAllNotificationSubscriptions();
  Future<bool> isNotificationAlreadySubscribed(
      NotificationSubscription notificationSubscription);
  Future<int> deleteNotificationSubscription(
      NotificationSubscription notification);

  Future<int> deleteOldNotificationSubscriptions();

  Future<int> saveSubscription(NotificationSubscription notification);

  Future<int> countLocations();
}
