import 'dart:async';
import 'package:prevoz_org/data/helpers/event_bus/database_initialisation.dart';
import 'package:prevoz_org/my_app.dart';

/// Values shared between databases used for mocks and the real database
class DatabaseCommons {
  //static DB values
  String get databaseName => "prevoz.db";

  /// lets the HomePage know when default search history is populated in DB
  /// on apps first startup.
  // StreamController<String> searchHistoryBroadcastController =
  //     new StreamController.broadcast();

  /// used for migrating notifications from android app to flutter app database
  /// lets the app know when the notifications are migrated
  StreamController<String> migratedAndroidNotificationsBroadcastController =
      new StreamController.broadcast();

  // used
  static String countriesTable = "Countries";
  static String locationsTable = "Locations";
  static String searchHistoryTable = "SearchHistory";
  static String notificationTable = "Notification";

  //tables
  String get tableCountries => "Countries";
  String get tableLocations => "Locations";
  String get tableSearchHistory => "SearchHistory";
  String get tableNotification => "Notification";
  //country columns
  String get columnCountryCode => "countryCode";
  String get columnCountryLanguage => "language";
  String get columnCountryName => "name";
  //location columns
  String get columnLocationId => "id";
  String get columnLocationSortNumber => "sortNumber";
  String get columnLocationName => "name";
  String get columnLocationNameAscii => "nameAscii";
  String get columnLocationCountry => "country";
  String get columnLocationLat => "lat";
  String get columnLocationLng => "lng";
  //search history columns
  String get columnSearchHistryLocFrom => "locationFrom";
  String get columnSearchHistryCountryFrom => "countryFrom";
  String get columnSearchHistryLocTo => "locationTo";
  String get columnSearchHistryCountryTo => "countryTo";
  String get columnSearchHistrySearchDate => "searchDate";
  String get columnSearchHistryId => "id";
  //notification columns
  String get columnNotificationLocationFrom => "locationFrom";
  String get columnNotificationCountrFrom => "countryFrom";
  String get columnNotificationLocationTo => "locationTo";
  String get columnNotificationCountryTo => "countryTo";
  String get columnNotificationRideDate => "date";
  String get columnNotificationId => "id";
  // notifications columns for static access
  static String notificationLocationFromColumn = "locationFrom";
  static String notificationCountrFromColumn = "countryFrom";
  static String notificationLocationToColumn =
      "locationTo"; // columnNotificationLocationTo
  static String notificationCountryToColumn = "countryTo";
  static String notificationRideDateColumn = "date";
  static String notificationIdColumn = "id";

  //* DB CREATION SCRIPTS
  List<String> get dbCreationScripts => [
        createCountriesTable,
        createLocationsTable,
        createSearchHistoryTable,
        createNotificationsTable
      ];

  /// v1 doesnt contain notifications [this is used for testing only]
  List<String> get version1creationScripts => [
        createCountriesTable,
        createLocationsTable,
        createSearchHistoryTable,
      ];

  String get createCountriesTable =>
      '''CREATE TABLE $tableCountries($columnCountryCode TEXT PRIMARY KEY, 
        $columnCountryLanguage TEXT, 
        $columnCountryName TEXT)''';

  String get createLocationsTable =>
      '''CREATE TABLE $tableLocations($columnLocationId INTEGER PRIMARY KEY AUTOINCREMENT, 
          $columnLocationSortNumber INTEGER, 
          $columnLocationName TEXT, $columnLocationNameAscii TEXT, 
          $columnLocationCountry TEXT, $columnLocationLat REAL,
         $columnLocationLng REAL)''';

  String get createSearchHistoryTable =>
      '''CREATE TABLE $tableSearchHistory($columnSearchHistryId INTEGER PRIMARY KEY AUTOINCREMENT, 
          $columnSearchHistryLocFrom TEXT, $columnSearchHistryCountryFrom TEXT, 
          $columnSearchHistryLocTo TEXT, $columnSearchHistryCountryTo TEXT,
         $columnSearchHistrySearchDate TEXT)''';

  String get createNotificationsTable =>
      '''CREATE TABLE $tableNotification($columnNotificationId INTEGER PRIMARY KEY AUTOINCREMENT, 
          $columnNotificationLocationTo TEXT, $columnNotificationCountryTo TEXT, 
          $columnNotificationLocationFrom TEXT, $columnNotificationCountrFrom TEXT,
          $columnNotificationRideDate INTEGER);''';

  String get removeAllLocations => '''DELETE FROM $tableLocations;''';

  String get createNotificationsAndDeleteLocations =>
      '''CREATE TABLE $tableNotification($columnNotificationId INTEGER PRIMARY KEY AUTOINCREMENT, 
          $columnNotificationLocationTo TEXT, $columnNotificationCountryTo TEXT, 
          $columnNotificationLocationFrom TEXT, $columnNotificationCountrFrom TEXT,
          $columnNotificationRideDate INTEGER); DELETE FROM $tableLocations;''';

  String get removeUnnecessaryCountries =>
      '''DELETE FROM $tableCountries WHERE $columnCountryName = 'Finska' OR $columnCountryName = 'Latvija' OR $columnCountryName = 'Portugalska' OR $columnCountryName = 'Estonija';''';

  //* DB MIGRATION SCRIPTS
  List<String> get v1migrations =>
      [createNotificationsTable, removeAllLocations];

  List<String> get v2migrations => [removeUnnecessaryCountries];

  List<List<String>> get migrationScriptsForAllVersions =>
      [v1migrations, v2migrations];

  /// this is used only to avoid linter errors on StreamController not closing
  void testTest() {
    migratedAndroidNotificationsBroadcastController.close();
  }

  void triggerDbInitEvent(DbInitStates dbInitState) {
    MyApp.eventBus.fire(DatabaseInitialisationEvent(dbInitState));
    DbInitState.addStateIfNotPresent(dbInitState);
  }
}
