import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:logger/logger.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:prevoz_org/data/file_reader.dart';
import 'package:prevoz_org/data/helpers/database/database_commons.dart';
import 'package:prevoz_org/data/helpers/database/old_android/android_migration_helper.dart';
import 'package:prevoz_org/data/helpers/event_bus/database_initialisation.dart';
import 'package:prevoz_org/data/models/android_migration_data.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/mock_data.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:sqflite/sqflite.dart';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';

class PrevozDatabase extends DatabaseCommons implements BaseDatabaseHelper {
  final logger = Logger();
  //static DB values
  static const String _databaseName = "prevoz.db";

  //DB itself
  PrevozDatabase._privateConstructor();
  static final PrevozDatabase _instance = PrevozDatabase._privateConstructor();
  factory PrevozDatabase() => _instance;
  static Database _database;
  Future<Database> get database async {
    if (_database != null && _database.isOpen) {
      return _database;
    }
    _database = await initDB();
    return _database;
  }
  //tables

  Future<Database> initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    AndroidMigrationData androidMigrationData;

    return await openDatabase(path,
        version: migrationScriptsForAllVersions.length + 1,
        onCreate: (Database db, int version) async {
      //DatabaseCommons.fireInitialisationEvent(DbInitStates.onCreateStarted);
      triggerDbInitEvent(DbInitStates.onCreateStarted);

      try {
        if (Platform.isAndroid) {
          try {
            androidMigrationData =
            await AndroidMigrationHelper.getDataFromPreviousAndroidApp();

            logger.i("Android migration data $androidMigrationData");
          } catch (e) {
            androidMigrationData = null;
          }
        }

        dbCreationScripts.forEach((script) async => await db.execute(script));
        triggerDbInitEvent(DbInitStates.createScriptsExecuted);

        if (androidMigrationData != null) {
          migrateOldAndroidData(androidMigrationData);
        } else {
          migratedAndroidNotificationsBroadcastController.close();
          triggerDbInitEvent(DbInitStates.notificationsHandled);
          insertDefaultSearchHistoryIntoDb();
        }

        insertCountriesIntoDb();
        insertLocationsIntoDb();
      } catch (e) {
        DbInitState.resetState();
        Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(e, null);
      }
    }, onUpgrade: (Database db, int oldDbVersion, int newDbVersion) async {
      if (oldDbVersion == newDbVersion - 1) {
        await regularDbUpgrade(db, newDbVersion);
      } else {
        await dbUpgradeWithVersionGaps(db, oldDbVersion);
      }

      //* Version 2 adds new locations
      if (oldDbVersion == 1 && newDbVersion == 2) {
        insertLocationsIntoDb();
      }
    });
  }

  Future<void> migrateOldAndroidData(
      AndroidMigrationData androidMigrationData) async {
    //* handle notifcations
    if (androidMigrationData.notifications != null &&
        androidMigrationData.notifications.length > 0) {
      //? await insertNotification() is used as a workaround for:
      //* Error inserting notification, Bad state: This can happen if an inner synchronized block is spawned outside the block it was started from. Make sure the inner synchronized blocks are properly awaited
      await insertNotification(androidMigrationData.notifications[0]);
      await insertNotificationsFromOldAndroid(
          androidMigrationData.notifications);
    } else {
      migratedAndroidNotificationsBroadcastController.close();
    }
    triggerDbInitEvent(DbInitStates.notificationsHandled);

    //* handle search history
    if (androidMigrationData.searchHistory != null &&
        androidMigrationData.searchHistory.length > 0) {
      if (androidMigrationData.notifications != null &&
          androidMigrationData.notifications.length == 0) {
        //? workaround for:
        //* Bad state: This can happen if an inner synchronized block is spawned outside the block it was started from. Make sure the inner synchronized blocks are properly awaited
        await insertRoute(androidMigrationData.searchHistory[0]);
      }
      await insertSearchHistoryFromOldAndroid(
          androidMigrationData.searchHistory);
    } else {
      await insertDefaultSearchHistoryIntoDb();
    }
    triggerDbInitEvent(DbInitStates.searchHistoryInserted);
    //todo - consider deleting DB in later versions
    //deleteDatabase(androidMigrationData.dbPath);
  }

  /// used only for Android DB migration
  Future<int> insertNotification(NotificationSubscription notification) async {
    try {
      var dbClient = await database;
      var newlyInsertedId =
          await dbClient.insert("$tableNotification", notification.toMap());
      return newlyInsertedId;
    } catch (e) {
      logger.e("Error inserting notification, " + e.toString());
      return null;
    }
  }

  /// IRREGULAR UPGRADE with gaps between versions
  /// executes all upgrade scripts inbetween versions
  Future<void> dbUpgradeWithVersionGaps(Database db, int oldDbVersion) async {
    for (var i = (oldDbVersion - 1);
        i < migrationScriptsForAllVersions.length;
        i++) {
      List<String> migrationScriptsForVersion =
          migrationScriptsForAllVersions[i];

      migrationScriptsForVersion.forEach((String script) async {
        await db.execute(script);
      });
    }
  }

  /// ++1 REGULAR UPGRADE (1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7), no gaps between versions
  Future<void> regularDbUpgrade(Database db, int newDbVersion) async {
    for (var i = 0; i < migrationScriptsForAllVersions.length; i++) {
      //* run migration scripts only for new DB version
      if (i + 2 == newDbVersion) {
        List<String> migrationScriptsForVersion =
            migrationScriptsForAllVersions[i];

        print("________ DB upgrade for versionIndex " + (i + 2).toString());
        migrationScriptsForVersion.forEach((String script) async {
          print("______ DB Upgrade, executing script: $script");
          await db.execute(script);
        });
      }
    }
  }

  Future<List<Location>> queryLocations(String location, String country) async {
    try {
      var dbClient = await database;

      String whereClause;
      // if query contains potential cyriclic characters, query locations by their ASCII name column
      if (location.contains("s") ||
          location.contains("c") ||
          location.contains("z")) {
        whereClause =
            "$columnLocationCountry = ? AND $columnLocationNameAscii LIKE ?";
      } else {
        whereClause =
            "$columnLocationCountry = ? AND $columnLocationName LIKE ?";
      }

      List<Map> results = [];
      results = await dbClient.query(tableLocations,
          columns: [
            "$columnLocationCountry",
            "$columnLocationId",
            "$columnLocationLat",
            "$columnLocationLng",
            "$columnLocationName",
            "$columnLocationNameAscii",
            "$columnLocationSortNumber"
          ],
          where: whereClause,
          whereArgs: ["$country", '%' + location + '%']);

      return results.map((m) => Location.fromDb(m)).toList();
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  @override
  Future<List<Country>> queryCountries(String country) async {
    try {
      var dbClient = await database;
      String whereClause = "name LIKE ?";

      List<Map> results = [];
      results = await dbClient.query(tableCountries,
          columns: [
            "$columnCountryName",
            "$columnCountryCode",
            "$columnCountryLanguage"
          ],
          where: whereClause,
          whereArgs: ['%' + country + '%']);

      return results.map((m) => Country.fromDbRegular(m)).toList();
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  /// the latest routes searched by user
  Future<List<PrevozRoute>> getSearchHistoryRoutes(int numberOfRoutes) async {
    try {
      Database dbClient = await database;
      List<Map> results = await dbClient.query(tableSearchHistory,
          limit: numberOfRoutes,
          orderBy: "" + columnSearchHistrySearchDate + " DESC");

      return results.map((m) => PrevozRoute.fromDb(m)).toList();
    } catch (e) {
      handleError(error: e, message: "DB, getRoutesSearchHistory");
      return null;
    }
  }

  Future<void> insertCountriesIntoDb() async {
    final FileReader fileReader = FileReader();
    List<Country> countries = await fileReader.getCountriesFromTxtFile();

    for (var i = 0; i < countries.length; i++) {
      await insertCountry(countries[i]);
    }

    if (DbInitState.isInitInProgress()) {
      triggerDbInitEvent(DbInitStates.countriesInserted);
    }
  }

  Future<int> insertCountry(Country country) async {
    try {
      var dbClient = await database;
      var newlyInsertedId =
          await dbClient.insert("$tableCountries", country.toMapRegular());
      return newlyInsertedId;
    } catch (e) {
      print("Error: " + e.toString());
      return null;
    }
  }

  Future<void> insertLocationsIntoDb() async {
    final FileReader fileReader = FileReader();
    List<Location> locations = await fileReader.getLocationsFromTxtFile();

    var db = await database;
    var batch = db.batch();

    for (var i = 0; i < locations.length; i++) {
      batch.insert("$tableLocations", locations[i].toMap());
    }

    await batch.commit(exclusive: true, noResult: false);
    if (DbInitState.isInitInProgress()) {
      triggerDbInitEvent(DbInitStates.locationsInserted);
    }
  }

  Future<int> insertLocation(Location location) async {
    try {
      var dbClient = await database;

      Map<dynamic, dynamic> testMap = location.toMap();

      var newlyInsertedId = await dbClient.insert("$tableLocations", testMap);

      return newlyInsertedId;
    } catch (e) {
      print("Error: " + e.toString());
      return null;
    }
  }

  Future<void> insertNotificationsFromOldAndroid(
      List<NotificationSubscription> oldNotifications) async {
    for (var i = 0; i < oldNotifications.length; i++) {
      await insertNotification(oldNotifications[i]);
    }

    migratedAndroidNotificationsBroadcastController.close();
  }

  Future<void> insertSearchHistoryFromOldAndroid(
      List<PrevozRoute> oldSearchHistory) async {
    int numOfHistoryItems = oldSearchHistory.length;

    List<PrevozRoute> routesToBeInserted = [];
    //* decide what to do depending on number of historty items in old android DB
    if (numOfHistoryItems == 6) {
      routesToBeInserted = oldSearchHistory;
    } else if (numOfHistoryItems < 6) {
      List<PrevozRoute> popularRoutes = defaultPopularRoutes();

      for (var i = numOfHistoryItems; i < 6; i++) {
        oldSearchHistory.add(popularRoutes[i - numOfHistoryItems]);
      }

      routesToBeInserted = oldSearchHistory;
    } else {
      for (var i = 0; i < 6; i++) {
        //* add only 6 items
        routesToBeInserted.add(oldSearchHistory[i]);
      }
    }
    //* insert route in DB
    for (var i = 0; i < routesToBeInserted.length; i++) {
      logger.i("inserting route:  " + routesToBeInserted[i].toString());
      await insertRoute(routesToBeInserted[i]);
    }
  }

  Future<void> insertDefaultSearchHistoryIntoDb() async {
    List<PrevozRoute> popularRoutes = defaultPopularRoutes();
    for (var i = 0; i < popularRoutes.length; i++) {
      await insertRoute(popularRoutes[i]);
    }

    if (DbInitState.isInitInProgress()) {
      triggerDbInitEvent(DbInitStates.searchHistoryInserted);
    }
  }

  Future<int> saveSubscription(NotificationSubscription notification) async {
    try {
      var dbClient = await database;

      String fromLocation = notification.route.locationFrom;
      String toLocation = notification.route.locationTo;

      int milisecSinceEpoch =
          DateTimeUtils.startOfDayToMilisecSinceEpoch(notification.rideDate);

      String whereClause =
          "locationFrom='$fromLocation' AND locationTo='$toLocation' AND date='$milisecSinceEpoch'";
      List<Map> res =
          await dbClient.query(tableNotification, where: whereClause);
      // DELETE EXISTING
      if (res.length > 0) {
        await dbClient.delete(tableNotification, where: whereClause);
        return -1;
      }

      return await dbClient.insert(tableNotification, notification.toMap());
    } catch (e) {
      return handleError(
          error: e, message: "Error adding notification subscription");
    }
  }

  Future<int> deleteNotificationSubscription(
      NotificationSubscription notification) async {
    try {
      var dbClient = await database;
      String fromCity = notification.route.locationFrom;
      String toCity = notification.route.locationTo;
      String date =
          DateTimeUtils.dateToStringForApiCalls(notification.rideDate);
      String whereClause =
          "locationFrom='$fromCity' AND locationTo='$toCity' AND date='$date'";

      return await dbClient.delete(tableNotification, where: whereClause);
    } catch (e) {
      return handleError(error: e, message: "DBHelper, deleting notif sub.");
    }
  }

  Future<int> deleteOldNotificationSubscriptions() async {
    try {
      var dbClient = await database;

      int todayInMilisecSinceEpoch =
          DateTimeUtils.startOfDayToMilisecSinceEpoch(DateTime.now());

      int affectedColumns = await dbClient.delete(tableNotification,
          where: "$columnNotificationRideDate < ?",
          whereArgs: [todayInMilisecSinceEpoch]);
      return affectedColumns;
    } catch (e) {
      return handleError(
          error: e, message: "DB helper, deleteOldNotificationSubscriptions");
    }
  }

  Future<List<NotificationSubscription>>
      getAllNotificationSubscriptions() async {
    try {
      Database dbClient = await database;
      List<Map> results = await dbClient.query(tableNotification,
          orderBy: "" + columnNotificationRideDate + " ASC");

      List<NotificationSubscription> subs =
          results.map((m) => NotificationSubscription.fromDb(m)).toList();

      return subs;
    } catch (e) {
      handleError(error: e, message: "DB, getAllNotificationSubscriptions");
      return null;
    }
  }

  @override
  Future<bool> isNotificationAlreadySubscribed(
      NotificationSubscription notification) async {
    try {
      var dbClient = await database;

      String fromLocation = notification.route.locationFrom;
      String toLocation = notification.route.locationTo;
      int date =
          DateTimeUtils.startOfDayToMilisecSinceEpoch(notification.rideDate);

      String whereClause =
          "locationFrom='$fromLocation' AND locationTo='$toLocation' AND date='$date'";
      List<Map> res =
          await dbClient.query(tableNotification, where: whereClause);

      if (res.length > 0) {
        return true;
      }

      return false;
    } catch (e) {}

    return null;
  }

  Future<int> addRouteToSearchHistory(PrevozRoute addedRoute) async {
    try {
      var dbClient = await database;
      String fromCity = addedRoute.locationFrom;
      String toCity = addedRoute.locationTo;
      String whereClause =
          "locationFrom='$fromCity' AND locationTo='$toCity' OR locationFrom='$toCity' AND locationTo='$fromCity'";
      List<Map> res =
          await dbClient.query(tableSearchHistory, where: whereClause);
      // UPDATE EXISTING
      if (res.length > 0) {
        PrevozRoute existingRoute = PrevozRoute.fromDb(res[0]);

        //* do a switch up if route are oposite
        if (PrevozRoute.isOpositeRoute(existingRoute, addedRoute)) {
          existingRoute.countryFrom = addedRoute.countryFrom;
          existingRoute.countryTo = addedRoute.countryTo;
          existingRoute.locationFrom = addedRoute.locationFrom;
          existingRoute.locationTo = addedRoute.locationTo;
        }
        existingRoute.searchDate = DateTime.now().toString();
        return await dbClient.update(tableSearchHistory, existingRoute.toMap(),
            where: '$columnSearchHistryId = ?', whereArgs: [existingRoute.id]);
      }
      // ADD NEW
      addedRoute.searchDate = DateTime.now().toString();
      return await dbClient.insert(tableSearchHistory, addedRoute.toMap());
    } catch (err) {
      print("Error: " + err.toString());
      return null;
    }
  }

  Future<int> insertRoute(PrevozRoute route) async {
    try {
      var dbClient = await database;
      var newlyInsertedId =
          await dbClient.insert("$tableSearchHistory", route.toMap());
      return newlyInsertedId;
    } catch (e) {
      print("Error: " + e.toString());
      return null;
    }
  }

  List<PrevozRoute> defaultPopularRoutes() {
    return MockData.defaultSearchHistory();
  }

  List<Country> defaultSearchedCountries() {
    return MockData.defaultSearchedCountries();
  }

  /// Assumes the given path is a text-file-asset.
  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  Future closeDb() async {
    var dbClient = await database;
    await dbClient.close();
    _database = null;
  }

  /// creates a specific table if it doesnt exist
  @override
  Future<void> checkDBSanity(String tableName) async {
    try {
      if (DbInitState.isInitInProgress()) {
        return;
      }
      bool tableExist = await checkIfTableExists(tableName);

      var db = await database;

      if (tableExist == false) {
        switch (tableName) {
          case "Countries":
            await db.execute(createCountriesTable);
            await insertCountriesIntoDb();
            break;
          case "Locations":
            await db.execute(createLocationsTable);
            await insertLocationsIntoDb();
            break;
          case "SearchHistory":
            await db.execute(createSearchHistoryTable);
            await insertDefaultSearchHistoryIntoDb();
            break;

          case 'Notification':
            await db.execute(createNotificationsTable);
            break;
        }
        return;
      }

      bool isTablePopulated = await checkIfTablePopulated(tableName);
      if (isTablePopulated == false) {
        switch (tableName) {
          case "Countries":
            await insertCountriesIntoDb();
            break;
          case "Locations":
            await insertLocationsIntoDb();
            break;
          case "SearchHistory":
            await insertDefaultSearchHistoryIntoDb();
            break;
        }
      }
    } catch (e) {
      handleError(error: e, message: "DBHelper, check DB sanity");
    }
  }

  Future<bool> checkIfTablePopulated(String tableName) async {
    var db = await database;
    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $tableName'));

    if (count < 1) {
      return false;
    }

    return true;
  }

  Future<bool> checkIfTableExists(String tableName) async {
    List<Map<String, dynamic>> tables = await getAllDatabaseTables();

    bool tableExist = false;

    tables.forEach((Map<String, dynamic> table) {
      if (table["name"] == tableName) {
        tableExist = true;
      }
    });

    return tableExist;
  }

  Future<List<Map<String, dynamic>>> getAllDatabaseTables() async {
    var db = await database;

    List<Map<String, dynamic>> tables = await db.rawQuery(
        "SELECT name FROM sqlite_master WHERE type='table'", null);

    return tables;
  }

  @override
  Future<int> countLocations() async {
    var db = await database;
    return Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $tableLocations'));
  }

  @override
  Future<void> sanitizeLocationsTable() async {
    var db = await database;
    await db.delete("$tableLocations");
    await insertLocationsIntoDb();
  }
}
