import 'dart:async';

import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/helpers/database/database_commons.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/registered_notification.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/mock_data.dart';
import 'package:sqflite/sqlite_api.dart';

class MockDatabaseHelper extends DatabaseCommons implements BaseDatabaseHelper {
  List<NotificationSubscription> subscribedNotifications = [];

  @override
  Future<int> addRouteToSearchHistory(PrevozRoute route) {
    return Future.value(1);
  }

  @override
  List<PrevozRoute> defaultPopularRoutes() {
    return MockData.defaultSearchHistory();
  }

  @override
  Future<List<PrevozRoute>> getSearchHistoryRoutes(int numberOfRoutes) {
    return Future.value(MockData.searchHistoryMock());
  }

  @override
  Future<int> insertCountry(Country country) {
    return Future.value(1);
  }

  @override
  void insertDefaultSearchHistoryIntoDb() {}

  @override
  void insertLocationsIntoDb() {}

  @override
  Future<int> insertLocation(Location location) {
    return Future.value(1);
  }

  @override
  Future<Database> initDB() {
    // TODO: implement initDB
    return null;
  }

  @override
  void insertCountriesIntoDb() {
    // TODO: implement insertCountriesIntoDb
  }

  @override
  Future<int> insertRoute(PrevozRoute route) {
    return Future.value(1);
  }

  @override
  Future<List<Location>> queryLocations(String location, String country) async {
    List<Location> allLocations = [];
    allLocations.addAll([
      Location(
          country: "SI",
          sortNumber: 1,
          nameAscii: "he",
          name: "Ljubljana",
          lng: 23.23,
          lat: 53.53,
          id: 1),
      Location(
          country: "SI",
          sortNumber: 2,
          nameAscii: "he",
          name: "Maribor",
          lng: 23.23,
          lat: 53.53,
          id: 2),
      Location(
          country: "SI",
          sortNumber: 1,
          nameAscii: "he",
          name: "Celje",
          lng: 23.23,
          lat: 53.53,
          id: 3),
      Location(
          country: "SI",
          sortNumber: 1,
          nameAscii: "he",
          name: "Velenje",
          lng: 23.23,
          lat: 53.53,
          id: 4),
      Location(
          country: "SI",
          sortNumber: 1,
          nameAscii: "he",
          name: "Koper",
          lng: 23.23,
          lat: 53.53,
          id: 5),
      Location(
          country: "SI",
          sortNumber: 1,
          nameAscii: "he",
          name: "Žalec",
          lng: 23.23,
          lat: 53.53,
          id: 6),
    ]);

    return Future.value(allLocations);
  }

  @override
  Future<List<Country>> queryCountries(String country) {
    return null;
  }

  @override
  Future<void> checkDBSanity(String tableName) {
    return null;
  }

  @override
  Future<int> saveSubscription(NotificationSubscription notification) {
    bool notificationAlreadyExist = false;

    for (var i = 0; i < subscribedNotifications.length; i++) {
      NotificationSubscription notfSub = subscribedNotifications[i];

      if (PrevozRoute.areRoutesTheSame(notfSub.route, notification.route)) {
        if (DateTimeUtils.areDatesTheSame(
            notfSub.rideDate, notification.rideDate)) {
          notificationAlreadyExist = true;
          subscribedNotifications.removeAt(i);
        }
      }
    }

    if (!notificationAlreadyExist) {
      subscribedNotifications.add(notification);
      return Future.value(1);
    }

    return Future.value(-1);
  }

  @override
  Future<int> deleteNotificationSubscription(
      NotificationSubscription notification) {
    // TODO: implement deleteNotificationSubscription
    return null;
  }

  @override
  Future<int> deleteOldNotificationSubscriptions() {
    // TODO: implement deleteOldNotificationSubscriptions
    return null;
  }

  @override
  Future<List<NotificationSubscription>> getAllNotificationSubscriptions() {
    return Future.value([]);
  }

  @override
  Future<bool> isNotificationAlreadySubscribed(
      NotificationSubscription notificationSubscription) {
    for (var i = 0; i < subscribedNotifications.length; i++) {
      NotificationSubscription notfSub = subscribedNotifications[i];

      if (PrevozRoute.areRoutesTheSame(
          notfSub.route, notificationSubscription.route)) {
        if (DateTimeUtils.areDatesTheSame(
            notfSub.rideDate, notificationSubscription.rideDate)) {
          return Future.value(true);
        }
      }
    }
    return Future.value(false);
  }

  @override
  Future<bool> checkIfTableExists(String tableName) {
    // TODO: implement checkIfTableExists
    return null;
  }

  @override
  Future<int> countLocations() {
    // TODO: implement countLocations
    return Future.value(1214);
  }

  @override
  Future<void> sanitizeLocationsTable() {
    // TODO: implement sanitizeLocationsTable
    return null;
  }
}
