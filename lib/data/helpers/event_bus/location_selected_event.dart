import 'package:prevoz_org/data/models/location.dart';

enum LocationType { FROM, TO }

class LocationSelectedEvent {
  final LocationType locationType;
  final Location location;

  LocationSelectedEvent(this.locationType, this.location);
}
