class DatabaseInitialisationEvent {
  final DbInitStates state;
  DatabaseInitialisationEvent(this.state);
}

enum DbInitStates {
  onCreateStarted,
  createScriptsExecuted,
  notificationsHandled, // these can be either inserted (when migration) or just the table created
  searchHistoryInserted,
  countriesInserted,
  locationsInserted
}

class DbInitState {
  static List<DbInitStates> dbInitStates = [];

  static void addStateIfNotPresent(DbInitStates state) {
    if (!dbInitStates.contains(state)) {
      dbInitStates.add(state);
      print("_____ DB INIT, ADDED STATE " + state.toString());
    }
  }

  static bool isInitInProgress() {
    if (dbInitStates.length == 0 ||
        dbInitStates.length == DbInitStates.values.length) {
      return false;
    }
    if (!dbInitStates.contains(DbInitStates.onCreateStarted)) {
      return false;
    }
    return true;
  }

  static void resetState() {
    dbInitStates = [];
  }
}
