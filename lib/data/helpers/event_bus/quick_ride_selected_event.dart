import 'package:prevoz_org/data/models/rides_search_result.dart';

class QuickRideSelectedEvent {
  final RideModel quickRide;
  QuickRideSelectedEvent(this.quickRide);
}
