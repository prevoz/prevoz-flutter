import 'package:prevoz_org/data/models/rides_search_result.dart';

class RideSubmitedEvent {
  final RideModel ride;
  RideSubmitedEvent(this.ride);
}
