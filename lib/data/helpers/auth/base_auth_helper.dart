import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:prevoz_org/data/blocs/authentication/auth_error_type.dart';
import 'package:prevoz_org/data/models/user.dart';

abstract class BaseAuthHelper {
  static FirebaseAnalytics analytics = FirebaseAnalytics();

  static void showLogoutDialog(BuildContext ctx) {}

  static void showSigninDialog(BuildContext ctx) {}

  static void getAccessToken() {}

  static void launchBrowsersForAuthorisation() {}

  Future<String> getApiBearer();

  Future<User> getUserFromSharedPrefs();

  Future<List<dynamic>> getProfilePhoneNumbers();

  SnackBar createAuthSnackBar({
    @required BuildContext context,
    @required AuthErrorType errorType,
  });
}
