import 'dart:async';

import 'package:flutter/material.dart';
import 'package:prevoz_org/data/blocs/authentication/auth_error_type.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/models/user.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

/// used in tests where the user is already logged in on app startup
class MockAuthHelperWithUserProfile implements BaseAuthHelper {

  Future<String> getApiBearer() {
    return null;
  }

  @override
  Future<List> getProfilePhoneNumbers() {
    return Future.value(["123456"]);
  }

  /// returns user with expired lastLoginTimestamp
  /// this is then used on AppStartup
  /// to test automatic checkup for changed phone numbers
  @override
  Future<User> getUserFromSharedPrefs() {
    return Future.value(User(
        username: "test",
        accessToken: "test",
        expiryTimestamp: 1000,
        lastLoginTimestamp: 1000,
        oauth2: true,
        phoneNumbers: ["123456", "567890"],
        refreshToken: "test"));
  }

  @override
  SnackBar createAuthSnackBar({
    @required BuildContext context,
    @required AuthErrorType errorType,
  }) {
    return SnackBar(
      content: Text(
          _getMessageForErrorType(context, errorType)
      ),
      duration: Duration(milliseconds: 1500),
    );
  }

  String _getMessageForErrorType(BuildContext context, AuthErrorType errorType) {
    switch (errorType) {
      case AuthErrorType.SIGN_IN_ERROR:
        return AppLocalizations.of(context).authErrorLoggingIn;
      case AuthErrorType.ACCOUNT_ACTIVATION_ERROR:
        return AppLocalizations.of(context).authErrorActivatingAccount;
      case AuthErrorType.CONNECTION_ERROR:
        return AppLocalizations.of(context).notConnectedToInternet;
      case AuthErrorType.INVALID_TOKEN_ERROR:
        return AppLocalizations.of(context).authErrorInvalidToken;
    }

    return AppLocalizations.of(context).notConnectedToInternet;
  }
}
