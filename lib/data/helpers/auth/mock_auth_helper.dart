import 'dart:async';

import 'package:flutter/material.dart';
import 'package:prevoz_org/data/blocs/authentication/auth_error_type.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/models/user.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';

class MockAuthHelper implements BaseAuthHelper {

  Future<String> getApiBearer() {
    return null;
  }

  @override
  Future<List> getProfilePhoneNumbers() {
    return null;
  }

  @override
  Future<User> getUserFromSharedPrefs() {
    return null;
  }

  @override
  SnackBar createAuthSnackBar({
    @required BuildContext context,
    @required AuthErrorType errorType,
  }) {
    return SnackBar(
      content: Text(
          _getMessageForErrorType(context, errorType)
      ),
      duration: Duration(milliseconds: 1500),
    );
  }

  String _getMessageForErrorType(BuildContext context, AuthErrorType errorType) {
    switch (errorType) {
      case AuthErrorType.SIGN_IN_ERROR:
        return AppLocalizations.of(context).authErrorLoggingIn;
      case AuthErrorType.ACCOUNT_ACTIVATION_ERROR:
        return AppLocalizations.of(context).authErrorActivatingAccount;
      case AuthErrorType.CONNECTION_ERROR:
        return AppLocalizations.of(context).notConnectedToInternet;
      case AuthErrorType.INVALID_TOKEN_ERROR:
        return AppLocalizations.of(context).authErrorInvalidToken;
    }

    return AppLocalizations.of(context).notConnectedToInternet;
  }
}
