import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/auth_error_type.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_event.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/models/app_tab.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/data/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_bloc.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication_event.dart';

class AuthHelper implements BaseAuthHelper {
  static FirebaseAnalytics analytics = FirebaseAnalytics();

  static _logout(BuildContext context) async {
    BlocProvider.of<AuthenticationBloc>(context).add(LogoutEvent());
  }

  static showLogoutDialog(BuildContext ctx) {
    // flutter defined function
    showDialog(
      context: ctx,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            AppLocalizations.of(context).signOutDialogTitle,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: new Text(AppLocalizations.of(context).signOutDialogContent),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: Text(
                AppLocalizations.of(context).cancel.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(
                AppLocalizations.of(context).signout.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () async {
                await _logout(context);
                analytics.logEvent(name: "logout");
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static showSigninDialog(BuildContext ctx) {
    // flutter defined function
    showDialog(
      context: ctx,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            AppLocalizations.of(context).signInDialogTitle,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: new Text(AppLocalizations.of(context).signInDialogContent),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: Text(
                AppLocalizations.of(context).cancel.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(
                AppLocalizations.of(context).signin.toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.1),
              ),
              onPressed: () async {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                BlocProvider.of<BottomNavigationBloc>(context)
                    .add(UpdateTabBottomNavEvent(AppTab.profile));
              },
            ),
          ],
        );
      },
    );
  }

  Future<String> getApiBearer() async {
    try {
      SharedPreferences prefRef = await SharedPreferences.getInstance();
      return prefRef.getString(MyConstants.PREF_KEY_BEARER_TOKEN);
    } catch (e) {
      print("Error while checking for bearer token");
      print(e.toString());
      return null;
    }
  }

  Future<User> getUserFromSharedPrefs() async {
    try {
      SharedPreferences prefRef = await SharedPreferences.getInstance();
      String accessToken = prefRef.getString(MyConstants.PREF_KEY_API_TOKEN);

      if (accessToken == null) {
        return null;
      }
      // refresh auth if required
      int expiryTimestamp = prefRef.getInt(MyConstants.KEY_EXPIRES);
      //TODO: reauthenticate on expired timestamp
      /*if (expiryTimestamp < DateTime.now().millisecondsSinceEpoch) {
        
        String refreshToken =
            prefRef.getString(MyConstants.PREF_KEY_ACCESS_TOKEN);
        Response refreshTokenResponse = await api.getRefreshedToken(
            "refresh_token",
            refreshToken,
            PrevozApi.CLIENT_ID,
            PrevozApi.CLIENT_SECRET,
            "read write");
        accessToken = refreshTokenResponse.data["access_token"];
        PrevozApi.bearer = accessToken;
        prefRef.setString(MyConstants.PREF_KEY_REFRESH_TOKEN, accessToken);
        prefRef.setBool(MyConstants.KEY_OAUTH2, true);

        expiryTimestamp = DateTime.now().millisecondsSinceEpoch +
            refreshTokenResponse.data["expires_in"] * 1000;

        prefRef.setInt(MyConstants.KEY_EXPIRES, expiryTimestamp);
      }*/
      String username = prefRef.getString(MyConstants.PREF_KEY_USERNAME);
      if (username == null) {
        return null;
      }
      List<dynamic> phoneNumbers = await getProfilePhoneNumbers();

      int lastLoginTimestamp = prefRef.getInt(MyConstants.LAST_LOGIN);

      User user = User(
          accessToken: accessToken,
          username: username,
          lastLoginTimestamp: lastLoginTimestamp,
          expiryTimestamp: expiryTimestamp, //todo needs work on backend
          phoneNumbers: phoneNumbers,
          oauth2: true // legacy stuff from old native android app
          );
      return user;
    } catch (e) {
      print("Error while checking authentication");
      print(e.toString());
      return null;
    }
  }

  Future<List<dynamic>> getProfilePhoneNumbers() async {
    SharedPreferences prefRef = await SharedPreferences.getInstance();
    String accessToken = prefRef.getString(MyConstants.PREF_KEY_API_TOKEN);
    if (accessToken == null) {
      return null;
    }

    List<dynamic> phoneNumbers =
        prefRef.getStringList(MyConstants.USERS_PHONE_NUMBERS);

    return phoneNumbers;
  }

  @override
  SnackBar createAuthSnackBar({
    @required BuildContext context,
    @required AuthErrorType errorType,
  }) {
    return SnackBar(
      content: Text(
          _getMessageForErrorType(context, errorType)
      ),
    );
  }

  String _getMessageForErrorType(BuildContext context, AuthErrorType errorType) {
    switch (errorType) {
      case AuthErrorType.SIGN_IN_ERROR:
        return AppLocalizations.of(context).authErrorLoggingIn;
      case AuthErrorType.ACCOUNT_ACTIVATION_ERROR:
        return AppLocalizations.of(context).authErrorActivatingAccount;
      case AuthErrorType.CONNECTION_ERROR:
        return AppLocalizations.of(context).notConnectedToInternet;
      case AuthErrorType.INVALID_TOKEN_ERROR:
        return AppLocalizations.of(context).authErrorInvalidToken;
    }

    return AppLocalizations.of(context).notConnectedToInternet;
  }
}
