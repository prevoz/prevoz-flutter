import 'dart:async';
import 'package:prevoz_org/mocks/base_mock.dart';
import 'package:prevoz_org/mocks/mock_auth_helper.dart';
import 'package:prevoz_org/mocks/mock_google_sign_in.dart';
import 'package:prevoz_org/mocks/mock_prevoz_api.dart';

class MockHelper {

  final MockAuthHelper _mockAuthHelper;
  final MockPrevozApi _mockPrevozApi;
  final MockGoogleSignIn _mockGoogleSignIn;

  MockHelper(
      this._mockAuthHelper,
      this._mockPrevozApi,
      this._mockGoogleSignIn
  );

  Future<String> handleMessage(String message) async {
    List<String> parsedCommand = message.split("_");
    BaseMock baseMock = _getMockForTag(parsedCommand[0]);
    baseMock.handleCommand(parsedCommand[1]);

    return null;
  }

  BaseMock _getMockForTag(String tag) {
    switch (tag) {
      case MockAuthHelper.TAG: return _mockAuthHelper;
      case MockPrevozApi.TAG: return _mockPrevozApi;
      case MockGoogleSignIn.TAG: return _mockGoogleSignIn;
    }

    return null;
  }
}