
import 'dart:async';

abstract class BaseMock {

  Future<String> handleCommand(String command);
}