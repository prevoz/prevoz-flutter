import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/helpers/push_notifications/notification_helper.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/quick_ride.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/models/route_statistics.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';
import 'package:prevoz_org/mocks/base_mock.dart';
import 'package:prevoz_org/utils/mock_data.dart';

class MockPrevozApi extends Mock implements BaseMock, BasePrevozApi {
  static const TAG = "MockPrevozApi";

  MockPrevozApi();


  @override
  Future<String> handleCommand(String command) {
    switch (command) {
      case "mockLoginSuccessful":
        _mockAccessTokenSuccessful();
        _mockLoginSuccessful();
        break;
      case "mockFullProfile":
        _mockFullProfile();
        break;
      case "mockEmptyProfile":
        _mockEmptyProfile();
        break;
      case "mockErrorFetchingProfile":
        _mockErrorFetchingProfile();
        break;
      case "mockProfilePostSuccessful":
        _mockProfilePostSuccessful();
        break;
    }

    return null;
  }

  void _mockAccessTokenSuccessful() {
    Map map = Map();
    map['access_token'] = 'asdasdasdasdasdasdasdasd';
    map['expires_in'] = '234234234234234234';
    map['token_type'] = 'Beer';
    map['scope'] = 'asdasdasdasdasdasdasdasd';
    map['refresh_token'] = 'asdasdasdasdasdasdasdasd';

    Response response = Response();
    response.data = map;
    response.statusCode = 200;

    when(this.getAccessToken(any, any, any, any, any))
        .thenAnswer((_) => Future.value(response));
  }

  void _mockLoginSuccessful() {
    // TODO implement
  }

  void _mockFullProfile() {
    List<dynamic> phoneNumbers = ["040040040"];
    Map data = Map();
    data["username"] = "testniUporabnik";
    data["first_name"] = "Janez";
    data["last_name"] = "Novak";
    data['address'] = "Za cesto 1, 1000 Ljubljana";
    data["phone_numbers"] = phoneNumbers;
    data["email"] = "info@test.si";

    Response response = Response();
    response.data = data;
    response.statusCode = 200;

    when(this.getUserProfile()).thenAnswer((_) => Future.value(response));
  }

  void _mockEmptyProfile() {
    Map data = Map();
    data["username"] = "testniUporabnik";
    data["email"] = "info@test.si";

    Response response = Response();
    response.data = data;
    response.statusCode = 200;

    when(this.getUserProfile()).thenAnswer((_) => Future.value(response));
  }

  void _mockErrorFetchingProfile() {
    Map data = Map();
    data["error"] = "some error message";
    Response response = Response();
    response.data = data;
    response.statusCode = 400;

    when(this.getUserProfile()).thenAnswer((_) => Future.value(response));
  }

  void _mockProfilePostSuccessful() {
    List<dynamic> phoneNumbers = ["040040040"];
    Map data = Map();
    data["username"] = "testniUporabnik";
    data["first_name"] = "Janez";
    data["last_name"] = "Novak";
    data['address'] = "Za cesto 1, 1000 Ljubljana";
    data["phone_numbers"] = phoneNumbers;
    data["email"] = "info@test.si";

    Response response = Response();
    response.data = data;
    response.statusCode = 200;

    when(this.postUserProfile(any)).thenAnswer((_) => Future.value(response));
  }

  _isRideValid(RideModel ride) {
    //* for now this checks only if fields are empty
    if (ride.from == null ||
        ride.fromCountry == null ||
        ride.to == null ||
        ride.toCountry == null ||
        ride.time == null ||
        ride.numPeople == null) {
      return false;
    }

    return true;
  }

  Response _buildErrorReponse(RideModel ride) {
    //var error = {};
    Map<String, dynamic> error = Map();
    if (ride.from == null) {
      error["from"] = ["Kraj odhoda - to polje je obvezno."];
    }

    if (ride.to == null) {
      error["to"] = ["Kraj prihoda - to polje je obvezno."];
    }

    if (ride.time == null) {
      error["time"] = ["Cas odhoda - to polje je obvezno."];
    }

    if (ride.numPeople == null) {
      error["num_people"] = ["Stevilo prostih mest - to polje je obvezno."];
    }

    return Response(data: {"status": "error", "error": error});
  }

  @override
  Future<Response> addRide(RideModel ride) async {
    await Future.delayed(Duration(milliseconds: 300));

    if (!_isRideValid(ride)) {
      return _buildErrorReponse(ride);
    }

    Response response = Response(data: {"id": 1212}, statusCode: 200);

    //* add new ride to mock data
    MockData.jsonMyRides["carshare_list"] = [
      {
        "id": 6011392,
        "to": "Ljubljana",
        "date": "sun, 24.3",
        "from": "Polzela",
        "full": "false",
        "time": "ob 19:00",
        "type": 0,
        "added": "2019-03-18T16:52:53+01:00",
        "price": 5,
        "to_id": 3196359,
        "author": "Amadej",
        "score_negative": 0,
        "score_positive": 3,
        "comment": "sms",
        "contact": "041123456",
        "from_id": 3194452,
        "insured": "true",
        "bookmark": null,
        "car_info": "",
        "is_author": "false",
        "num_people": 3,
        "picks_midway": "true",
        "share_type": "share",
        "to_country": "SI",
        "carshare_id": null,
        "date_iso8601": "2019-03-24T19:00:00+01:00",
        "from_country": "SI",
        "business_name": null,
        "business_active": null,
        "to_country_name": "Slovenia",
        "business_verified": null,
        "confirmed_contact": "true",
        "from_country_name": "Slovenia"
      },
      {
        "id": 1212,
        "to": "Ljubljana",
        "date": "sun, 24.3",
        "from": "Maribor",
        "full": false,
        "time": "ob 19:00",
        "type": 0,
        "added": "2019-03-18T16:52:53+01:00",
        "price": 5,
        "to_id": 3196359,
        "author": "Amadej",
        "score_negative": 0,
        "score_positive": 3,
        "comment": "sms",
        "contact": "040040040",
        "from_id": 3194452,
        "insured": "true",
        "bookmark": null,
        "car_info": "",
        "is_author": "false",
        "picks_midway": "true",
        "num_people": 3,
        "share_type": "share",
        "to_country": "SI",
        "carshare_id": null,
        "date_iso8601": "2019-03-24T19:00:00+01:00",
        "from_country": "SI",
        "business_name": null,
        "business_active": null,
        "to_country_name": "Slovenia",
        "business_verified": null,
        "confirmed_contact": "true",
        "from_country_name": "Slovenia"
      }
    ];

    return response;
  }

  @override
  Future deleteRide(String id) {
    // TODO: implement deleteRide
    return null;
  }

  @override
  Future<RidesSearchResult> fetchRides({RideSearchRequest searchRequest}) {
    return Future.value(RidesSearchResult.fromJson(MockData.jsonSearchResult));
  }

  @override
  Future<RouteStatisticsResult> fetchRouteStatistics(
      RouteStatisticsRequest request) {
    // TODO: implement fetchRouteStatistics
    //return Response(data: MockData.routeStatsMock(), statusCode: 200);

    return Future.value(
        RouteStatisticsResult.fromJson(MockData.routeStatsMock()));
  }

  @override
  Future<RidesSearchResult> fetchUsersAddedRides() {
    RidesSearchResult result = RidesSearchResult.fromJson(MockData.jsonMyRides);

    return Future.value(result);
  }

  @override
  Future<RidesSearchResult> fetchUsersBookmarkedRides() {
    RidesSearchResult result =
    RidesSearchResult.fromJson(MockData.jsonMyBookmarks);
    return Future.value(result);
  }

//  @override
//  Future getAccessToken(String grantType, String clientId, String clientSecret,
//      String code, String redirectUri) {
//    Map map = Map();
//    map['access_token'] = 'asdasdasdasdasdasdasdasd';
//    map['expires_in'] = '234234234234234234';
//    map['token_type'] = 'Beer';
//    map['scope'] = 'asdasdasdasdasdasdasdasd';
//    map['refresh_token'] = 'asdasdasdasdasdasdasdasd';
//
//    Response response = Response();
//    response.data = map;
//    response.statusCode = 200;
//
//    return Future.value(response);
//  }

  @override
  Future getAccountStatus() {
    // TODO: implement getAccountStatus

    /**
     * {is_authenticated: true, username: kunaigor44, apikey: lLzWURofVTQhlhg2oHsLxcC6Pga6gmg6ZbJio4NUeu4}
     */

    Map data = Map();
    data["is_authenticated"] = "true";
    data["username"] = "testniUporabnik";
    data["apikey"] = "hpzWURofVTQhlhg2oHsLxcC6Pga6gmg3sbJio4NUeu4";
    data["needs_verification"] = true;

    Response response = Response();
    response.data = data;
    return Future.value(response);
  }

  @override
  Future getRefreshedToken(String grantType, String refreshToken,
      String clientId, String clientSecret, String scope) {
    // TODO: implement getRefreshedToken
    return null;
  }

//  @override
//  Future getUserProfile() {
//    Response response = Response();
//    List<dynamic> phoneNumbers = ["040040040"];
//    Map data = Map();
//    data["username"] = "testniUporabnik";
//    data["first_name"] = "Janez";
//    data["last_name"] = "Novak";
//    data["phone_numbers"] = phoneNumbers;
//    data["email"] = "info@test.si";
//    response.data = data;
//    return Future.value(response);
//  }

  @override
  Future setRideBookmark(String id, String state) {
    return Future.value(
        Response(data: MockData.jsonSetBookmarkResult, statusCode: 200));
  }

  @override
  Future<Response> setSubscriptionStatus(
      PrevozRoute route, DateTime date, String status) {
    return Future.value(Response(data: "success", statusCode: 200));
  }

  @override
  void configureFirebaseMessaging() {
    NotificationHelper.fcmToken = "dummyToken";
  }

  @override
  Future<Response> setFull(int rideId, String state) {
    var data;
    if (state == "full") {
      data = {"full": "true"};
    } else {
      data = {"full": "false"};
    }
    return Future.value(Response(data: data, statusCode: 200));
  }

  @override
  Future<List<RideModel>> getQuickRides() async {
    await Future.delayed(Duration(seconds: 1));

    QuickRideResponse quickRideResponse =
    QuickRideResponse.fromJson(MockData.jsonQuickRides);

    return Future.value(quickRideResponse.getRides);
  }

  @override
  Future<Response> uploadGoogleToken({@required String idToken}) {
    Response response = Response();
    response.statusCode = 200;
    response.data = {
      "token": "bbb456"
    };

    return Future.value(response);
  }
}