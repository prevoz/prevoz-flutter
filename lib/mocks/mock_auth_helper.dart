import 'dart:async';

import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/models/user.dart';
import 'package:prevoz_org/mocks/base_mock.dart';

class MockAuthHelper extends Mock implements BaseMock, BaseAuthHelper {
  static const String TAG = "MockAuthHelper";

  MockAuthHelper();

  @override
  Future<String> handleCommand(String command) {
    return null;
  }

  @override
  Future<List> getProfilePhoneNumbers() {
    return null;
  }

  @override
  Future<User> getUserFromSharedPrefs() {
    return null;
  }
}