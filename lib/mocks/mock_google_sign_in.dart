
import 'dart:async';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/mocks/base_mock.dart';

class MockGoogleSignInAuthentication extends Mock implements GoogleSignInAuthentication {}
class MockGoogleSignInAccount extends Mock implements GoogleSignInAccount {}

class MockGoogleSignIn extends Mock implements BaseMock, GoogleSignIn {

  static const String TAG = "MockGoogleSignIn";

  MockGoogleSignIn();

  @override
  Future<String> handleCommand(String command) {
    switch (command) {
      case "mockSuccessfulGoogleSignIn":
        _mockSuccessfulGoogleSignIn();
        break;
    }

    return null;
  }

  void _mockSuccessfulGoogleSignIn() {
    GoogleSignInAuthentication googleAuthentication = MockGoogleSignInAuthentication();
    when(googleAuthentication.idToken).thenReturn("google-id-token");
    GoogleSignInAccount googleAccount = MockGoogleSignInAccount();
    when(googleAccount.authentication).thenAnswer((_) => Future.value(googleAuthentication));
    when(this.signIn()).thenAnswer((_) => Future.value(googleAccount));
  }
}