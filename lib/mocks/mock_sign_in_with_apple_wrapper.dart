
import 'dart:async';

import 'package:mockito/mockito.dart';
import 'package:prevoz_org/mocks/base_mock.dart';
import 'package:prevoz_org/utils/sign_in_with_apple_wrapper.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class MockSignInWithAppleWrapper extends Mock implements BaseMock,  SignInWithAppleWrapper {

  MockSignInWithAppleWrapper();

  @override
  Future<String> handleCommand(String command) {
    switch (command) {
      case "mockSuccessfulAppleSignIn":
        _mockSuccessfulAppleSignIn();
        break;
      case "mockAuthorizationErrorCodeCanceled":
        _mockAuthorizationErrorCodeCanceled();
        break;
    }

    return null;
  }

  void _mockSuccessfulAppleSignIn() {
    AuthorizationCredentialAppleID credentialAppleID = AuthorizationCredentialAppleID(
      userIdentifier: "user-id",
      givenName: "given-name",
      familyName: "family-name",
      email: "email",
      authorizationCode: "authorization-code",
      identityToken: "identity-token",
      state: null
    );
    when(this.getAppleIDCredential(scopes: anyNamed("scopes"))).thenAnswer((_) => Future.value(credentialAppleID));
  }
  void _mockAuthorizationErrorCodeCanceled() {
    when(this.getAppleIDCredential(scopes: anyNamed("scopes"))).thenThrow(
      SignInWithAppleAuthorizationException(
          code: AuthorizationErrorCode.canceled,
          message: "mocked-message"
      )
    );
  }

}