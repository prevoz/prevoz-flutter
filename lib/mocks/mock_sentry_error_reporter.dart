
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';

class MockSentryErrorReporter extends Mock implements SentryErrorReporter {}