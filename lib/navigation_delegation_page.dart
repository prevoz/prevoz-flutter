import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bloc.dart';
import 'package:prevoz_org/data/blocs/bottom_navigation/bottom_navigation_state.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/models/app_tab.dart';
import 'package:prevoz_org/data/models/ride_form_change.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/pages/add_ride/add_ride_page.dart';
import 'package:prevoz_org/pages/home_page/home_page.dart';
import 'package:prevoz_org/pages/my_rides/blocs/my_rides_list_bloc.dart/my_rides_list_bloc.dart';
import 'package:prevoz_org/pages/my_rides/my_rides_page.dart';
import 'package:prevoz_org/pages/notifications/notifications_page.dart';
import 'package:prevoz_org/pages/profile/profile_page.dart';
import 'package:prevoz_org/pages/search_results/bloc/notification_bloc.dart';
import 'package:prevoz_org/utils/app_config.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/prevoz_styles.dart';

import 'data/blocs/authentication/auth_error_type.dart';
import 'data/blocs/authentication/authentication_bloc.dart';
import 'data/blocs/authentication/authentication_state.dart';
import 'locale/localization/localizations.dart';

/// responsible for 3 types of navigation events
/// - bottom navigation bar taps
/// - navigate from AddRide to UsersRides after add/edit/delete ride in rideform
/// - navigating to editMode in AddRidePage from UsersRides->AddedTab
class NavigationDelegationPage extends StatelessWidget {
  final String accessTokenFromOnCreate;

  NavigationDelegationPage({Key key, this.accessTokenFromOnCreate})
      : super(key: key);

  final FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    var config = AppConfig.of(context);

    if (config.pushEnabled) {
      BlocProvider.of<NotificationSubscriptionBloc>(context)
          .configureFirebaseMessaging();
    }

    return BlocBuilder(
      cubit: BlocProvider.of<BottomNavigationBloc>(context),
      builder: (context, BottomNavigationState bottomNavState) {
        AppTab activeTab = _getActiveTab(bottomNavState);
        RideFormChange rideFormChange =
            _getRideFormChange(bottomNavState, context);
        RideModel ride = _getRide(bottomNavState);

        return Scaffold(
          body: Builder(builder: (context) =>
              BlocListener<AuthenticationBloc, AuthenticationState>(
                listener: (context, state) {
                  if (state is AuthStateError && state.type != AuthErrorType.CONNECTION_ERROR && !BlocProvider.of<AuthenticationBloc>(context).isUserLoggedIn()) {
                    Scaffold.of(context)
                      ..hideCurrentSnackBar()
                      ..showSnackBar(Injector.getInjector().get<BaseAuthHelper>().createAuthSnackBar(
                          context: context,
                          errorType: state.type
                      ));
                  }
                },
                child: _getBodyBasedOnTab(activeTab, context,
                    rideFormChange: rideFormChange, ride: ride),
              )
          ),
          bottomNavigationBar: _setupBottomNavigationBasedOnTab(
            context,
            activeTab,
          ),
        );
      },
    );
  }

  RideModel _getRide(BottomNavigationState bottomNavigationState) {
    if (bottomNavigationState is EditRidePageActive) {
      return bottomNavigationState.ride;
    }
    return null;
  }

  /// know what type of change has occured in AddRideForm
  /// 1) new ride added
  /// 2) existing ride updated
  /// 3) existing ride deleted
  RideFormChange _getRideFormChange(
      BottomNavigationState bottomNavState, BuildContext context) {
    if (bottomNavState is RideSuccessfullyAdded) {
      // check if MyRides were previously fetched.
      // if not, fetch them now and over-ride default app-wide-state approach
      BlocProvider.of<MyRidesListBloc>(context).fetchMyRides();
      return RideFormChange.rideAdded;
    }

    if (bottomNavState is RideSuccessfullyUpdated) {
      return RideFormChange.rideUpdated;
    }

    if (bottomNavState is RideSuccessfullyDeleted) {
      return RideFormChange.rideDeleted;
    }

    return null;
  }

  AppTab _getActiveTab(BottomNavigationState bottomNavState) {
    if (bottomNavState is HomePageActive) {
      return AppTab.home;
    }
    if (bottomNavState is MyRidesPageActive ||
        bottomNavState is RideSuccessfullyAdded ||
        bottomNavState is RideSuccessfullyUpdated ||
        bottomNavState is RideSuccessfullyDeleted) {
      return AppTab.myrides;
    }
    if (bottomNavState is AddRidePageActive ||
        bottomNavState is EditRidePageActive) {
      return AppTab.addride;
    }
    if (bottomNavState is ProfilePageActive) {
      return AppTab.profile;
    }

    if (bottomNavState is NotificationsPageActive) {
      return AppTab.notifications;
    }

    return AppTab.home;
  }

  Widget _setupBottomNavigationBasedOnTab(
      BuildContext context, AppTab activeTab) {
    return BottomNavigationBar(
      currentIndex: _getCurrentIndex(activeTab, context),
      type: BottomNavigationBarType.fixed,
      unselectedFontSize: 12,
      selectedFontSize: 13,
      onTap: (index) {
        String tabName = AppTab.values[index].toString().split(".")[1];
        analytics.logEvent(name: "bottom_nav_click_$tabName");
        updateTab(
            context, AppTab.values[index]); //* change this for build flavor...
      },
      items: _buildBottomNavigationItems(
          activeTab, context), //* change this for build flavor...
    );
  }

  int _getCurrentIndex(AppTab activeTab, BuildContext context) {
    var config = AppConfig.of(context);

    int currentIndex = AppTab.values.indexOf(activeTab);

    if (!config.pushEnabled) {
      if (activeTab == AppTab.profile) {
        //* notifications tab is hidden when push disabled, therefor profile index is one less
        currentIndex = currentIndex - 1;
      }
    }

    return currentIndex;
  }

  List<BottomNavigationBarItem> _buildBottomNavigationItems(
      AppTab activeTab, BuildContext context) {
    var config = AppConfig.of(context);

    if (config.pushEnabled) {
      return [
        BottomNavigationBarItem(
          icon: Image.asset(
            'assets/icon/search_bottom_gray.png',
            key: Key("_bottomNavSearchIcon"),
            height: 24,
            width: 24,
            color: activeTab == AppTab.home
                ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                : HexColor("#333333"),
          ),
          label: AppLocalizations.of(context).navigationLabelSearch,
        ),
        BottomNavigationBarItem(
            icon: Image.asset(
              'assets/icon/camel_bottom_gray.png',
              key: Key("_bottomNavMyRidesIcon"),
              height: 24,
              width: 24,
              color: activeTab == AppTab.myrides
                  ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                  : HexColor("#333333"),
            ),
            label: AppLocalizations.of(context).navigationLabelMyRides,
        ),
        BottomNavigationBarItem(
            icon: Image.asset(
              'assets/icon/dodaj_bottom_gray.png',
              key: Key("_bottomNavAddRideIcon"),
              height: 24,
              width: 24,
              color: activeTab == AppTab.addride
                  ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                  : HexColor("#333333"),
            ),
            label: AppLocalizations.of(context).navigationLabelAddRide,
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.notifications,
              key: Key("_bottomNavNotificationIcon"),
              color: activeTab == AppTab.notifications
                  ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                  : HexColor("#333333"),
            ),
            label: AppLocalizations.of(context).navigationLabelNotifications
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              key: Key("_bottomNavProfileIcon"),
              color: activeTab == AppTab.profile
                  ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                  : HexColor("#333333"),
            ),
            label: AppLocalizations.of(context).navigationLabelMyProfile
        ),
      ];
    } else {
      return [
        BottomNavigationBarItem(
          icon: Image.asset(
            'assets/icon/search_bottom_gray.png',
            key: Key("_bottomNavSearchIcon"),
            height: 24,
            width: 24,
            color: activeTab == AppTab.home
                ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                : HexColor("#333333"),
          ),
          label: AppLocalizations.of(context).navigationLabelSearch,
        ),
        BottomNavigationBarItem(
            icon: Image.asset(
              'assets/icon/camel_bottom_gray.png',
              key: Key("_bottomNavMyRidesIcon"),
              height: 24,
              width: 24,
              color: activeTab == AppTab.myrides
                  ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                  : HexColor("#333333"),
            ),
            label: AppLocalizations.of(context).navigationLabelMyRides
        ),
        BottomNavigationBarItem(
            icon: Image.asset(
              'assets/icon/dodaj_bottom_gray.png',
              key: Key("_bottomNavAddRideIcon"),
              height: 24,
              width: 24,
              color: activeTab == AppTab.addride
                  ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                  : HexColor("#333333"),
            ),
            label: AppLocalizations.of(context).navigationLabelAddRide
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              key: Key("_bottomNavProfileIcon"),
              color: activeTab == AppTab.profile
                  ? HexColor(PrevozStyles.PREVOZ_ORANGE_HEX)
                  : HexColor("#333333"),
            ),
            label: AppLocalizations.of(context).navigationLabelMyProfile
        ),
      ];
    }
  }

  void updateTab(BuildContext context, AppTab appTab) {
    var config = AppConfig.of(context);

    if (!config.pushEnabled) {
      if (appTab == AppTab.notifications) {
        appTab = AppTab.profile;
      }
    }

    BlocProvider.of<BottomNavigationBloc>(context)
        .add(UpdateTabBottomNavEvent(appTab));
  }

  Widget _getBodyBasedOnTab(AppTab activeTab, BuildContext context,
      {RideFormChange rideFormChange, RideModel ride}) {

    scheduleMicrotask(() async {
      try {
        var connectivityResult = await (Connectivity().checkConnectivity());
        if (connectivityResult == ConnectivityResult.none) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(Injector.getInjector().get<BaseAuthHelper>().createAuthSnackBar(
                context: context,
                errorType: AuthErrorType.CONNECTION_ERROR
            ));
        }
      } catch (e) {
        // ignore
      }
    });

    if (activeTab == AppTab.home) {
      return HomePage();
    }

    if (activeTab == AppTab.myrides) {
      return MyRidesPage(
        accessTokenFromOnCreate,
        rideFormChange: rideFormChange,
      );
    }

    if (activeTab == AppTab.addride) {
      return AddRidePage(accessTokenFromOnCreate, ride);
    }

    if (activeTab == AppTab.notifications) {
      return NotificationsPage();
    }

    if (activeTab == AppTab.profile) {
      return ProfilePage();
    }

    return Container();
  }
}
