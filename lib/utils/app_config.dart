import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';

class AppConfig extends InheritedWidget {
  final bool pushEnabled;
  final BaseDatabaseHelper appDatabase;
  final bool debugMode;

  AppConfig(
      {@required this.pushEnabled,
      @required Widget child,
      @required this.debugMode,
      @required this.appDatabase})
      : super(child: child);

  static AppConfig of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
