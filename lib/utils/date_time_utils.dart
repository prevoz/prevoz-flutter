import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:prevoz_org/data/network/handle_error.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:time_machine/time_machine.dart';

class DateTimeUtils {
  /// Formats DateTime into string "2019-03-28"
  static dateToStringForApiCalls(DateTime datetime) {
    String selectedYear = datetime.year.toString();
    String selectedMonth = datetime.month > 9
        ? datetime.month.toString()
        : "0" + datetime.month.toString();

    String selectedDay = datetime.day > 9
        ? datetime.day.toString()
        : "0" + datetime.day.toString();

    return selectedYear + "-" + selectedMonth + "-" + selectedDay;
  }

  static String timeToStringForApiCalls(DateTime dt) {
    String hour = dt.hour.toString();
    String minutes = dt.minute.toString();
    if (dt.minute < 10) {
      minutes = "0$minutes";
    }
    if (dt.hour < 10) {
      hour = "0$hour";
    }
    print("timeToString, api calls: " + hour + ":" + minutes);
    return hour + ":" + minutes;
  }

  static bool selectableDatesForDatePicker(DateTime selectedDate) {
    DateTime now = new DateTime.now();
    DateTime todaysDate = new DateTime(now.year, now.month, now.day);
    if (selectedDate.isBefore(todaysDate)) {
      return false;
    }
    DateTime fortyFiveDaysFromNow = DateTime.now().add(Duration(days: 45));

    if (selectedDate.isAfter(fortyFiveDaysFromNow)) {
      return false;
    }

    return true;
  }

  static String dateTimeToDayNameWithDate(
      DateTime dateTime, BuildContext context) {
    try {
      String month = dateTime.month.toString();
      String day = dateTime.day.toString();

      return getDayName(dateTime, context) + ", " + day + "." + month + ".";
    } catch (e) {
      handleError(error: e, message: "DateTimeUtils, dayNameWithDate");
      return '';
    }
  }

  /// without relative time descriptions (no today, tomorrow, etc)
  /// without year display
  static String dateTimeToDisplayableDate(
      DateTime dateTime, BuildContext context) {
    try {
      return "${getDayName(dateTime, context)}, ${dateTime.day.toString()}.${dateTime.month.toString()}.";
    } catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(e, StackTrace.fromString(dateTime.toString()));
      return AppLocalizations.of(context).errorSelectingDate;
    }
  }

  static String dateTimeToDisplayableDateWithShortenedDayname(
      DateTime dateTime, BuildContext context) {
    try {
      String dayName = getDayName(dateTime, context);
      dayName = dayName.substring(0, 3) + ".";
      return "$dayName, ${dateTime.day.toString()}.${dateTime.month.toString()}.";
    } catch (e) {
      Injector.getInjector().get<SentryErrorReporter>().reportErrorToSentry(e, StackTrace.fromString(dateTime.toString()));
      return AppLocalizations.of(context).errorSelectingDate;
    }
  }

  static String dateTimeToMonthName(DateTime dateTime, BuildContext context) {
    switch (dateTime.month) {
      case 1:
        return AppLocalizations.of(context).january;
        break;
      case 2:
        return AppLocalizations.of(context).february;
        break;
      case 3:
        return AppLocalizations.of(context).march;
        break;
      case 4:
        return AppLocalizations.of(context).april;
        break;
      case 5:
        return AppLocalizations.of(context).may;
        break;
      case 6:
        return AppLocalizations.of(context).june;
        break;
      case 7:
        return AppLocalizations.of(context).july;
        break;
      case 8:
        return AppLocalizations.of(context).august;
        break;
      case 9:
        return AppLocalizations.of(context).september;
        break;
      case 10:
        return AppLocalizations.of(context).october;
        break;
      case 11:
        return AppLocalizations.of(context).november;
        break;
      case 12:
        return AppLocalizations.of(context).december;
        break;
      default:
        return "";
    }
  }

  /// for example - 19. julij
  ///
  static String dateTimeToDayAndMonthName(
      DateTime dateTime, BuildContext context) {
    try {
      return dateTime.day.toString() +
          ". " +
          dateTimeToMonthName(dateTime, context);
    } catch (e) {
      handleError(
          error: e, message: "DateTimeUtils, dateTimeToDayAndMonthName");
      return '';
    }
  }

  /// compares day, month and year of two dates
  static bool areDatesTheSame(DateTime firstDate, DateTime secondDate) {
    if (firstDate.day == secondDate.day &&
        firstDate.month == secondDate.month &&
        firstDate.year == secondDate.year) {
      return true;
    }

    return false;
  }

  static String dateToStringForDisplay(
      DateTime selectedDate, BuildContext context) {
    try {
      return dateWithDayName(selectedDate, context);
    } catch (e) {
      handleError(error: e, message: "dateToStringForDisplay");
      return "";
    }
  }

  static DateTime fortyFiveDaysFromNow() {
    return DateTime.now().add(Duration(days: 45));
  }

  static TimeOfDay nextFullHour() {
    int hourPlusOne = TimeOfDay.now().hour == 23 ? 0 : TimeOfDay.now().hour + 1;
    TimeOfDay nextFullHour = new TimeOfDay(hour: hourPlusOne, minute: 0);
    return nextFullHour;
  }

  static TimeOfDay timeStringToTimeOfDay(String time) {
    TimeOfDay timeOfDay = TimeOfDay.now();

    try {
      if (time.contains("ob ")) {
        time = time.replaceAll("ob ", "");
      }
      List<String> timeSplit = time.split(":");
      int hour = int.parse(timeSplit[0]);
      int minute = int.parse(timeSplit[1]);

      timeOfDay = timeOfDay.replacing(hour: hour, minute: minute);

      return timeOfDay;
    } catch (e) {
      return timeOfDay;
    }
  }

  static int timeStringToSeconds(String time) {
    TimeOfDay timeOfDay = DateTimeUtils.timeStringToTimeOfDay(time);

    return timeOfDay.hour * 3600 + timeOfDay.minute * 60;
  }

  /// For dates, API returns: date: sob., 29.6., ob 16:45
  /// to displaying it propery in edit ride mode, we need to remove ', ob 16:46'
  static removeTimeFromRidesDate(String date) {
    List<String> stringSplit = date.split(", ob");
    String shortenedDayName = stringSplit[0].substring(0, 3) + ".";

    return shortenedDayName;
  }

  /// for example, ponedeljek, 10.09.
  static String dateWithDayName(DateTime selectedDate, BuildContext context) {
    String selectedMonth = selectedDate.month > 9
        ? selectedDate.month.toString()
        : "0" + selectedDate.month.toString();

    String selectedDay = selectedDate.day > 9
        ? selectedDate.day.toString()
        : "0" + selectedDate.day.toString();

    String dayName = getDayName(selectedDate, context);

    return dayName + ", " + selectedDay + "." + selectedMonth + ".";
  }

  static bool isSelectedDateBeforeCurrentDate(DateTime selectedDate) {
    DateTime today = DateTime.now();
    if (selectedDate.day == today.day &&
        selectedDate.month == today.month &&
        selectedDate.year == today.year) {
      return false;
    }

    if (selectedDate.isBefore(DateTime.now())) {
      return true;
    }

    return false;
  }

  static String getDayName(DateTime datetime, BuildContext context) {
    try {
      switch (datetime.weekday) {
        case 1:
          return AppLocalizations.of(context).monday.toLowerCase();
          break;
        case 2:
          return AppLocalizations.of(context).tuesday.toLowerCase();
          break;
        case 3:
          return AppLocalizations.of(context).wednessday.toLowerCase();
          break;

        case 4:
          return AppLocalizations.of(context).thursday.toLowerCase();
          break;

        case 5:
          return AppLocalizations.of(context).friday.toLowerCase();
          break;

        case 6:
          return AppLocalizations.of(context).saturday.toLowerCase();
          break;

        case 7:
          return AppLocalizations.of(context).sunday.toLowerCase();
          break;
        default:
          return "";
          break;
      }
    } catch (e) {
      handleError(
          error: e.toString(),
          message: "DateTimeUtils, getDayName() for datetime = " +
              (datetime != null ? datetime.toString() : "null"));
      return "";
    }
  }

  /// 2019-08-22:
  static String getDateString(DateTime dateTime) {
    String year = dateTime.year.toString();
    String month = dateTime.month.toString();
    String day = dateTime.day.toString();
    if (dateTime.month < 10) {
      month = "0" + month;
    }
    if (dateTime.day < 10) {
      day = "0" + day;
    }
    return "$year-$month-$day";
  }

  /// used for route stats
  static List<DateTime> todayTomorrowDayAfterTomorrow() {
    return [
      DateTime.now(),
      DateTime.now().add(Duration(days: 1)),
      DateTime.now().add(Duration(days: 2))
    ];
  }

  static String dateTimeToPrettyString(DateTime dateTime) {
    String hour = dateTime.hour.toString();
    String minutes = dateTime.minute.toString();
    if (dateTime.minute < 10) {
      minutes = "0$minutes";
    }
    if (dateTime.hour < 10) {
      hour = "0$hour";
    }
    print("timeToString, api calls: " + hour + ":" + minutes);
    return hour + ":" + minutes;
  }

  static String timeOfDayToPrettyString(TimeOfDay selectedTime) {
    String hour = selectedTime.hour.toString();
    String minutes = selectedTime.minute.toString();
    if (selectedTime.minute < 10) {
      minutes = "0$minutes";
    }
    if (selectedTime.hour < 10) {
      hour = "0$hour";
    }
    print("timeToString, api calls: " + hour + ":" + minutes);
    return hour + ":" + minutes;
  }

  /// api returns time like this: "ob 12:00"
  /// this returns just 12:00
  static String pretifyApiTime(String time) {
    try {
      if (time.contains("ob ")) {
        List<String> strings = time.split("ob ");
        return strings[1];
      }

      return time;
    } catch (e) {
      handleError(error: e, message: "DateTimeUtils, pretifyApiTime");
      return "";
    }
  }

  static int startOfDayToMilisecSinceEpoch(DateTime date) {
    ZonedDateTime startOfDay = ZonedDateTime.atStartOfDay(
        LocalDate.dateTime(date), DateTimeZone.forOffset(Offset.hours(1)));
    print("______ miliseconds since epoch: " +
        startOfDay.toDateTimeLocal().millisecondsSinceEpoch.toString());

    return startOfDay.toDateTimeLocal().millisecondsSinceEpoch;
  }

  /// converts "2019-09-15" to datetime object
  static DateTime stringToDateTime(String dateString) {
    try {
      List<String> stringSplits = dateString.split("-");
      int year = int.parse(stringSplits[0]);
      int month = int.parse(stringSplits[1]);
      int day = int.parse(stringSplits[2]);

      return DateTime(year, month, day);
    } catch (e) {
      return DateTime.now();
    }
  }

  static DateTime stringTimeToDateTime(String timeString) {
    try {
      DateTime now = DateTime.now();

      String hour = timeString.split(":")[0];
      String minute = timeString.split(":")[1];

      DateTime dateTime = DateTime(
          now.year, now.month, now.day, int.parse(hour), int.parse(minute));

      return dateTime;
    } catch (e) {
      return DateTime.now();
    }
  }

  static bool isLeapYear(int year) =>
      (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0));

  static String getNextFullHour() {
    int currentHour = DateTime.now().hour;
    if (currentHour == 23) {
      return "00:00";
    }
    if (currentHour + 1 < 10) {
      return "0${currentHour + 1}:00";
    }
    return "${currentHour + 1}:00";
  }
}
