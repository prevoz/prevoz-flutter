///
/// Flutter Captured Error Reporting
/// Created by Simon Lightfoot
///
/// Copyright (C) DevAngels Limited 2018
/// License: APACHE 2.0 - https://www.apache.org/licenses/LICENSE-2.0
///
import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui show window;
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:logger/logger.dart';
import 'package:package_info/package_info.dart';
import 'package:device_info/device_info.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:system_info/system_info.dart';
import 'package:connectivity/connectivity.dart';
import 'package:sentry/sentry.dart';

const int MEGABYTE = 1024 * 1024;
typedef Future<Null> ErrorReporter(Object error, StackTrace stackTrace);
typedef Widget ErrorReportingWidget(ErrorReporter reporter);

class SentryErrorReporter {
  SentryClient sentry;
  bool isInDebugMode;
  Logger logger = Logger(printer: CustomLogPrinter("SentryErrorReporter"));

  runCapturedApp(ErrorReportingWidget app,
      {@required String dsn, @required bool debugMode}) {
    sentry = SentryClient(dsn: dsn);
    isInDebugMode = debugMode == null ? true : debugMode;
    FlutterError.onError = (FlutterErrorDetails details) {
      if (debugMode) {
        //* In development mode simply print to console.
        FlutterError.dumpErrorToConsole(details);
      } else {
        //* In production mode report to the application zone to report to Sentry.
        Zone.current.handleUncaughtError(details.exception, details.stack);
      }
    };
    runZoned(() {
      runApp(app((error, stackTrace) => reportErrorToSentry(error, stackTrace)));
    }, onError: (Object error, StackTrace stackTrace) async {
      await reportErrorToSentry(error, stackTrace);
    });
  }

  Future<Null> submitMessageToSentry({@required String message, @required SeverityLevel level, Map<String, dynamic> extraData}) async {
    if (isInDebugMode == null || isInDebugMode) {
      logger.i(message);
      logger.w('In dev mode. Not sending message to Sentry.io.');
      return;
    }

    final PackageInfo info = await PackageInfo.fromPlatform();
    Map<String, dynamic> extra = await getExtraData();
    Map<String, dynamic> tags = await getTags(info);
    if (extraData != null) {
      extra.addAll(extraData);
    }

    final Event event = Event(
      loggerName: '',
      message: message,
      level: level,
      release: '${info.version}_${info.buildNumber}',
      //environment: 'qa',
      tags: tags,
      extra: extra,
    );

    try {
      if (sentry == null) {
        sentry = SentryClient(
            dsn: MyConstants.SENTRY_DSN);
      }
      final SentryResponse response = await sentry.capture(event: event);
      if (response.isSuccessful) {
        logger.v('Success! Event ID: ${response.eventId}');
      } else {
        logger.e('Failed to report to Sentry.io: ${response.error}');
      }
    } catch (e, stackTrace) {
      logger
          .e('Exception whilst reporting to Sentry.io\n' + stackTrace.toString());
    }
  }

  Future<Null> reportErrorToSentry(Object error, StackTrace stackTrace) async {
    logger.e('Caught error: $error');

    if (isInDebugMode == null || isInDebugMode) {
      logger.e(stackTrace);
      logger.w('In dev mode. Not sending report to Sentry.io.');
      return;
    }

    final PackageInfo info = await PackageInfo.fromPlatform();
    Map<String, dynamic> extra = await getExtraData();
    Map<String, dynamic> tags = await getTags(info);

    final Event event = Event(
      loggerName: '',
      exception: error,
      stackTrace: stackTrace,
      release: '${info.version}_${info.buildNumber}',
      //environment: 'qa',
      tags: tags,
      extra: extra,
    );

    try {
      if (sentry == null) {
        sentry = SentryClient(
            dsn: MyConstants.SENTRY_DSN);
      }
      final SentryResponse response = await sentry.capture(event: event);
      if (response.isSuccessful) {
        logger.v('Success! Event ID: ${response.eventId}');
      } else {
        logger.e('Failed to report to Sentry.io: ${response.error}');
      }
    } catch (e, stackTrace) {
      logger
          .e('Exception whilst reporting to Sentry.io\n' + stackTrace.toString());
    }
  }

  Future<Map<String, dynamic>> getTags(PackageInfo info) async {
    Map<String, String> tags = {};
    tags['platform'] =
        defaultTargetPlatform.toString().substring('TargetPlatform.'.length);
    tags['package_name'] = info.packageName;
    tags['build_number'] = info.buildNumber;
    tags['version'] = info.version;
    tags['locale'] = ui.window.locale.toString();

    ConnectivityResult connectivity = await (Connectivity().checkConnectivity());
    tags['connectivity'] =
        connectivity.toString().substring('ConnectivityResult.'.length);

    return tags;
  }

  Future<Map<String, dynamic>> getExtraData() async {
    Map<String, dynamic> extra = {};
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (defaultTargetPlatform == TargetPlatform.android) {
      try {
        AndroidDeviceInfo devInfo = await deviceInfo.androidInfo;
        extra["device_info"] = getAndroidDeviceInfo(devInfo);
      } catch (e) {
        extra['device_info'] = "Android - couldn't get Android device info";
      }
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      try {
        IosDeviceInfo devInfo = await deviceInfo.iosInfo;
        extra["device_info"] = getIOSDeviceInfo(devInfo);
      } catch (e) {
        extra['device_info'] = "iOS - couldn't get iOS device info";
      }
    }

    Map<String, dynamic> uiValues = {};
    uiValues['locale'] = ui.window.locale.toString();
    uiValues['pixel_ratio'] = ui.window.devicePixelRatio;
    uiValues['default_route'] = ui.window.defaultRouteName;
    uiValues['physical_size'] = [
      ui.window.physicalSize.width,
      ui.window.physicalSize.height
    ];
    uiValues['text_scale_factor'] = ui.window.textScaleFactor;
    uiValues['view_insets'] = [
      ui.window.viewInsets.left,
      ui.window.viewInsets.top,
      ui.window.viewInsets.right,
      ui.window.viewInsets.bottom
    ];
    uiValues['padding'] = [
      ui.window.padding.left,
      ui.window.padding.top,
      ui.window.padding.right,
      ui.window.padding.bottom
    ];

    extra['ui'] = uiValues;

    //* supported only on Android
    if (Platform.isAndroid) {
      Map<String, dynamic> memory = {};
      memory['phys_total'] = '${SysInfo.getTotalPhysicalMemory() ~/ MEGABYTE}MB';
      memory['phys_free'] = '${SysInfo.getFreePhysicalMemory() ~/ MEGABYTE}MB';
      memory['virt_total'] = '${SysInfo.getTotalVirtualMemory() ~/ MEGABYTE}MB';
      memory['virt_free'] = '${SysInfo.getFreeVirtualMemory() ~/ MEGABYTE}MB';
      extra['memory'] = memory;
    }

    extra['dart_version'] = Platform.version;

    return extra;
  }

  Map<String, dynamic> getAndroidDeviceInfo(AndroidDeviceInfo devInfo) {
    Map<String, dynamic> deviceProperties = Map();
    deviceProperties["product"] = devInfo.product;
    deviceProperties["supportedAbis"] = devInfo.supportedAbis;
    deviceProperties["supported32BitAbis"] = devInfo.supported32BitAbis;
    deviceProperties["manufacturer"] = devInfo.manufacturer;
    deviceProperties["tags"] = devInfo.tags;
    deviceProperties["supported64BitAbis"] = devInfo.supported64BitAbis;
    deviceProperties["bootloader"] = devInfo.bootloader;
    deviceProperties["fingerprint"] = devInfo.fingerprint;
    deviceProperties["host"] = devInfo.host;
    deviceProperties["model"] = devInfo.model;
    deviceProperties["id"] = devInfo.id;
    deviceProperties["brand"] = devInfo.brand;
    deviceProperties["device"] = devInfo.device;
    deviceProperties["board"] = devInfo.board;
    deviceProperties["androidId"] = devInfo.androidId;
    deviceProperties["hardware"] = devInfo.hardware;

    return deviceProperties;
  }

  Map<String, dynamic> getIOSDeviceInfo(IosDeviceInfo devInfo) {
    Map<String, dynamic> deviceProperties = Map();
    deviceProperties["name"] = devInfo.name;
    deviceProperties["model"] = devInfo.model;
    deviceProperties["localizedModel"] = devInfo.localizedModel;
    deviceProperties["systemName"] = devInfo.systemName;
    deviceProperties["systemVersion"] = devInfo.systemVersion;
    deviceProperties["identifierForVendor"] = devInfo.identifierForVendor;
    deviceProperties["isPhysicalDevice"] = devInfo.isPhysicalDevice.toString();

    return deviceProperties;
  }
}