
import 'dart:async';

import 'package:meta/meta.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class SignInWithAppleWrapper implements SignInWithApple {

  Future<AuthorizationCredentialAppleID> getAppleIDCredential({
    @required List<AppleIDAuthorizationScopes> scopes,
    WebAuthenticationOptions webAuthenticationOptions,
    String nonce,
    String state,
  }) async {
    return SignInWithApple.getAppleIDCredential(
      scopes: scopes,
      webAuthenticationOptions: webAuthenticationOptions,
      nonce: nonce,
      state: state
    );
  }
}