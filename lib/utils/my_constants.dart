import 'package:prevoz_org/data/models/country.dart';

class MyConstants {
  static const String CLIENT_ID = "QTwUBJLA8ZngFS5iK2h2kcV68qAftyLIi2gjXkIy";
  static const String CLIENT_SECRET =
      "qcPKCeIScAU8Ca009BwokX4xW86AhSaPZu1rqu2ZCygMPhsrG57sF1gMcryzCBqf2qwSuZpGFVkurl0ZtiVwLi62B3pLYoawTk2z0qX2PcSePZvVkrjGsntxSbxOroSc";
  static const String redirectUrl = "prevoz://auth/done/";

  //todo: auth token response related fields have un-matching value names because they're copied
  //todo  from existing android app for potential reuse and sharing between shared prefs of existing
  //todo  android app an flutter app that will replace it. But this probably wont be the case
  //todo  and users will have to reauthenticate with the new flutter app.
  static const String PREF_KEY_USERNAME = "authAccount";

  static const String PREF_KEY_BEARER_TOKEN = "password";

  static const String PREF_KEY_REFRESH_TOKEN = "authtoken";

  static const String PREF_KEY_API_TOKEN = "api_token";

  static const String PREF_KEY_PROFILE_USERNAME = "profile_username";

  static const String PREF_KEY_PROFILE_EMAIL = "profile_email";

  static const String PREF_KEY_PROFILE_FIRST_NAME = "profile_first_name";

  static const String PREF_KEY_PROFILE_LAST_NAME = "profile_last_name";

  static const String PREF_KEY_PROFILE_ADDRESS = "profile_address";

  static const String KEY_ACCOUNT_TYPE = "accountType";

  static const String TYPE_PREVOZ_ACCOUNT = "org.prevoz.account";

  static const String KEY_OAUTH2 = "oauth2";

  static const String KEY_EXPIRES = "oauth2.token_expires";

  static const String LAST_LOGIN = "lastlogin";

  static const String KEY_ERROR_CODE = "errorCode";

  static const String KEY_ERROR_MSG = "errorMessage";

  static const String SUCCESS_ADD = "SuccessAdd";

  static const String SUCCESS_DELETE = "SuccessDelete";

  static const String SUCCESS_EDIT = "SuccessEdit";

  static const String GOING_WITH = "going_with";

  static const String OUT_OF_SEATS = "out_of_seats";

  static const String NOT_GOING_WITH = "not_going_with";

  static const String BOOKMARK = "bookmark";

  static const String USERS_PHONE_NUMBERS = "users_phone_numbers";

  static const double INNER_CARD_PADDING = 14;

  static const NUM_OF_LOCATIONS_IN_DATABASE = 1214;

  static const SENTRY_DSN =
      "https://82049105e6cd4ab794b3f4a513451a07@sentry.legit.si/1";

  static String get navigateToRideDetailsKey => "NavigateToRideDetails";

  static String get fromLocationKey => "FromLocationKey";

  static String get toLocationKey => "ToLocationKey";

  static String get eventPopulatedDefaultSearchHistory =>
      "populatedDefaultSearchHistory";

  static String get searchButtonKey => "searchButtonKey";

  static get resultsCircularProgressKey => "circularProgressKeyResults";

  static String get resultsScrollViewKey => "resultsScrollViewKey";

  static String get searchResultsColumn => "searchResultsColumn";

  static String get commentContainerKey => "CommentContainer";

  static String get notTheFirstTimeInApp => "NotTheFirstTimeInApp";

  ///used for quick access without DB or FileReader
  static List<Country> countriesList = [
    Country(countryCode: "SI", name: "Slovenija"),
    Country(countryCode: "CZ", name: "Češka"),
    Country(countryCode: "DK", name: "Danska"),
    Country(countryCode: "DE", name: "Nemčija"),
    Country(countryCode: "EE", name: "Estonija"),
    Country(countryCode: "ES", name: "Španija"),
    Country(countryCode: "FI", name: "Finska"),
    Country(countryCode: "FR", name: "Francija"),
    Country(countryCode: "GR", name: "Grčija"),
    Country(countryCode: "HR", name: "Hrvaška"),
    Country(countryCode: "HU", name: "Madžarska"),
    Country(countryCode: "IT", name: "Italija"),
    Country(countryCode: "LT", name: "Litva"),
    Country(countryCode: "LV", name: "Latvija"),
    Country(countryCode: "ME", name: "Črna gora"),
    Country(countryCode: "MK", name: "Makedonija"),
    Country(countryCode: "NL", name: "Nizozemska"),
    Country(countryCode: "PL", name: "Poljska"),
    Country(countryCode: "PT", name: "Portugalska"),
    Country(countryCode: "RO", name: "Romunija"),
    Country(countryCode: "RS", name: "Srbija"),
    Country(countryCode: "SE", name: "Švedska"),
    Country(countryCode: "SK", name: "Slovaška"),
    Country(countryCode: "AT", name: "Avstrija"),
    Country(countryCode: "BA", name: "Bosna in Hercegovina"),
    Country(countryCode: "CH", name: "Švica"),
    Country(countryCode: "BE", name: "Belgija"),
    Country(countryCode: "BG", name: "Bolgarija"),
  ];

  static Map countries = {
    "countries": [
      Country(countryCode: "SI", name: "Slovenija"),
      Country(countryCode: "CZ", name: "Češka"),
      Country(countryCode: "DK", name: "Danska"),
      Country(countryCode: "DE", name: "Nemčija"),
      Country(countryCode: "EE", name: "Estonija"),
      Country(countryCode: "ES", name: "Španija"),
      Country(countryCode: "FI", name: "Finska"),
      Country(countryCode: "FR", name: "Francija"),
      Country(countryCode: "GR", name: "Grčija"),
      Country(countryCode: "HR", name: "Hrvaška"),
      Country(countryCode: "HU", name: "Madžarska"),
      Country(countryCode: "IT", name: "Italija"),
      Country(countryCode: "LT", name: "Litva"),
      Country(countryCode: "LV", name: "Latvija"),
      Country(countryCode: "ME", name: "Črna gora"),
      Country(countryCode: "MK", name: "Makedonija"),
      Country(countryCode: "NL", name: "Nizozemska"),
      Country(countryCode: "PL", name: "Poljska"),
      Country(countryCode: "PT", name: "Portugalska"),
      Country(countryCode: "RO", name: "Romunija"),
      Country(countryCode: "RS", name: "Srbija"),
      Country(countryCode: "SE", name: "Švedska"),
      Country(countryCode: "SK", name: "Slovaška"),
      Country(countryCode: "AT", name: "Avstrija"),
      Country(countryCode: "BA", name: "Bosna in Hercegovina"),
      Country(countryCode: "CH", name: "Švica"),
      Country(countryCode: "BE", name: "Belgija"),
      Country(countryCode: "BG", name: "Bolgarija"),
    ]
  };
}
