import 'package:flutter/material.dart';
import 'package:prevoz_org/data/models/add_ride_input_types.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/utils/hex_color.dart';
import 'package:prevoz_org/utils/ui_color.dart';

class PrevozStyles {
  static const int maxDateTextLength = 16;

  static const String GREY_TEXT_HEX = "#4D4D4D";
  static const String PREVOZ_ORANGE_HEX = "#EC7100";
  static const String PREVOZ_CARD_GREY = "#FAF7F2";
  static const String BOOKMARK_ACTIVE = "F20E38";
  static const String BOOKMARK_UNACTIVE = "808080";

  static const formInuptTextStyle = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w600, fontFamily: "InterRegular");

  static TextStyle cardTitleStyle = TextStyle(
    fontSize: 18,
    fontFamily: "InterBlack",
  );

  static Widget myRidesFirstItemInList(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    double firstItemHeight = 47;
    if (deviceHeight > 680) {
      firstItemHeight = 57;
    }
    return SizedBox(
      height: firstItemHeight,
    );
  }

  static InputDecoration searchFormCardInputDecoration(
      String label, TextEditingController controller) {
    double fontSizeForLabel = 14;

    if (controller.text != null && controller.text.length > 0) {
      fontSizeForLabel = 20;
    }

    return InputDecoration(
        labelText: label,
        labelStyle: TextStyle(
            color: HexColor("808080"),
            fontSize: fontSizeForLabel,
            fontFamily: "InterRegular"),
        fillColor: HexColor("#FFFFFF"),
        prefix: SizedBox(
          width: 5,
        ),
        contentPadding: EdgeInsets.all(12),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 3)));
  }

  /// setupResponsiveDecoration() is used for responding to
  /// to changes in focus of TextInputFields in AddRideForm on following ways:
  /// 1) change label size when field is focused (because default Flutter label sizes on focused inputs are too small)
  /// 2) add fill color to certain fields if edit mode
  static InputDecoration setupResponsiveDecoration(
      {String label,
      IconData iconData,
      FocusNode focusNode,
      TextEditingController textController,
      BuildContext context,
      bool isMultiline = false,
      bool isEditMode = false,
      AddRideInputType inputType}) {
    //* check if field focused or has content
    bool hasFocusOrContent =
        _fieldHasFocusOrContent(node: focusNode, controller: textController);
    //* shorten the label for number of people when it gains focus
    if (context != null &&
        inputType == AddRideInputType.numOfPpl &&
        hasFocusOrContent) {
      label = AppLocalizations.of(context).numOfPeopleShortened;
    }
    return InputDecoration(
      suffixIcon: iconData != null
          ? Icon(
              iconData,
              size: 20,
            )
          : null,
      labelText: label,
      // comments is a multiline textfield that needs alignLabelWithHint set to true
      alignLabelWithHint: isMultiline ? true : false,
      labelStyle: _getLabelStyle(hasFocusOrContent, isEditMode, inputType),
      contentPadding: EdgeInsets.only(top: 12, left: 10, right: 10, bottom: 8),
      fillColor: _editModeFillColor(),
      filled: _determineIfInputIsFilled(isEditMode, inputType),
      border: _getInputBoarder(isEditMode,
          inputType), //UIColor(red:0.88, green:0.87, blue:0.85, alpha:1)
      errorMaxLines: 5,
    );
  }

  static HexColor _editModeFillColor() {
    return HexColor("#EEEEEE");
  }

  static OutlineInputBorder _getInputBoarder(
      bool isEditMode, AddRideInputType inputType) {
    //todo remove borders on edit mode unchangable fields
    /* if (isEditMode &&
        (inputType == AddRideInputType.location ||
            inputType == AddRideInputType.date ||
            inputType == AddRideInputType.price ||
            inputType == AddRideInputType.phoneNumber)) {
      print("_____shit real be yooo");
      return OutlineInputBorder(
          borderSide: BorderSide(width: 0, color: _editModeFillColor()));
    }*/

    return OutlineInputBorder(
        borderSide: BorderSide(
            width: 1,
            color: UIColor.fromARGB(
                red: 0.88, green: 0.87, blue: 0.85, alpha: 1)));
  }

  /// Hide labels for AddRideForm in EditMode
  static TextStyle _getLabelStyle(
      bool hasFocusOrContent, bool isEditMode, AddRideInputType inputType) {
    if (isEditMode &&
        (inputType == AddRideInputType.location ||
            inputType == AddRideInputType.date ||
            inputType == AddRideInputType.price ||
            inputType == AddRideInputType.phoneNumber)) {
      return TextStyle(fontSize: 0);
    }

    return TextStyle(
        fontSize: hasFocusOrContent ? 16 : 14,
        fontFamily: "InterRegular",
        fontWeight: FontWeight.w600);
  }

  ///
  static bool _determineIfInputIsFilled(
      bool isEditMode, AddRideInputType inputType) {
    // time, numPpl, carDesc, comment can be changed in edit mode, therefor input is not filled
    if (inputType == AddRideInputType.time ||
        inputType == AddRideInputType.numOfPpl ||
        inputType == AddRideInputType.carDescription ||
        inputType == AddRideInputType.comment) {
      return false;
    }

    if (isEditMode) {
      return true;
    }

    return false;
  }

  static ThemeData appThemeData(BuildContext context) {
    return ThemeData(
        primaryColor: HexColor("#ed6f00"),
        primaryColorDark: Color(0xFFE65100),
        accentColor: Color(0xFFFFB74D),
        dividerColor: HexColor("#D8D8D8"), // Color(0xFFE000000),
        fontFamily: 'Inter',
        //!text theme!
        //todo: figureout how to set text theme properly!
        primaryTextTheme:
            TextTheme(headline4: TextStyle(color: HexColor("#4D4D4D"))),
        textTheme: TextTheme(
            headline6: TextStyle(fontFamily: "Inter"),
            headline1: TextStyle(
                fontFamily: "Inter",
                fontSize: 25,
                color: Colors.white,
                fontWeight: FontWeight.bold),
            headline2: TextStyle(
                fontFamily: "Inter",
                fontSize: 30,
                color: HexColor("#4D4D4D"),
                fontWeight: FontWeight.bold))
        // textTheme: Theme.of(context).textTheme.copyWith(
        //     display4: TextStyle(
        //         fontSize: 25, color: Colors.white, fontWeight: FontWeight.bold),
        //     display3: TextStyle(
        //         fontSize: 30,
        //         color: HexColor("#4D4D4D"),
        //         fontWeight: FontWeight.bold))

        );
  }

  /// helps correctly resize textfield labels when they are
  /// animated from the inside of the textfield to its edge and vice versa
  static bool _fieldHasFocusOrContent(
      {FocusNode node, TextEditingController controller}) {
    if (node != null) {
      if (node.hasFocus || controller.text.length > 0) {
        return true;
      }
    }
    // this is the case in to/from location fields in AddRidePage and SearchFormCard
    if (node == null && controller.text != null && controller.text.length > 0) {
      return true;
    }

    return false;
  }

  static const TextStyle LABEL_TEXT =
      TextStyle(fontFamily: "InterNormal", color: Colors.grey);
  static const TextStyle BOLD_INFO_TEXT =
      TextStyle(fontFamily: "Inter", fontSize: 21);

  static const EdgeInsetsGeometry DESERT_SKY_PADDING =
      EdgeInsets.only(top: 25, bottom: 20, left: 20, right: 20);

  static const EdgeInsets CARDS_OUTTER_PADDING =
      EdgeInsets.only(left: 20, right: 20);

  static const EdgeInsets textFieldNormalPadding =
      EdgeInsets.symmetric(horizontal: 16.0, vertical: 14.0);

  static const TextStyle dropDownLabelStyle =
      TextStyle(color: Colors.black, fontSize: 16.0);

  static const TextStyle textFieldStyle = TextStyle(
      color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.bold);

  static const TextStyle textInAppBars =
      TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold);

  /// for "No rides found", "Error loading rides" scenarios
  static const TextStyle commonUserNoticeTextStyle =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  static const Color firstHeaderInputColor = Color(0xFFF47D15);
  static const Color secondHeaderInputColor = Color(0xFFEF772C);

  static const TextStyle srcHisCardCounterTxtStyle = TextStyle(
      fontSize: 23, fontWeight: FontWeight.w900, fontFamily: "InterRegular");

  static const TextStyle srcHisCardDayTxtStyle = TextStyle(
      fontSize: 14, fontFamily: "InterRegular", fontWeight: FontWeight.w500);
  // used to control and override DatePicker behaviour
  static const int smallerScreenHeight = 600;
}
