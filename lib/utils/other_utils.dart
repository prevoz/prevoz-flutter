import 'package:flutter/material.dart';

class OtherUtils {
  static String removeZerosFromPrice(double ridePrice) {
    if (ridePrice != null) {
      String price = ridePrice
          .toStringAsFixed(ridePrice.truncateToDouble() == ridePrice ? 0 : 1);

      return price.replaceAll(".", ',');
    }
    return null;
  }

  //? currently this only adds a divider/border where needed, later on it could also add radius and be
  //? aware of where to add a bottom border and where to add a top border (one before last list item)
  //* ISSUE WHEN USING BORDER AND BORDER RADIUS SIMOULTANEOUSLY
  //* you can only have either border or borderRadius at the same time. If both are placed you get a conflict.
  //* getCardsBoxDecoration provides a workaround by being aware of cards position in the list
  static BoxDecoration getCardsBoxDecoration(
      BuildContext context, bool isLastInList) {
    if (isLastInList) {
      //add both radiuses to card...
      return BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(4), bottomRight: Radius.circular(4)));
    }

    return BoxDecoration(
        color: Colors.white,
        border:
            Border(bottom: BorderSide(color: Theme.of(context).dividerColor)));
  }

  static String capitaliseFirstLetter(String text) {
    if (text.length == 1) {
      return text.toUpperCase();
    }

    return ('${text[0].toUpperCase()}${text.substring(1)}');
  }

  static Widget addFormSpacingBasedOnDeviceHeight(double deviceHeight) {
    double formSpacing = 10;
    if (deviceHeight > 680) {
      formSpacing = 25;
    }

    return SizedBox(
      height: formSpacing,
    );
  }
}
