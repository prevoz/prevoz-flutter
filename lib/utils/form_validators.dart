import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';

class FormValidators {
  static Logger logger = Logger(printer: CustomLogPrinter("FormValidators"));

  static String usernameValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("username")) {
      List<dynamic> errors = errorsFromApi["username"];
      return errors[0];
    }

    return null;
  }

  static String emailValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("email")) {
      List<dynamic> errors = errorsFromApi["email"];
      return errors[0];
    }

    return null;
  }

  static String passwordValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("password")) {
      List<dynamic> errors = errorsFromApi["password"];
      return errors[0];
    }

    return null;
  }

  static String numPplValidator(
      val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("num_people")) {
      List<dynamic> errors = errorsFromApi["num_people"];
      return errors[0];
    }

    return null;
  }

  static String dateAndTimeValidator(DateTime datetime,
      Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("date")) {
      List<dynamic> errors = errorsFromApi["date"];
      return errors[0];
    }

    if (errorsFromApi != null && errorsFromApi.containsKey("time")) {
      List<dynamic> errors = errorsFromApi["time"];
      return errors[0];
    }
    return null;
  }

  static String timeValidator(String val, Map<String, dynamic> errorsFromApi,
      BuildContext context, DateTime selectedDate) {
    if (errorsFromApi != null && errorsFromApi.containsKey("time")) {
      List<dynamic> errors = errorsFromApi["time"];
      return errors[0];
    }
    return null;
  }

  static String dateValidator(
      DateTime date, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("date")) {
      print("____ error from api contains date");
      List<dynamic> errors = errorsFromApi["date"];
      return errors[0];
    }
    return null;
  }

  static String priceValidator(
      val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (val == null || val.length == 0) {
      return null;
    }
    if (errorsFromApi != null && errorsFromApi.containsKey("__all__")) {
      List<dynamic> errors = errorsFromApi["__all__"];
      return errors[0];
    }

    return null;
  }

  static String fromLocationValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("from")) {
      List<dynamic> errors = errorsFromApi["from"];
      return errors[0];
    }

    if (errorsFromApi != null && errorsFromApi.containsKey("from_country")) {
      List<dynamic> errors = errorsFromApi["from_country"];
      return errors[0];
    }
    return null;
  }

  static String toLocationValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("to")) {
      List<dynamic> errors = errorsFromApi["to"];
      return errors[0];
    }

    if (errorsFromApi != null && errorsFromApi.containsKey("to_country")) {
      List<dynamic> errors = errorsFromApi["to_country"];
      return errors[0];
    }
    return null;
  }

  static String contactValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("contact")) {
      List<dynamic> errors = errorsFromApi["contact"];
      return errors[0];
    }

    return null;
  }

  static String firstNameValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("first_name")) {
      List<dynamic> errors = errorsFromApi["first_name"];
      return errors[0];
    }

    return null;
  }

  static String lastNameValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("last_name")) {
      List<dynamic> errors = errorsFromApi["last_name"];
      return errors[0];
    }

    return null;
  }

  static String addressValidator(
      String val, Map<String, dynamic> errorsFromApi, BuildContext context) {
    if (errorsFromApi != null && errorsFromApi.containsKey("address")) {
      List<dynamic> errors = errorsFromApi["address"];
      return errors[0];
    }

    return null;
  }
}
