import 'dart:ui';

/// used to transform RGB procents into numbers
class UIColor extends Color {
  UIColor.fromARGB({double red, double green, double blue, int alpha})
      : super.fromARGB(alpha, (red * 2.55).round(), (green * 2.55).round(),
            (blue * 2.55).round());
}
