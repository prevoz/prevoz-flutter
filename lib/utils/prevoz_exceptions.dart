class CustomException implements Exception {
  String cause;
  CustomException(this.cause);
}

class LocationsCountException implements Exception {
  String cause;
  LocationsCountException(this.cause);
}
