import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/helpers/auth/mock_auth_helper.dart';
import 'package:prevoz_org/data/blocs/simple_bloc_delegate.dart';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/helpers/database/mocks/android_migration_mock_database.dart';
import 'package:prevoz_org/data/helpers/database/mocks/mock_database_helper.dart';
import 'package:prevoz_org/data/helpers/database/mocks/slow_db_initialization_mock.dart';
import 'package:prevoz_org/data/helpers/database/prevoz_database.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';
import 'package:prevoz_org/data/network/mock_prevoz_api.dart';
import 'package:prevoz_org/data/network/prevoz_api.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/data/repositories/countries_repository.dart';
import 'package:prevoz_org/data/repositories/locations_repository.dart';
import 'package:prevoz_org/data/repositories/notification_subscription_repository.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/data/repositories/search_history_repository.dart';
import 'package:prevoz_org/data/repositories/user_profile_repository.dart';
import 'package:prevoz_org/mocks/mock_google_sign_in.dart';
import 'package:prevoz_org/mocks/mock_sign_in_with_apple_wrapper.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/utils/app_config.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/sign_in_with_apple_wrapper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'data/di/app_module.dart';
import 'data/di/mock_app_module.dart';
import 'data/helpers/auth/mock_auth_helper_with_user_profile.dart';

main(List<String> paramsFromTests) async {
  bool _isIntegrationTest() {
    if (paramsFromTests.length > 0 &&
        paramsFromTests[0] == "integration_test") {
      return true;
    }
    return false;
  }

  MockSignInWithAppleWrapper mockSignInWithAppleWrapper;
  MockGoogleSignIn mockGoogleSignIn;

  if (_isIntegrationTest()) {
    mockSignInWithAppleWrapper = MockSignInWithAppleWrapper();
    mockGoogleSignIn = MockGoogleSignIn();
    enableFlutterDriverExtension(handler: mockGoogleSignIn.handleCommand);
  }

  MyApp myApp;
  BasePrevozApi prevozApi =
      PrevozApi(); //! replace MockPrevozApi with PrevozApi
  BaseDatabaseHelper databaseHelper = PrevozDatabase();
  Injector injector;

  Future<void> _integrationTestsSetup() async {
    WidgetsApp.debugAllowBannerOverride =
        false; // hides debug bannger for screenshots
    prevozApi = MockPrevozApi();
    databaseHelper = MockDatabaseHelper();
    BaseAuthHelper authHelper = MockAuthHelper();

    //* Integration tests for migrating old android DB
    if (paramsFromTests.length == 2) {
      if (paramsFromTests[1] == "android_to_flutter_1") {
        databaseHelper = AndroidMigrationMockDatabase.assetDbName(
            "dataprovider_3_searchhistory_1_notification");
      }
      if (paramsFromTests[1] == "android_to_flutter_2") {
        databaseHelper = AndroidMigrationMockDatabase.assetDbName(
            "dataprovider_5_searchhistory_2_notifications");
      }
      if (paramsFromTests[1] == "android_to_flutter_3") {
        databaseHelper = AndroidMigrationMockDatabase.assetDbName(
            "dataprovider_9_searchhistory_3_notifications");
      }

      if (paramsFromTests[1] == "android_to_flutter_4") {
        databaseHelper = AndroidMigrationMockDatabase.assetDbName(
            "dataprovider_5_searchhistory_2_empty_ones");
      }

      if (paramsFromTests[1] == "slow_db_init") {
        databaseHelper = SlowDatabaseInitializationMock();
      }

      if (paramsFromTests[1] == "refresh_user_profile_on_startup") {
        authHelper = MockAuthHelperWithUserProfile();
      }
    }
    injector = await MockAppModule().initialise(
        Injector.getInjector(),
        authHelper,
        mockSignInWithAppleWrapper,
        mockGoogleSignIn
    );
    WidgetsFlutterBinding.ensureInitialized();
  }

  Future<void> _normalSetup() async {
    WidgetsFlutterBinding.ensureInitialized();
    prevozApi = PrevozApi();
    databaseHelper = PrevozDatabase();
    injector = await AppModule().initialise(Injector.getInjector());
  }

  //* Integration tests, use mocks
  if (_isIntegrationTest()) {
    await _integrationTestsSetup();
  } else {
    await _normalSetup();
  }

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarIconBrightness: Brightness.dark, //top bar icons
  ));

  myApp = MyApp(
    databaseHelper,
    AuthRepository(
        api: prevozApi,
        baseAuthHelper: injector.get<BaseAuthHelper>(),
        signInWithAppleWrapper: injector.get<SignInWithAppleWrapper>(),
        googleSignIn: injector.get<GoogleSignIn>(),
        sharedPreferences: injector.get<SharedPreferences>(),
        logger: Logger(printer: CustomLogPrinter("AuthRepo"))
    ),
    RidesRepository(prevozApi),
    LocationsRepository(databaseHelper),
    CountriesRepository(databaseHelper),
    SearchHistoryRepository(databaseHelper),
    NotificationSubscriptionRepository(databaseHelper, prevozApi),
    UserProfileRepository(prevozApi)
  );
  //* regular build

  AppConfig appConfig = new AppConfig(
    pushEnabled: Platform.isAndroid ? true : false,
    child: myApp,
    appDatabase: databaseHelper,
    debugMode: false,
  );

  Bloc.observer = SimpleBlocObserver(appConfig);

  runApp(appConfig);
}
