import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:prevoz_org/locale/l10n/messages_all.dart';

///Taken from: https://github.com/tensor-programming/Weather_Tutorial/blob/master/lib/localization/localizations.dart

class AppLocalizations {
  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get appTitle {
    return Intl.message(
      'Prevoz',
      name: 'appTitle',
      desc: '',
    );
  }

  //errorLoadingRidesTryAgain

  String get whereAreYouHeadingToday {
    return Intl.message(
      'Kam greš danes?',
      name: 'whereAreYouHeadingToday',
      desc: '',
    );
  }

  String get searchHistoryNotLoaded {
    return Intl.message(
      'Napaka pri nalaganju iskalne zgodovine',
      name: 'searchHistoryNotLoaded',
      desc: '',
    );
  }

  String get search {
    return Intl.message(
      'Išči',
      name: 'search',
      desc: '',
    );
  }

  String get noRidesFound {
    return Intl.message(
      'Ta dan ni prevozov na izbrani relaciji',
      name: 'noRidesFound',
      desc: '',
    );
  }

  String get noRidesAdded {
    return Intl.message(
      'Ne ponujaš nobenega prevoza.',
      name: 'noRidesAdded',
      desc: '',
    );
  }

  String get noBookmarkedRides {
    return Intl.message(
      'Nimaš shranjenih prevozov.',
      name: 'noBookmarkedRides',
      desc: '',
    );
  }

  String get noSubscriptions {
    return Intl.message(
      'Nisi naročen na obveščanja o novih prevozih.',
      name: 'noSubscriptions',
      desc: '',
    );
  }

  String get slovenia {
    return Intl.message(
      'Slovenija',
      name: 'slovenia',
      desc: '',
    );
  }

  String get international {
    return Intl.message(
      'Mednarodno',
      name: 'international',
      desc: '',
    );
  }

  String get noLocationsFound {
    return Intl.message(
      'Brez najdenih lokacij',
      name: 'noLocationsFound',
      desc: '',
    );
  }

  String get erroLoadingLocations {
    return Intl.message(
      'Napaka pri nalaganju lokacij',
      name: 'erroLoadingLocations',
      desc: '',
    );
  }

  String get searchError {
    return Intl.message(
      'Med iskanjem je prišlo do napake. Poskusi ponovno.',
      name: 'searchError',
      desc: '',
    );
  }

  //String get rideSuccessfullyAdded => null;
  String get rideSuccessfullyAdded {
    return Intl.message(
      'Prevoz uspešno dodan.',
      name: 'rideSuccessfullyAdded',
      desc: '',
    );
  }

  String get rideSuccessfullyUpdated {
    return Intl.message(
      'Prevoz uspešno urejen.',
      name: 'rideSuccessfullyUpdated',
      desc: '',
    );
  }

  String get rideSuccessfullyDeleted {
    return Intl.message(
      'Prevoz uspešno izbrisan.',
      name: 'rideSuccessfullyDeleted',
      desc: '',
    );
  }

  //
  //

  String get bookmarksSearchError {
    return Intl.message(
      'Napaka pri nalaganju zaznamkov.',
      name: 'bookmarksSearchError',
      desc: '',
    );
  }

  String get bookmark {
    return Intl.message(
      'Zaznamek',
      name: 'bookmark',
      desc: '',
    );
  }

  String get searchResultsPageTitle {
    return Intl.message(
      'Rezultati iskanja',
      name: 'searchResultsPageTitle',
    );
  }

  String get searchPageTitle {
    return Intl.message(
      'Iskanje',
      name: 'searchPageTitle',
    );
  }

  String get ridePageTitle {
    return Intl.message(
      'Podrobnosti prevoza',
      name: 'ridePageTitle',
    );
  }

  //String get business => null;

  String get business {
    return Intl.message(
      'Podjetje',
      name: 'business',
    );
  }

  String get userScore {
    return Intl.message(
      'Ocena',
      name: 'userScore',
    );
  }

  String get drajvScore {
    return Intl.message(
      'DRAJV ocena',
      name: 'drajvScore',
    );
  }

  String get noData {
    return Intl.message(
      'Ni podatka',
      name: 'noData',
    );
  }

  String get noUserScore {
    return Intl.message(
      'Ni ocen',
      name: 'noUserScore',
    );
  }

  //String userScore;

  String get counterText {
    return Intl.message(
      'Povečaj število za 1',
      name: 'counterText',
    );
  }

  String get btnSearchRide {
    return Intl.message(
      'Najdi prevoz',
      name: 'btnSearchRide',
    );
  }

  String get from {
    return Intl.message(
      'Od',
      name: 'from',
    );
  }

  String get to {
    return Intl.message(
      'Do',
      name: 'to',
    );
  }

  String get allPlaces {
    return Intl.message(
      'Vsi kraji',
      name: 'allPlaces',
    );
  }

  //from_all_places
  String get departureLocation {
    return Intl.message(
      'Od',
      name: 'departureLocation',
    );
  }

  String get arrivalLocation {
    return Intl.message(
      'Do',
      name: 'arrivalLocation',
    );
  }

  String get formErrEnterDepartureLocation {
    return Intl.message(
      'Vnesi kraj odhoda',
      name: 'formErrEnterDepartureLocation',
    );
  }

  String get formErrEnterArrivalLocation {
    return Intl.message(
      'Vnesi kraj prihoda',
      name: 'formErrEnterArrivalLocation',
    );
  }

  String get formErrFutreDate {
    return Intl.message(
      'Vnešen datum je že pretekel',
      name: 'formErrFutreDate',
    );
  }

  String get formErrDateTypeError {
    return Intl.message(
      'Napačna oblika datuma',
      name: 'formErrDateTypeError',
    );
  }

  //formErrEnterPhoneNumber
  String get formErrEnterPhoneNumber {
    return Intl.message(
      'Vnesi telefonsko številko',
      name: 'formErrEnterPhoneNumber',
    );
  }

  String get formErrPhoneNumErr {
    return Intl.message(
      'Napačna oblika telefonske številke',
      name: 'formErrPhoneNumErr',
    );
  }

  String get formErrDateTimeIsEmpty {
    return Intl.message(
      'Vnesi datum in čas odhoda',
      name: 'formErrDateTimeIsEmpty',
    );
  }

  String get formErrTimeIsEmpty {
    return Intl.message(
      'Vnesi čas odhoda',
      name: 'formErrTimeIsEmpty',
    );
  }

  String get timeAndDateAlreadyPast {
    return Intl.message(
      'Datum in čas prevoza je že minil.',
      name: 'timeAndDateAlreadyPast',
    );
  }

  String get formErrDateIsEmpty {
    return Intl.message(
      'Vnesi datum odhoda',
      name: 'formErrDateIsEmpty',
    );
  }

  String get formErrNumPplError {
    return Intl.message(
      'Napačen vnos za število ljudi (1 do 6)',
      name: 'formErrNumPplError',
    );
  }

  String get formErrNumPplMissing {
    return Intl.message(
      'Vnesi število prostih mest',
      name: 'formErrNumPplMissing',
    );
  }

  String get formErrInvalidPrice {
    return Intl.message(
      'Napačen vnos za ceno',
      name: 'formErrInvalidPrice',
    );
  }

  String get chooseStartingLocation {
    return Intl.message(
      'Izberi kraj odhoda',
      name: 'chooseStartingLocation',
    );
  }

  String get chooseFinalLocation {
    return Intl.message(
      'Izberi kraj prihoda',
      name: 'chooseFinalLocation',
    );
  }

  String get datum {
    return Intl.message(
      'Datum',
      name: 'datum',
    );
  }

  String get hour {
    return Intl.message(
      'Ura',
      name: 'hour',
    );
  }

  String get january {
    return Intl.message(
      'januar',
      name: 'january',
    );
  }

  String get february {
    return Intl.message(
      'februar',
      name: 'february',
    );
  }

  String get march {
    return Intl.message(
      'marec',
      name: 'march',
    );
  }

  String get april {
    return Intl.message(
      'april',
      name: 'april',
    );
  }

  String get may {
    return Intl.message(
      'maj',
      name: 'may',
    );
  }

  String get june {
    return Intl.message(
      'junij',
      name: 'june',
    );
  }

  String get july {
    return Intl.message(
      'julij',
      name: 'july',
    );
  }

  String get august {
    return Intl.message(
      'avgust',
      name: 'august',
    );
  }

  String get september {
    return Intl.message(
      'september',
      name: 'september',
    );
  }

  String get october {
    return Intl.message(
      'oktober',
      name: 'october',
    );
  }

  String get november {
    return Intl.message(
      'november',
      name: 'november',
    );
  }

  String get december {
    return Intl.message(
      'december',
      name: 'december',
    );
  }

  String get monday {
    return Intl.message(
      'Ponedeljek',
      name: 'monday',
    );
  }

  String get tuesday {
    return Intl.message(
      'Torek',
      name: 'tuesday',
    );
  }

  String get wednessday {
    return Intl.message(
      'Sreda',
      name: 'wednessday',
    );
  }

  String get thursday {
    return Intl.message(
      'Četrtek',
      name: 'thursday',
    );
  }

  String get friday {
    return Intl.message(
      'Petek',
      name: 'friday',
    );
  }

  String get saturday {
    return Intl.message(
      'Sobota',
      name: 'saturday',
    );
  }

  //String get chooseTime => null;

  String get sunday {
    return Intl.message(
      'Nedelja',
      name: 'sunday',
    );
  }

  String get confirmedContact {
    return Intl.message(
      'Potrjena številka',
      name: 'confirmedContact',
    );
  }

  String get unconfirmedContact {
    return Intl.message(
      'številka ni potrjena',
      name: 'unconfirmedContact',
    );
  }

  String get signOutDialogTitle {
    return Intl.message(
      'Se res želiš odjaviti?',
      name: 'signOutDialogTitle',
    );
  }

  //String get removeSubscriptionDialogContent => null;
  String get removeSubscriptionDialogContent {
    return Intl.message(
      'S tem boš nehal prejemati obvestila za izbrano relacijo.',
      name: 'removeSubscriptionDialogContent',
    );
  }

  // get notifications => null;
  String get notifications {
    return Intl.message(
      'Obvestila',
      name: 'notifications',
    );
  }

  String get signInDialogTitle {
    return Intl.message(
      'Za shranjevanje prevozov je potrebna prijava.',
      name: 'signInDialogTitle',
    );
  }

  String get signOutDialogContent {
    return Intl.message(
      'Potrdi svojo izbiro s klikom na enega od gumbov spodaj.',
      name: 'signOutDialogContent',
    );
  }

  String get signInDialogContent {
    return Intl.message(
      'Preusmerjen boš na obrazec za prijavo na spletni strani.',
      name: 'signInDialogContent',
    );
  }

  String get myRidesLoginText {
    return Intl.message(
      'Za pregled objavljenih in shranjenih prevozov je potrebna prijava.',
      name: 'myRidesLoginText',
    );
  }

  String get login {
    return Intl.message(
      'Prijavi se',
      name: 'login',
    );
  }

  String get youAreCurrentlyNotLoggedIn {
    return Intl.message(
      "Trenutno nisi prijavljen",
      name: 'youAreCurrentlyNotLoggedIn',
    );
  }

  String get signInWithGoogleButton {
    return Intl.message(
        "Prijava z Google računom",
        name: 'signInWithGoogleButton'
    );
  }

  String get signInWithAppleButton {
    return Intl.message(
        "Prijava z Apple računom",
        name: 'signInWithAppleButton'
    );
  }

  String get loginWithUsernameAndPasswordButton {
    return Intl.message(
      "Prijava z uporabniškim imenom",
      name: 'loginWithUsernameAndPasswordButton'
    );
  }

  String get loginPageUsernameTitle {
    return Intl.message(
        "Uporabniško ime",
        name: 'loginPageUsernameTitle'
    );
  }

  String get loginPagePasswordTitle {
    return Intl.message(
        "Geslo",
        name: 'loginPagePasswordTitle'
    );
  }

  String get loginPageLoginButton {
    return Intl.message(
        "Prijava",
        name: 'loginPageLoginButton'
    );
  }

  String get loginPageGmailError {
    return Intl.message(
      "Za gmail uporabnike je prijava mogoče le s spodnjim gumbom.",
      name: 'loginPageGmailError'
    );
  }

  String get loginPageGoToPasswordResetPage {
    return Intl.message(
        "Pozabljeno geslo?",
        name: 'loginPageGoToPasswordResetPage'
    );
  }

  String get loginPageGoToRegistrationPage {
    return Intl.message(
      "Ustvari nov račun",
      name: 'loginPageGoToRegistrationPage'
    );
  }

  String get registrationPageTitle {
    return Intl.message(
        "Registracija",
        name: 'registrationPageTitle'
    );
  }

  String get registrationPageUsernameTitle {
    return Intl.message(
        "Uporabniško ime",
        name: 'registrationPageUsernameTitle'
    );
  }

  String get registrationPageEmailTitle {
    return Intl.message(
        "E-mail",
        name: 'registrationPageEmailTitle'
    );
  }

  String get registrationPagePasswordTitle {
    return Intl.message(
        "Geslo",
        name: 'registrationPagePasswordTitle'
    );
  }

  String get registrationPageRegisterButton {
    return Intl.message(
        "Registriraj se",
        name: 'loginPageLoginButton'
    );
  }

  String get registrationPageCheckEmail {
    return Intl.message(
      "Uspešno ste se registrirali. Na vnešeni naslov boste prejeli e-mail. Sledite povezavi v e-mailu za dokončanje procesa registracije.",
      name: 'registrationPageCheckEmail'
    );
  }

  String get registrationPageGoToLoginPage {
    return Intl.message(
        "Že imam račun",
        name: 'registrationPageGoToLoginPage'
    );
  }

  String get forgotPasswordPageTitle {
    return Intl.message(
        "Pozabljeno geslo",
        name: 'forgotPasswordPageTitle'
    );
  }

  String get forgotPasswordPageInfo {
    return Intl.message(
        "Če ste pozabili geslo, vnesi svoj elektronski naslov. Poslali ti bomo elektronsko sporočilo s povezavo za resetiranje gesla.",
        name: 'forgotPasswordPageInfo'
    );
  }

  String get forgotPasswordPageEmailTitle {
    return Intl.message(
        "E-mail",
        name: 'forgotPasswordPageEmailTitle'
    );
  }

  String get forgotPasswordPageSubmitButton {
    return Intl.message(
        "Pošlji",
        name: 'forgotPasswordPageSubmitButton'
    );
  }

  String get passwordResetPageTitle {
    return Intl.message(
        "Ponastavi geslo",
        name: 'passwordResetPageTitle'
    );
  }

  String get passwordResetPageInfo {
      return Intl.message(
          "Vnesite vaše novo geslo dvakrat, da se izognete tipkarskim napakam.",
          name: 'passwordResetPageInfo'
      );
  }

  String get passwordResetPagePasswordTitle {
    return Intl.message(
        "Novo geslo",
        name: 'passwordResetPagePasswordTitle'
    );
  }

  String get passwordResetPagePasswordCheckTitle {
    return Intl.message(
        "Potrditev gesla",
        name: 'passwordResetPagePasswordCheckTitle'
    );
  }

  String get passwordResetPageSubmitButton {
    return Intl.message(
        "Spremeni moje geslo",
        name: 'passwordResetPageSubmitButton'
    );
  }

  String get deleteRideDialogTitle {
    return Intl.message(
      'Si res želiš izbrisati prevoz?',
      name: 'deleteRideDialogTitle',
    );
  }

  String get errorLoadingSearchHistory {
    return Intl.message(
      'Napaka pri nalaganju iskalne zgodovine',
      name: 'deleteRideDialogTitle',
    );
  }

  String get rideCompensation {
    return Intl.message(
      'Stroški',
      name: 'rideCompensation',
    );
  }

  String get numOfPeople {
    return Intl.message(
      'Prosta mesta',
      name: 'numOfPeople',
    );
  }

  //numOfPeopleShortened
  String get numOfPeopleShortened {
    return Intl.message(
      'Prosta mesta',
      name: 'numOfPeopleShortened',
    );
  }

  String get cancel {
    return Intl.message(
      'Prekliči',
      name: 'cancel',
    );
  }

  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
    );
  }

  String get signout {
    return Intl.message(
      'Odjavi',
      name: 'signout',
    );
  }

  String get signin {
    return Intl.message(
      'Prijavi se',
      name: 'signin',
    );
  }

  String get insured {
    return Intl.message(
      'Ima zavarovanje',
      name: 'insured',
    );
  }

  String get insurance {
    return Intl.message(
      'Zavarovanje',
      name: 'insurance',
    );
  }

  //notInsured
  String get notInsured {
    return Intl.message(
      'Nima zavarovanja',
      name: 'notInsured',
    );
  }

  String get added {
    return Intl.message(
      'Ponujeni prevozi',
      name: 'added',
    );
  }

  String get myQuickEntries {
    return Intl.message(
      "Moji hitri vnosi",
      name: 'myQuickEntries',
    );
  }

  String get at {
    return Intl.message(
      'ob',
      name: 'at',
    );
  }

  String get noRidesForRoute {
    return Intl.message(
      "Za izbrano relacijo ni prevozov",
      name: 'noRidesForRoute',
    );
  }

  String get uncorrectDateSelected {
    return Intl.message(
      'Nepravilen datum! Možna izbira samo od danes naprej.',
      name: 'uncorrectDateSelected',
    );
  }

  String get chooseAgain {
    return Intl.message(
      'Izberi ponovno',
      name: 'chooseAgain',
    );
  }

  //String get removeSubscriptionDialogTitle => null;
  String get removeSubscriptionDialogTitle {
    return Intl.message(
      'Odstrani obvestilo?',
      name: 'removeSubscriptionDialogTitle',
    );
  }

  String get stopSubscribing {
    return Intl.message(
      "Prenehaj z obveščanjem",
      name: 'stopSubscribing',
    );
  }

  //"Obveščaj me o novih prevozih"
  String get startSubscribing {
    return Intl.message(
      "Obveščaj me o novih prevozih",
      name: 'startSubscribing',
    );
  }

  //"Napaka pri shranjevanju zaznamka"
  String get errorSavingBookmark {
    return Intl.message(
      "Napaka pri shranjevanju zaznamka",
      name: 'errorSavingBookmark',
    );
  }

  // String get notConnectedToInternet => null;
  String get notConnectedToInternet {
    return Intl.message(
      'Ni internetne povezave. Poveži se na internet.',
      name: 'notConnectedToInternet',
    );
  }

  //selectLocationsWarning

  String get selectLocationsWarning {
    return Intl.message(
      'Izberi začetno in končno destinacijo poti.',
      name: 'selectLocationsWarning',
    );
  }

  String get noResultsForThisQuery {
    return Intl.message(
      'Ni rezultatov za ta iskalni niz',
      name: 'noResultsForThisQuery',
    );
  }

  //String get noResultsForThisQuery => null;

  //get numOfPeopleNotSpecified => null;

  String get priceNotSpecified {
    return Intl.message(
      'Strošek ni določen.',
      name: 'priceNotSpecified',
    );
  }

  String get errorSelectingDate {
    return Intl.message(
      "Napaka pri izbiri datuma.",
      name: 'errorSelectingDate',
    );
  }

  String get authErrorLoggingIn {
    return Intl.message(
      "Napaka pri prijavi.",
      name: 'authErrorLoggingIn',
    );
  }
  
  String get authErrorActivatingAccount {
    return Intl.message(
      "Napaka pri aktivaciji računa.",
      name: 'authErrorActivatingAccount',
    );
  }

  String get authErrorInvalidToken {
    return Intl.message(
      "Neveljaven žeton. Potrebna je ponovna prijava",
      name: 'authErrorInvalidToken'
    );
  }

  String get price {
    return Intl.message(
      'Strošek',
      name: 'price',
    );
  }

  String get phoneNumber {
    return Intl.message(
      'Telefonska številka',
      name: 'phoneNumber',
    );
  }

  //passangerInsurance
  String get passangerInsurance {
    return Intl.message(
      'Nezgodno zavarovanje potnikov.',
      name: 'passangerInsurance',
    );
  }

  //  get phoneNumber => null;

  String get numOfPeopleNotSpecified {
    return Intl.message(
      'Število prostih mest ni določeno.',
      name: 'numOfPeopleNotSpecified',
    );
  }

  String get noResultsAppBarTitle {
    return Intl.message(
      'Ni Rezultatov',
      name: 'noResultsAppBarTitle',
    );
  }

  String get bookmarks {
    return Intl.message(
      'Shranjeni prevozi',
      name: 'bookmarks',
    );
  }

  //get noResultsAppBarTitle => null;

  String get numberOfFreePlacesSingular {
    return Intl.message(
      'prosto mesto',
      name: 'numberOfFreePlacesSingular',
    );
  }

  //get errorLoadingQuickRides => null;
  String get errorLoadingQuickRides {
    return Intl.message(
      'Napaka pri nalaganju hitrih prevozov.',
      name: 'errorLoadingQuickRides',
    );
  }

  String get freePlaces {
    return Intl.message(
      'Prosta mesta',
      name: 'freePlaces',
    );
  }

  String get enterASearchQuery {
    return Intl.message(
      'Vnesi iskalni niz',
      name: 'enterASearchQuery',
      desc: '',
    );
  }

  String get quickRidesNewLine {
    return Intl.message(
      'Hitri \nvnosi',
      name: 'quickRidesNewLine',
      desc: '',
    );
  }

  String get quickRides {
    return Intl.message(
      'Hitri vnosi',
      name: 'quickRides',
      desc: '',
    );
  }

  String get myRides {
    return Intl.message(
      'Moji prevozi',
      name: 'myRides',
      desc: '',
    );
  }

  //get myRides => null;

  String get numberOfFreePlacesTwo {
    return Intl.message(
      'prosti mesti',
      name: 'numberOfFreePlacesTwo',
    );
  }

  String get numberOfFreePlacesThreeAndFour {
    return Intl.message(
      'prosta mesta',
      name: 'numberOfFreePlacesThreeAndFour',
    );
  }

  String get addNewRideTitle {
    return Intl.message(
      'Dodaj prevoz',
      name: 'addNewRideTitle',
    );
  }

  String get editUserData {
    return Intl.message(
      'Uredi podatke',
      name: 'editUserData',
    );
  }

  String get errorPostingDataToServer {
    return Intl.message(
        'Napaka pri komunikaciji s strežnikom',
        name: 'errorPostingDataToServer'
    );
  }

  String get errorFetchingDataFromServer {
    return Intl.message(
        'Napaka pri pridobivanju podatkov s strežnika',
        name: 'errorFetchingDataFromServer'
    );
  }

  String get errorFetchingProfileDataFromServer {
    return Intl.message(
      'Napaka pri pridobivanju podatkov o profilu s strežnika',
      name: 'errorFetchingProfileDataFromServer'
    );
  }

  String get profileEntryFirstAndLastName {
    return Intl.message(
      'Ime in Priimek',
      name: 'profileEntryFirstAndLastName'
    );
  }

  String get profileEntryFirstAndLastNameEmpty {
    return Intl.message(
        'Dodaj ime in priimek',
        name: 'profileEntryFirstAndLastNameEmpty'
    );
  }

  String get profileEntryAddress {
    return Intl.message(
      'Naslov',
      name: 'profileEntryAddress'
    );
  }

  String get profileEntryAddressEmpty {
    return Intl.message(
        'Dodaj naslov',
        name: 'profileEntryAddressEmpty'
    );
  }

  String get profileEntryEmail {
    return Intl.message(
        'E-mail',
        name: 'profileEntryEmail'
    );
  }

  String get profileEntryEmailEmpty {
    return Intl.message(
        'Dodaj E-mail',
        name: 'profileEntryEmailEmpty'
    );
  }

  String get profileEntryPhone {
    return Intl.message(
        'Telefonska številka',
        name: 'profileEntryPhone'
    );
  }

  String get profileEntryPhoneEmpty {
    return Intl.message(
        'Dodaj številko',
        name: 'profileEntryPhoneEmpty'
    );
  }

  String get logMeOut {
    return Intl.message(
      'Odjavi me',
      name: 'logMeOut',
    );
  }

  String get editProfilePageDefaultSaveButtonText {
    return Intl.message(
      'Shrani podatke',
      name: 'editProfilePageDefaultSaveButtonText'
    );
  }

  String get editProfilePageDefaultSuccessMessage {
    return Intl.message(
      'Profil uspešno posodobljen',
      name: 'editProfilePageDefaultSuccessMessage'
    );
  }

  String get editProfilePageFirstAndLastNameTitle {
    return Intl.message(
        'Uredi uporabniške podatke',
        name: 'editProfilePageFirstAndLastNameTitle'
    );
  }

  String get editProfilePageFirstAndLastEntryTitleName {
    return Intl.message(
        'Ime',
        name: 'editProfilePageFirstAndLastEntryTitleName'
    );
  }

  String get editProfilePageFirstAndLastEntryTitleLastName {
    return Intl.message(
        'Priimek',
        name: 'editProfilePageFirstAndLastEntryTitleLastName'
    );
  }

  String get editProfilePageAddressTitle {
    return Intl.message(
        'Uredi uporabniške podatke',
        name: 'editProfilePageAddressTitle'
    );
  }

  String get editProfilePageAddressEntryTitle {
    return Intl.message(
      'Naslov',
      name: 'editProfilePageAddressEntryTitle'
    );
  }

  String get editProfilePageAddressEntryHint {
    return Intl.message(
      'Ulica in hišna št., pošta in kraj',
      name: 'editProfilePageAddressEntryHint'
    );
  }

  String get confirm {
    return Intl.message(
      'Potrdi',
      name: 'confirm',
    );
  }

  String get publishRide {
    return Intl.message(
      'Objavi prevoz',
      name: 'publishRide',
    );
  }

  String get editRideTitle {
    return Intl.message(
      'Uredi prevoz',
      name: 'editRideTitle',
    );
  }

  String get addNewRideBtn {
    return Intl.message(
      'Oddaj',
      name: 'addNewRideBtn',
    );
  }

  String get needsLogin {
    return Intl.message(
      'Za ogled in dodajanje zaznamkov ter prevozov moraš biti prijavljen.',
      name: 'needsLogin',
    );
  }

  String get addRide {
    return Intl.message(
      'Dodaj prevoz',
      name: 'addRide',
    );
  }

  String get rideIsFull {
    return Intl.message(
      'Prevoz je poln',
      name: 'rideIsFull',
    );
  }

  String get comments {
    return Intl.message(
      'Opombe',
      name: 'comments',
    );
  }

  String get saveRideBtn {
    return Intl.message(
      'Shrani spremembe',
      name: 'saveRideBtn',
    );
  }

  String get deleteRideBtn {
    return Intl.message(
      'Izbriši',
      name: 'deleteRideBtn',
    );
  }

  String get numberOfFreePlacesFiveAndMore {
    return Intl.message(
      'prostih mest',
      name: 'numberOfFreePlacesFiveAndMore',
    );
  }

  //get profile => null;

  String get profile {
    return Intl.message(
      'Profil',
      name: 'profile',
    );
  }

  String get firstName {
    return Intl.message(
      'Ime',
      name: 'firstName'
    );
  }

  String get lastName {
    return Intl.message(
        'Priimek',
        name: 'lastName'
    );
  }

  String get address {
    return Intl.message(
        'Naslov (Ulica in hišna št., pošta in kraj)',
        name: 'address'
    );
  }

  String get ridesNotLoaded {
    return Intl.message(
      'Napaka pri nalaganju prevozov',
      name: 'ridesNotLoaded',
    );
  }

  String get errorSigningIn {
    return Intl.message(
      "Napaka pri vpisu",
      name: 'errorSigningIn',
    );
  }

  String get unexpectedErrorWhileSigningIn {
    return Intl.message(
      "Nepričakovana napaka med prijavo. Poskusi ponovno.",
      name: 'unexpectedErrorWhileSigningIn',
    );
  }

  //unexpectedErrorWhileAddingRide
  String get unexpectedErrorWhileAddingRide {
    return Intl.message(
      "Nepričakovana napaka med dodajanjem prevoza.",
      name: 'unexpectedErrorWhileAddingRide',
    );
  }

  String get car {
    return Intl.message(
      'Avto',
      name: 'car',
    );
  }

  String get sendSms {
    return Intl.message(
      'Pošlji SMS',
      name: 'sendSms',
    );
  }

  String get pickMidway {
    return Intl.message(
      'Z veseljem ustavim na večjem kraju na poti do cilja po dogovoru z iskalci.',
      name: 'pickMidway',
    );
  }

  String get picksMidway {
    return Intl.message(
      'Pobira po poti',
      name: 'picksMidway',
    );
  }

  //String get pickMidway => null;

  String get errorLoadingRidesTryAgain {
    return Intl.message(
      'Napaka pri nalaganju prevozov.',
      name: 'errorLoadingRidesTryAgain',
    );
  }

  String get driver {
    return Intl.message(
      'Voznik/ca',
      name: 'driver',
    );
  }

  String get errorLoadingAddedRides {
    return Intl.message(
      'Napaka pri nalaganju ponujenih prevozov.',
      name: 'errorLoadingAddedRides',
    );
  }

  // get remove => null;
  String get remove {
    return Intl.message(
      'Odstrani',
      name: 'remove',
    );
  }

  String get tryAgain {
    return Intl.message(
      'Poizkusi ponovno',
      name: 'tryAgain',
    );
  }

  String get deleteRide {
    return Intl.message(
      'Izbriši prevoz',
      name: 'deleteRide',
    );
  }

  String get unexpectedError {
    return Intl.message(
      'Prišlo je do nepričakovane napake.',
      name: 'unexpectedError',
    );
  }

  String get errorLoadingCountries {
    return Intl.message(
      'Napaka pri nalaganju držav',
      name: 'errorLoadingCountries',
    );
  }

  String get call {
    return Intl.message(
      'Pokliči',
      name: 'call',
    );
  }

  String get validationFieldIsMandatory {
    return Intl.message(
      'To polje je obvezno.',
      name: 'validationFieldIsMandatory'
    );
  }

  String get labourLaw {
    return Intl.message(
        'Za zagotavljanje skladnosti z zakonodajo je potrebno za oddajo prevoza vnesti osebne podatke.\nNa spletni strani bo javno objavljeno samo ime.',
        name: 'labourLaw'
    );
  }

  String get navigationLabelSearch {
    return Intl.message(
        'Iskanje',
        name: 'navigationLabelSearch'
    );
  }

  String get navigationLabelMyRides {
    return Intl.message(
        'Moji prevozi',
        name: 'navigationLabelMyRides'
    );
  }

  String get navigationLabelAddRide {
    return Intl.message(
        'Dodaj',
        name: 'navigationLabelAddRide'
    );
  }

  String get navigationLabelNotifications {
    return Intl.message(
        'Obvestila',
        name: 'navigationLabelNotifications'
    );
  }

  String get navigationLabelMyProfile {
    return Intl.message(
        'Profil',
        name: 'navigationLabelMyProfile'
    );
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['sl'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) {
    return false;
  }
}
