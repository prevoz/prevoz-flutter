// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "app_title" : MessageLookupByLibrary.simpleMessage("Prevoz"),
    "counter_text" : MessageLookupByLibrary.simpleMessage("Povečajte število za 1"),
    "ride_page_title" : MessageLookupByLibrary.simpleMessage("Podrobnosti prevoza"),
    "search_page_title" : MessageLookupByLibrary.simpleMessage("Iskanje"),
    "search_results_page_title" : MessageLookupByLibrary.simpleMessage("Rezultati iskanja")
  };
}
