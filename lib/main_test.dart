
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logger/logger.dart';
import 'package:prevoz_org/data/blocs/simple_bloc_delegate.dart';
import 'package:prevoz_org/data/repositories/user_profile_repository.dart';
import 'package:prevoz_org/mocks/mock_google_sign_in.dart';
import 'package:prevoz_org/mocks/mock_helper.dart';
import 'package:prevoz_org/mocks/mock_sign_in_with_apple_wrapper.dart';
import 'package:prevoz_org/my_app.dart';
import 'package:prevoz_org/utils/app_config.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/data/repositories/rides_repository.dart';
import 'package:prevoz_org/data/repositories/countries_repository.dart';
import 'package:prevoz_org/data/repositories/locations_repository.dart';
import 'package:prevoz_org/data/helpers/database/base_database_helper.dart';
import 'package:prevoz_org/data/repositories/search_history_repository.dart';
import 'package:prevoz_org/data/helpers/database/mocks/mock_database_helper.dart';
import 'package:prevoz_org/data/repositories/notification_subscription_repository.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';
import 'package:prevoz_org/utils/sign_in_with_apple_wrapper.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'data/di/mock_app_module.dart';
import 'data/helpers/auth/base_auth_helper.dart';
import 'mocks/mock_auth_helper.dart';
import 'mocks/mock_prevoz_api.dart';

main() async {

  MockPrevozApi prevozApi = MockPrevozApi();
  MockAuthHelper mockAuthHelper = MockAuthHelper();
  BaseDatabaseHelper databaseHelper = MockDatabaseHelper();
  MockSignInWithAppleWrapper mockSignInWithAppleWrapper = MockSignInWithAppleWrapper();
  MockGoogleSignIn mockGoogleSignIn = MockGoogleSignIn();

  MockHelper mockHelper = MockHelper(
      mockAuthHelper,
      prevozApi,
      mockGoogleSignIn
  );
  enableFlutterDriverExtension(handler: mockHelper.handleMessage);

  Injector injector = await MockAppModule().initialise(
      Injector.getInjector(),
      mockAuthHelper,
      mockSignInWithAppleWrapper,
      mockGoogleSignIn
  );

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarIconBrightness: Brightness.dark, //top bar icons
  ));
  MyApp myApp;

  WidgetsApp.debugAllowBannerOverride = false;


  myApp = MyApp(
    databaseHelper,
    AuthRepository(
        api: prevozApi,
        baseAuthHelper: injector.get<BaseAuthHelper>(),
        signInWithAppleWrapper: injector.get<SignInWithAppleWrapper>(),
        googleSignIn: injector.get<GoogleSignIn>(),
        sharedPreferences: injector.get<SharedPreferences>(),
        logger: Logger(printer: CustomLogPrinter("AuthRepo"))
    ),
    RidesRepository(prevozApi),
    LocationsRepository(databaseHelper),
    CountriesRepository(databaseHelper),
    SearchHistoryRepository(databaseHelper),
    NotificationSubscriptionRepository(databaseHelper, prevozApi),
    UserProfileRepository(prevozApi)
  );

  AppConfig appConfig = new AppConfig(
    pushEnabled: Platform.isAndroid ? true : false,
    child: myApp,
    appDatabase: databaseHelper,
    debugMode: false,
  );

  Bloc.observer = SimpleBlocObserver(appConfig);

  runApp(appConfig);
}
