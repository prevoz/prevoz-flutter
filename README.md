# prevoz_org

A new Flutter project.

## Running builds for different environments

The app is separated onto three different environments: one for release build for fully featured app (currently for Android), one for release build without push notifications (currently iOS) and one for development. Release build environments include Sentry crash reporting that is not used in the development environment to enable logging in the console.

To run the development build, call this command:

- **flutter run -t lib/main_dev.dart**

To run the production ready build for Android, call this command:

- **flutter run -t lib/main.dart**

To create the Android release build, call this command:

- **flutter build apk -t lib/main.dart**

To create the iOS release build:

- **flutter build ios -t lib/main_no_push.dart**
