#!/bin/bash

set -e
flutter test
flutter driver --target=test_driver/add_ride.dart
flutter driver --target=test_driver/add_ride_edit_mode.dart
flutter driver --target=test_driver/bottom_nav.dart
flutter driver --target=test_driver/home_results_details.dart
flutter driver --target=test_driver/push_notifications.dart
flutter driver --target=test_driver/slow_db_init.dart
flutter driver --target=test_driver/quick_ride.dart
flutter driver --target=test_driver/profile.dart
flutter driver --target=test_driver/profile_edit_name.dart
flutter driver --target=test_driver/profile_edit_address.dart
flutter driver --target=test_driver/refresh_user_profile_on_startup.dart
flutter driver --target=test_driver/android_to_flutter/db_migration_1.dart
flutter driver --target=test_driver/android_to_flutter/db_migration_2.dart
flutter driver --target=test_driver/android_to_flutter/db_migration_3.dart
flutter driver --target=test_driver/android_to_flutter/db_migration_4.dart
