import 'package:flutter_test/flutter_test.dart';
import 'package:prevoz_org/utils/date_time_utils.dart';
import 'package:prevoz_org/utils/other_utils.dart';

void main() {
  group('test util functions', () {
    test('Testing Search Page', () {
      expect(OtherUtils.removeZerosFromPrice(4.60), '4,6');
      expect(
          DateTimeUtils.isSelectedDateBeforeCurrentDate(
              DateTime.now().subtract(Duration(days: 2))),
          true);
      expect(
          DateTimeUtils.isSelectedDateBeforeCurrentDate(
              DateTime.now().add(Duration(days: 1))),
          false);
      expect(
          DateTimeUtils.isSelectedDateBeforeCurrentDate(DateTime.now()), false);

      DateTime date1 = new DateTime(2019, 1, 1);
      expect(DateTimeUtils.dateToStringForApiCalls(date1), "2019-01-01");
      DateTime date2 = new DateTime(2019, 12, 31);
      expect(DateTimeUtils.dateToStringForApiCalls(date2), "2019-12-31");
    });
  });
}
