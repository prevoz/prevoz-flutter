import 'package:dio/dio.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/data/models/ride_search_request.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/utils/mock_data.dart';
import 'package:test/test.dart';
import 'package:prevoz_org/data/network/prevoz_api.dart';

void main() {
  test("Testing the network call", () async {
    final api = PrevozApi();

    api.dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions options) {
        return Response(data: MockData.jsonSearchResult, statusCode: 200);
      },
    ));

    final RidesSearchResult item = await api.fetchRides(
        searchRequest:
            RideSearchRequest(route: PrevozRoute(), date: DateTime.now()));
    RideModel firstRide = item.getRides[0];
    expect(firstRide.id, 6011392);
    expect(firstRide.to, "Ljubljana");
    expect(firstRide.from, "Nova Gorica");
    expect(firstRide.full, false);
    expect(firstRide.time, "ob 19:00");
    expect(firstRide.type, 0);
    expect(firstRide.added, DateTime.parse("2019-03-18T16:52:53+01:00"));
    expect(firstRide.price, 5);
    expect(firstRide.author, "Amadej");
    expect(firstRide.comment, "sms");
    expect(firstRide.contact, "041123456");
    expect(firstRide.insured, true);
    expect(firstRide.bookmark, "bookmark");
    expect(firstRide.carInfo, "");
    expect(firstRide.isAuthor, false);
    expect(firstRide.numPeople, 3);
    expect(firstRide.toCountry, "SI");
    expect(firstRide.dateIso8601, DateTime.parse("2019-03-24T19:00:00+01:00"));
    expect(firstRide.fromCountry, "SI");
    expect(firstRide.confirmedContact, true);

    RideModel secondRide = item.getRides[1];
    expect(secondRide.id, 6011498);
    expect(secondRide.to, "Ljubljana");
    expect(secondRide.from, "Novo mesto");
    expect(secondRide.full, false);
    expect(secondRide.time, "ob 18:00");
    expect(secondRide.type, 0);
    expect(secondRide.added, DateTime.parse("2019-03-18T17:29:57+01:00"));
    expect(secondRide.price, 5);
    expect(secondRide.author, "jayjay");
    expect(secondRide.comment, "Sms na telefonsko stevilko 040000000");
    expect(secondRide.contact, "+4368138640000000");
    expect(secondRide.insured, true);
    expect(secondRide.bookmark, "bookmark");
    expect(secondRide.carInfo, "Kia sportage");
    expect(secondRide.isAuthor, false);
    expect(secondRide.numPeople, 4);
    expect(secondRide.toCountry, "SI");
    expect(secondRide.dateIso8601, DateTime.parse("2019-03-24T18:00:00+01:00"));
    expect(secondRide.fromCountry, "SI");
    expect(secondRide.confirmedContact, true);
  });
}
