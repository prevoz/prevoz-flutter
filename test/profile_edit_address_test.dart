
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:logger/logger.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_state.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/profile_edit/edit_profile_page_address.dart';
import 'package:prevoz_org/pages/profile_edit/widgets/successful_profile_update_widget.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';

class MockProfile extends Mock implements UserProfile {}
class MockResponse extends Mock implements Response {}
class MockAuthenticationBloc extends MockBloc<AuthenticationState> implements AuthenticationBloc {}
class MockEditProfileBloc extends MockBloc<EditProfileState> implements EditProfileBloc {}

void main() {

  group("EditProfilePageAddress", () {
    AuthenticationBloc authenticationBloc;
    EditProfileBloc editProfileBloc;
    Logger logger = Logger(printer: CustomLogPrinter("EditProfilePageAddress"));

    setUp(() {
      authenticationBloc = MockAuthenticationBloc();
      editProfileBloc = MockEditProfileBloc();
    });

    tearDown(() {
      authenticationBloc.close();
      editProfileBloc.close();
    });

    Widget _createScreen(UserProfile userProfile) {
      return MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>.value(value: authenticationBloc),
          BlocProvider<EditProfileBloc>.value(value: editProfileBloc)
        ],
        child: MaterialApp(
          home: Scaffold(
            body: EditProfilePageAddress(userProfile: userProfile),
          ),
          localizationsDelegates: [
            AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          supportedLocales: [Locale('sl', "SI")],
        ),

      );
    }

    testWidgets("Init with full profile", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(InitialEditProfileState());

      // Mock user profile
      final userProfile = MockProfile();
      when(userProfile.address).thenAnswer((_) => "Za cesto 1, 1000 Ljubljana");

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(userProfile));
      await tester.pumpAndSettle();

      logger.i("Expect the address prefill");
      final addressWidget = find.byKey(Key("_textFieldWithTitle_address")).evaluate().single.widget as TextField;
      expect(
          addressWidget.controller.text,
          "Za cesto 1, 1000 Ljubljana"
      );

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect labour law to be shown");
      expect(find.byKey(Key("_editProfilePageAddress_labourLaw")), findsOneWidget);

      logger.i("Expect no error to be shown");
      expect(find.byKey(Key("_editProfilePageAddress_generalError")), findsNothing);
    });

    testWidgets("Init with empty profile", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(InitialEditProfileState());

      // Mock user profile
      final userProfile = MockProfile();

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(userProfile));
      await tester.pumpAndSettle();

      logger.i("Expect the address to be empty");
      final addressWidget = find.byKey(Key("_textFieldWithTitle_address")).evaluate().single.widget as TextField;
      expect(
          addressWidget.controller.text,
          ""
      );

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect labour law to be shown");
      expect(find.byKey(Key("_editProfilePageAddress_labourLaw")), findsOneWidget);

      logger.i("Expect no error to be shown");
      expect(find.byKey(Key("_editProfilePageAddress_generalError")), findsNothing);
    });

    testWidgets("Show successful submit layout", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(SuccessfullySubmittedProfile(MockProfile()));

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(MockProfile()));
      await tester.pumpAndSettle();

      logger.i("Expect the submit button to be gone");
      expect(find.text("Shrani podatke"), findsNothing);

      logger.i("Expect the success message to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsOneWidget);

      logger.i("Expect no error to be shown");
      expect(find.byKey(Key("_editProfilePageAddress_generalError")), findsNothing);
    });

    testWidgets("Show general error submit layout", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(ErrorSubmittingProfile(MockResponse()));

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(MockProfile()));
      await tester.pumpAndSettle();

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect general error to be shown");
      expect(find.byKey(Key("_editProfilePageAddress_generalError")), findsOneWidget);
    });

    testWidgets("Show specific submit errors", (WidgetTester tester) async {
      final response = MockResponse();
      final data = {
        "error" : {
          "address" : ["To polje je obvezno"],
        }
      };
      when(response.data).thenReturn(data);

      // set initial state
      when(editProfileBloc.state).thenReturn(ErrorSubmittingProfile(response));

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(MockProfile()));
      await tester.pumpAndSettle();

      final addressWidget = find.byKey(Key("_textFieldWithTitle_address")).evaluate().single.widget as TextField;
      expect(addressWidget.decoration.errorText, "To polje je obvezno");

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect general error not to be shown");
      expect(find.byKey(Key("_editProfilePageAddress_generalError")), findsNothing);
    });

  });
}