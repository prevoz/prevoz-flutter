
import 'dart:async';
import 'dart:io';

import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/authentication/auth_error_type.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/models/user.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:test/test.dart';

class MockAuthRepository extends Mock implements AuthRepository {}
class MockResponse extends Mock implements Response {}
class MockUser extends Mock implements User {}

void main() {
  AuthRepository authRepository;

  setUp(() {
    authRepository = MockAuthRepository();
  });

  Response _getDefaultAccountStatusResponse() {
    Response response = MockResponse();
    when(response.statusCode).thenReturn(200);
    when(response.data).thenReturn({
      "username" : "janezK",
      "is_authenticated": "true",
      "needs_verification": true
    });

    return response;
  }

  Response _getDefaultProfileResponse(List<String> phoneNumbers) {
    Response response = MockResponse();
    when(response.statusCode).thenReturn(200);
    when(response.data).thenReturn({
      "email": "janez.k@email",
      "phone_numbers": phoneNumbers
    });

    return response;
  }

  blocTest(
      "Should try to exchange existing bearer token on app start",
      build: () {
        final String bearerToken = "aaa123";
        when(authRepository.getApiBearer()).thenAnswer((realInvocation) => Future.value(bearerToken));
        Response tokenExchangeResponse = MockResponse();
        when(tokenExchangeResponse.statusCode).thenReturn(200);
        when(tokenExchangeResponse.data).thenReturn({
          "token" : "bbb456"
        });
        when(authRepository.exchangeBearerToken(bearerToken)).thenAnswer((_) => Future.value(tokenExchangeResponse));
        when(authRepository.getAccountStatus()).thenAnswer((_) => Future.value(_getDefaultAccountStatusResponse()));
        when(authRepository.getUserProfile()).thenAnswer((_) => Future.value(_getDefaultProfileResponse(null)));
        when(authRepository.saveAuthData(token: "bbb456", username: "janezK")).thenAnswer((_) => Future.value(true));
        when(authRepository.savePhoneNumbersToSharedPrefs(null)).thenAnswer((_) => Future.value(true));

        return AuthenticationBloc(authRepository: authRepository);
      },
    act: (bloc) => bloc.add(AppStarted()),
    skip: 0,
    expect: [
      AuthStateLoading(),
      isA<AuthStateAuthenticated>()
    ],
    verify: (_) async {
        verify(authRepository.clearApiBearer()).called(1);
        verify(authRepository.saveToken("bbb456")).called(1);
    }
  );

  blocTest(
    "Should force logout on failed bearer token exchange",
    build: () {
      final String bearerToken = "aaa123";
      when(authRepository.getApiBearer()).thenAnswer((realInvocation) => Future.value(bearerToken));
      Response tokenExchangeResponse = MockResponse();
      when(tokenExchangeResponse.statusCode).thenReturn(400);
      when(tokenExchangeResponse.data).thenReturn({
        "error" : "Napaka"
      });
      when(authRepository.exchangeBearerToken(bearerToken)).thenAnswer((_) => Future.value(tokenExchangeResponse));
      return AuthenticationBloc(authRepository: authRepository);
    },
    act: (bloc) => bloc.add(AppStarted()),
    skip: 0,
    expect: [
      AuthStateLoading(),
      AuthStateUnauthenticated()
    ],
    verify: (_) async {
      verify(authRepository.logout()).called(1);
    }
  );

  blocTest(
    "On app start, user from shared preferences present, go to authenticated state",
    build: () {
      final User user = MockUser();
      when(user.accessToken).thenReturn("bbb456");
      when(user.username).thenReturn("janezK");
      when(user.phoneNumbers).thenReturn(null);
      when(user.lastLoginTimestamp).thenReturn(DateTime.now().millisecondsSinceEpoch);
      when(authRepository.getUserFromSharedPrefs()).thenAnswer((_) => Future.value(user));

      when(authRepository.getAccountStatus()).thenAnswer((_) => Future.value(_getDefaultAccountStatusResponse()));

      return AuthenticationBloc(authRepository: authRepository);
    },
    act: (bloc) => bloc.add(AppStarted()),
    skip: 0,
    expect: [
      AuthStateLoading(),
      isA<AuthStateAuthenticated>()
    ],
  );

  blocTest(
    "On app start, user from shared preferences is null, go to unauthenticated state",
    build: () {
      when(authRepository.getUserFromSharedPrefs()).thenAnswer((_) => Future.value(null));
      when(authRepository.getAccountStatus()).thenAnswer((_) => Future.value(_getDefaultAccountStatusResponse()));
      return AuthenticationBloc(authRepository: authRepository);
    },
    act: (bloc) => bloc.add(AppStarted()),
    skip: 0,
    expect: [
      AuthStateLoading(),
      AuthStateUnauthenticated()
    ],
  );

  blocTest(
    "Should create a successful login on successful GoogleSignIn",
    build: () {
      Response googleSignInResponse = MockResponse();
      when(googleSignInResponse.statusCode).thenReturn(200);
      when(googleSignInResponse.data).thenReturn({
        "token": "bbb456"
      });
      when(authRepository.signInWithGoogle()).thenAnswer((_) => Future.value(googleSignInResponse));
      when(authRepository.getAccountStatus()).thenAnswer((_) => Future.value(_getDefaultAccountStatusResponse()));
      when(authRepository.getUserProfile()).thenAnswer((_) => Future.value(_getDefaultProfileResponse(null)));
      when(authRepository.saveAuthData(token: "bbb456", username: "janezK")).thenAnswer((_) => Future.value(true));
      when(authRepository.savePhoneNumbersToSharedPrefs(null)).thenAnswer((_) => Future.value(true));

      return AuthenticationBloc(authRepository: authRepository);
    },
    act: (bloc) => bloc.add(AuthenticationGoogleSignIn()),
    skip: 0,
    expect: [
      AuthStateLoading(),
      isA<AuthStateAuthenticated>()
    ],
    verify: (_) async {
      verify(authRepository.saveToken("bbb456")).called(1);
    }
  );

  blocTest(
    "Should create an auth error state on unsuccessful GoogleSignIn",
    build: () {
      Response googleSignInResponse = MockResponse();
      when(googleSignInResponse.statusCode).thenReturn(400);
      when(googleSignInResponse.data).thenReturn({
        "error": "Napaka"
      });
      when(authRepository.signInWithGoogle()).thenAnswer((_) => Future.value(googleSignInResponse));
      return AuthenticationBloc(authRepository: authRepository);
    },
    act: (bloc) => bloc.add(AuthenticationGoogleSignIn()),
    skip: 0,
    expect: [
      AuthStateLoading(),
      AuthStateError(type: AuthErrorType.SIGN_IN_ERROR)
    ],
    verify: (_) async {
      verify(authRepository.logout()).called(1);
    }
  );

  blocTest(
      "Should create a successful login on successful AppleSignIn",
      build: () {
        Response appleSignInResponse = MockResponse();
        when(appleSignInResponse.statusCode).thenReturn(200);
        when(appleSignInResponse.data).thenReturn({
          "token": "bbb456"
        });
        when(authRepository.signInWithApple()).thenAnswer((_) => Future.value(appleSignInResponse));
        when(authRepository.getAccountStatus()).thenAnswer((_) => Future.value(_getDefaultAccountStatusResponse()));
        when(authRepository.getUserProfile()).thenAnswer((_) => Future.value(_getDefaultProfileResponse(null)));
        when(authRepository.saveAuthData(token: "bbb456", username: "janezK")).thenAnswer((_) => Future.value(true));
        when(authRepository.savePhoneNumbersToSharedPrefs(null)).thenAnswer((_) => Future.value(true));

        return AuthenticationBloc(authRepository: authRepository);
      },
      act: (bloc) => bloc.add(AuthenticationAppleSignIn()),
      skip: 0,
      expect: [
        AuthStateLoading(),
        isA<AuthStateAuthenticated>()
      ],
      verify: (_) async {
        verify(authRepository.saveToken("bbb456")).called(1);
      }
  );

  blocTest(
      "Should create an auth error state on unsuccessful AppleSignIn",
      build: () {
        Response appleSignInResponse = MockResponse();
        when(appleSignInResponse.statusCode).thenReturn(400);
        when(appleSignInResponse.data).thenReturn({
          "error": "Napaka"
        });
        when(authRepository.signInWithApple()).thenAnswer((_) => Future.value(appleSignInResponse));
        return AuthenticationBloc(authRepository: authRepository);
      },
      act: (bloc) => bloc.add(AuthenticationAppleSignIn()),
      skip: 0,
      expect: [
        AuthStateLoading(),
        AuthStateError(type: AuthErrorType.SIGN_IN_ERROR)
      ],
      verify: (_) async {
        verify(authRepository.logout()).called(1);
      }
  );

  blocTest(
      "Should persist data on default loggedInEvent",
      build: () {
        when(authRepository.getAccountStatus()).thenAnswer((_) => Future.value(_getDefaultAccountStatusResponse()));
        when(authRepository.getUserProfile()).thenAnswer((_) => Future.value(_getDefaultProfileResponse(null)));
        when(authRepository.saveAuthData(token: "bbb456", username: "janezK")).thenAnswer((_) => Future.value(true));
        when(authRepository.savePhoneNumbersToSharedPrefs(null)).thenAnswer((_) => Future.value(true));
        return AuthenticationBloc(authRepository: authRepository);
      },
      act: (bloc) => bloc.add(AuthenticationLoggedIn(token: "bbb456")),
      skip: 0,
      expect: [
        AuthStateLoading(),
        isA<AuthStateAuthenticated>()
      ],
      verify: (_) async {
        verify(authRepository.saveToken("bbb456")).called(1);
      }
  );

  blocTest(
      "Should create a successful login on successful account activation",
      build: () {
        Response activateAccountResponse = MockResponse();
        when(activateAccountResponse.statusCode).thenReturn(200);
        when(activateAccountResponse.data).thenReturn({
          "token": "bbb456"
        });
        when(authRepository.activateAccount(activationToken: "123456")).thenAnswer((_) => Future.value(activateAccountResponse));
        when(authRepository.getAccountStatus()).thenAnswer((_) => Future.value(_getDefaultAccountStatusResponse()));
        when(authRepository.getUserProfile()).thenAnswer((_) => Future.value(_getDefaultProfileResponse(null)));
        when(authRepository.saveAuthData(token: "bbb456", username: "janezK")).thenAnswer((_) => Future.value(true));
        when(authRepository.savePhoneNumbersToSharedPrefs(null)).thenAnswer((_) => Future.value(true));

        return AuthenticationBloc(authRepository: authRepository);
      },
      act: (bloc) => bloc.add(AuthenticationActivateAccount(activationToken: "123456")),
      skip: 0,
      expect: [
        AuthStateLoading(),
        isA<AuthStateAuthenticated>()
      ],
      verify: (_) async {
        verify(authRepository.saveToken("bbb456")).called(1);
      }
  );

  blocTest(
      "Should create an auth error state on unsuccessful account activation",
      build: () {
        Response activateAccountResponse = MockResponse();
        when(activateAccountResponse.statusCode).thenReturn(400);
        when(activateAccountResponse.data).thenReturn({
          "error": "Napaka"
        });
        when(authRepository.activateAccount(activationToken: "123456")).thenAnswer((_) => Future.value(activateAccountResponse));

        return AuthenticationBloc(authRepository: authRepository);
      },
      act: (bloc) => bloc.add(AuthenticationActivateAccount(activationToken: "123456")),
      skip: 0,
      expect: [
        AuthStateLoading(),
        AuthStateError(type: AuthErrorType.ACCOUNT_ACTIVATION_ERROR)
      ],
      verify: (_) async {
        verify(authRepository.logout()).called(1);
      }
  );

  blocTest(
      "Should not logout in _mapAppStartedToState on DioError-SocketException",
      build: () {
        final User user = MockUser();
        when(user.accessToken).thenReturn("bbb456");
        when(user.username).thenReturn("janezK");
        when(user.phoneNumbers).thenReturn(null);
        when(user.lastLoginTimestamp).thenReturn(DateTime.now().millisecondsSinceEpoch);
        when(authRepository.getUserFromSharedPrefs()).thenAnswer((_) => Future.value(user));

        when(authRepository.getAccountStatus()).thenThrow(DioError(type: DioErrorType.DEFAULT, error: SocketException("Could not resolve host")));

        return AuthenticationBloc(authRepository: authRepository);
      },
      act: (bloc) => bloc.add(AppStarted()),
      skip: 0,
      expect: [
        AuthStateLoading(),
        AuthStateError(type: AuthErrorType.CONNECTION_ERROR)
      ],
      verify: (_) async {
        verifyNever(authRepository.logout());
      }
  );

  blocTest(
      "Should not logout in _mapAuthenticationRefreshAccountStatusToState on DioError-SocketException",
      build: () {
        final User user = MockUser();
        when(user.accessToken).thenReturn("bbb456");
        when(user.username).thenReturn("janezK");
        when(user.phoneNumbers).thenReturn(null);
        when(user.lastLoginTimestamp).thenReturn(DateTime.now().millisecondsSinceEpoch);
        when(authRepository.getUserFromSharedPrefs()).thenAnswer((_) => Future.value(user));

        when(authRepository.getAccountStatus()).thenThrow(DioError(type: DioErrorType.DEFAULT, error: SocketException("Could not resolve host")));

        return AuthenticationBloc(authRepository: authRepository);
      },
      act: (bloc) => bloc.add(AuthenticationRefreshAccountStatus()),
      skip: 0,
      expect: [
        AuthStateLoading(),
        AuthStateError(type: AuthErrorType.CONNECTION_ERROR)
      ],
      verify: (_) async {
        verifyNever(authRepository.logout());
      }
  );
}