@Skip("sqflite cannot run on the machine.")
// test/sqlite_test.dart

// skip this file to avoid getting errors when running your unit tests
import 'dart:async';

import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:prevoz_org/data/file_reader.dart';
import 'package:prevoz_org/data/models/country.dart';
import 'package:prevoz_org/data/models/location.dart';
import 'package:prevoz_org/data/models/prevoz_route.dart';
import 'package:prevoz_org/utils/mock_data.dart';
import "package:test/test.dart";
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as pathLib;

/// Runs SQLite database tests. Assumes running in a fresh database all the time
/// Database is NOT reset between tests, tests should assume data exists from other test
/// runs.
/// Database is removed once all tests are complete

void main() {
  //static DB values

  String locationsTable = "Locations";

  //tables
  String tableCountries = "Countries";
  String tableLocations = "Locations";
  String tableSearchHistory = "SearchHistory";
  String tableNotification = "Notification";
  //country columns
  String columnCountryCode = "countryCode";
  String columnCountryLanguage = "language";
  String columnCountryName = "name";
  //location columns
  String columnLocationId = "id";
  String columnLocationSortNumber = "sortNumber";
  String columnLocationName = "name";
  String columnLocationNameAscii = "nameAscii";
  String columnLocationCountry = "country";
  String columnLocationLat = "lat";
  String columnLocationLng = "lng";
  //search history columns
  String columnSearchHistryLocFrom = "locationFrom";
  String columnSearchHistryCountryFrom = "countryFrom";
  String columnSearchHistryLocTo = "locationTo";
  String columnSearchHistryCountryTo = "countryTo";
  String columnSearchHistrySearchDate = "searchDate";
  String columnSearchHistryId = "id";
  //notification columns
  String columnNotificationLocationFrom = "locationFrom";
  String columnNotificationCountrFrom = "countryFrom";
  String columnNotificationLocationTo = "locationTo";
  String columnNotificationCountryTo = "countryTo";
  String columnNotificationRideDate = "date";
  String columnNotificationId = "id";

  String createCountriesTable =
      '''CREATE TABLE $tableCountries($columnCountryCode TEXT PRIMARY KEY, 
        $columnCountryLanguage TEXT, 
        $columnCountryName TEXT)''';

  String createLocationsTable =
      '''CREATE TABLE $tableLocations($columnLocationId INTEGER PRIMARY KEY AUTOINCREMENT, 
          $columnLocationSortNumber INTEGER, 
          $columnLocationName TEXT, $columnLocationNameAscii TEXT, 
          $columnLocationCountry TEXT, $columnLocationLat REAL,
         $columnLocationLng REAL)''';

  String createSearchHistoryTable =
      '''CREATE TABLE $tableSearchHistory($columnSearchHistryId INTEGER PRIMARY KEY AUTOINCREMENT, 
          $columnSearchHistryLocFrom TEXT, $columnSearchHistryCountryFrom TEXT, 
          $columnSearchHistryLocTo TEXT, $columnSearchHistryCountryTo TEXT,
         $columnSearchHistrySearchDate TEXT)''';

  String createNotificationsTable =
      '''CREATE TABLE $tableNotification($columnNotificationId INTEGER PRIMARY KEY AUTOINCREMENT, 
          $columnNotificationLocationTo TEXT, $columnNotificationCountryTo TEXT, 
          $columnNotificationLocationFrom TEXT, $columnNotificationCountrFrom TEXT,
          $columnNotificationRideDate INTEGER);''';

  List<String> version1creationScripts = [
    createCountriesTable,
    createLocationsTable,
    createSearchHistoryTable,
  ];

  String removeAllLocations = '''DELETE FROM $tableLocations;''';

  //* DB MIGRATION SCRIPTS

  List<String> v1migrations = [createNotificationsTable, removeAllLocations];
  List<List<String>> migrationScriptsForAllVersions = [v1migrations];

  Future<int> insertCountry(Country country, Database database) async {
    try {
      var dbClient = database;
      var newlyInsertedId =
          await dbClient.insert("$tableCountries", country.toMapRegular());
      return newlyInsertedId;
    } catch (e) {
      print("error inserting country");
      print("Error: " + e.toString());
      return null;
    }
  }

  Future<void> insertCountriesIntoDb(Database database) async {
    final FileReader fileReader = FileReader();
    List<Country> countries = await fileReader.getCountriesFromTxtFile();

    for (var i = 0; i < countries.length; i++) {
      await insertCountry(countries[i], database);
    }
    print("___________ INSERTED ALL COUNTRIES");
  }

  Future<int> insertLocation(Location location, Database database) async {
    try {
      var dbClient = database;

      Map<dynamic, dynamic> testMap = location.toMap();

      var newlyInsertedId = await dbClient.insert("$tableLocations", testMap);

      return newlyInsertedId;
    } catch (e) {
      print("error inserting location");
      print("Error: " + e.toString());
      return null;
    }
  }

  Future<void> insertLocationsIntoDb(Database database) async {
    final FileReader fileReader = FileReader();
    List<Location> locations = await fileReader.getLocationsFromTxtFile();

    for (var i = 0; i < locations.length; i++) {
      await insertLocation(locations[i], database);
    }
  }

  Future<int> insertRoute(PrevozRoute route, Database database) async {
    try {
      var dbClient = database;
      var newlyInsertedId =
          await dbClient.insert("$tableSearchHistory", route.toMap());
      return newlyInsertedId;
    } catch (e) {
      print("error inserting route");
      print("Error: " + e.toString());
      return null;
    }
  }

  List<PrevozRoute> defaultPopularRoutes() {
    return MockData.defaultSearchHistory();
  }

  Future<void> insertDefaultSearchHistoryIntoDb(Database database) async {
    List<PrevozRoute> popularRoutes = defaultPopularRoutes();
    for (var i = 0; i < popularRoutes.length; i++) {
      await insertRoute(popularRoutes[i], database);
    }
    print("___________ INSERTED ALL DEFAULT SEARCH HISTORY");
  }

  group("SQLiteDao tests", () {
    setUp(() async {});

    // Delete the database so every test run starts with a fresh database
    tearDownAll(() async {
      //? await deleteDatabase((await getDatabasesPath()) + "/" + testDBPath);
    });

    test("test upgrade of DB", () async {
      //* 1) Setup path
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      String path = pathLib.join(documentsDirectory.path,
          "prevoztest.db"); //  join(documentsDirectory.path, _databaseName);

      //* 2) Open DB version 1
      Database database = await openDatabase(path, version: 1,
          onCreate: (Database db, int version) async {
        //* CREATE tables
        version1creationScripts
            .forEach((script) async => await db.execute(script));
        print("_______DB CREATION");

        //* INSERT data
        insertCountriesIntoDb(db);
        insertLocationsIntoDb(db);
        insertDefaultSearchHistoryIntoDb(db);
      });

      //* 3) wait for data to get inserted (altough that should probably be done by await in insert functions)
      await Future.delayed(Duration(seconds: 10));
      print("_______ABOUT TO TEST QUERIES");

      //* 4) Test some queries on DB version 1
      var result2 = await database.query('$locationsTable',
          where: "$columnLocationName = ?", whereArgs: ['Polzela']);
      expect(result2.length, equals(1));

      print("_______ABOUT TO CLOSE DB VERSION 1");
      //* 5) Close DB version 1
      await database.close();
      //* 6) Open DB anew, this time it's version 2 and we run it's onUpgrade()
      database =
          await openDatabase(path, version: 2, onOpen: (Database database) {
        print("_______ON OPEN, INSERT NEW LOCATIONS");
        //* insert NEW locations
        insertLocationsIntoDb(database);
      }, onUpgrade: (Database db, int oldVersion, int newVersion) async {
        //* alter the DB
        migrationScriptsForAllVersions.forEach((List<String> migrationScripts) {
          migrationScripts.forEach((String script) async {
            await db.execute(script);
            print("_______ON UPGRADE, finished $script");
          });
        });

        print("_______ON UPGRADE, finished all migration scripts");
      });

      //* 7) wait
      await Future.delayed(Duration(seconds: 10));
      //* 8) Test some queries on DB version 2
      var result3 = await database.query('$locationsTable',
          where: "$columnLocationName = ?", whereArgs: ["Ljubljana (Šiška)"]);
      expect(result3.length, equals(1));

      var result4 = await database.query('$locationsTable',
          where: "$columnLocationName = ?", whereArgs: ['Polzela']);
      expect(result4.length, equals(1));

      await database.close();
    });
  });
}
