
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:logger/logger.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_state.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/profile_edit/edit_profile_page_first_and_last_name.dart';
import 'package:prevoz_org/pages/profile_edit/widgets/successful_profile_update_widget.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';

class MockProfile extends Mock implements UserProfile {}
class MockResponse extends Mock implements Response {}
class MockAuthenticationBloc extends MockBloc<AuthenticationState> implements AuthenticationBloc {}
class MockEditProfileBloc extends MockBloc<EditProfileState> implements EditProfileBloc {}

void main() {
  
  group("EditProfilePageFirstAndLastName", () {
    AuthenticationBloc authenticationBloc;
    EditProfileBloc editProfileBloc;
    Logger logger = Logger(printer: CustomLogPrinter("EditProfilePageFirstAndLastName"));

    setUp(() {
      authenticationBloc = MockAuthenticationBloc();
      editProfileBloc = MockEditProfileBloc();
    });

    tearDown(() {
      authenticationBloc.close();
      editProfileBloc.close();
    });

    Widget _createScreen(UserProfile userProfile) {
      return MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>.value(value: authenticationBloc),
          BlocProvider<EditProfileBloc>.value(value: editProfileBloc)
        ],
        child: MaterialApp(
          home: Scaffold(
            body: EditProfilePageFirstAndLastName(userProfile: userProfile),
          ),
          localizationsDelegates: [
            AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          supportedLocales: [Locale('sl', "SI")],
        ),

      );
    }

    testWidgets("Init with full profile", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(InitialEditProfileState());

      // Mock user profile
      final userProfile = MockProfile();
      when(userProfile.firstName).thenAnswer((_) => "Janez");
      when(userProfile.lastName).thenAnswer((_) => "Novak");

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(userProfile));
      await tester.pumpAndSettle();

      logger.i("Expect the first name prefill");
      final firstNameWidget = find.byKey(Key("_textFieldWithTitle_firstName")).evaluate().single.widget as TextField;
      expect(
          firstNameWidget.controller.text,
          "Janez"
      );

      logger.i("Expect the last name prefill");
      final lastNameWidget = find.byKey(Key("_textFieldWithTitle_lastName")).evaluate().single.widget as TextField;
      expect(
          lastNameWidget.controller.text,
          "Novak"
      );

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect labour law to be shown");
      expect(find.byKey(Key("_editProfilePageFirstAndLastName_labourLaw")), findsOneWidget);

      logger.i("Expect no error to be shown");
      expect(find.byKey(Key("_editProfilePageFirstAndLastName_generalError")), findsNothing);
    });

    testWidgets("Init with empty profile", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(InitialEditProfileState());

      // Mock user profile
      final userProfile = MockProfile();

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(userProfile));
      await tester.pumpAndSettle();

      logger.i("Expect the first name to be empty");
      final firstNameWidget = find.byKey(Key("_textFieldWithTitle_firstName")).evaluate().single.widget as TextField;
      expect(
          firstNameWidget.controller.text,
          ""
      );

      logger.i("Expect the last name to be empty");
      final lastNameWidget = find.byKey(Key("_textFieldWithTitle_lastName")).evaluate().single.widget as TextField;
      expect(
          lastNameWidget.controller.text,
          ""
      );

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect labour law to be shown");
      expect(find.byKey(Key("_editProfilePageFirstAndLastName_labourLaw")), findsOneWidget);

      logger.i("Expect no error to be shown");
      expect(find.byKey(Key("_editProfilePageFirstAndLastName_generalError")), findsNothing);
    });

    testWidgets("Show successful submit layout", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(SuccessfullySubmittedProfile(MockProfile()));

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(MockProfile()));
      await tester.pumpAndSettle();

      logger.i("Expect the submit button to be gone");
      expect(find.text("Shrani podatke"), findsNothing);

      logger.i("Expect the success message to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsOneWidget);

      logger.i("Expect no error to be shown");
      expect(find.byKey(Key("_editProfilePageFirstAndLastName_generalError")), findsNothing);
    });

    testWidgets("Show general error submit layout", (WidgetTester tester) async {
      // set initial state
      when(editProfileBloc.state).thenReturn(ErrorSubmittingProfile(MockResponse()));

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(MockProfile()));
      await tester.pumpAndSettle();

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect general error to be shown");
      expect(find.byKey(Key("_editProfilePageFirstAndLastName_generalError")), findsOneWidget);
    });

    testWidgets("Show specific submit errors", (WidgetTester tester) async {
      final response = MockResponse();
      final data = {
        "error" : {
          "first_name" : ["To polje je obvezno"],
          "last_name" : ["To polje je obvezno"]
        }
      };
      when(response.data).thenReturn(data);

      // set initial state
      when(editProfileBloc.state).thenReturn(ErrorSubmittingProfile(response));

      // Create the profile edit widget
      await tester.pumpWidget(_createScreen(MockProfile()));
      await tester.pumpAndSettle();

      final firstNameWidget = find.byKey(Key("_textFieldWithTitle_firstName")).evaluate().single.widget as TextField;
      expect(firstNameWidget.decoration.errorText, "To polje je obvezno");

      final lastNameWidget = find.byKey(Key("_textFieldWithTitle_lastName")).evaluate().single.widget as TextField;
      expect(lastNameWidget.decoration.errorText, "To polje je obvezno");

      logger.i("Expect the submit button to be shown");
      expect(find.text("Shrani podatke"), findsOneWidget);

      logger.i("Expect the success message not to be shown");
      expect(find.byType(SuccessfulProfileUpdateWidget), findsNothing);

      logger.i("Expect general error not to be shown");
      expect(find.byKey(Key("_editProfilePageFirstAndLastName_generalError")), findsNothing);
    });

  });
}