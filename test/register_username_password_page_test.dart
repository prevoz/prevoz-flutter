
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/registration/registration_bloc.dart';
import 'package:prevoz_org/data/blocs/registration/registration_state.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/register/register_username_password_page.dart';
import 'package:prevoz_org/widgets/other/google_sign_in_button.dart';

class MockResponse extends Mock implements Response {}
class MockAuthRepository extends Mock implements AuthRepository {}
class MockAuthenticationBloc extends MockBloc<AuthenticationState> implements AuthenticationBloc {}
class MockRegistrationBlock extends MockBloc<RegistrationState> implements RegistrationBloc {}

void main() {

  group("RegistrationPage", () {
    AuthRepository authRepository;
    AuthenticationBloc authenticationBloc;
    RegistrationBloc registrationBloc;

    setUp(() {
      authRepository = MockAuthRepository();
      authenticationBloc = MockAuthenticationBloc();
      registrationBloc = MockRegistrationBlock();
    });

    Widget _createScreen() {
      return RepositoryProvider(
        create: (context) => authRepository,
        child: MultiBlocProvider(
          providers: [
            BlocProvider<AuthenticationBloc>.value(value: authenticationBloc),
            BlocProvider<RegistrationBloc>.value(value: registrationBloc)
          ],
          child: MaterialApp(
            home: Scaffold(
              body: RegisterUsernamePasswordPage(),
            ),
            localizationsDelegates: [
              AppLocalizationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            supportedLocales: [Locale('sl', "SI")],
          ),
        ),
      );
    }

    testWidgets("Should show GoogleSignInButton on RegistrationErrorGmail", (WidgetTester tester) async {
      // set login bloc state
      when(registrationBloc.state).thenReturn(RegistrationErrorGmail());

      await tester.pumpWidget(_createScreen());
      await tester.pumpAndSettle();

      expect((find.byType(GoogleSignInButton)), findsOneWidget);
    });

    testWidgets("Should show info on successful registration", (WidgetTester tester) async {
      // set login bloc state
      when(registrationBloc.state).thenReturn(SuccessfullySubmittedRegistration());

      await tester.pumpWidget(_createScreen());
      await tester.pumpAndSettle();

      expect((find.text("Uspešno ste se registrirali. Na vnešeni naslov boste prejeli e-mail. Sledite povezavi v e-mailu za dokončanje procesa registracije.")), findsOneWidget);
    });

    tearDown(() {
      authenticationBloc.close();
      registrationBloc.close();
    });

  });
}