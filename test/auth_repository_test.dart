
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logger/logger.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/helpers/auth/base_auth_helper.dart';
import 'package:prevoz_org/data/network/base_prevoz_api.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/mocks/mock_sentry_error_reporter.dart';
import 'package:prevoz_org/mocks/mock_sign_in_with_apple_wrapper.dart';
import 'package:prevoz_org/utils/my_constants.dart';
import 'package:prevoz_org/utils/sentry_error_reporter.dart';
import 'package:prevoz_org/utils/sign_in_with_apple_wrapper.dart';
import 'package:sentry/sentry.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test/test.dart';

class MockSentryClient extends Mock implements SentryClient {}
class MockPrevozApi extends Mock implements BasePrevozApi {}
class MockAuthHelper extends Mock implements BaseAuthHelper {}
class MockGoogleSignIn extends Mock implements GoogleSignIn {}
class MockGoogleSignInAccount extends Mock implements GoogleSignInAccount {}
class MockGoogleSignInAuthentication extends Mock implements GoogleSignInAuthentication {}
class MockSharedPreferences extends Mock implements SharedPreferences {}
class MockLogger extends Mock implements Logger {}

void main() {
  final injector = Injector.getInjector();
  injector.map<SentryErrorReporter>((sentryErrorReporter) => MockSentryErrorReporter(), isSingleton: true);

  group('Token handling', () {
    BasePrevozApi api;
    BaseAuthHelper authHelper;
    SignInWithAppleWrapper signInWithAppleWrapper;
    GoogleSignIn googleSignIn;
    SharedPreferences sharedPreferences;
    AuthRepository authRepository;

    setUp(() {
      api = MockPrevozApi();
      authHelper = MockAuthHelper();
      signInWithAppleWrapper = MockSignInWithAppleWrapper();
      googleSignIn = MockGoogleSignIn();
      sharedPreferences = MockSharedPreferences();

      authRepository = AuthRepository(
        api: api,
        baseAuthHelper: authHelper,
        signInWithAppleWrapper: signInWithAppleWrapper,
        googleSignIn: googleSignIn,
        sharedPreferences: sharedPreferences,
        logger: MockLogger()
      );
    });

    test("Should get deprecated bearer token", () async {
      await authRepository.getApiBearer();
      verify(authHelper.getApiBearer()).called(1);
    });

    test("Should clear the bearer token", () async {
      await authRepository.clearApiBearer();
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_BEARER_TOKEN, captureAny)).captured, [null]);
    });

    test("Should get api token", () async {
      await authRepository.saveToken("bbb456");
      expect(verify(api.setApiToken(captureAny)).captured, ["bbb456"]);
      expect(verify(sharedPreferences.setString(captureAny, captureAny)).captured, [MyConstants.PREF_KEY_API_TOKEN, "bbb456"]);
    });

    test("Should exchange bearer token", () async {
      await authRepository.exchangeBearerToken("oldToken");
      expect(verify(api.exchangeBearerToken(captureAny)).captured, ["oldToken"]);
    });
  });

  group('Login types', () {
    BasePrevozApi api;
    BaseAuthHelper authHelper;
    SignInWithAppleWrapper signInWithAppleWrapper;
    GoogleSignIn googleSignIn;
    SharedPreferences sharedPreferences;
    AuthRepository authRepository;

    setUp(() {
      when(injector.get<SentryErrorReporter>().reportErrorToSentry(any, any)).thenAnswer((_) => Future.value());
      api = MockPrevozApi();
      authHelper = MockAuthHelper();
      signInWithAppleWrapper = MockSignInWithAppleWrapper();
      googleSignIn = MockGoogleSignIn();
      sharedPreferences = MockSharedPreferences();

      authRepository = AuthRepository(
          api: api,
          baseAuthHelper: authHelper,
          signInWithAppleWrapper: signInWithAppleWrapper,
          googleSignIn: googleSignIn,
          sharedPreferences: sharedPreferences,
          logger: MockLogger()
      );
    });

    test("Should not report to Sentry on AuthorizationErrorCode.canceled", () async {
      (signInWithAppleWrapper as MockSignInWithAppleWrapper).handleCommand("mockAuthorizationErrorCodeCanceled");
      Response response = await authRepository.signInWithApple();
      expect(response.statusCode, 400);
      verifyNever(injector.get<SentryErrorReporter>().reportErrorToSentry(any, any));
    });

    test("Should sign in with google", () async {
      GoogleSignInAuthentication googleAuthentication = MockGoogleSignInAuthentication();
      when(googleAuthentication.idToken).thenReturn("google-id-token");
      GoogleSignInAccount googleAccount = MockGoogleSignInAccount();
      when(googleAccount.authentication).thenAnswer((_) => Future.value(googleAuthentication));
      when(googleSignIn.signIn()).thenAnswer((_) => Future.value(googleAccount));

      await authRepository.signInWithGoogle();
      expect(verify(api.uploadGoogleToken(idToken: captureAnyNamed("idToken"))).captured, ["google-id-token"]);
    });

    test("Should clear data on logout", () async {
      await authRepository.logout();

      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_USERNAME, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_EMAIL, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.USERS_PHONE_NUMBERS, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_FIRST_NAME, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_LAST_NAME, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_PROFILE_ADDRESS, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_API_TOKEN, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_BEARER_TOKEN, captureAny)).captured, [null]);
      expect(verify(sharedPreferences.setString(MyConstants.PREF_KEY_REFRESH_TOKEN, captureAny)).captured, [null]);

      verify(googleSignIn.signOut()).called(1);
    });

  });
}