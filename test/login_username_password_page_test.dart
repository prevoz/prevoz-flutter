
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/blocs/login/login_bloc.dart';
import 'package:prevoz_org/data/blocs/login/login_state.dart';
import 'package:prevoz_org/data/repositories/auth_repository.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/login/login_username_password_page.dart';
import 'package:prevoz_org/widgets/other/google_sign_in_button.dart';

class MockResponse extends Mock implements Response {}
class MockAuthRepository extends Mock implements AuthRepository {}
class MockAuthenticationBloc extends MockBloc<AuthenticationState> implements AuthenticationBloc {}
class MockLoginBlock extends MockBloc<LoginState> implements LoginBloc {}

void main() {

  group("LoginPage", () {
    AuthRepository authRepository;
    AuthenticationBloc authenticationBloc;
    LoginBloc loginBloc;

    setUp(() {
      authRepository = MockAuthRepository();
      authenticationBloc = MockAuthenticationBloc();
      loginBloc = MockLoginBlock();
    });

    Widget _createScreen() {
      return RepositoryProvider(
        create: (context) => authRepository,
        child: MultiBlocProvider(
          providers: [
            BlocProvider<AuthenticationBloc>.value(value: authenticationBloc),
            BlocProvider<LoginBloc>.value(value: loginBloc)
          ],
          child: MaterialApp(
            home: Scaffold(
              body: LoginUsernamePasswordPage(),
            ),
            localizationsDelegates: [
              AppLocalizationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            supportedLocales: [Locale('sl', "SI")],
          ),
        ),
      );
    }

    testWidgets("Should show GoogleSignInButton on LoginErrorGmail", (WidgetTester tester) async {
      // set login bloc state
      when(loginBloc.state).thenReturn(LoginErrorGmail());

      await tester.pumpWidget(_createScreen());
      await tester.pumpAndSettle();

      expect((find.byType(GoogleSignInButton)), findsOneWidget);
    });

    tearDown(() {
      authenticationBloc.close();
      loginBloc.close();
    });

  });
}