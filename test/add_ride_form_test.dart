
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:logger/logger.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_bloc.dart';
import 'package:prevoz_org/data/blocs/add_ride/add_ride_state.dart';
import 'package:prevoz_org/data/blocs/add_ride_profile/add_ride_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/add_ride_profile/add_ride_profile_state.dart';
import 'package:prevoz_org/data/blocs/authentication/authentication.dart';
import 'package:prevoz_org/data/models/rides_search_result.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/locale/localization/localizations.dart';
import 'package:prevoz_org/pages/add_ride/add_ride_form.dart';
import 'package:prevoz_org/pages/add_ride/widgets/date_and_time.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/bloc/bloc.dart';
import 'package:prevoz_org/pages/add_ride/widgets/quick_rides/quick_entries_widget.dart';
import 'package:prevoz_org/utils/custom_log_printer.dart';

class MockRideModel extends Mock implements RideModel {}
class MockProfile extends Mock implements UserProfile {}
class MockResponse extends Mock implements Response {}
class MockAuthenticationBloc extends MockBloc<AuthenticationState> implements AuthenticationBloc {}
class MockAddRideProfileBloc extends MockBloc<AddRideProfileState> implements AddRideProfileBloc {}
class MockAddRideBloc extends MockBloc<AddRideState> implements AddRideBloc {}
class MockQuickEntriesBloc extends MockBloc<QuickEntriesState> implements QuickEntriesBloc {}

void main() {

  group("AddRideForm", () {
    AuthenticationBloc authenticationBloc;
    AddRideProfileBloc profileBloc;
    AddRideBloc addRideBloc;
    QuickEntriesBloc quickEntriesBloc;
    Logger logger = Logger(printer: CustomLogPrinter("AddRideForm"));

    setUp(() {
      authenticationBloc = MockAuthenticationBloc();
      profileBloc = MockAddRideProfileBloc();
      addRideBloc = MockAddRideBloc();
      quickEntriesBloc = MockQuickEntriesBloc();
    });

    tearDown(() {
      authenticationBloc.close();
      profileBloc.close();
      addRideBloc.close();
      quickEntriesBloc.close();
    });

    Widget _createScreen(bool needsVerification) {
      return MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>.value(value: authenticationBloc),
          BlocProvider<AddRideProfileBloc>.value(value: profileBloc),
          BlocProvider<AddRideBloc>.value(value: addRideBloc),
          BlocProvider<QuickEntriesBloc>.value(value: quickEntriesBloc)
        ],
        child: MaterialApp(
          home: Scaffold(
            body: AddRideForm(
                ride: null,
                isLargeDevice: false,
                needsVerification: needsVerification
            ),
          ),
          localizationsDelegates: [
            AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          supportedLocales: [Locale('sl', "SI")],
        ),

      );
    }

    void _expectBasicFormWidgets() {
      logger.i("Expect basic form widgets");
      expect(find.byKey(Key("addRideScrollView")), findsOneWidget);
      expect(find.byType(QuickEntriesWidget), findsOneWidget);
      expect(find.byKey(Key("_addRideFromLocation")), findsOneWidget);
      expect(find.byKey(Key("_addRideToLocation")), findsOneWidget);
      expect(find.byType(DateAndTimeFields), findsOneWidget);
      expect(find.byKey(Key("addRideNumOfPlaces")), findsOneWidget);
      expect(find.byKey(Key("addRidePrice")), findsOneWidget);
      expect(find.byKey(Key("addRideCarDescription")), findsOneWidget);
      expect(find.byKey(Key("phoneNumberField")), findsOneWidget);
      expect(find.byKey(Key("commentField")), findsOneWidget);
      expect(find.byKey(Key("addRideInsurance")), findsOneWidget);
      expect(find.byKey(Key("addRidePicksMidway")), findsOneWidget);
      expect(find.text("Prekliči"), findsOneWidget);
      expect(find.byKey(Key("addRideSubmitBtn")), findsOneWidget);
    }

    testWidgets("Init with needs_verification = false", (WidgetTester tester) async {
      // set initial Authentication state
      when(authenticationBloc.state).thenReturn(AuthStateAuthenticated(
        "info@test.si", null, false
      ));
      // set initial AddRideProfile state
      when(profileBloc.state).thenReturn(InitialAddRideProfileState());
      // set initial AddRide state
      when(addRideBloc.state).thenReturn(InitialAddRideState());

      // create widget
      await tester.pumpWidget(_createScreen(false));
      await tester.pumpAndSettle();

      _expectBasicFormWidgets();

      logger.i("Expect profile progress indicator to be hidden");
      expect(find.byKey(Key("addRide_progressIndicatorContainer")), findsNothing);

      logger.i("Expect labour law to be hidden");
      expect(find.byKey(Key("addRide_labourLawContainer")), findsNothing);

      logger.i("Expect the first name field to be hidden");
      expect(find.byKey(Key("addRideFirstName")), findsNothing);

      logger.i("Expect the last name field to be hidden");
      expect(find.byKey(Key("addRideLasttName")), findsNothing);

      logger.i("Expect the address field to be hidden");
      expect(find.byKey(Key("addRideAddress")), findsNothing);
    });

    testWidgets("Init with needs_verification = true, empty profile", (WidgetTester tester) async {
      // set initial Authentication state
      when(authenticationBloc.state).thenReturn(AuthStateAuthenticated(
          "info@test.si", null, true
      ));
      // set initial AddRideProfile state
      final userProfile = MockProfile();
      when(profileBloc.state).thenReturn(SuccessfullyFetchedAddRideProfile(userProfile));
      // set initial AddRide state
      when(addRideBloc.state).thenReturn(InitialAddRideState());

      // create widget
      await tester.pumpWidget(_createScreen(true));
      await tester.pumpAndSettle();

      _expectBasicFormWidgets();

      logger.i("Expect profile progress indicator to be hidden");
      expect(find.byKey(Key("addRide_progressIndicatorContainer")), findsNothing);

      logger.i("Expect labour law to be shown");
      expect(find.byKey(Key("addRide_labourLawContainer")), findsOneWidget);

      logger.i("Expect the first name field to be empty");
      final firstNameWidget = find.byKey(Key("addRideFirstName")).evaluate().single.widget as TextFormField;
      expect(firstNameWidget.controller.text, "");

      logger.i("Expect the last name field to be empty");
      final lastNameWidget = find.byKey(Key("addRideLastName")).evaluate().single.widget as TextFormField;
      expect(lastNameWidget.controller.text, "");

      logger.i("Expect the address field to be empty");
      final addressWidget = find.byKey(Key("addRideAddress")).evaluate().single.widget as TextFormField;
      expect(addressWidget.controller.text, "");
    });

    testWidgets("Init with needs_verification = true, full profile", (WidgetTester tester) async {
      // set initial Authentication state
      when(authenticationBloc.state).thenReturn(AuthStateAuthenticated(
          "info@test.si", null, true
      ));
      // set initial AddRideProfile state
      final userProfile = MockProfile();
      when(userProfile.firstName).thenReturn("Janez");
      when(userProfile.lastName).thenReturn("Novak");
      when(userProfile.address).thenReturn("Za cesto 1, 1000 Ljubljana");
      when(profileBloc.state).thenReturn(SuccessfullyFetchedAddRideProfile(userProfile));
      // set initial AddRide state
      when(addRideBloc.state).thenReturn(InitialAddRideState());

      // create widget
      await tester.pumpWidget(_createScreen(true));
      await tester.pumpAndSettle();

      _expectBasicFormWidgets();

      logger.i("Expect profile progress indicator to be hidden");
      expect(find.byKey(Key("addRide_progressIndicatorContainer")), findsNothing);

      logger.i("Expect labour law to be shown");
      expect(find.byKey(Key("addRide_labourLawContainer")), findsOneWidget);

      logger.i("Expect the first name field to be pre-filled");
      final firstNameWidget = find.byKey(Key("addRideFirstName")).evaluate().single.widget as TextFormField;
      expect(firstNameWidget.controller.text, "Janez");

      logger.i("Expect the last name field to be pre-filled");
      final lastNameWidget = find.byKey(Key("addRideLastName")).evaluate().single.widget as TextFormField;
      expect(lastNameWidget.controller.text, "Novak");

      logger.i("Expect the address field to be pre-filled");
      final addressWidget = find.byKey(Key("addRideAddress")).evaluate().single.widget as TextFormField;
      expect(addressWidget.controller.text, "Za cesto 1, 1000 Ljubljana");
    });

    testWidgets("Init with needs_verification = true, errored profile", (WidgetTester tester) async {
      // set initial Authentication state
      when(authenticationBloc.state).thenReturn(AuthStateAuthenticated(
          "info@test.si", null, true
      ));
      // set initial AddRideProfile state
      final response = MockResponse();
      when(response.statusCode).thenReturn(400);
      when(profileBloc.state).thenReturn(ErrorFetchingAddRideProfile(response, null));
      // set initial AddRide state
      when(addRideBloc.state).thenReturn(InitialAddRideState());

      // create widget
      await tester.pumpWidget(_createScreen(true));
      await tester.pumpAndSettle();

      _expectBasicFormWidgets();

      logger.i("Expect profile progress indicator to be hidden");
      expect(find.byKey(Key("addRide_progressIndicatorContainer")), findsNothing);

      logger.i("Expect labour law to be hidden");
      expect(find.byKey(Key("addRide_labourLawContainer")), findsNothing);

      logger.i("Expect the first name field to be hidden");
      expect(find.byKey(Key("addRideFirstName")), findsNothing);

      logger.i("Expect the last name field to be hidden");
      expect(find.byKey(Key("addRideLasttName")), findsNothing);

      logger.i("Expect the address field to be hidden");
      expect(find.byKey(Key("addRideAddress")), findsNothing);

      logger.i("Expect the profile error to be shwon");
      expect(find.byKey(Key("addRide_generalProfileError")), findsOneWidget);
    });

  });
}