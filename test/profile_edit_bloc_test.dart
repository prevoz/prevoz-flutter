import 'dart:async';
import 'package:bloc_test/bloc_test.dart';
import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_bloc.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_event.dart';
import 'package:prevoz_org/data/blocs/edit_profile/edit_profile_state.dart';
import 'package:prevoz_org/data/models/userProfile.dart';
import 'package:prevoz_org/data/repositories/user_profile_repository.dart';
import 'package:test/test.dart';

class MockUserProfileRepository extends Mock implements UserProfileRepository {}
class MockResponse extends Mock implements Response {}

void main() {
  UserProfileRepository userProfileRepository;

  setUp(() {
    userProfileRepository = MockUserProfileRepository();
  });

  blocTest(
      "Expect post name data is correct",
      build: () {
        final data = {
          "first_name" : "Janez",
          "last_name" : "Novak"
        };
        final response = MockResponse();
        when(response.statusCode).thenReturn(200);
        when(response.data).thenReturn(data);

        when(userProfileRepository.postProfile(any)).thenAnswer((_) => Future.value(response));

        return EditProfileBloc(userProfileRepository);
      },
      act: (bloc) => bloc.add(PostFirstAndLastName(firstName: "Janez", lastName: "Novak")),
      skip: 0,
      expect: [
        SubmittingProfile(),
        isA<SuccessfullySubmittedProfile>()
      ],
      verify: (_) async {
        expect(verify(userProfileRepository.postProfile(captureAny))
            .captured, [{ "first_name": "Janez", "last_name": "Novak" }]);
        var matcher = TypeMatcher<UserProfile>()
            .having((source) => source.firstName, "firstName", equals("Janez"))
            .having((source) => source.lastName, "lastName", equals("Novak"));
        expect(verify(userProfileRepository.saveToSharedPrefs(captureAny)).captured, [matcher]);
      }
  );

  blocTest(
      "Expect post address data is correct",
      build: () {
        final data = {
          "address" : "Za cesto 1, 1000 Ljubljana",
        };
        final response = MockResponse();
        when(response.statusCode).thenReturn(200);
        when(response.data).thenReturn(data);

        when(userProfileRepository.postProfile(any)).thenAnswer((_) => Future.value(response));

        return EditProfileBloc(userProfileRepository);
      },
      act: (bloc) => bloc.add(PostAddress(address: "Za cesto 1, 1000 Ljubljana")),
      skip: 0,
      expect: [
        SubmittingProfile(),
        isA<SuccessfullySubmittedProfile>()
      ],
      verify: (_) async {
        expect(verify(userProfileRepository.postProfile(captureAny))
            .captured, [{ "address": "Za cesto 1, 1000 Ljubljana"}]);
        var matcher = TypeMatcher<UserProfile>()
            .having((source) => source.address, "firstName", equals("Za cesto 1, 1000 Ljubljana"));
        expect(verify(userProfileRepository.saveToSharedPrefs(captureAny)).captured, [matcher]);
      }
  );
}