import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:prevoz_org/data/blocs/locations/locations_bloc.dart';
import 'package:prevoz_org/data/blocs/locations/locations_events.dart';
import 'package:prevoz_org/data/blocs/locations/locations_state.dart';
import 'package:prevoz_org/data/repositories/locations_repository.dart';

class MockLocationsRepository extends Mock implements LocationsRepository {}

void main() {
  MockLocationsRepository locationsRepository;

  setUp(() {
    locationsRepository = MockLocationsRepository();
  });

  blocTest(
      "emits [InitialLocationsState, LocationsLoading, SuccessLoadingLocations] when successfull",
      build: () {
        when(locationsRepository.countLocations())
            .thenAnswer((_) => Future.value(1214));

        when(locationsRepository.queryLocations("test", "test"))
            .thenAnswer((_) async => []);

        return LocationsBloc(locationsRepo: locationsRepository);
      },
      skip: 0,
      act: (bloc) => bloc.add(FetchLocations("test", "test")),
      expect: [
        LocationsLoading(),
        isA<SuccessLoadingLocations>()
      ]);

  blocTest(
      "emits [InitialLocationsState, LocationsLoading, ErrorLoadingLocations] when locations count is wrong",
      build: () {
        when(locationsRepository.countLocations())
            .thenAnswer((_) => Future.value(0));

        when(locationsRepository.queryLocations("test", "test"))
            .thenAnswer((_) async => []);

        return LocationsBloc(locationsRepo: locationsRepository);
      },
      skip: 0,
      act: (bloc) => bloc.add(FetchLocations("test", "test")),
      expect: [
        LocationsLoading(),
        //! it looks like blocTest library cant register state from events triggered witin blocs that are tested
        //! because here SanitizeLocationsTable event gets triggered and sanitisation takes progress
        //! but blocTest doesnt know about it
      ]);

  blocTest(
      "emits [InitialLocationsState, SanitisingLocations, SuccessLoadingLocations] for sanitising the DB table for locations",
      build: () {
        when(locationsRepository.countLocations())
            .thenAnswer((_) => Future.value(1214));

        when(locationsRepository.queryLocations("test", "test"))
            .thenAnswer((_) async => []);

        return LocationsBloc(locationsRepo: locationsRepository);
      },
      skip: 0,
      act: (bloc) => bloc.add(SanitizeLocationsTable("test", "test")),
      expect: [
        SanitisingLocations(),
        isA<SuccessLoadingLocations>(),
      ]);
}
